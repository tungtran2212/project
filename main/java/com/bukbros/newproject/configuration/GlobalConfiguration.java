package com.bukbros.newproject.configuration;

public class GlobalConfiguration {	
	public static final String USER_AVATAR_FOLDER = "upload/user/avatar";
	public static final String USER_AVATAR_LINK = "/resources/user/avatar/";
	
	public static final String POINT_FOLDER = "upload/point";
	public static final String POINT_LINK = "/resources/point/";
	
	public static final boolean DEV_ENV = true;
}
