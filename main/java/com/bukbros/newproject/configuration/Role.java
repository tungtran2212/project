package com.bukbros.newproject.configuration;

import java.util.HashMap;

public class Role {
	private final static String ROLE_PREFIX = "ROLE_";
	public final static String ROLE_ADMIN = ROLE_PREFIX + "ADMIN";
	public final static String ROLE_STAFF = ROLE_PREFIX + "STAFF";
	public final static String ROLE_STUDENT = ROLE_PREFIX + "STUDENT";
	
	public static HashMap<String, String> roles(){
		HashMap<String, String> roles = new HashMap<>();
		roles.put("ROLE_ADMIN", ROLE_ADMIN);
		roles.put("ROLE_STAFF", ROLE_STAFF);
		roles.put("ROLE_STUDENT", ROLE_STUDENT);
		return roles;
	}
	
}
