package com.bukbros.newproject.configuration;

import java.util.Properties;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class DatabaseConfig {
	private final String DRIVER_CLASS_NAME = "com.mysql.cj.jdbc.Driver";
	private final String SERVER = "localhost";
	private final String DATABASE_NAME = "project";
	private final String PORT = "3306";
	
	private final String USERNAME = "root";
	private final String PASSWORD = "";
//	private final String PASSWORD = "BukBr0$1207";
	private final String PACKAGES_NAME = "com.bukbros.newproject";	
		
	
	@Bean(name="dataSource")
	public DataSource dataSource() {
		PoolProperties poolProperties = new PoolProperties();
		poolProperties.setUrl("jdbc:mysql://"+SERVER+":"+PORT+"/"+DATABASE_NAME);
		poolProperties.setDriverClassName(DRIVER_CLASS_NAME);
		poolProperties.setUsername(USERNAME);
        poolProperties.setPassword(PASSWORD);
        poolProperties.setJmxEnabled(true);
        poolProperties.setTestOnBorrow(true);
        poolProperties.setValidationQuery("SELECT 1");        
        poolProperties.setValidationInterval(30000);
        /*
        poolProperties.setValidationInterval(30000);
        poolProperties.setMaxActive(100);
        poolProperties.setInitialSize(10);
        poolProperties.setMaxWait(10000);
        poolProperties.setMinIdle(10);
        */
        poolProperties.setConnectionProperties("characterEncoding=UTF-8");  
                   
        DataSource dataSource = new DataSource();
		dataSource.setPoolProperties(poolProperties);
		return dataSource;
	}
	
	@Bean(name="jdbcTemplate")
	public JdbcTemplate jdbcTemplate(DataSource dataSource) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		return jdbcTemplate;
	}
	
	@Bean(name = "sessionFactory")
	public LocalSessionFactoryBean sessionFactory() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		properties.put("hibernate.show_sql", true);
		properties.put("hibernate.format_sql", false);
		properties.put("javax.persistence.validation.mode", "none");

		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setPackagesToScan(PACKAGES_NAME);
		sessionFactory.setDataSource(dataSource());
		sessionFactory.setHibernateProperties(properties);
		return sessionFactory;
	}

	@Bean(name = "transactionManager")
	public HibernateTransactionManager getTransactionManager() {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager();
		transactionManager.setSessionFactory(sessionFactory().getObject());
		return transactionManager;
	}
}
