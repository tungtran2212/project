package com.bukbros.newproject.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.filter.CharacterEncodingFilter;

import com.bukbros.newproject.user.UserDatabase;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	@Autowired
	DataSource dataSource;
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder authManagerBuilder) throws Exception {
		String sqlUser = "SELECT "+UserDatabase.COLUMN_USERNAME+", "
				+UserDatabase.COLUMN_PASSWORD+", "
				+UserDatabase.COLUMN_STATUS+" "
				+"FROM "+UserDatabase.TABLE+" "
				+"WHERE "+UserDatabase.COLUMN_USERNAME+" = ?";
		String sqlAuth = "SELECT "+UserDatabase.COLUMN_USERNAME+", "
				+UserDatabase.COLUMN_ROLE+" "
				+"FROM "+UserDatabase.TABLE+" "
				+"WHERE "+UserDatabase.COLUMN_USERNAME+" = ?";			
		authManagerBuilder
			.jdbcAuthentication()
			.dataSource(dataSource)
			.passwordEncoder(new BCryptPasswordEncoder())
			.usersByUsernameQuery(sqlUser)
			.authoritiesByUsernameQuery(sqlAuth);		
	}
	
	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		CharacterEncodingFilter filter = new CharacterEncodingFilter();
		filter.setEncoding("UTF-8");
		filter.setForceEncoding(true);
		httpSecurity.addFilterBefore(filter, CsrfFilter.class);
		httpSecurity
		.authorizeRequests()
			.antMatchers("/", "/register").permitAll()
			.antMatchers("/public/**").permitAll()
			.antMatchers("/admin/**").access("hasAnyRole('ADMIN','STAFF','STUDENT')")
			.antMatchers("/user/create").access("hasAnyRole('USER', 'ADMIN')")	
			.antMatchers("/account").access("hasAnyRole('USER', 'ADMIN')")	
			.and()
		.formLogin()
			.loginPage("/login")
			.usernameParameter("username")
			.passwordParameter("password")
			.loginProcessingUrl("/login")
			.defaultSuccessUrl("/admin")
			.failureUrl("/login?message=invalid")
			.permitAll()
			.and()
		.rememberMe()
        	.rememberMeParameter("remember-me")
        	.key("uniqueAndSecret")
            .and()
		.logout()
			.logoutUrl("/logout")
			.permitAll()
			.logoutSuccessUrl("/login?message=logout")
			.and()
		.exceptionHandling()
			.accessDeniedPage("/forbidden")
			.and()
		.csrf();
	}
}
