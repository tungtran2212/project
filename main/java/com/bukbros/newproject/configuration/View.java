package com.bukbros.newproject.configuration;

public class View {
	public static final String VIEW_MODULE_FOLDER = "module/";
	public static final String VIEW_ADMIN_URL = "admin/";	
	public static final String VIEW_INDEX = "/index";
}
