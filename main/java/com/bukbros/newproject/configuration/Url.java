package com.bukbros.newproject.configuration;

import java.util.HashMap;

import com.bukbros.newproject.classroom.ClassroomConfig;
import com.bukbros.newproject.course.CourseConfig;
import com.bukbros.newproject.detailTime.DetailTimeConfig;
import com.bukbros.newproject.faculty.FacultyConfig;
import com.bukbros.newproject.major.MajorConfig;
import com.bukbros.newproject.majorSubject.MajorSubjectConfig;
import com.bukbros.newproject.registration.RegistrationConfig;
import com.bukbros.newproject.room.RoomConfig;
import com.bukbros.newproject.staff.StaffConfig;
import com.bukbros.newproject.student.StudentConfig;
import com.bukbros.newproject.subject.SubjectConfig;
import com.bukbros.newproject.user.UserConfig;

public class Url {

	public static final String URL_ADMIN = "/admin";
	public static final String URL_LOGIN = "/login";
	public static final String URL_LOGOUT = "/logout";
	public static final String URL_REGISTER = "/register";
	public static final String URL_FORGOT_PASSWORD = "/forgot-password";
	public static final String URL_CHANGE_PASSWORD = "/change-password";
	public static final String URL_ERROR_NOT_FOUND = "/error-not-found";	
	
	public static final String URL_MENU_HOME = "/home";
	public static final String URL_USER = UserConfig.MODULE_ADMIN_URL;
	public static final String URL_STAFF = StaffConfig.MODULE_ADMIN_URL;
	public static final String URL_STUDENT = StudentConfig.MODULE_ADMIN_URL; 
	public static final String URL_SUBJECT = SubjectConfig.MODULE_ADMIN_URL;
	public static final String URL_ROOM = RoomConfig.MODULE_ADMIN_URL;
	public static final String URL_COURSE = CourseConfig.MODULE_ADMIN_URL;
	public static final String URL_CLASSROOM = ClassroomConfig.MODULE_ADMIN_URL;
	public static final String URL_TIME_TABLE = RegistrationConfig.URL_TIMETABLE;
	public static final String URL_TIME = DetailTimeConfig.MODULE_ADMIN_URL;
	public static final String URL_MAJOR = MajorConfig.MODULE_ADMIN_URL;
	public static final String URL_FACULTY = FacultyConfig.MODULE_ADMIN_URL;
	public static final String URL_REGISTRATION = RegistrationConfig.MODULE_URL;
	public static final String URL_MAJOR_SUBJECT = MajorSubjectConfig.MODULE_ADMIN_URL;
	public static final String URL_CLASS = "/class";
	public static final String URL_POINT = "/point";
	public static final String URL_STUDENTS = StudentConfig.MODULE_URL; 
	public static final String URL_EXCEL = "/excel"; 
	public static final String URL_OPEN = "/open"; 
	public static final String URL_DOWNLOAD = "/download"; 
	public static final String URL_UPLOAD = "/upload"; 
	
	public static final String URL_CREATE = "/create";
	public static final String URL_UPDATE = "/update";
	public static final String URL_DELETE = "/delete";	
	public static final String URL_ACTIVE = "/active";
	public static final String URL_DEACTIVE = "/deactive";
	public static final String URL_DETAIL = "/detail";
	public static final String URL_ROLE_USER = "/role-user";
	public static final String URL_ROLE_ADMIN = "/role-admin";
	
	public static HashMap<String, String> urls() {
		HashMap<String, String> urlGlobals = new HashMap<>();
		urlGlobals.put("URL_ADMIN", URL_ADMIN);
		urlGlobals.put("URL_LOGIN", URL_LOGIN);
		urlGlobals.put("URL_LOGOUT", URL_LOGOUT);
		urlGlobals.put("URL_REGISTER", URL_REGISTER);
		urlGlobals.put("URL_FORGOT_PASSWORD", URL_FORGOT_PASSWORD);
		urlGlobals.put("URL_CHANGE_PASSWORD", URL_CHANGE_PASSWORD);
		urlGlobals.put("URL_ERROR_NOT_FOUND", URL_ERROR_NOT_FOUND);
		
		urlGlobals.put("URL_MENU_HOME", URL_MENU_HOME);
		urlGlobals.put("URL_USER", URL_USER);	
		urlGlobals.put("URL_STAFF", URL_STAFF);	
		urlGlobals.put("URL_STUDENT", URL_STUDENT);	
		urlGlobals.put("URL_SUBJECT", URL_SUBJECT);	
		urlGlobals.put("URL_ROOM", URL_ROOM);
		urlGlobals.put("URL_COURSE", URL_COURSE);
		urlGlobals.put("URL_CLASSROOM", URL_CLASSROOM);
		urlGlobals.put("URL_TIME_TABLE", URL_TIME_TABLE);
		urlGlobals.put("URL_TIME", URL_TIME);
		urlGlobals.put("URL_MAJOR", URL_MAJOR);
		urlGlobals.put("URL_FACULTY", URL_FACULTY);
		urlGlobals.put("URL_REGISTRATION", URL_REGISTRATION);
		urlGlobals.put("URL_MAJOR_SUBJECT", URL_MAJOR_SUBJECT);
		urlGlobals.put("URL_CLASS", URL_CLASS);
		urlGlobals.put("URL_POINT", URL_POINT);
		urlGlobals.put("URL_STUDENTS", URL_STUDENTS);
		urlGlobals.put("URL_EXCEL", URL_EXCEL);
		urlGlobals.put("URL_OPEN", URL_OPEN);
		urlGlobals.put("URL_DOWNLOAD", URL_DOWNLOAD);
		urlGlobals.put("URL_UPLOAD", URL_UPLOAD);
		
		urlGlobals.put("URL_CREATE", URL_CREATE);
		urlGlobals.put("URL_UPDATE", URL_UPDATE);
		urlGlobals.put("URL_DELETE", URL_DELETE);		
		urlGlobals.put("URL_ACTIVE", URL_ACTIVE);
		urlGlobals.put("URL_DEACTIVE", URL_DEACTIVE);
		urlGlobals.put("URL_DETAIL", URL_DETAIL);
		urlGlobals.put("URL_ROLE_ADMIN", URL_ROLE_ADMIN);
		urlGlobals.put("URL_ROLE_USER", URL_ROLE_USER);
		return urlGlobals;
	}
}
