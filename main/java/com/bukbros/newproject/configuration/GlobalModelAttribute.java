package com.bukbros.newproject.configuration;

import java.security.Principal;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.bukbros.newproject.language.LabelLanguage;
import com.bukbros.newproject.user.User;
import com.bukbros.newproject.user.interfaces.UserServiceInterface;

@ControllerAdvice
public class GlobalModelAttribute {
	@Autowired
	private UserServiceInterface userService;

	@ModelAttribute("appUrl")
	public HashMap<String, String> home() {
		return Url.urls();
	}

	@ModelAttribute("appLabel")
	public HashMap<String, String> getLabels() {
		return LabelLanguage.labels();
	}

	@ModelAttribute("role")
	public HashMap<String, String> roles() {
		return Role.roles();
	}

	@ModelAttribute("currentUser")
	public User getCurrntUser(Principal principal) {
		if (principal != null) {
			return userService.getUserByUsername(principal.getName());
		}
		return null;
	}
}
