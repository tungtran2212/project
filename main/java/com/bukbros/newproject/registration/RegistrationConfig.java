package com.bukbros.newproject.registration;

import java.util.HashMap;

import com.bukbros.newproject.configuration.Url;
import com.bukbros.newproject.configuration.View;

public class RegistrationConfig {
	public static final String MODULE_CODE = "registration";
	public static final String MODULE_NAME = "Đăng ký học";
	public static final String MODULE_URL = "/" + MODULE_CODE;
	public static final String MODULE_ADMIN_URL = Url.URL_ADMIN + MODULE_URL;
	public static final String MODULE_DATABASE_TABLE = MODULE_CODE;
	public static final String MODULE_VIEW_ADMIN = View.VIEW_ADMIN_URL + View.VIEW_MODULE_FOLDER + MODULE_CODE + View.VIEW_INDEX;
	
	public static final String MODULE_TIMETABLE = "timetable";
	public static final String URL_TIMETABLE = "/" + MODULE_TIMETABLE;
	
	public static final long MAX_EXCEL_FILE_SIZE = 1024 * 1024 * 10; //10MB
	
	public static final String[] EXT_EXCEL_FILE_UPLOAD = {"xlsx"};
	
	public static HashMap<String, String> component() {
		HashMap<String, String> components = new HashMap<>();
		components.put("MODULE_CODE", MODULE_CODE);
		components.put("MODULE_NAME", MODULE_NAME);
		components.put("MODULE_URL", MODULE_URL);
		components.put("MODULE_ADMIN_URL", MODULE_ADMIN_URL);
		
		components.put("MODULE_TIMETABLE", MODULE_TIMETABLE);
		components.put("URL_TIMETABLE", URL_TIMETABLE);
		return components;
	}
}
