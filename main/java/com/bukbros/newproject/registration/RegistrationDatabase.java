package com.bukbros.newproject.registration;

public class RegistrationDatabase {
	public static final String TABLE = "cr_registration";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_UUID = "uuid";
	public static final String COLUMN_STUDENT_ID = "student_id";
	public static final String COLUMN_DETAIL_ID = "detail_id";
	public static final String COLUMN_FIRST_POINT = "first_point";
	public static final String COLUMN_SECOND_POINT = "second_point";
	public static final String COLUMN_FINAL_POINT = "final_point";
	public static final String COLUMN_STATUS = "status";
}
