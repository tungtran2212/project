package com.bukbros.newproject.registration.interfaces;

import java.util.List;

import com.bukbros.newproject.interfaces.RepositoryInterface;
import com.bukbros.newproject.registration.Registration;

public interface RegistrationRepositoryInterface extends RepositoryInterface<Registration>{
	List<Registration> getRegistrationByDetailId (long detailId);
	
	List<Registration> getRegistrationByStudent (long studentId);
	
	Registration getRegistrationByDetailByStudent(long detailId, long studentId);
	
	long countStudentByDetail(long detailId);
}
