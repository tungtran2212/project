package com.bukbros.newproject.registration.interfaces;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import com.bukbros.newproject.detail.Detail;
import com.bukbros.newproject.interfaces.ServiceInterface;
import com.bukbros.newproject.registration.Registration;

public interface RegistrationServiceInterface extends ServiceInterface<Registration>{
	List<Registration> getRegistrationByDetailId(long detailId);
	
	List<Registration> getRegistrationByStudent (long studentId);
	
	boolean isRegistrationExisted(long detailId, long studentId);
	
	Registration getRegistrationByDetailByStudent(long detailId, long studentId);
	
	void getRegistrationByDetailExcel(HttpServletResponse response, Detail detail);
	
	boolean updatePointExcel(HttpServletRequest request, Detail detail, MultipartFile uploadFile);
	
	long countStudentByDetail(long detailId);
}
