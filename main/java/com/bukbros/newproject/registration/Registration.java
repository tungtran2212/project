package com.bukbros.newproject.registration;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import com.bukbros.newproject.detail.Detail;
import com.bukbros.newproject.student.Student;

@Entity
@Table(name = RegistrationDatabase.TABLE)
public class Registration {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = RegistrationDatabase.COLUMN_ID)
	private long id;
	
	@Column(name = RegistrationDatabase.COLUMN_UUID, updatable = false)
	private UUID uuid;
	
	@ManyToOne
	@JoinColumn(name = RegistrationDatabase.COLUMN_STUDENT_ID, updatable = false)
	private Student student;
	
	@ManyToOne
	@JoinColumn(name = RegistrationDatabase.COLUMN_DETAIL_ID, updatable = false)
	private Detail detail;
	
	@Column(name = RegistrationDatabase.COLUMN_FIRST_POINT, insertable = false)
	private double firstPoint;
	
	@Column(name = RegistrationDatabase.COLUMN_SECOND_POINT, insertable = false)
	private double secondPoint;
	
	@Column(name = RegistrationDatabase.COLUMN_FINAL_POINT, insertable = false)
	private double finalPoint;
	
	@Column(name = RegistrationDatabase.COLUMN_STATUS, insertable = false, updatable = false)
	private byte status;
	
	public Registration() {}

	
	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public UUID getUuid() {
		return uuid;
	}


	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}


	public Student getStudent() {
		return student;
	}


	public void setStudent(Student student) {
		this.student = student;
	}


	public Detail getDetail() {
		return detail;
	}


	public void setDetail(Detail detail) {
		this.detail = detail;
	}	


	public double getFirstPoint() {
		return firstPoint;
	}


	public void setFirstPoint(double firstPoint) {
		this.firstPoint = firstPoint;
	}


	public double getSecondPoint() {
		return secondPoint;
	}


	public void setSecondPoint(double secondPoint) {
		this.secondPoint = secondPoint;
	}

	public void setFinalPoint(double finalPoint) {
		this.finalPoint = finalPoint;
	}

	public double getFinalPoint() {
		return finalPoint;
	}


	public byte getStatus() {
		return status;
	}


	public void setStatus(byte status) {
		this.status = status;
	}


	@PrePersist
	public void prePersist() {
		uuid = UUID.randomUUID();
	}
}
