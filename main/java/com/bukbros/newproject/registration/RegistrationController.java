package com.bukbros.newproject.registration;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.newproject.configuration.Role;
import com.bukbros.newproject.configuration.Url;
import com.bukbros.newproject.detail.Detail;
import com.bukbros.newproject.detail.DetailConfig;
import com.bukbros.newproject.detail.interfaces.DetailServiceInterface;
import com.bukbros.newproject.detailTime.DetailTime;
import com.bukbros.newproject.detailTime.interfaces.DetailTimeServiceInterface;
import com.bukbros.newproject.language.MessageLanguage;
import com.bukbros.newproject.registration.interfaces.RegistrationServiceInterface;
import com.bukbros.newproject.staff.Staff;
import com.bukbros.newproject.staff.interfaces.StaffServiceInterface;
import com.bukbros.newproject.student.Student;
import com.bukbros.newproject.student.StudentLanguage;
import com.bukbros.newproject.student.interfaces.StudentServiceInterface;
import com.bukbros.newproject.upload.UploadLanguage;
import com.bukbros.newproject.upload.UploadService;

@Controller
@RequestMapping(value = RegistrationConfig.MODULE_URL)
public class RegistrationController {
	@Autowired
	private RegistrationServiceInterface service;

	@Autowired
	private StudentServiceInterface studentService;

	@Autowired
	private DetailTimeServiceInterface detailTimeService;

	@Autowired
	private DetailServiceInterface detailService;

	@Autowired
	private StaffServiceInterface staffService;

	@Autowired
	private UploadService uploadService;

	@ModelAttribute("module")
	public HashMap<String, String> module() {
		return RegistrationConfig.component();
	}

	@ModelAttribute("moduleLang")
	public HashMap<String, String> moduleLang() {
		return RegistrationLanguage.label();
	}

	@ModelAttribute("studentLang")
	public HashMap<String, String> studentLang() {
		return StudentLanguage.labels();
	}

	@ModelAttribute("registrationMenu")
	public boolean registrationMenu() {
		return true;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String getAllRegistration(Principal principal, Model model, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		Student currentUser = studentService.getStudentByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getUser().getRole().equalsIgnoreCase(Role.ROLE_STUDENT)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:/";
		}
		model.addAttribute("listMode", true);
		model.addAttribute("student", currentUser);
		List<DetailTime> detailTimes = detailTimeService.getDetailTimeByMajor(currentUser.getClassroom().getMajor().getId(), currentUser.getUserId());
		if (detailTimes == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
		} else {
			model.addAttribute("detailTimes", detailTimes);
		}
		return RegistrationConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping("/{uuidDetail}" + Url.URL_CREATE)
	public String postCreate(@PathVariable("uuidDetail") String uuidDetail,
							 Principal principal,
							 Model model,
							 RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		Student currentUser = studentService.getStudentByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getUser().getRole().equalsIgnoreCase(Role.ROLE_STUDENT)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:/";
		}
		Detail originalDetail = detailService.read(uuidDetail);
		if ((originalDetail == null) || (originalDetail.getStatus() != 1)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + RegistrationConfig.MODULE_URL;
		}
		if (currentUser.getClassroom().getMajor().getId() != originalDetail.getMajorSubject().getMajor().getId()) {
			redirectAttributes.addFlashAttribute("errorMessage", RegistrationLanguage.VALID_ERROR_MAJOR);
			return "redirect:" + RegistrationConfig.MODULE_URL;
		}
		boolean isRegistrationExisted = service.isRegistrationExisted(originalDetail.getId(), currentUser.getUser().getId());
		if (isRegistrationExisted) {
			redirectAttributes.addFlashAttribute("errorMessage", RegistrationLanguage.VALID_REGISTRATION);
			return "redirect:" + RegistrationConfig.MODULE_URL;
		}
		List<DetailTime> detailTimes = detailTimeService.getDetailTimeByDetailId(originalDetail.getId(), false);
		if (detailTimes == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + RegistrationConfig.MODULE_URL;
		}
		List<DetailTime> timeTables = detailTimeService.getDetailTimeByStudent(currentUser.getUserId());
		if (timeTables != null) {
			boolean hasError = false;
			for (DetailTime timeTable : timeTables) {
				for (DetailTime detailTime : detailTimes) {
					if ((timeTable.getWeekDay() == detailTime.getWeekDay()) && (timeTable.getBlock() == detailTime.getBlock())) {
						hasError = true;
					}
				}
			}
			if (hasError) {
				redirectAttributes.addFlashAttribute("errorMessage", RegistrationLanguage.VALID_TIME);
				return "redirect:" + RegistrationConfig.MODULE_URL;
			}
		}
		List<Registration> registrations = service.getRegistrationByDetailId(originalDetail.getId());
		if (registrations != null) {
			if (registrations.size() > 30) {
				redirectAttributes.addFlashAttribute("errorMessage", RegistrationLanguage.VALID_SIZE_REGISTRATION);
				return "redirect:" + RegistrationConfig.MODULE_URL;
			}
		}
		Registration registration = new Registration();
		registration.setDetail(originalDetail);
		registration.setStudent(currentUser);
		boolean createItem = service.create(registration);
		if (createItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_CREATE_SUCCESS);
		} else {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_CREATE_ERROR);
			return RegistrationConfig.MODULE_VIEW_ADMIN;
		}
		model.asMap().clear();
		return "redirect:" + Url.URL_REGISTRATION;
	}

	@PostMapping("/{uuidDetail}" + Url.URL_DELETE)
	public String postDelete(@PathVariable("uuidDetail") String uuidDetail,
							 Model model,
							 Principal principal,
							 RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		Student currentUser = studentService.getStudentByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getUser().getRole().equalsIgnoreCase(Role.ROLE_STUDENT)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:/";
		}
		Detail originalDetail = detailService.read(uuidDetail);
		if ((originalDetail == null) || (originalDetail.getStatus() != 1)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + RegistrationConfig.MODULE_URL;
		}
		Registration originalRegistration = service.getRegistrationByDetailByStudent(originalDetail.getId(), currentUser.getUserId());
		if ((originalRegistration == null) || (originalRegistration.getStatus() != 1)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + RegistrationConfig.MODULE_URL;
		}
		boolean delete = service.delete(originalRegistration);
		if (delete) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DELETE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DELETE_ERROR);
		}
		return "redirect:" + Url.URL_REGISTRATION;
	}

	@GetMapping("/{uuid}" + Url.URL_UPDATE)
	public String GetUpdate(@PathVariable("uuid") String uuid,
							Model model,
							Principal principal,
							RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		Staff currentUser = staffService.getStaffByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		Registration originalRegistration = service.read(uuid);
		if (originalRegistration == null ||
				((originalRegistration.getDetail().getStatus() != 2) && (!currentUser.getUser().getRole().equalsIgnoreCase(Role.ROLE_ADMIN)))) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + DetailConfig.MODULE_ADMIN_URL;
		}
		redirectAttributes.addFlashAttribute("registration", originalRegistration);
		return "redirect:" + Url.URL_CLASS + "/" + originalRegistration.getDetail().getUuid() + Url.URL_DETAIL;
	}

	@PostMapping("/{uuid}" + Url.URL_UPDATE)
	public String postUpdate(@PathVariable("uuid") String uuid,
							 @ModelAttribute("validRegistration") @Valid Registration registration,
							 BindingResult results,
							 Model model,
							 Principal principal,
							 RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		Staff currentUser = staffService.getStaffByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		Registration originalRegistration = service.read(uuid);
		if (originalRegistration == null ||
				((originalRegistration.getDetail().getStatus() != 2) && (!currentUser.getUser().getRole().equalsIgnoreCase(Role.ROLE_ADMIN)))) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + DetailConfig.MODULE_ADMIN_URL;
		}
		boolean update = service.update(originalRegistration.getId(), registration);
		if (update) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_UPDATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_UPDATE_ERROR);
			redirectAttributes.addFlashAttribute("registration", registration);
		}
		return "redirect:" + Url.URL_CLASS + "/" + originalRegistration.getDetail().getUuid() + Url.URL_DETAIL;
	}

	@GetMapping("/{uuidDetail}" + Url.URL_EXCEL + Url.URL_DOWNLOAD)
	public void downloadExcelData(@PathVariable("uuidDetail") String uuidDetail, HttpServletResponse response, Principal principal) {
		if (principal != null) {
			Staff currentUser = staffService.getStaffByUsername(principal.getName());
			if (currentUser != null) {
				Detail detail = detailService.read(uuidDetail);
				if (detail != null) {
					service.getRegistrationByDetailExcel(response, detail);
				}
			}
		}
	}

	@PostMapping("/{uuidDetail}" + Url.URL_EXCEL + Url.URL_UPLOAD)
	public String uploadExcelData(@PathVariable("uuidDetail") String uuidDetail,
								  @RequestParam(name = "uploadFile", required = false) MultipartFile uploadFile,
								  HttpServletRequest request,
								  Model model,
								  Principal principal,
								  RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		Staff currentUser = staffService.getStaffByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		Detail detail = detailService.read(uuidDetail);
		if ((detail == null) || ((detail.getStatus() != 2) && (!currentUser.getUser().getRole().equalsIgnoreCase(Role.ROLE_ADMIN)))) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:/";
		}
		if ((uploadFile == null) || (uploadFile.getSize() == 0)) {
			redirectAttributes.addFlashAttribute("errorMessage", UploadLanguage.VALIDATION_NOT_FILE);
			return "redirect:" + Url.URL_CLASS + "/" + detail.getUuid() + Url.URL_DETAIL;
		} else {
			if (!uploadService.validFileSize(uploadFile, RegistrationConfig.MAX_EXCEL_FILE_SIZE)) {
				redirectAttributes.addFlashAttribute("errorMessage", UploadLanguage.VALIDATION_FILE_SIZE_MAX);
				return "redirect:" + Url.URL_CLASS + "/" + detail.getUuid() + Url.URL_DETAIL;
			}
			if (!uploadService.validFileExt(uploadFile, RegistrationConfig.EXT_EXCEL_FILE_UPLOAD)) {
				redirectAttributes.addFlashAttribute("errorMessage", UploadLanguage.VALIDATION_FILE_EXT);
				return "redirect:" + Url.URL_CLASS + "/" + detail.getUuid() + Url.URL_DETAIL;
			}
		}
		boolean updatePoint = service.updatePointExcel(request, detail, uploadFile);
		if (updatePoint) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_UPDATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_UPDATE_ERROR);
		}
		return "redirect:" + Url.URL_CLASS + "/" + detail.getUuid() + Url.URL_DETAIL;
	}
}
