package com.bukbros.newproject.registration;

import java.util.HashMap;

public class RegistrationLanguage {
	
	public static final String VALID_REGISTRATION = "Môn học đã được đăng ký";
	public static final String VALID_ERROR_MAJOR = "Bạn không thuộc đối tượng đăng ký học kỳ này";
	public static final String VALID_TIME = "Môn này trùng thời gian với môn bạn đã đăng ký";
	public static final String VALID_SIZE_REGISTRATION = "Lớp đã đầy";
	
	public static final String LABEL_LIST = "Đăng ký học ngành";
	public static final String LABEL_COURSE = "Khóa học";
	public static final String LABEL_SUBJECT = "Môn học";
	public static final String LABEL_STAFF = "Giáo viên";
	public static final String LABEL_ROOM = "Phòng";
	public static final String LABEL_TIME = "Thời gian";
	public static final String LABEL_QUANTITY = "Số lượng";
	
	public static HashMap<String, String> label() {
		HashMap<String, String> labels = new HashMap<>();		
		labels.put("LABEL_LIST", LABEL_LIST);		
		labels.put("LABEL_COURSE", LABEL_COURSE);		
		labels.put("LABEL_SUBJECT", LABEL_SUBJECT);		
		labels.put("LABEL_STAFF", LABEL_STAFF);		
		labels.put("LABEL_ROOM", LABEL_ROOM);		
		labels.put("LABEL_TIME", LABEL_TIME);		
		labels.put("LABEL_QUANTITY", LABEL_QUANTITY);		
		return labels;
	}
	
	public static HashMap<Integer, String> headerExcel() {
		HashMap<Integer, String> headerExcel = new HashMap<>();
		headerExcel.put(0, "Mã sinh viên");
		headerExcel.put(1, "Họ và tên");
		headerExcel.put(2, "Điểm 1");
		headerExcel.put(3, "Điểm 2");
		headerExcel.put(4, "Điểm tổng kết (hệ thống tự tính)");
		return headerExcel;
	}
}
