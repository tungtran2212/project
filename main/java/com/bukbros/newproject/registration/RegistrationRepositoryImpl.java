package com.bukbros.newproject.registration;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bukbros.newproject.registration.interfaces.RegistrationRepositoryInterface;

@Repository
@Transactional
public class RegistrationRepositoryImpl implements RegistrationRepositoryInterface{
	@Autowired
	private SessionFactory sessionFactory;
	
	private Logger logger = LogManager.getLogger(RegistrationRepositoryImpl.class);

	@Override
	public boolean create(Registration registration) {
		try {
			sessionFactory.getCurrentSession().persist(registration);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public List<Registration> read(boolean isActive) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Registration> criteria = builder.createQuery(Registration.class);
			Root<Registration> root = criteria.from(Registration.class);
			criteria.select(root);
			if (isActive) {
				criteria.where(builder.equal(root.get("status"), 1),
				               builder.equal(root.get("student").get("user").get("status"), 1),
				               builder.equal(root.get("detailTime").get("status"), 1));
			}
			criteria.orderBy(builder.desc(root.get("id")));
			List<Registration> registrations = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !registrations.isEmpty() ? registrations : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean update(Registration registration) {
		try {
			sessionFactory.getCurrentSession().update(registration);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public boolean delete(Registration registration) {
		try {
			sessionFactory.getCurrentSession().delete(registration);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public Registration read(long id) {
		try {
			Registration registrations = sessionFactory.getCurrentSession().get(Registration.class, id);
			return registrations;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}	
	}

	@Override
	public boolean status(long id, int status) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaUpdate<Registration> criteria = builder.createCriteriaUpdate(Registration.class);
		    Root<Registration> root = criteria.from(Registration.class);
		    criteria.set(root.get("status"), status)
		    		.where(builder.equal(root.get("id"), id));
		    sessionFactory.getCurrentSession().createQuery(criteria).executeUpdate();
		    return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public Registration read(String uuid) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<Registration> criteria = builder.createQuery(Registration.class);
		    Root<Registration> root = criteria.from(Registration.class);
		    criteria.select(root)
		     		.where(builder.equal(root.get("uuid"), UUID.fromString(uuid)));
		    Registration registrations = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
		    return registrations; 
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public List<Registration> getRegistrationByDetailId(long detailId) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Registration> criteria = builder.createQuery(Registration.class);
			Root<Registration> root = criteria.from(Registration.class);
			List<Predicate> predicates = new ArrayList<>();
			predicates.add(builder.equal(root.get("detail"), detailId));
			criteria.select(root)
					.where(predicates.toArray(new Predicate[predicates.size()]))
					.orderBy(builder.asc(root.get("student").get("user").get("username")));
			List<Registration> registrations = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !registrations.isEmpty() ? registrations : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public Registration getRegistrationByDetailByStudent(long detailId, long studentId) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<Registration> criteria = builder.createQuery(Registration.class);
		    Root<Registration> root = criteria.from(Registration.class);
		    List<Predicate> predicates = new ArrayList<>();
		    predicates.add(builder.equal(root.get("detail"), detailId));
		    predicates.add(builder.equal(root.get("student"),studentId));
			criteria.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
			Registration registration = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
		    return registration;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public List<Registration> getRegistrationByStudent(long studentId) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Registration> criteria = builder.createQuery(Registration.class);
			Root<Registration> root = criteria.from(Registration.class);
			List<Predicate> predicates = new ArrayList<>();
			predicates.add(builder.equal(root.get("student"), studentId));
			criteria.select(root)
					.where(predicates.toArray(new Predicate[predicates.size()]))
					.orderBy(builder.asc(root.get("detail").get("id")));
			List<Registration> registrations = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !registrations.isEmpty() ? registrations : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public long countStudentByDetail(long detailId) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
			Root<Registration> root = criteria.from(Registration.class);
			criteria.select(builder.count(root))
					.where(builder.equal(root.get("detail"), detailId));
			long count = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
			return count;
		} catch (Exception e) {
			logger.error(e);
			return 0;
		}
	}
}
