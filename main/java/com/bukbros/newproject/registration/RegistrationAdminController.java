package com.bukbros.newproject.registration;

import java.security.Principal;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.newproject.configuration.Role;
import com.bukbros.newproject.configuration.Url;
import com.bukbros.newproject.detail.DetailConfig;
import com.bukbros.newproject.language.MessageLanguage;
import com.bukbros.newproject.registration.interfaces.RegistrationServiceInterface;
import com.bukbros.newproject.user.User;
import com.bukbros.newproject.user.interfaces.UserServiceInterface;

@Controller
@RequestMapping(value = RegistrationConfig.MODULE_ADMIN_URL)
public class RegistrationAdminController {
	@Autowired
	private RegistrationServiceInterface service;
	
	@Autowired UserServiceInterface userService;
	
	@ModelAttribute("module")
	public HashMap<String, String> module() {
		return RegistrationConfig.component();
	}

	@ModelAttribute("moduleLang")
	public HashMap<String, String> moduleLang() {
		return RegistrationLanguage.label();
	}
	
	@ModelAttribute("detailMenu")
	public boolean detailMenu() {
		return true;
	}
	
	@PostMapping("/{uuid}" + Url.URL_ACTIVE)
	public String postActive(@PathVariable("uuid") String uuid,
							 Model model,
							 Principal principal,
							 RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:/";
		}
		Registration originalRegistration = service.read(uuid);
		if (originalRegistration == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + DetailConfig.MODULE_ADMIN_URL;
		}
		boolean active = service.status(originalRegistration.getId(), 1);
		if (active) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_ACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ACTIVE_ERROR);
		}
		return "redirect:" + Url.URL_CLASS + "/" + originalRegistration.getDetail().getUuid() + Url.URL_DETAIL;
	}
	
	@PostMapping("/{uuid}" + Url.URL_DEACTIVE)
	public String postDeactive(@PathVariable("uuid") String uuid,
							 Model model,
							 Principal principal,
							 RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:/";
		}
		Registration originalRegistration = service.read(uuid);
		if (originalRegistration == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + DetailConfig.MODULE_ADMIN_URL;
		}
		boolean active = service.status(originalRegistration.getId(), 0);
		if (active) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DEACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DEACTIVE_ERROR);
		}
		return "redirect:" + Url.URL_CLASS + "/" + originalRegistration.getDetail().getUuid() + Url.URL_DETAIL;
	}
	
	@PostMapping("/{uuid}" + Url.URL_DELETE)
	public String postDelete(@PathVariable("uuid") String uuid,
							 Model model,
							 Principal principal,
							 RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:/";
		}
		Registration originalRegistration = service.read(uuid);
		if (originalRegistration == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + DetailConfig.MODULE_ADMIN_URL;
		}
		boolean delete = service.delete(originalRegistration);
		if (delete) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DELETE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DELETE_ERROR);
		}
		return "redirect:" + Url.URL_CLASS + "/" + originalRegistration.getDetail().getUuid() + Url.URL_DETAIL;
	}
}
