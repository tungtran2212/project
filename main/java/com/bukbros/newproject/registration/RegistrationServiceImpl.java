package com.bukbros.newproject.registration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.bukbros.newproject.configuration.GlobalConfiguration;
import com.bukbros.newproject.detail.Detail;
import com.bukbros.newproject.office.ExcelFunction;
import com.bukbros.newproject.registration.interfaces.RegistrationRepositoryInterface;
import com.bukbros.newproject.registration.interfaces.RegistrationServiceInterface;
import com.bukbros.newproject.student.Student;
import com.bukbros.newproject.student.interfaces.StudentServiceInterface;
import com.bukbros.newproject.upload.UploadService;

@Service
public class RegistrationServiceImpl implements RegistrationServiceInterface {
	@Autowired
	private RegistrationRepositoryInterface repository;

	private Logger logger = LogManager.getLogger(RegistrationServiceImpl.class);
	
	@Autowired
	private UploadService uploadService;
	
	@Autowired
	private StudentServiceInterface studentService;

	@Override
	public boolean create(Registration registration) {
		return repository.create(registration);
	}

	@Override
	public List<Registration> read(boolean isActive) {
		return repository.read(isActive);
	}

	@Override
	public boolean update(long id, Registration registration) {
		registration.setId(id);
		DecimalFormat decimalFormat = new DecimalFormat("#.#");
		double finalPoint = (registration.getFirstPoint() + registration.getSecondPoint() * 2) / 3;
		registration.setFinalPoint(Double.valueOf(decimalFormat.format(finalPoint)));
		return repository.update(registration);
	}

	@Override
	public boolean delete(Registration registration) {
		return repository.delete(registration);
	}

	@Override
	public Registration read(long id) {
		return repository.read(id);
	}

	@Override
	public boolean status(long id, int status) {
		return repository.status(id, status);
	}

	@Override
	public Registration read(String uuid) {
		return repository.read(uuid);
	}

	@Override
	public List<Registration> getRegistrationByDetailId(long detailId) {
		return repository.getRegistrationByDetailId(detailId);
	}

	@Override
	public boolean isRegistrationExisted(long detailId, long studentId) {
		return repository.getRegistrationByDetailByStudent(detailId, studentId) != null ? true : false;
	}

	@Override
	public Registration getRegistrationByDetailByStudent(long detailId, long studentId) {
		return repository.getRegistrationByDetailByStudent(detailId, studentId);
	}

	@Override
	public List<Registration> getRegistrationByStudent(long studentId) {
		return repository.getRegistrationByStudent(studentId);
	}

	@Override
	public void getRegistrationByDetailExcel(HttpServletResponse response, Detail detail) {
		String fileName = detail.getMajorSubject().getCourse().getCode() + "-" + detail.getMajorSubject().getMajor().getCode() + "-" +
				detail.getMajorSubject().getSubject().getTitle() + "-" + detail.getRoom().getName();
		List<Registration> registrations = repository.getRegistrationByDetailId(detail.getId());
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet(fileName);
		sheet.setDefaultColumnWidth(30);
		XSSFCellStyle styleHeader = ExcelFunction.styleHeader(workbook);
		XSSFCellStyle styleCell = ExcelFunction.styleCell(workbook);
		XSSFRow title = sheet.createRow(0);
		title.createCell(0).setCellValue(fileName);
		title.getCell(0).setCellStyle(styleCell);
		XSSFRow header = sheet.createRow(1);
		for (HashMap.Entry<Integer, String> headerExcel : RegistrationLanguage.headerExcel().entrySet()) {
			header.createCell(headerExcel.getKey()).setCellValue(headerExcel.getValue());
			header.getCell(headerExcel.getKey()).setCellStyle(styleHeader);
		}
		if (registrations != null) {
			int rowCount = 2;
			for (Registration registration : registrations) {
				XSSFRow row = sheet.createRow(rowCount++);
				row.createCell(0).setCellValue(registration.getStudent().getUser().getUsername());
				row.getCell(0).setCellStyle(styleCell);
				row.createCell(1)
						.setCellValue(registration.getStudent().getUser().getFirstName() + " " + registration.getStudent().getUser().getLastName());
				row.getCell(1).setCellStyle(styleCell);
				row.createCell(2).setCellValue(registration.getFirstPoint());
				row.getCell(2).setCellStyle(styleCell);
				row.createCell(3).setCellValue(registration.getSecondPoint());
				row.getCell(3).setCellStyle(styleCell);
				row.createCell(4).setCellValue(registration.getFinalPoint());
				row.getCell(4).setCellStyle(styleCell);
			}
		}
		response.setContentType("application/octet-stream");
		response.addHeader("Content-Disposition", "attachment; filename=" + fileName + ".xlsx");
		try {
			OutputStream outputStream = response.getOutputStream();
			workbook.write(outputStream);
			workbook.close();
			outputStream.close();
		} catch (IOException e) {
			logger.error(e);
		}
	}
	@Override
	public boolean updatePointExcel(HttpServletRequest request, Detail detail, MultipartFile uploadFile) {
			String fileName = uploadService.generateFileName(uploadFile, detail.getMajorSubject().getSubject().getCode());
			boolean saveFile = uploadService.saveFile(uploadFile, fileName, GlobalConfiguration.POINT_FOLDER, request);
			String pathFile = "";
			Student student = null;
			Registration registration = null;
			double firstPoint = 0;
			double secondPoint = 0;
			boolean update = false;
			if (saveFile) {
				try {
					pathFile = GlobalConfiguration.POINT_FOLDER + "/" + fileName;
					FileInputStream excelFile = new FileInputStream(new File(request.getServletContext().getRealPath(pathFile)));
					Workbook workbook = new XSSFWorkbook(excelFile);
					Sheet datatypeSheet = workbook.getSheetAt(0);
					DataFormatter fmt = new DataFormatter();
					for (int rowNum = 2; rowNum <= datatypeSheet.getLastRowNum(); rowNum++) {
						Row currentRow = datatypeSheet.getRow(rowNum);
						student = studentService.getStudentByUsername(fmt.formatCellValue(currentRow.getCell(0)));
						if (student == null) {
							workbook.close();
							excelFile.close();
							uploadService.deleteFile(pathFile, request);
							return false;
						}
						registration = repository.getRegistrationByDetailByStudent(detail.getId(), student.getUserId());
						if (registration == null) {
							workbook.close();
							excelFile.close();
							uploadService.deleteFile(pathFile, request);
							return false;
						}
						try {
							firstPoint = currentRow.getCell(2).getNumericCellValue();
						} catch (Exception e) {
							firstPoint = 0;
							logger.error(e);
						}
						try {
							secondPoint = currentRow.getCell(3).getNumericCellValue();
						} catch (Exception e) {
							secondPoint = 0;
							logger.error(e);
						}
						Registration newRegistration = new Registration();
						newRegistration.setFirstPoint(firstPoint);
						newRegistration.setSecondPoint(secondPoint);
						update = update(registration.getId(), newRegistration);
						if (!update) {
							workbook.close();
							excelFile.close();
							uploadService.deleteFile(pathFile, request);
							return false;
						}
					}
					workbook.close();
					excelFile.close();
					uploadService.deleteFile(pathFile, request);
					return true;
				} catch (Exception e) {
					uploadService.deleteFile(pathFile, request);
					logger.error(e);
					return false;
				}
			}
			return false;
	}

	@Override
	public long countStudentByDetail(long detailId) {
		return repository.countStudentByDetail(detailId);
	}
}
