package com.bukbros.newproject.classroom;

import java.util.HashMap;

public class ClassroomLanguage {
	public static final String LABEL_NAME = "Lớp";
	public static final String LABEL_COURSE_NAME = "Khóa";
	public static final String LABEL_FACULTY_NAME = "Khoa";
	public static final String LABEL_MAJOR_NAME = "Ngành";
	public static final String LABEL_STAFF_NAME = "Chủ nhiệm";
	
	public static final String IS_NAME_EXISTED = "Tên lớp đã tồn tại. Vui lòng nhập lại";	
	public static final String VALIDATON_CLASSROOM = "Lớp học không hợp lệ. Vui lòng chọn lại";	
	public static final String VALIDATON_NAME = "Tên lớp không hợp lệ. Vui lòng nhập lại";	
	
	public static final String LABEL_CREATE = "Thêm mới lớp";
	public static final String LABEL_UPDATE = "Cập nhật lớp";
	public static final String LABEL_LIST = "Danh sách lớp";
	
	public static HashMap<String, String> label() {
		HashMap<String, String> labels = new HashMap<>();
		labels.put("LABEL_NAME", LABEL_NAME);
		labels.put("LABEL_COURSE_NAME", LABEL_COURSE_NAME);
		labels.put("LABEL_FACULTY_NAME", LABEL_FACULTY_NAME);
		labels.put("LABEL_MAJOR_NAME", LABEL_MAJOR_NAME);
		labels.put("LABEL_STAFF_NAME", LABEL_STAFF_NAME);
		
		labels.put("LABEL_CREATE", LABEL_CREATE);		
		labels.put("LABEL_UPDATE", LABEL_UPDATE);		
		labels.put("LABEL_LIST", LABEL_LIST);		
		return labels;
	}
}
