package com.bukbros.newproject.classroom;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.bukbros.newproject.course.Course;
import com.bukbros.newproject.function.StringFunction;
import com.bukbros.newproject.major.Major;
import com.bukbros.newproject.staff.Staff;

@Entity
@Table(name = ClassroomDatabase.TABLE)
public class Classroom {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = ClassroomDatabase.COLUMN_ID)
	private long id;
	
	@Column(name = ClassroomDatabase.COLUMN_UUID, updatable = false)
	private UUID uuid;
	
	@ManyToOne
	@JoinColumn(name = ClassroomDatabase.COLUMN_COURSE_ID)
	private Course course;
	
	@ManyToOne
	@JoinColumn(name = ClassroomDatabase.COLUMN_MAJOR_ID)
	private Major major;
	
	@ManyToOne
	@JoinColumn(name = ClassroomDatabase.COLUMN_USER_ID)
	private Staff staff;
		
	@Size(min = 1, max = 150, message = ClassroomLanguage.VALIDATON_NAME)
	@Column(name = ClassroomDatabase.COLUMN_NAME)
	private String name;
	
	@Column(name = ClassroomDatabase.COLUMN_STATUS)
	private byte status;
	
	public Classroom() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Major getMajor() {
		return major;
	}

	public void setMajor(Major major) {
		this.major = major;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = StringFunction.trimSpace(name);
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}	
	
	@PrePersist
	public void prePersist() {
		uuid = UUID.randomUUID();
	}
}
