package com.bukbros.newproject.classroom;

public class ClassroomDatabase {
	public static final String TABLE = "cr_classroom";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_UUID = "uuid";
	public static final String COLUMN_COURSE_ID = "course_id";
	public static final String COLUMN_MAJOR_ID = "major_id";
	public static final String COLUMN_USER_ID = "user_id";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_STATUS = "status";
}
