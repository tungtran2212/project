package com.bukbros.newproject.classroom.interfaces;

import java.util.List;

import com.bukbros.newproject.classroom.Classroom;
import com.bukbros.newproject.interfaces.RepositoryInterface;

public interface ClassroomRepositoryInterface extends RepositoryInterface<Classroom>{
	boolean isNameExisted(String name, long id);
	
	List<Classroom> getClassroomsInUpdate(long id);
	
	List<Classroom> getClassroomsByUser(long userId, boolean isActive);
	
	boolean isClassroomUsed(long id, String reference, Class<? extends Object> referenceClass);
	
	long countClassroom();
}
