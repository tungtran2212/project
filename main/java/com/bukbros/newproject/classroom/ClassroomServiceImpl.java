package com.bukbros.newproject.classroom;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bukbros.newproject.classroom.interfaces.ClassroomRepositoryInterface;
import com.bukbros.newproject.classroom.interfaces.ClassroomServiceInterface;
@Service
public class ClassroomServiceImpl implements ClassroomServiceInterface{
	@Autowired
	private ClassroomRepositoryInterface repository;
	
	@Override
	public boolean create(Classroom classroom) {
		return repository.create(classroom);
	}

	@Override
	public List<Classroom> read(boolean isActive) {
		return repository.read(isActive);
	}

	@Override
	public boolean update(long id, Classroom classroom) {
		classroom.setId(id);
		return repository.update(classroom);
	}

	@Override
	public boolean delete(Classroom classroom) {
		return repository.delete(classroom);
	}

	@Override
	public Classroom read(long id) {
		return repository.read(id);
	}

	@Override
	public boolean status(long id, int status) {
		return repository.status(id, status);
	}

	@Override
	public Classroom read(String uuid) {
		return repository.read(uuid);
	}

	@Override
	public boolean isNameExisted(String name, long id) {		
		return repository.isNameExisted(name, id);
	}

	@Override
	public boolean isClassroomValid(long id, int type) {
		if (id == 0) {
			return true;
		}
		Classroom classroom = repository.read(id);
		if (classroom != null) {
			if (type != 0) {
				if (classroom.getStatus() == type) {
					return true;
				}
				return false;
			}
			return true;
		}
		return false;
	}

	@Override
	public List<Classroom> getClassroomsInUpdate(long id) {
		return repository.getClassroomsInUpdate(id);
	}

	@Override
	public List<Classroom> getClassroomsByUser(long userId, boolean isActive) {
		return repository.getClassroomsByUser(userId, isActive);
	}

	@Override
	public boolean isClassroomUsed(long id, String reference, Class<? extends Object> referenceClass) {
		return repository.isClassroomUsed(id, reference, referenceClass);
	}

	@Override
	public long countClassroom() {
		return repository.countClassroom();
	}
}
