package com.bukbros.newproject.classroom;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.newproject.classroom.interfaces.ClassroomServiceInterface;
import com.bukbros.newproject.configuration.Role;
import com.bukbros.newproject.configuration.Url;
import com.bukbros.newproject.course.CourseLanguage;
import com.bukbros.newproject.course.interfaces.CourseServiceInterface;
import com.bukbros.newproject.faculty.FacultyLanguage;
import com.bukbros.newproject.faculty.interfaces.FacultyServiceInterface;
import com.bukbros.newproject.language.LabelLanguage;
import com.bukbros.newproject.language.MessageLanguage;
import com.bukbros.newproject.major.MajorLanguage;
import com.bukbros.newproject.major.interfaces.MajorServiceInterface;
import com.bukbros.newproject.staff.StaffLanguage;
import com.bukbros.newproject.staff.interfaces.StaffServiceInterface;
import com.bukbros.newproject.student.Student;
import com.bukbros.newproject.student.StudentLanguage;
import com.bukbros.newproject.student.interfaces.StudentServiceInterface;
import com.bukbros.newproject.user.User;
import com.bukbros.newproject.user.interfaces.UserServiceInterface;

@Controller
@RequestMapping(value = ClassroomConfig.MODULE_ADMIN_URL)
public class ClassroomController {
	@Autowired
	private ClassroomServiceInterface service;

	@Autowired
	private CourseServiceInterface courseService;

	@Autowired
	private FacultyServiceInterface facultyService;

	@Autowired
	private MajorServiceInterface majorService;

	@Autowired
	private UserServiceInterface userService;

	@Autowired
	private StaffServiceInterface staffService;
	
	@Autowired
	private StudentServiceInterface studentService;

	@ModelAttribute("module")
	public HashMap<String, String> module() {
		return ClassroomConfig.component();
	}

	@ModelAttribute("moduleLang")
	public HashMap<String, String> moduleLang() {
		return ClassroomLanguage.label();
	}

	@ModelAttribute("labelLang")
	public HashMap<String, String> labelLanguage() {
		return LabelLanguage.labels();
	}
	
	@ModelAttribute("studentLang")
	public HashMap<String, String> studentLang() {
		return StudentLanguage.labels();
	}

	@ModelAttribute("classroomMenu")
	public boolean classroomMenu() {
		return true;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String getAllClassroom(Principal principal, Model model, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("listMode", true);
		List<Classroom> classrooms = service.read(false);
		if (classrooms == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
		} else {
			model.addAttribute("classrooms", classrooms);
		}
		return ClassroomConfig.MODULE_VIEW_ADMIN;
	}

	@GetMapping(Url.URL_CREATE)
	public String getCreate(Model model, Principal principal, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("createMode", true);
		model.addAttribute("faculties", facultyService.read(true));
		model.addAttribute("courses", courseService.read(true));
		model.addAttribute("majors", majorService.read(true));
		model.addAttribute("staffs", staffService.read(true));
		return ClassroomConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping(Url.URL_CREATE)
	public String postCreate (@ModelAttribute("validClassroom") @Valid Classroom classroom,
							  BindingResult results,
							  Model model,
							  Principal principal,
							  RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("createMode", true);
		model.addAttribute("faculties", facultyService.read(true));
		model.addAttribute("courses", courseService.read(true));
		model.addAttribute("majors", majorService.read(true));
		model.addAttribute("staffs", staffService.read(true));
		model.addAttribute("classroom", classroom);
		if (results.hasErrors()) {
			return ClassroomConfig.MODULE_VIEW_ADMIN;
		} else {
			boolean isNameExisted = service.isNameExisted(classroom.getName(), 0);
			if (isNameExisted) {
				results.addError(new FieldError("validClassroom", "name", ClassroomLanguage.IS_NAME_EXISTED));
			}			
			boolean validCourse = courseService.isCourseValid(classroom.getCourse().getId(), 1);
			if (!validCourse) {
				results.addError(new FieldError("validClassroom", "course.id", CourseLanguage.VALIDATION_COURSE));
			}
			boolean validFaculty = facultyService.isFacultyValid(classroom.getMajor().getFaculty().getId(), 1);
			if (!validFaculty) {
				results.addError(new FieldError("validClassroom", "major.faculty.id", FacultyLanguage.VALIDATION_FACULTY));
			}
			boolean validMajor = majorService.isMajorValid(classroom.getMajor().getId(), 1, classroom.getMajor().getFaculty().getId());
			if (!validMajor) {
				results.addError(new FieldError("validClassroom", "major.id", MajorLanguage.VALIDATION_MAJOR));
			}
			boolean validStaff = staffService.isStaffValid(classroom.getStaff().getUserId(), 1, classroom.getMajor().getFaculty().getId());
			if (!validStaff) {
				results.addError(new FieldError("validClassroom", "staff.userId", StaffLanguage.VALIDATION_STAFF));
			}
			if (results.hasFieldErrors("course.id")	|| results.hasFieldErrors("major.id") || 
				results.hasFieldErrors("major.faculty.id") || results.hasFieldErrors("staff.userId") || results.hasFieldErrors("name")) {
				return ClassroomConfig.MODULE_VIEW_ADMIN;
			}
		}
		boolean createItem = service.create(classroom);
		if (createItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_CREATE_SUCCESS);
		} else {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_CREATE_ERROR);
			return ClassroomConfig.MODULE_VIEW_ADMIN;
		}
		model.asMap().clear();
		return "redirect:" + Url.URL_CLASSROOM;
	}

	@GetMapping("/{uuid}" + Url.URL_UPDATE)
	public String getUpdate(@PathVariable("uuid") String uuid, Model model, Principal principal, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Classroom originalClassroom = service.read(uuid);
		if (originalClassroom == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + ClassroomConfig.MODULE_ADMIN_URL;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("faculties", facultyService.getFacultiesInUpdate(originalClassroom.getMajor().getFaculty().getId()));
		model.addAttribute("courses", courseService.getCoursesInUpdate(originalClassroom.getCourse().getId()));
		model.addAttribute("majors", majorService.getMajorsInUpdate(originalClassroom.getMajor().getId()));
		model.addAttribute("staffs", staffService.getStaffsInUpdate(originalClassroom.getStaff().getUserId()));
		model.addAttribute("classroom", originalClassroom);
		return ClassroomConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping("/{uuid}" + Url.URL_UPDATE)
	public String postUpdate (@PathVariable("uuid") String uuid,
	                          @ModelAttribute("validClassroom") @Valid Classroom classroom,
	                          BindingResult results,
	                          Principal principal,
							  Model model,
							  RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Classroom originalClassroom = service.read(uuid);
		if (originalClassroom == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + ClassroomConfig.MODULE_ADMIN_URL;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("faculties", facultyService.getFacultiesInUpdate(originalClassroom.getMajor().getFaculty().getId()));
		model.addAttribute("courses", courseService.getCoursesInUpdate(originalClassroom.getCourse().getId()));
		model.addAttribute("majors", majorService.getMajorsInUpdate(originalClassroom.getMajor().getId()));
		model.addAttribute("staffs", staffService.getStaffsInUpdate(originalClassroom.getStaff().getUserId()));
		model.addAttribute("classroom", classroom);
		if (results.hasErrors()) {
			return ClassroomConfig.MODULE_VIEW_ADMIN;
		} else {
			boolean isNameExisted = service.isNameExisted(classroom.getName(), originalClassroom.getId());
			if (isNameExisted) {
				results.addError(new FieldError("validClassroom", "name", ClassroomLanguage.IS_NAME_EXISTED));
			}				
			boolean validCourse = false;
			if (originalClassroom.getCourse().getId() == classroom.getCourse().getId()) {
				validCourse = courseService.isCourseValid(classroom.getCourse().getId(), 0);
			} else {
				validCourse = courseService.isCourseValid(classroom.getCourse().getId(), 1);
			}
			if (!validCourse) {
				results.addError(new FieldError("validClassroom", "course.id", CourseLanguage.VALIDATION_COURSE));
			}
			boolean validFaculty = false;
			if (originalClassroom.getMajor().getFaculty().getId() == classroom.getMajor().getFaculty().getId()) {
				validFaculty = facultyService.isFacultyValid(classroom.getMajor().getFaculty().getId(), 0);
			} else {
				validFaculty = facultyService.isFacultyValid(classroom.getMajor().getFaculty().getId(), 1);
			}
			if (!validFaculty) {
				results.addError(new FieldError("validClassroom", "major.faculty.id", FacultyLanguage.VALIDATION_FACULTY));
			}
			boolean validMajor = false;
			if (originalClassroom.getMajor().getId() == classroom.getMajor().getId()) {
				validMajor = majorService.isMajorValid(classroom.getMajor().getId(), 0, classroom.getMajor().getFaculty().getId());
			} else {
				validMajor = majorService.isMajorValid(classroom.getMajor().getId(), 1, classroom.getMajor().getFaculty().getId());
			}
			if (!validMajor) {
				results.addError(new FieldError("validClassroom", "major.id", MajorLanguage.VALIDATION_MAJOR));
			}
			boolean validStaff = false;
			if (originalClassroom.getStaff().getUserId() == classroom.getStaff().getUserId()) {
				validStaff = staffService.isStaffValid(classroom.getStaff().getUserId(), 0, classroom.getMajor().getFaculty().getId());
			} else {
				validStaff = staffService.isStaffValid(classroom.getStaff().getUserId(), 1, classroom.getMajor().getFaculty().getId());
			}
			if (!validStaff) {
				results.addError(new FieldError("validClassroom", "staff.userId", StaffLanguage.VALIDATION_STAFF));
			}
			if (results.hasFieldErrors("course.id")	|| results.hasFieldErrors("major.id") || 
				results.hasFieldErrors("major.faculty.id") || results.hasFieldErrors("staff.userId") || results.hasFieldErrors("name")) {
				return ClassroomConfig.MODULE_VIEW_ADMIN;
			}
		}
		boolean updateItem = service.update(originalClassroom.getId(), classroom);
		if (updateItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_UPDATE_SUCCESS);
		} else {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_UPDATE_ERROR);
			return ClassroomConfig.MODULE_VIEW_ADMIN;
		}
		model.asMap().clear();
		return "redirect:" + ClassroomConfig.MODULE_ADMIN_URL;
	}

	@PostMapping("/{uuid}" + Url.URL_ACTIVE)
	public String postActive(@PathVariable("uuid") String uuid, Principal principal, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Classroom originalClassroom = service.read(uuid);
		if (originalClassroom == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + ClassroomConfig.MODULE_ADMIN_URL;
		}
		boolean active = service.status(originalClassroom.getId(), 1);
		if (active) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_ACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ACTIVE_ERROR);
		}
		return "redirect:" + ClassroomConfig.MODULE_ADMIN_URL;
	}

	@PostMapping("/{uuid}" + Url.URL_DEACTIVE)
	public String postDeactive(@PathVariable("uuid") String uuid, Principal principal, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Classroom originalClassroom = service.read(uuid);
		if (originalClassroom == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + ClassroomConfig.MODULE_ADMIN_URL;
		}
		boolean deactive = service.status(originalClassroom.getId(), 0);
		if (deactive) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DEACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DEACTIVE_ERROR);
		}
		return "redirect:" + ClassroomConfig.MODULE_ADMIN_URL;
	}

	@PostMapping("/{uuid}" + Url.URL_DELETE)
	public String postDelete(@PathVariable("uuid") String uuid, Model model, Principal principal, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Classroom originalClassroom = service.read(uuid);
		if (originalClassroom == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + ClassroomConfig.MODULE_ADMIN_URL;
		}
		boolean isClassroomUsedStudent = service.isClassroomUsed(originalClassroom.getId(), "classroom", Student.class);
		if (isClassroomUsedStudent) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_CONSTRAIN);
			return "redirect:" + ClassroomConfig.MODULE_ADMIN_URL + "/" + originalClassroom.getUuid() + Url.URL_UPDATE;
		}
		boolean delete = service.delete(originalClassroom);
		if (delete) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DELETE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DELETE_ERROR);
		}
		return "redirect:" + ClassroomConfig.MODULE_ADMIN_URL;
	}
	
	@GetMapping("/{uuid}" + Url.URL_DETAIL)
	public String getDetail(@PathVariable("uuid") String uuid, Model model, Principal principal, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Classroom originalClassroom = service.read(uuid);
		if (originalClassroom == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + ClassroomConfig.MODULE_ADMIN_URL;
		}
		model.addAttribute("detailMode", true);
		model.addAttribute("classroom", originalClassroom);
		model.addAttribute("students", studentService.getStudentByClassroom(originalClassroom.getId(), false));
		return ClassroomConfig.MODULE_VIEW_ADMIN;
	}
}
