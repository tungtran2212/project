package com.bukbros.newproject.classroom;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bukbros.newproject.classroom.interfaces.ClassroomRepositoryInterface;

@Repository
@Transactional
public class ClassroomRepositoryImpl implements ClassroomRepositoryInterface{
	@Autowired
	private SessionFactory sessionFactory;
	
	private Logger logger = LogManager.getLogger(ClassroomRepositoryImpl.class);
	@Override
	public boolean create(Classroom classroom) {
		try {
			sessionFactory.getCurrentSession().persist(classroom);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public List<Classroom> read(boolean isActive) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Classroom> criteria = builder.createQuery(Classroom.class);
			Root<Classroom> root = criteria.from(Classroom.class);
			criteria.select(root);
			if (isActive) {
				criteria.where(builder.equal(root.get("status"), 1),
				               builder.equal(root.get("course").get("status"), 1),
				               builder.equal(root.get("major").get("status"), 1),
				               builder.equal(root.get("major").get("faculty").get("status"), 1));
			}
			criteria.orderBy(builder.desc(root.get("id")));
			List<Classroom> classrooms = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !classrooms.isEmpty() ? classrooms : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean update(Classroom classroom) {
		try {
			sessionFactory.getCurrentSession().update(classroom);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public boolean delete(Classroom classroom) {
		try {
			sessionFactory.getCurrentSession().delete(classroom);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public Classroom read(long id) {
		try {
			Classroom classrooms = sessionFactory.getCurrentSession().get(Classroom.class, id);
			return classrooms;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}	
	}

	@Override
	public boolean status(long id, int status) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaUpdate<Classroom> criteria = builder.createCriteriaUpdate(Classroom.class);
		    Root<Classroom> root = criteria.from(Classroom.class);
		    criteria.set(root.get("status"), status)
		    		.where(builder.equal(root.get("id"), id));
		    sessionFactory.getCurrentSession().createQuery(criteria).executeUpdate();
		    return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public Classroom read(String uuid) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<Classroom> criteria = builder.createQuery(Classroom.class);
		    Root<Classroom> root = criteria.from(Classroom.class);
		    criteria.select(root)
		     		.where(builder.equal(root.get("uuid"), UUID.fromString(uuid)));
		    Classroom classrooms = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();//tra ve null khi ko tim dc
		    return classrooms; 
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean isNameExisted(String name, long id) {
		try {
			String dbName;
			String sql = "SELECT " + ClassroomDatabase.COLUMN_NAME
								   + " FROM " + ClassroomDatabase.TABLE
								   + " WHERE " + ClassroomDatabase.COLUMN_NAME + " REGEXP :" + ClassroomDatabase.COLUMN_NAME;
			String nameRegex = "^" + name + "$";
			if (id == 0) {
				dbName = sessionFactory.getCurrentSession().createSQLQuery(sql)
						   							.setParameter(ClassroomDatabase.COLUMN_NAME, nameRegex)
						   							.uniqueResult().toString();
			} else {
				sql += " AND " + ClassroomDatabase.COLUMN_ID + " != :" + ClassroomDatabase.COLUMN_ID;
        		dbName = sessionFactory.getCurrentSession().createSQLQuery(sql)
        											.setParameter(ClassroomDatabase.COLUMN_NAME, nameRegex)
        											.setParameter(ClassroomDatabase.COLUMN_ID, id)
        											.uniqueResult().toString();
			}
			return name.equalsIgnoreCase(dbName) ? true : false;
		} catch(Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public List<Classroom> getClassroomsInUpdate(long id) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Classroom> criteria = builder.createQuery(Classroom.class);
			Root<Classroom> root = criteria.from(Classroom.class);
			criteria.select(root)
					.where(builder.or(builder.and(builder.equal(root.get("status"), 1),
									              builder.equal(root.get("course").get("status"), 1),
									              builder.equal(root.get("major").get("status"), 1),
									              builder.equal(root.get("major").get("faculty").get("status"), 1)),
									  builder.equal(root.get("id"), id)))
					.orderBy(builder.asc(root.get("id")));
			List<Classroom> majors = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !majors.isEmpty() ? majors : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public List<Classroom> getClassroomsByUser(long userId, boolean isActive) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Classroom> criteria = builder.createQuery(Classroom.class);
			Root<Classroom> root = criteria.from(Classroom.class);
			List<Predicate> predicates = new ArrayList<>();
			predicates.add(builder.equal(root.get("staff"), userId));
			if (isActive) {
				predicates.add(builder.equal(root.get("status"), 1));
				predicates.add(builder.equal(root.get("course").get("status"), 1));
				predicates.add(builder.equal(root.get("major").get("status"), 1));
				predicates.add(builder.equal(root.get("major").get("faculty").get("status"), 1));
			}
			criteria.select(root)
					.where(predicates.toArray(new Predicate[predicates.size()]))
					.orderBy(builder.asc(root.get("id")));
			List<Classroom> majors = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !majors.isEmpty() ? majors : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean isClassroomUsed(long id, String reference, Class<? extends Object> referenceClass) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
			Root<?> root = criteria.from(referenceClass);
			List<Predicate> predicates = new ArrayList<>();
			predicates.add(builder.equal(root.get(reference), id));
			criteria.select(builder.count(root))
					.where(predicates.toArray(new Predicate[predicates.size()]));
			int count = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult().intValue();
			return count > 0 ? true : false;
		} catch (Exception e) {
			logger.error(e);
			return true;
		}
	}

	@Override
	public long countClassroom() {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
			Root<Classroom> root = criteria.from(Classroom.class);
			criteria.select(builder.count(root));
			long count = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
			return count;
		} catch (Exception e) {
			logger.error(e);
			return 0;
		}
	}
}
