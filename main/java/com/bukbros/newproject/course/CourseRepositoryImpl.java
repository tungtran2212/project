package com.bukbros.newproject.course;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bukbros.newproject.course.interfaces.CourseRepositoryInterface;
@Repository
@Transactional
public class CourseRepositoryImpl implements CourseRepositoryInterface{
	@Autowired
	private SessionFactory sessionFactory;
	
	private Logger logger = LogManager.getLogger(CourseRepositoryImpl.class);
	@Override
	public boolean create(Course course) {
		try {
			sessionFactory.getCurrentSession().persist(course);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public List<Course> read(boolean isActive) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Course> criteria = builder.createQuery(Course.class);
			Root<Course> root = criteria.from(Course.class);
			criteria.select(root);
			if (isActive) {
				criteria.where(builder.equal(root.get("status"), 1));
			}
			criteria.orderBy(builder.desc(root.get("id")));
			List<Course> courses = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !courses.isEmpty() ? courses : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean update(Course course) {
		try {
			sessionFactory.getCurrentSession().update(course);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public boolean delete(Course course) {
		try {
			sessionFactory.getCurrentSession().delete(course);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public Course read(long id) {
		try {
			Course courses = sessionFactory.getCurrentSession().get(Course.class, id);
			return courses;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}		
	}

	@Override
	public boolean status(long id, int status) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaUpdate<Course> criteria = builder.createCriteriaUpdate(Course.class);
		    Root<Course> root = criteria.from(Course.class);
		    criteria.set(root.get("status"), status)
		    		.where(builder.equal(root.get("id"), id));
		    sessionFactory.getCurrentSession().createQuery(criteria).executeUpdate();
		    return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public Course read(String uuid) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<Course> criteria = builder.createQuery(Course.class);
		    Root<Course> root = criteria.from(Course.class);
		    criteria.select(root)
		     		.where(builder.equal(root.get("uuid"), UUID.fromString(uuid)));
		    Course courses = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();//tra ve null khi ko tim dc
		    return courses; 
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}
	
	@Override
	public boolean isCodeExisted(String code, long id) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<String> criteria = builder.createQuery(String.class);
		    Root<Course> root = criteria.from(Course.class);
		    List<Predicate> predicates = new ArrayList<>();
			predicates.add(builder.equal(root.get("code"), code));
			if (id != 0) {
				predicates.add(builder.notEqual(root.get("id"), id));
			}
			criteria.select(root.get("code")).where(new Predicate[predicates.size()]);
		    String dbCode = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
		    return dbCode.equalsIgnoreCase(dbCode) ? true : false;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public List<Course> getCoursesInUpdate(long id) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Course> criteria = builder.createQuery(Course.class);
			Root<Course> root = criteria.from(Course.class);
			criteria.select(root)
					.where(builder.or(builder.equal(root.get("status"), 1),
									  builder.equal(root.get("id"), id)))
					.orderBy(builder.asc(root.get("id")));
			List<Course> courses = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !courses.isEmpty() ? courses : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean isCourseUsed(long id, String reference, Class<? extends Object> referenceClass) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
			Root<?> root = criteria.from(referenceClass);
			List<Predicate> predicates = new ArrayList<>();
			predicates.add(builder.equal(root.get(reference), id));
			criteria.select(builder.count(root))
					.where(predicates.toArray(new Predicate[predicates.size()]));
			int count = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult().intValue();
			return count > 0 ? true : false;
		} catch (Exception e) {
			logger.error(e);
			return true;
		}
	}

	@Override
	public long countCourse() {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
			Root<Course> root = criteria.from(Course.class);
			criteria.select(builder.count(root));
			long count = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
			return count;
		} catch (Exception e) {
			logger.error(e);
			return 0;
		}
	}
}
