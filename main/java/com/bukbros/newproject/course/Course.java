package com.bukbros.newproject.course;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = CourseDatabase.TABLE)
public class Course {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = CourseDatabase.COLUMN_ID)
	private long id;
	
	@Column(name = CourseDatabase.COLUMN_UUID, updatable = false)
	private UUID uuid;
	
	@Pattern(regexp = "^[a-zA-Z0-9]{1,10}$", message = CourseLanguage.VALIDATION_CODE)
	@Column(name = CourseDatabase.COLUMN_CODE)
	private String code;	
	
	@NotNull(message = CourseLanguage.VALIDATION_DATE)
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@Temporal(TemporalType.DATE)
	@Column(name = CourseDatabase.COLUMN_START_TIME)
	private Date startTime;
	
	@NotNull(message = CourseLanguage.VALIDATION_DATE)
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@Temporal(TemporalType.DATE)
	@Column(name = CourseDatabase.COLUMN_END_TIME)
	private Date endTime;
	
	@Column(name = CourseDatabase.COLUMN_STATUS)
	private byte status;
	
	public Course() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code.trim();
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}
	
	@PrePersist // random UUID trc khi tao moi
	public void prePersist() {
		uuid = UUID.randomUUID();
	}
}
