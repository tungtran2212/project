package com.bukbros.newproject.course;

import java.util.HashMap;

public class CourseLanguage {
	public static final String VALIDATION_SPECIAL_CHARACTERS = "Không được sử dụng ký tự đặc biệt";	
	public static final String VALIDATION_NOT_EMPTY = "Không được để trống";
	public static final String VALIDATION_END_TIME = "Thời gian kết thúc không hợp lệ. Vui lòng nhập lại";
	public static final String VALIDATION_CODE = "Khóa học không đúng, vui lòng nhập lại";
	public static final String VALIDATION_DATE = "Ngày không hợp lệ. Vui lòng nhập theo định dạng : ngày/tháng/năm";
	public static final String VALIDATION_COURSE = "Khóa không hợp lệ. Vui lòng chọn lại";
	public static final String VALIDATION_TERM = "Kỳ học không đúng, vui lòng chọn lại";
	
	public static final String MESSAGE_CODE_EXISTED = "Khóa học đã tồn tại";
	
	public static final String LABEL_CODE = "Khóa học";
	public static final String LABEL_TERM = "Kỳ học";
	public static final String LABEL_OPEN_START = "Thời gian mở";
	public static final String LABEL_OPEN_END = "Thời gian đóng";
	public static final String LABEL_START_TIME = "Thời gian bắt đầu";
	public static final String LABEL_END_TIME = "Thời gian kết thúc";
	
	public static final String LABEL_CREATE = "Thêm mới khóa học";
	public static final String LABEL_UPDATE = "Cập nhật khóa học";
	public static final String LABEL_LIST = "Danh sách khóa học";
	
	public static HashMap<String, String> label() {
		HashMap<String, String> labels = new HashMap<>();
		labels.put("LABEL_CODE", LABEL_CODE);
		labels.put("LABEL_TERM", LABEL_TERM);
		labels.put("LABEL_OPEN_START", LABEL_OPEN_START);
		labels.put("LABEL_OPEN_END", LABEL_OPEN_END);
		labels.put("LABEL_START_TIME", LABEL_START_TIME);
		labels.put("LABEL_END_TIME", LABEL_END_TIME);	
		
		labels.put("LABEL_CREATE", LABEL_CREATE);		
		labels.put("LABEL_UPDATE", LABEL_UPDATE);		
		labels.put("LABEL_LIST", LABEL_LIST);		
		return labels;
	}
}
