package com.bukbros.newproject.course.interfaces;

import java.util.List;

import com.bukbros.newproject.course.Course;
import com.bukbros.newproject.interfaces.ServiceInterface;

public interface CourseServiceInterface extends ServiceInterface<Course>{
	boolean isCodeExisted(String code, long id);
	
	List<Course> getCoursesInUpdate(long id);
	
	boolean isCourseValid(long id, int status);
	
	boolean isCourseUsed(long id, String reference, Class<? extends Object> referenceClass);
	
	long countCourse();
}
