package com.bukbros.newproject.course.interfaces;

import java.util.List;

import com.bukbros.newproject.course.Course;
import com.bukbros.newproject.interfaces.RepositoryInterface;

public interface CourseRepositoryInterface extends RepositoryInterface<Course>{
	boolean isCodeExisted(String code, long id);
	
	List<Course> getCoursesInUpdate(long id);
	
	boolean isCourseUsed(long id, String reference, Class<? extends Object> referenceClass);
	
	long countCourse();
}
