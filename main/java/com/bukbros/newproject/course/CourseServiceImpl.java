package com.bukbros.newproject.course;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bukbros.newproject.course.interfaces.CourseRepositoryInterface;
import com.bukbros.newproject.course.interfaces.CourseServiceInterface;
@Service
public class CourseServiceImpl implements CourseServiceInterface{
	@Autowired
	private CourseRepositoryInterface repository;
	
	@Override
	public boolean create(Course course) {
		return repository.create(course);
	}

	@Override
	public List<Course> read(boolean isActive) {
		return repository.read(isActive);
	}

	@Override
	public boolean update(long id, Course course) {
		course.setId(id);
		return repository.update(course);
	}

	@Override
	public boolean delete(Course course) {
		return repository.delete(course);
	}

	@Override
	public Course read(long id) {
		return repository.read(id);
	}

	@Override
	public boolean status(long id, int status) {
		return repository.status(id, status);
	}

	@Override
	public Course read(String uuid) {
		return repository.read(uuid);
	}

	@Override
	public boolean isCodeExisted(String code, long id) {
		return repository.isCodeExisted(code, id);
	}

	@Override
	public List<Course> getCoursesInUpdate(long id) {
		return repository.getCoursesInUpdate(id);
	}

	@Override
	public boolean isCourseValid(long id, int type) {
		if (id == 0) {
			return true;
		}
		Course course = repository.read(id);
		if (course != null) {
			if (type != 0) {
				if (course.getStatus() == type) {
					return true;
				}
				return false;
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean isCourseUsed(long id, String reference, Class<? extends Object> referenceClass) {
		return repository.isCourseUsed(id, reference, referenceClass);
	}

	@Override
	public long countCourse() {
		return repository.countCourse();
	}

}
