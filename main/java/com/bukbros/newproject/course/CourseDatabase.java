package com.bukbros.newproject.course;

public class CourseDatabase {
	public static final String TABLE = "cr_course";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_UUID = "uuid";
	public static final String COLUMN_CODE = "code";
	public static final String COLUMN_START_TIME = "start_time";
	public static final String COLUMN_END_TIME = "end_time";
	public static final String COLUMN_STATUS = "status";
}
