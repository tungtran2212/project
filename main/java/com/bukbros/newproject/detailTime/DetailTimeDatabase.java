package com.bukbros.newproject.detailTime;

public class DetailTimeDatabase {
	public static final String TABLE = "cr_detail_time";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_UUID = "uuid";
	public static final String COLUMN_DETAIL_ID = "detail_id";
	public static final String COLUMN_WEEK_DAY = "week_day";
	public static final String COLUMN_BLOCK = "block";
	public static final String COLUMN_STATUS = "status";
}
