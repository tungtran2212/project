package com.bukbros.newproject.detailTime;

import java.security.Principal;
import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.newproject.configuration.Role;
import com.bukbros.newproject.configuration.Url;
import com.bukbros.newproject.detail.Detail;
import com.bukbros.newproject.detail.DetailConfig;
import com.bukbros.newproject.detail.interfaces.DetailServiceInterface;
import com.bukbros.newproject.detailTime.interfaces.DetailTimeServiceInterface;
import com.bukbros.newproject.language.LabelLanguage;
import com.bukbros.newproject.language.MessageLanguage;
import com.bukbros.newproject.user.User;
import com.bukbros.newproject.user.interfaces.UserServiceInterface;

@Controller
@RequestMapping(value = DetailConfig.MODULE_ADMIN_URL + "/{uuidDetail}" + DetailTimeConfig.MODULE_URL)
public class DetailTimeController {
	@Autowired
	private DetailTimeServiceInterface service;
	
	@Autowired
	private DetailServiceInterface detailService;
	
	@Autowired
	private UserServiceInterface userService;
	
	@ModelAttribute("module")
	public HashMap<String, String> module() {
		return DetailConfig.component();
	}
	
	@ModelAttribute("moduleLang")
	public HashMap<String, String> moduleLang() {
		return DetailTimeLanguage.label();
	}
	
	@ModelAttribute("labelLang")
	public HashMap<String, String> labelLanguage() {
		return LabelLanguage.labels();
	}
	
	@ModelAttribute("detailMenu")
	public boolean detailMenu() {
		return true;
	}
	
	@GetMapping(Url.URL_CREATE)
	public String getCreate (@PathVariable("uuidDetail") String uuidDetail,
	                         Model model,
	                         Principal principal,
							 RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Detail originalDetail = detailService.read(uuidDetail);
		if (originalDetail == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + DetailConfig.MODULE_ADMIN_URL;
		}
		model.addAttribute("createMode", true);
		model.addAttribute("detail", originalDetail);
		model.addAttribute("detailTimes", service.getDetailTimeByDetailId(originalDetail.getId(), false));
		model.addAttribute("maxBlock", DetailTimeConfig.MAX_BLOCK);
		return DetailTimeConfig.MODULE_VIEW_ADMIN;
	}
	
	@PostMapping(Url.URL_CREATE)
	public String postCreate (@PathVariable("uuidDetail") String uuidDetail,
	                          @ModelAttribute("validDetailTime") @Valid DetailTime detailTime,
							  BindingResult results,
							  Model model,
							  Principal principal,
							  RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Detail originalDetail = detailService.read(uuidDetail);
		if (originalDetail == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + DetailConfig.MODULE_ADMIN_URL;
		}
		model.addAttribute("createMode", true);
		model.addAttribute("detail", originalDetail);
		model.addAttribute("detailTimes", service.getDetailTimeByDetailId(originalDetail.getId(), false));
		model.addAttribute("detailTime", detailTime);
		model.addAttribute("maxBlock", DetailTimeConfig.MAX_BLOCK);
		if (results.hasErrors()) {
			return DetailTimeConfig.MODULE_VIEW_ADMIN;
		} else {	
			boolean isDetailTimeExists = false;
			boolean isDetailTimeExistsByRoom = false;
			boolean isDetailTimeExistsByStaff = false;
			for (int block : detailTime.getBlocks()) {
					if ((block < 1) || (block > DetailTimeConfig.MAX_BLOCK)) {
						results.addError(new FieldError("validDetailTime", "blocks", DetailTimeLanguage.VALIDATION_BLOCK));
						break;
					} else {
						isDetailTimeExists = service.isDetailTimeExists(originalDetail.getId(), detailTime.getWeekDay(), block);
						if (isDetailTimeExists) {
							model.addAttribute("errorMessage", DetailTimeLanguage.IS_DETAIL_TIME_EXISTED);
							break;
						}
						isDetailTimeExistsByRoom = service.isDetailTimeExistsByRoom(originalDetail.getRoom().getId(), detailTime.getWeekDay(), block);
						if (isDetailTimeExistsByRoom) {
							model.addAttribute("errorMessage", DetailTimeLanguage.IS_DETAIL_TIME_EXISTED);
							break;
						}
						isDetailTimeExistsByStaff = service.isDetailTimeExistsByStaff(originalDetail.getStaff().getUserId(), detailTime.getWeekDay(), block);
						if (isDetailTimeExistsByStaff) {
							model.addAttribute("errorMessage", DetailTimeLanguage.IS_DETAIL_TIME_EXISTED_BY_STAFF);
							break;
						}
					}
				}
			if (isDetailTimeExists || isDetailTimeExistsByRoom || isDetailTimeExistsByStaff || results.hasFieldErrors("blocks")) {
				return DetailTimeConfig.MODULE_VIEW_ADMIN;
			}
		}
		detailTime.setDetail(originalDetail);
		boolean createItem = service.create(detailTime);
		if (createItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_CREATE_SUCCESS);
		} else {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_CREATE_ERROR);
			return DetailTimeConfig.MODULE_VIEW_ADMIN;
		}
		model.asMap().clear();
		return "redirect:" + DetailConfig.MODULE_ADMIN_URL + "/" + originalDetail.getUuid() + DetailConfig.URL_TIME + Url.URL_CREATE;
	}
	
	@PostMapping("/{uuid}" + Url.URL_ACTIVE)
	public String postActive(@PathVariable("uuidDetail") String uuidDetail,
	                         @PathVariable("uuid") String uuid,
	                         Model model,
	                         Principal principal,
	                         RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		DetailTime originalDetailTime = service.read(uuid);
		Detail originalDetail = detailService.read(uuidDetail);
		if ((originalDetailTime == null) || (originalDetail == null) || (originalDetailTime.getDetail().getId() != originalDetail.getId())) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_DETAIL;
		}
		boolean isDetailTimeExists = false;
		boolean isDetailTimeExistsByStaff = false;
		isDetailTimeExists = service.isDetailTimeExistsByRoom(originalDetail.getRoom().getId(), originalDetailTime.getWeekDay(), originalDetailTime.getBlock());
		if (isDetailTimeExists) {
			redirectAttributes.addFlashAttribute("errorMessage", DetailTimeLanguage.IS_DETAIL_TIME_EXISTED);
			return "redirect:" + DetailConfig.MODULE_ADMIN_URL + "/" + originalDetail.getUuid() + DetailConfig.URL_TIME + Url.URL_CREATE;
		}
		isDetailTimeExistsByStaff = service.isDetailTimeExistsByStaff(originalDetail.getStaff().getUserId(), originalDetailTime.getWeekDay(), originalDetailTime.getBlock());
		if (isDetailTimeExistsByStaff) {
			redirectAttributes.addFlashAttribute("errorMessage", DetailTimeLanguage.IS_DETAIL_TIME_EXISTED_BY_STAFF);
			return "redirect:" + DetailConfig.MODULE_ADMIN_URL + "/" + originalDetail.getUuid() + DetailConfig.URL_TIME + Url.URL_CREATE;
		}
		boolean active = service.status(originalDetailTime.getId(), 1);
		if (active) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_ACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ACTIVE_ERROR);
		}
		return "redirect:" + DetailConfig.MODULE_ADMIN_URL + "/" + originalDetail.getUuid() + DetailConfig.URL_TIME + Url.URL_CREATE;
	}
	
	@PostMapping("/{uuid}" + Url.URL_DEACTIVE)
	public String postDeactive(@PathVariable("uuidDetail") String uuidDetail,
	                         @PathVariable("uuid") String uuid,
	                         Model model,
	                         Principal principal,
	                         RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		DetailTime originalDetailTime = service.read(uuid);
		Detail originalDetail = detailService.read(uuidDetail);
		if ((originalDetailTime == null) || (originalDetail == null) || (originalDetailTime.getDetail().getId() != originalDetail.getId())) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_DETAIL;
		}
		boolean active = service.status(originalDetailTime.getId(), 0);
		if (active) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_ACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ACTIVE_ERROR);
		}
		return "redirect:" + DetailConfig.MODULE_ADMIN_URL + "/" + originalDetail.getUuid() + DetailConfig.URL_TIME + Url.URL_CREATE;
	}
	
	@PostMapping("/{uuid}" + Url.URL_DELETE )
	public String postDelete(@PathVariable("uuidDetail") String uuidDetail,
	                         @PathVariable("uuid") String uuid,
	                          Model model,
	                          Principal principal,
	                          RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		DetailTime originalDetailTime = service.read(uuid);
		Detail originalDetail = detailService.read(uuidDetail);
		if ((originalDetailTime == null) || (originalDetail == null) || (originalDetailTime.getDetail().getId() != originalDetail.getId())) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_DETAIL;
		}
		boolean delete = service.delete(originalDetailTime);
		if (delete) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DELETE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DELETE_ERROR);
		}
		return "redirect:" + DetailConfig.MODULE_ADMIN_URL + "/" + originalDetail.getUuid() + DetailConfig.URL_TIME + Url.URL_CREATE;
	}
}
