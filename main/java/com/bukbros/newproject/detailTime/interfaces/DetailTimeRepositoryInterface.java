package com.bukbros.newproject.detailTime.interfaces;

import java.util.List;

import com.bukbros.newproject.detailTime.DetailTime;
import com.bukbros.newproject.interfaces.RepositoryInterface;

public interface DetailTimeRepositoryInterface extends RepositoryInterface<DetailTime>{
	boolean isDetailTimeExistsByRoom(long roomId, int weekDay, int block);
	
	boolean isDetailTimeExists(long detailId, int weekDay, int block);
	
	boolean isDetailTimeExistsByStaff(long staffId, int weekDay, int block);
	
	List<DetailTime> getDetailTimeByDetailId(long detailId);
	
	List<DetailTime> getDetailTimeByMajor(long majorId);
	
	List<DetailTime> getDetailTimeByStudent(long studentId);
	
	List<DetailTime> getTimeTableByStudent(long studentId);
	
	List<DetailTime> getDetailTimeByStaff(long staffId);
	
}
