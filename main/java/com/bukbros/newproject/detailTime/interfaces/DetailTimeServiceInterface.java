package com.bukbros.newproject.detailTime.interfaces;

import java.util.List;

import com.bukbros.newproject.detailTime.DetailTime;
import com.bukbros.newproject.interfaces.ServiceInterface;

public interface DetailTimeServiceInterface extends ServiceInterface<DetailTime>{
	boolean isDetailTimeExistsByRoom(long roomId, int weekDay, int block);
	
	boolean isDetailTimeExists(long detailId, int weekDay, int block);
	
	boolean isDetailTimeExistsByStaff(long staffId, int weekDay, int block);
	
	List<DetailTime> getDetailTimeByDetailId(long detailId, boolean isGetBlocks);
	
	List<DetailTime> getDetailTimeByMajor(long majorId, long currentStudentId);
	
	List<DetailTime> getDetailTimeByStudent(long studentId);
	
	List<DetailTime> getTimeTableByStudent(long studentId);
	
	List<DetailTime> getTimeTableByStaff(long staffId);
}
