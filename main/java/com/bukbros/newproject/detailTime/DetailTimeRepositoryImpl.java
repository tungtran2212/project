package com.bukbros.newproject.detailTime;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bukbros.newproject.detailTime.interfaces.DetailTimeRepositoryInterface;
import com.bukbros.newproject.registration.Registration;
@Repository
@Transactional
public class DetailTimeRepositoryImpl implements DetailTimeRepositoryInterface{
	@Autowired
	private SessionFactory sessionFactory;
	
	private Logger logger = LogManager.getLogger(DetailTimeRepositoryImpl.class);
	
	@Override
	public boolean create(DetailTime detailTime) {
		try {
			sessionFactory.getCurrentSession().persist(detailTime);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public List<DetailTime> read(boolean isActive) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<DetailTime> criteria = builder.createQuery(DetailTime.class);
			Root<DetailTime> root = criteria.from(DetailTime.class);
			criteria.select(root);
			if (isActive) {
				criteria.where(builder.equal(root.get("status"), 1));
			}
			criteria.orderBy(builder.desc(root.get("id")));
			List<DetailTime> detailTimes = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !detailTimes.isEmpty() ? detailTimes : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean update(DetailTime object) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(DetailTime detailTime) {
		try {
			sessionFactory.getCurrentSession().delete(detailTime);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public DetailTime read(long id) {
		try {
			DetailTime detailTime = sessionFactory.getCurrentSession().get(DetailTime.class, id);
			return detailTime;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}	
	}

	@Override
	public boolean status(long id, int status) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaUpdate<DetailTime> criteria = builder.createCriteriaUpdate(DetailTime.class);
		    Root<DetailTime> root = criteria.from(DetailTime.class);
		    criteria.set(root.get("status"), status)
		    		.where(builder.equal(root.get("id"), id));
		    sessionFactory.getCurrentSession().createQuery(criteria).executeUpdate();
		    return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public DetailTime read(String uuid) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<DetailTime> criteria = builder.createQuery(DetailTime.class);
		    Root<DetailTime> root = criteria.from(DetailTime.class);
		    criteria.select(root)
		     		.where(builder.equal(root.get("uuid"), UUID.fromString(uuid)));
		    DetailTime detailTime = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
		    return detailTime; 
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean isDetailTimeExistsByRoom(long roomId, int weekDay, int block) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<DetailTime> criteria = builder.createQuery(DetailTime.class);
			Root<DetailTime> root = criteria.from(DetailTime.class);
			List<Predicate> predicates = new ArrayList<>();
		    predicates.add(builder.equal(root.get("weekDay"), weekDay));
		    predicates.add(builder.equal(root.get("block"), block));
		    predicates.add(builder.equal(root.get("detail").get("room"), roomId));
		    predicates.add(builder.equal(root.get("status"), 1));
		    predicates.add(builder.equal(root.get("detail").get("status"), 1));
		    predicates.add(builder.equal(root.get("detail").get("majorSubject").get("status"), 1));
		    predicates.add(builder.equal(root.get("detail").get("majorSubject").get("course").get("status"), 1));
			criteria.select(root)
					.where(predicates.toArray(new Predicate[predicates.size()]))
					.orderBy(builder.asc(root.get("id")));
			DetailTime detailTimes = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
			return detailTimes != null ? true : false;
		} catch (Exception e) {
			logger.error(e);
			return true;
		}
	}

	@Override
	public List<DetailTime> getDetailTimeByDetailId(long detailId) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<DetailTime> criteria = builder.createQuery(DetailTime.class);
			Root<DetailTime> root = criteria.from(DetailTime.class);
			criteria.select(root).where(builder.equal(root.get("detail"), detailId))
								 .orderBy(builder.asc(root.get("weekDay")), builder.asc(root.get("block")));
			List<DetailTime> detailTimes = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !detailTimes.isEmpty() ? detailTimes : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public List<DetailTime> getDetailTimeByMajor(long majorId) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<DetailTime> criteria = builder.createQuery(DetailTime.class);
			Root<DetailTime> root = criteria.from(DetailTime.class);
			List<Predicate> predicates = new ArrayList<>();
		    predicates.add(builder.equal(root.get("detail").get("majorSubject").get("major"), majorId));
		    predicates.add(builder.equal(root.get("status"), 1));
		    predicates.add(builder.equal(root.get("detail").get("room").get("status"), 1));
		    predicates.add(builder.equal(root.get("detail").get("staff").get("user").get("status"), 1));
		    predicates.add(builder.equal(root.get("detail").get("status"), 1));
		    predicates.add(builder.equal(root.get("detail").get("majorSubject").get("status"), 1));
		    predicates.add(builder.equal(root.get("detail").get("majorSubject").get("course").get("status"), 1));
		    predicates.add(builder.equal(root.get("detail").get("majorSubject").get("major").get("status"), 1));
		    predicates.add(builder.equal(root.get("detail").get("majorSubject").get("subject").get("status"), 1));
			criteria.select(root).where(predicates.toArray(new Predicate[predicates.size()]))
					.orderBy(builder.asc(root.get("detail").get("majorSubject").get("course").get("id")), 
							 builder.asc(root.get("detail").get("id")), 
							 builder.asc(root.get("weekDay")), 
							 builder.asc(root.get("block")));
			List<DetailTime> detailTimes = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !detailTimes.isEmpty() ? detailTimes : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public List<DetailTime> getDetailTimeByStudent(long studentId) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<DetailTime> criteria = builder.createQuery(DetailTime.class);
			Root<DetailTime> root = criteria.from(DetailTime.class);
			Root<Registration> registration = criteria.from(Registration.class);
			List<Predicate> predicates = new ArrayList<>();
			predicates.add(builder.equal(registration.get("student"), studentId));
			predicates.add(builder.equal(root.get("detail").get("status"), 1));
			predicates.add(builder.equal(root.get("detail"), registration.get("detail")));
			criteria.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
			List<DetailTime> detailTimes = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !detailTimes.isEmpty() ? detailTimes : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean isDetailTimeExistsByStaff(long staffId, int weekDay, int block) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<DetailTime> criteria = builder.createQuery(DetailTime.class);
			Root<DetailTime> root = criteria.from(DetailTime.class);
			List<Predicate> predicates = new ArrayList<>();
		    predicates.add(builder.equal(root.get("weekDay"), weekDay));
		    predicates.add(builder.equal(root.get("block"), block));
		    predicates.add(builder.equal(root.get("detail").get("staff"), staffId));
		    predicates.add(builder.equal(root.get("status"), 1));
		    predicates.add(builder.equal(root.get("detail").get("status"), 1));
		    predicates.add(builder.equal(root.get("detail").get("majorSubject").get("status"), 1));
		    predicates.add(builder.equal(root.get("detail").get("majorSubject").get("course").get("status"), 1));
			criteria.select(root)
					.where(predicates.toArray(new Predicate[predicates.size()]));
			DetailTime detailTimes = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
			return detailTimes != null ? true : false;
		} catch (Exception e) {
			logger.error(e);
			return true;
		}
	}

	@Override
	public List<DetailTime> getDetailTimeByStaff(long staffId) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<DetailTime> criteria = builder.createQuery(DetailTime.class);
			Root<DetailTime> root = criteria.from(DetailTime.class);
			Root<Registration> registration = criteria.from(Registration.class);
			List<Predicate> predicates = new ArrayList<>();
			predicates.add(builder.equal(registration.get("detail").get("staff"), staffId));
			predicates.add(builder.equal(root.get("detail").get("status"), 2));
			predicates.add(builder.equal(root.get("detail"), registration.get("detail")));
			criteria.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
			List<DetailTime> detailTimes = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !detailTimes.isEmpty() ? detailTimes : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public List<DetailTime> getTimeTableByStudent(long studentId) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<DetailTime> criteria = builder.createQuery(DetailTime.class);
			Root<DetailTime> root = criteria.from(DetailTime.class);
			Root<Registration> registration = criteria.from(Registration.class);
			List<Predicate> predicates = new ArrayList<>();
			predicates.add(builder.equal(registration.get("student"), studentId));
			predicates.add(builder.equal(root.get("detail").get("status"), 2));
			predicates.add(builder.equal(root.get("detail"), registration.get("detail")));
			criteria.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
			List<DetailTime> detailTimes = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !detailTimes.isEmpty() ? detailTimes : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean isDetailTimeExists(long detailId, int weekDay, int block) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<DetailTime> criteria = builder.createQuery(DetailTime.class);
			Root<DetailTime> root = criteria.from(DetailTime.class);
			List<Predicate> predicates = new ArrayList<>();
		    predicates.add(builder.equal(root.get("weekDay"), weekDay));
		    predicates.add(builder.equal(root.get("block"), block));
		    predicates.add(builder.equal(root.get("detail"), detailId));
			criteria.select(root)
					.where(predicates.toArray(new Predicate[predicates.size()]));
			DetailTime detailTimes = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
			return detailTimes != null ? true : false;
		} catch (Exception e) {
			logger.error(e);
			return true;
		}
	}

}
