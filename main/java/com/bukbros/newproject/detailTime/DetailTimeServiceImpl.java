package com.bukbros.newproject.detailTime;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bukbros.newproject.detailTime.interfaces.DetailTimeRepositoryInterface;
import com.bukbros.newproject.detailTime.interfaces.DetailTimeServiceInterface;
import com.bukbros.newproject.registration.Registration;
import com.bukbros.newproject.registration.interfaces.RegistrationServiceInterface;
@Service
public class DetailTimeServiceImpl implements DetailTimeServiceInterface{
	@Autowired
	private DetailTimeRepositoryInterface repository;
	
	@Autowired
	private RegistrationServiceInterface registrationService;
	
	@Transactional
	@Override
	public boolean create(DetailTime detailTime) {
		for (int block : detailTime.getBlocks()) {
			boolean createDetailTime = repository.create(new DetailTime(detailTime.getDetail(), detailTime.getWeekDay(), block, detailTime.getStatus()));
			if (!createDetailTime) {
				return false;
			}
		}
		return true;
	}

	@Override
	public List<DetailTime> read(boolean isActive) {
		return repository.read(isActive);
	}

	@Override
	public boolean update(long id, DetailTime detailTime) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(DetailTime detailTime) {
		return repository.delete(detailTime);
	}

	@Override
	public DetailTime read(long id) {
		return repository.read(id);
	}

	@Override
	public boolean status(long id, int status) {
		return repository.status(id, status);
	}

	@Override
	public DetailTime read(String uuid) {
		return repository.read(uuid);
	}

	@Override
	public boolean isDetailTimeExistsByRoom(long roomId, int weekDay, int block) {
		return repository.isDetailTimeExistsByRoom(roomId, weekDay, block);
	}

	@Override
	public List<DetailTime> getDetailTimeByDetailId(long detailId, boolean isGetBlocks) {
		List<DetailTime> timeTableByDetail = repository.getDetailTimeByDetailId(detailId);
		if (timeTableByDetail == null) {
			return null;
		}
		if (!isGetBlocks) {
			return timeTableByDetail;
		}
		List<DetailTime> detailTimes = new ArrayList<DetailTime>();
		int weekDay = 0;
		DetailTime newDetailTime = null;
		List<Integer> blocks = null; 
		for (DetailTime detailTime : timeTableByDetail) {     
			if (weekDay != detailTime.getWeekDay()) {
				if (weekDay != 0) {
					newDetailTime.setBlocks(blocks);
					detailTimes.add(newDetailTime);						
				}
			    weekDay = detailTime.getWeekDay();
			    newDetailTime = new DetailTime();
			    newDetailTime = detailTime;	
			    blocks = new ArrayList<Integer>();
			    blocks.add(detailTime.getBlock());
			} else {
				blocks.add(detailTime.getBlock());
			}
		}
		newDetailTime.setBlocks(blocks);
		detailTimes.add(newDetailTime);
		return detailTimes;
	}

	@Override
	public List<DetailTime> getDetailTimeByMajor(long majorId,  long currentStudentId) {
		List<DetailTime> detailTimesByMajor = repository.getDetailTimeByMajor(majorId);
		if (detailTimesByMajor == null) {
			return null;
		}
		List<DetailTime> detailTimes = new ArrayList<DetailTime>();
		long detailId = 0;
		int weekDay = 0;
		DetailTime newDetailTime = null;
		String time = "";
		boolean isRegister = false;
		for (DetailTime detailTime : detailTimesByMajor) {  
			if (detailTime.getDetail().getId() != detailId) {
				if (detailId != 0) {
					List<Registration> registrations = registrationService.getRegistrationByDetailId(detailId);
					if (registrations == null) {
						newDetailTime.setCountStudent(0);
						newDetailTime.setIsRegister(false);
					} else {
						for (Registration registration : registrations) {
							if (registration.getStudent().getUserId() == currentStudentId) {
								isRegister = true;
							}
						}
						newDetailTime.setCountStudent(registrations.size());
						newDetailTime.setIsRegister(isRegister);
					}
					time = time + ")";
					newDetailTime.setTime(time);
					detailTimes.add(newDetailTime);
					isRegister = false;
				}
				newDetailTime = new DetailTime();
			    newDetailTime = detailTime;
			    detailId = detailTime.getDetail().getId();
			    weekDay = detailTime.getWeekDay();
			    time = new String();
			    time = "- Thứ " + detailTime.getWeekDay() + " (Ca " + detailTime.getBlock();
			} else {
				if (weekDay != detailTime.getWeekDay()) {
				    weekDay = detailTime.getWeekDay();
				    time = time + ")</br>" + "- Thứ " + detailTime.getWeekDay() + " (Ca " + detailTime.getBlock();
				} else {
					time = time + ", " + detailTime.getBlock();
				}
			}
		}
		List<Registration> registrations = registrationService.getRegistrationByDetailId(detailId);
		if (registrations == null) {
			newDetailTime.setCountStudent(0);
			newDetailTime.setIsRegister(false);
		} else {
			for (Registration registration : registrations) {
				if (registration.getStudent().getUserId() == currentStudentId) {
					isRegister = true;
				}
			}
			newDetailTime.setCountStudent(registrations.size());
			newDetailTime.setIsRegister(isRegister);
		}
		time = time + ")";
		newDetailTime.setTime(time);
		detailTimes.add(newDetailTime);
		return detailTimes;
	}

	@Override
	public List<DetailTime> getDetailTimeByStudent(long studentId) {
		return repository.getDetailTimeByStudent(studentId);
	}

	@Override
	public List<DetailTime> getTimeTableByStudent(long studentId) {
		List<DetailTime> timeTableByStudent = repository.getTimeTableByStudent(studentId);
		if (timeTableByStudent == null) {
			return null;
		}
		List<DetailTime> detailTimes = new ArrayList<DetailTime>();
		int weekDay = 0;
		DetailTime newDetailTime = null;
		List<Integer> blocks = null; 
		for (DetailTime detailTime : timeTableByStudent) {     
			if (weekDay != detailTime.getWeekDay()) {
				if (weekDay != 0) {
					newDetailTime.setBlocks(blocks);
					detailTimes.add(newDetailTime);						
				}
			    weekDay = detailTime.getWeekDay();
			    newDetailTime = new DetailTime();
			    newDetailTime = detailTime;	
			    blocks = new ArrayList<Integer>();
			    blocks.add(detailTime.getBlock());
			} else {
				blocks.add(detailTime.getBlock());
			}
		}
		newDetailTime.setBlocks(blocks);
		detailTimes.add(newDetailTime);
		return detailTimes;
	}

	@Override
	public boolean isDetailTimeExistsByStaff(long staffId, int weekDay, int block) {
		return repository.isDetailTimeExistsByStaff(staffId, weekDay, block);
	}

	@Override
	public List<DetailTime> getTimeTableByStaff(long staffId) {
		List<DetailTime> timeTableByStaff = repository.getDetailTimeByStaff(staffId);
		if (timeTableByStaff == null) {
			return null;
		}
		List<DetailTime> detailTimes = new ArrayList<DetailTime>();
		int weekDay = 0;
		DetailTime newDetailTime = null;
		List<Integer> blocks = null; 
		for (DetailTime detailTime : timeTableByStaff) {     
			if (weekDay != detailTime.getWeekDay()) {
				if (weekDay != 0) {
					newDetailTime.setBlocks(blocks);
					detailTimes.add(newDetailTime);						
				}
			    weekDay = detailTime.getWeekDay();
			    newDetailTime = new DetailTime();
			    newDetailTime = detailTime;	
			    blocks = new ArrayList<Integer>();
			    blocks.add(detailTime.getBlock());
			} else {
				blocks.add(detailTime.getBlock());
			}
		}
		newDetailTime.setBlocks(blocks);
		detailTimes.add(newDetailTime);
		return detailTimes;
	}

	@Override
	public boolean isDetailTimeExists(long detailId, int weekDay, int block) {
		return repository.isDetailTimeExists(detailId, weekDay, block);
	}
	
}
