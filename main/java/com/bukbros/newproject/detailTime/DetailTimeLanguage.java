package com.bukbros.newproject.detailTime;

import java.util.HashMap;

public class DetailTimeLanguage {
	public static final String VALIDATION_NOT_EMPTY = "Không được để trống";
	public static final String VALIDATION_DETAIL = "Buổi học không hợp lệ. Vui lòng chọn lại";
	public static final String VALIDATION_BLOCK = "Ca không đúng, vui lòng chọn lại";
	public static final String VALIDATION_WEEKDAY = "Thứ không đúng, vui lòng chọn lại";
	
	public static final String IS_DETAIL_TIME_EXISTED = "Thời gian này của phòng học đã được sử dụng.";
	public static final String IS_DETAIL_TIME_EXISTED_BY_STAFF = "Giáo viên đã có lớp dạy vào thời gian này.";
	
	public static final String LABEL_DETAIL = "Detail";
	public static final String LABEL_WEEKDAY = "Thứ";
	public static final String LABEL_BLOCK = "Ca";
	
	public static final String LABEL_CREATE = "Thêm mới thời gian học";
	public static final String LABEL_UPDATE = "Cập nhật thời gian học";
	public static final String LABEL_LIST = "Danh sách thời gian học";
	
	public static HashMap<String, String> label() {
		HashMap<String, String> labels = new HashMap<>();
		labels.put("LABEL_DETAIL", LABEL_DETAIL);
		labels.put("LABEL_WEEKDAY", LABEL_WEEKDAY);
		labels.put("LABEL_BLOCK", LABEL_BLOCK);
		
		labels.put("LABEL_CREATE", LABEL_CREATE);		
		labels.put("LABEL_UPDATE", LABEL_UPDATE);		
		labels.put("LABEL_LIST", LABEL_LIST);		
		return labels;
	}

}
