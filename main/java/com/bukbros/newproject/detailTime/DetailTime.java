package com.bukbros.newproject.detailTime;

import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Range;

import com.bukbros.newproject.detail.Detail;

@Entity
@Table(name = DetailTimeDatabase.TABLE)
public class DetailTime {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = DetailTimeDatabase.COLUMN_ID)
	private long id;

	@Column(name = DetailTimeDatabase.COLUMN_UUID, updatable = false)
	private UUID uuid;

	@ManyToOne
	@JoinColumn(name = DetailTimeDatabase.COLUMN_DETAIL_ID)
	private Detail detail;

	@Range(min = 2, max = 7, message = DetailTimeLanguage.VALIDATION_WEEKDAY)
	@Column(name = DetailTimeDatabase.COLUMN_WEEK_DAY)
	private int weekDay;

	@Column(name = DetailTimeDatabase.COLUMN_BLOCK)
	private int block;

	@Column(name = DetailTimeDatabase.COLUMN_STATUS)
	private byte status;

	@NotEmpty(message = DetailTimeLanguage.VALIDATION_NOT_EMPTY)
	@Transient
	private List<Integer> blocks;

	@Transient
	private String time;

	@Transient
	private int countStudent;

	@Transient
	private boolean isRegister;

	public DetailTime() {
	}

	/**
	 * @param detail
	 * @param weekDay
	 * @param block
	 * @param status
	 */
	public DetailTime(Detail detail, int weekDay, int block, byte status) {
		this.detail = detail;
		this.weekDay = weekDay;
		this.block = block;
		this.status = status;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public Detail getDetail() {
		return detail;
	}

	public void setDetail(Detail detail) {
		this.detail = detail;
	}

	public int getWeekDay() {
		return weekDay;
	}

	public void setWeekDay(int weekDay) {
		this.weekDay = weekDay;
	}

	public int getBlock() {
		return block;
	}

	public void setBlock(int block) {
		this.block = block;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public List<Integer> getBlocks() {
		return blocks;
	}

	public void setBlocks(List<Integer> blocks) {
		this.blocks = blocks;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getCountStudent() {
		return countStudent;
	}

	public void setCountStudent(int countStudent) {
		this.countStudent = countStudent;
	}

	public boolean getIsRegister() {
		return isRegister;
	}

	public void setIsRegister(boolean isRegister) {
		this.isRegister = isRegister;
	}

	@PrePersist
	public void prePersist() {
		uuid = UUID.randomUUID();
	}
}
