package com.bukbros.newproject.upload;

public class UploadLanguage {
	public static final String VALIDATION_FILE_SIZE_MAX = "Dung lượng file vượt quá quy định cho phép. Vui lòng làm lại.";
	public static final String VALIDATION_FILE_EXT = "File không hợp lệ. Vui lòng làm lại.";
	public static final String VALIDATION_NOT_FILE = "Vui lòng chọn 1 file.";
}
