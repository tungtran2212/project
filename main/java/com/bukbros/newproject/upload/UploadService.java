package com.bukbros.newproject.upload;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.bukbros.newproject.function.StringFunction;

@Service
public class UploadService {	
	
	public boolean saveFile(MultipartFile file, String fileName, String folder, HttpServletRequest request) {
		String uploadRootPath = request.getServletContext().getRealPath(folder);
		File uploadRootDir = new File(uploadRootPath);		
		if (!uploadRootDir.exists()) {
			uploadRootDir.mkdirs();
		}				
		try {
			BufferedOutputStream outputStream = new BufferedOutputStream(
														new FileOutputStream(
																new File(uploadRootDir.getAbsolutePath(), fileName)));
		
				outputStream.write(file.getBytes());
				outputStream.flush();
				outputStream.close();
				return true;
		} catch (IOException e) {
			return false;
		}
	}
	
	public boolean deleteFile(String path, HttpServletRequest request) {
		String uploadRootPath = request.getServletContext().getRealPath(path);	
		try {
			File file = new File(uploadRootPath);
			if(file.delete()) {
				return true;
			}
		} catch(Exception e) {
			return false;
		}		
		return false;
	}
	
	public String generateFileName(MultipartFile file, String title) {
		long currentTime = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTimeInMillis();
		String originalFileName = file.getOriginalFilename();
		String ext = originalFileName.substring(originalFileName.lastIndexOf("."), originalFileName.length());
		String newFileName = title.trim().length() > 0 ? currentTime +"-"+ title + ext : currentTime + ext;
		return newFileName;
	}
	
	public String generateOriginalFileName(MultipartFile file) {
		long currentTime = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTimeInMillis();
		String originalFileName = file.getOriginalFilename();
		String ext = originalFileName.substring(originalFileName.lastIndexOf("."), originalFileName.length());
		String fileName = originalFileName.substring(0, originalFileName.lastIndexOf("."));
		return currentTime +"-"+ StringFunction.convertTextToSlug(fileName) + ext;
	}
	
	public boolean validFile(MultipartFile file, long size, String[] exts) {
		return (!file.getOriginalFilename().isEmpty() && validFileSize(file, size) && validFileExt(file, exts)) ? true : false;
	}
	
	public boolean validFileExt(MultipartFile file, String[] exts) {
		String originalFileName = file.getOriginalFilename();
		String fileExt = originalFileName.substring(originalFileName.lastIndexOf("."), originalFileName.length()).replace(".", "");
		for(String ext : exts) {
			if(ext.equalsIgnoreCase(fileExt)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean validFileSize(MultipartFile file, long maxSize) {
		long size = file.getSize();
		return size <= maxSize ? true : false;
	}
	
}
