package com.bukbros.newproject.staff;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.newproject.classroom.interfaces.ClassroomServiceInterface;
import com.bukbros.newproject.configuration.Role;
import com.bukbros.newproject.configuration.Url;
import com.bukbros.newproject.faculty.FacultyLanguage;
import com.bukbros.newproject.faculty.interfaces.FacultyServiceInterface;
import com.bukbros.newproject.function.StringFunction;
import com.bukbros.newproject.language.LabelLanguage;
import com.bukbros.newproject.language.MessageLanguage;
import com.bukbros.newproject.staff.interfaces.StaffServiceInterface;
import com.bukbros.newproject.subject.Subject;
import com.bukbros.newproject.subject.SubjectLanguage;
import com.bukbros.newproject.subject.interfaces.SubjectServiceInterface;
import com.bukbros.newproject.upload.UploadService;
import com.bukbros.newproject.user.User;
import com.bukbros.newproject.user.UserConfig;
import com.bukbros.newproject.user.UserFunction;
import com.bukbros.newproject.user.UserLanguage;
import com.bukbros.newproject.user.interfaces.UserServiceInterface;

@Controller
@RequestMapping(StaffConfig.MODULE_ADMIN_URL)
public class StaffController {
	@Autowired
	private StaffServiceInterface service;

	@Autowired
	private FacultyServiceInterface facultyService;

	@Autowired
	private SubjectServiceInterface subjectService;

	@Autowired
	private UserServiceInterface userService;

	@Autowired
	private ClassroomServiceInterface classroomService;
	
	@Autowired
	private UploadService uploadService;

	@ModelAttribute("module")
	public HashMap<String, String> module() {
		return StaffConfig.component();
	}

	@ModelAttribute("moduleLang")
	public HashMap<String, String> moduleLang() {
		return StaffLanguage.labels();
	}

	@ModelAttribute("labelLang")
	public HashMap<String, String> labelLanguage() {
		return LabelLanguage.labels();
	}

	@ModelAttribute("staffMenu")
	public boolean staffMenu() {
		return true;
	}

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		binder.registerCustomEditor(List.class, "subjects", new CustomCollectionEditor(List.class) {
			@Override
			protected Object convertElement(Object element) {
				String subjectId = (String) element;
				Subject subject = new Subject();
				subject.setId(Long.parseLong(subjectId));
				return subject;
			}
		});
	}

	@RequestMapping(method = RequestMethod.GET)
	public String getStaffs(Principal principal, Model model, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("listMode", true);
		List<Staff> staffs = service.read(false);
		if (staffs == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
		} else {
			model.addAttribute("staffs", staffs);
		}
		return StaffConfig.MODULE_VIEW_ADMIN;
	}

	@GetMapping(Url.URL_CREATE)
	public String getCreate(Model model, Principal principal, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("createMode", true);
		model.addAttribute("faculties", facultyService.read(true));
		model.addAttribute("subjects", subjectService.read(true));
		return StaffConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping(Url.URL_CREATE)
	public String postCreate(HttpServletRequest request,
	                         Principal principal,
	                         @ModelAttribute("validStaff") @Valid Staff staff,
	                         BindingResult results,
	                         Model model,
	                         RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("createMode", true);
		model.addAttribute("staff", staff);
		model.addAttribute("faculties", facultyService.read(true));
		model.addAttribute("subjects", subjectService.read(true));
		MultipartFile avatarFile = staff.getUser().getAvatarFile();
		if (avatarFile.getSize() > 0) {
			if (!uploadService.validFileSize(avatarFile, UserConfig.MAX_IMAGE_FILE_SIZE)) {
				results.addError(new FieldError("validStaff", "user.avatarFile", StaffLanguage.VALIDATION_IMAGE_SIZE));
			}
			if (!(uploadService.validFileExt(avatarFile, UserConfig.EXT_IMAGE_FILE_UPLOAD))) {
				results.addError(new FieldError("validStaff", "user.avatarFile", StaffLanguage.VALIDATION_IMAGE_UPLOAD));
			}
		} else {
			results.addError(new FieldError("validStaff", "user.avatarFile", StaffLanguage.VALIDATION_NOT_EMPTY));
		}

		if (results.hasErrors()) {
			return StaffConfig.MODULE_VIEW_ADMIN;
		} else {
			boolean isUsernameExisted = userService.isUsernameExisted(staff.getUser().getUsername());
			if (isUsernameExisted) {
				results.addError(new FieldError("validStaff", "user.username", UserLanguage.MESSAGE_USERNAME_VALID));
			}
			boolean validEmail = userService.isEmailExisted(staff.getUser().getEmail());
			if (validEmail) {
				results.addError(new FieldError("validStaff", "user.email", UserLanguage.MESSAGE_EMAIL_VALID));
			}
			boolean confirmPassword = userService.confirmPassword(staff.getUser().getPassword(), staff.getUser().getConfirmPassword());
			if (!confirmPassword) {
				results.addError(new FieldError("validStaff", "user.confirmPassword", UserLanguage.VALIDATION_CONFIRM_PASS));
			}
			boolean maxDate = userService.maxDate(staff.getUser().getDob());
			if (maxDate) {
				results.addError(new FieldError("validStaff", "user.dob", UserLanguage.VALIDATION_DOB));
			}
			boolean validFaculty = facultyService.isFacultyValid(staff.getFaculty().getId(), 1);
			if (!validFaculty) {
				results.addError(new FieldError("validStaff", "faculty.id", FacultyLanguage.VALIDATION_FACULTY));
			}
			List<Subject> subjects = staff.getSubjects();
			if (subjects != null) {
				for (Subject subject : subjects) {
					if (!subjectService.isSubjectValid(subject.getId(), 1)) {
						results.addError(new FieldError("validStaff", "subjects", SubjectLanguage.VALIDATION_SUBJECT));
						break;
					}
				}
			}
			if (results.hasFieldErrors("user.username") || results.hasFieldErrors("user.email") || results.hasFieldErrors("user.confirmPassword")
			        || results.hasFieldErrors("user.dob") || results.hasFieldErrors("faculty.id") || results.hasFieldErrors("subjects")) {
				return StaffConfig.MODULE_VIEW_ADMIN;
			}
		}
		boolean createItem = service.create(request, staff);
		if (createItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_CREATE_SUCCESS);
		} else {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_CREATE_ERROR);
			return StaffConfig.MODULE_VIEW_ADMIN;
		}
		model.asMap().clear();
		return "redirect:" + Url.URL_STAFF;
	}

	@GetMapping("/{uuid}" + Url.URL_DETAIL)
	public String getDetail(@PathVariable("uuid") String uuid, Principal principal, Model model, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		Staff originalStaff = service.getStaffByUsername(principal.getName());
		if (originalStaff == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_ADMIN;
		}
		if (!originalStaff.getUser().getRole().equalsIgnoreCase(Role.ROLE_STAFF)
		        && !originalStaff.getUser().getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Staff staff = service.read(uuid);
		if (staff == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_STAFF;
		}
		staff.setCertificate(StringFunction.convertLineFeed(staff.getCertificate()));
		model.addAttribute("detailMode", true);
		model.addAttribute("staff", staff);
		model.addAttribute("classrooms", classroomService.getClassroomsByUser(staff.getUserId(), true));
		return StaffConfig.MODULE_VIEW_ADMIN;
	}

	@GetMapping("/{uuid}" + Url.URL_UPDATE)
	public String getUpdate(@PathVariable("uuid") String uuid, Principal principal, Model model, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Staff originalStaff = service.read(uuid);
		if (originalStaff == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_STAFF;
		}
		List<Long> originalSubjectIds = new ArrayList<Long>();
		if (!originalStaff.getSubjects().isEmpty()) {
			for (Subject subject : originalStaff.getSubjects()) {
				originalSubjectIds.add(subject.getId());
			}
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("faculties", facultyService.getFacultiesInUpdate(originalStaff.getFaculty().getId()));
		model.addAttribute("subjects", subjectService.getSubjectsInUpdate(originalSubjectIds));
		model.addAttribute("staff", originalStaff);
		model.addAttribute("originalStaff", originalStaff);
		return StaffConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping("/{uuid}" + Url.URL_UPDATE)
	public String postUpdate(@PathVariable("uuid") String uuid,
	                         HttpServletRequest request,
	                         Principal principal,
	                         @ModelAttribute("validStaff") @Valid Staff staff,
	                         BindingResult results,
	                         Model model,
	                         RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Staff originalStaff = service.read(uuid);
		if (originalStaff == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_STAFF;
		}
		List<Long> originalSubjectIds = new ArrayList<Long>();
		if (!originalStaff.getSubjects().isEmpty()) {
			for (Subject subject : originalStaff.getSubjects()) {
				originalSubjectIds.add(subject.getId());
			}
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("faculties", facultyService.getFacultiesInUpdate(originalStaff.getFaculty().getId()));
		model.addAttribute("subjects", subjectService.getSubjectsInUpdate(originalSubjectIds));
		model.addAttribute("staff", staff);
		model.addAttribute("originalStaff", originalStaff);
		MultipartFile avatarFile = staff.getUser().getAvatarFile();
		if (avatarFile.getSize() > 0) {
			if (!uploadService.validFileSize(avatarFile, UserConfig.MAX_IMAGE_FILE_SIZE)) {
				results.addError(new FieldError("validStaff", "user.avatarFile", UserLanguage.VALIDATION_IMAGE_SIZE));
			}
			if (!(uploadService.validFileExt(avatarFile, UserConfig.EXT_IMAGE_FILE_UPLOAD))) {
				results.addError(new FieldError("validStaff", "user.avatarFile", UserLanguage.VALIDATION_IMAGE_UPLOAD));
			}
		}
		if (results.hasErrors()) {
			return StaffConfig.MODULE_VIEW_ADMIN;
		} else {
			if (!StringFunction.trimSpace(staff.getUser().getEmail()).equalsIgnoreCase(originalStaff.getUser().getEmail())) {
				boolean isEmailExisted = userService.isEmailExisted(StringFunction.trimSpace(staff.getUser().getEmail()));
				if (isEmailExisted) {
					results.addError(new FieldError("validStaff", "user.email", UserLanguage.MESSAGE_EMAIL_VALID));
				}
			}
			boolean maxDate = userService.maxDate(staff.getUser().getDob());
			if (maxDate) {
				results.addError(new FieldError("validStaff", "user.dob", UserLanguage.VALIDATION_DOB));
			}
			List<Subject> subjects = staff.getSubjects();
			if (subjects != null) {
				if (!originalSubjectIds.isEmpty()) {
					for (Subject subject : subjects) {
						if (originalSubjectIds.contains(subject.getId())) {
							if (!subjectService.isSubjectValid(subject.getId(), 0)) {
								results.addError(new FieldError("validStaff", "subjects", SubjectLanguage.VALIDATION_SUBJECT));
								break;
							}
						} else {
							if (!subjectService.isSubjectValid(subject.getId(), 1)) {
								results.addError(new FieldError("validStaff", "subjects", SubjectLanguage.VALIDATION_SUBJECT));
								break;
							}
						}
					}
				} else {
					for (Subject subject : subjects) {
						if (!subjectService.isSubjectValid(subject.getId(), 1)) {
							results.addError(new FieldError("validStaff", "subjects", SubjectLanguage.VALIDATION_SUBJECT));
							break;
						}
					}
				}
			}
			if (results.hasFieldErrors("user.email") || results.hasFieldErrors("user.dob") || results.hasFieldErrors("subjects")) {
				return StaffConfig.MODULE_VIEW_ADMIN;
			}
		}
		if (service.update(request, originalStaff, staff)) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_UPDATE_SUCCESS);
		} else {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_UPDATE_ERROR);
			return StaffConfig.MODULE_VIEW_ADMIN;
		}
		model.asMap().clear();
		return "redirect:" + Url.URL_STAFF;
	}

	@GetMapping(Url.URL_CHANGE_PASSWORD)
	public String getChangePassword(Principal principal, Model model, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		Staff originalStaff = service.getStaffByUsername(principal.getName());
		if (originalStaff == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_ADMIN;
		}
		if (!originalStaff.getUser().getRole().equalsIgnoreCase(Role.ROLE_ADMIN)
		        && !originalStaff.getUser().getRole().equalsIgnoreCase(Role.ROLE_STAFF)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}

		model.addAttribute("changeCurrentPassMode", true);

		return StaffConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping(Url.URL_CHANGE_PASSWORD)
	public String postChangePassword(Principal principal,
	                                 @ModelAttribute("changePassword") @Valid Staff staff,
	                                 BindingResult results,
	                                 Model model,
	                                 RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN) && !currentUser.getRole().equalsIgnoreCase(Role.ROLE_STAFF)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Staff originalStaff = service.getStaffByUsername(currentUser.getUsername());
		if (originalStaff == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_STAFF;
		}
		model.addAttribute("changeCurrentPassMode", true);
		boolean checkPassword = UserFunction.checkPassword(staff.getUser().getOldPassword(), originalStaff.getUser().getPassword());
		if (!checkPassword) {
			results.addError(new FieldError("changePassword", "user.oldPassword", UserLanguage.VALIDATION_PASS));
		}
		boolean confirmPassword = userService.confirmPassword(staff.getUser().getPassword(), staff.getUser().getConfirmPassword());
		if (!confirmPassword) {
			results.addError(new FieldError("changePassword", "user.confirmPassword", UserLanguage.VALIDATION_CONFIRM_PASS));
		}
		if (results.hasFieldErrors("user.password") || results.hasFieldErrors("user.oldPassword") || results.hasFieldErrors("confirmPassword")) {
			return StaffConfig.MODULE_VIEW_ADMIN;
		}
		if (userService.updatePassword(originalStaff.getUser().getId(), staff.getUser().getPassword())) {
			redirectAttributes.addFlashAttribute("successMessage", UserLanguage.MESSAGE_CHANGE_PASS_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", UserLanguage.MESSAGE_CHANGE_PASS_FAILURE);
		}
		return "redirect:" + Url.URL_ADMIN;
	}

	@GetMapping("/{uuid}" + Url.URL_CHANGE_PASSWORD)
	public String getChangePassword(@PathVariable("uuid") String uuid, Principal principal, Model model, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Staff originalStaff = service.read(uuid);
		if (originalStaff == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_STAFF;
		}
		model.addAttribute("changePassMode", true);
		model.addAttribute("staff", originalStaff);
		return StaffConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping("/{uuid}" + Url.URL_CHANGE_PASSWORD)
	public String postChangePassword(@PathVariable("uuid") String uuid,
	                                 Principal principal,
	                                 @ModelAttribute("changePassword") @Valid Staff staff,
	                                 BindingResult results,
	                                 Model model,
	                                 RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Staff originalStaff = service.read(uuid);
		if (originalStaff == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_STAFF;
		}
		model.addAttribute("changePassMode", true);
		model.addAttribute("staff", originalStaff);
		boolean confirmPassword = userService.confirmPassword(staff.getUser().getPassword(), staff.getUser().getConfirmPassword());
		if (!confirmPassword) {
			results.addError(new FieldError("changePassword", "user.confirmPassword", UserLanguage.VALIDATION_CONFIRM_PASS));
		}
		if (results.hasFieldErrors("user.password") || results.hasFieldErrors("user.confirmPassword")) {
			return StaffConfig.MODULE_VIEW_ADMIN;
		}
		if (userService.updatePassword(originalStaff.getUser().getId(), staff.getUser().getPassword())) {
			redirectAttributes.addFlashAttribute("successMessage", UserLanguage.MESSAGE_CHANGE_PASS_SUCCESS);
		} else {
			model.addAttribute("errorMessage", UserLanguage.MESSAGE_CHANGE_PASS_FAILURE);
			return StaffConfig.MODULE_VIEW_ADMIN;
		}
		model.asMap().clear();
		return "redirect:" + Url.URL_STAFF;
	}

	@PostMapping("/{uuid}" + Url.URL_ROLE_USER)
	public String postUpdateRoleUser(@PathVariable("uuid") String uuid, Principal principal, Model model, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Staff originalStaff = service.read(uuid);
		if ((originalStaff == null) || (originalStaff.getUserId() == currentUser.getId())) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_STAFF;
		}
		boolean updateRole = userService.updateRole(originalStaff.getUser().getId(), Role.ROLE_STAFF);
		if (updateRole) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_UPDATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_UPDATE_ERROR);
		}
		return "redirect:" + Url.URL_STAFF;
	}

	@PostMapping("/{uuid}" + Url.URL_ROLE_ADMIN)
	public String postUpdateRoleAdmin(@PathVariable("uuid") String uuid, Principal principal, Model model, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Staff originalStaff = service.read(uuid);
		if (originalStaff == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_STAFF;
		}
		boolean updateRole = userService.updateRole(originalStaff.getUser().getId(), Role.ROLE_ADMIN);
		if (updateRole) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_UPDATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_UPDATE_ERROR);
		}
		return "redirect:" + Url.URL_STAFF;
	}

	@PostMapping("/{uuid}" + Url.URL_ACTIVE)
	public String postActive(@PathVariable("uuid") String uuid, Principal principal, Model model, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Staff originalStaff = service.read(uuid);
		if ((originalStaff == null) || (originalStaff.getUserId() == currentUser.getId())) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_STAFF;
		}
		boolean activeItem = userService.status(originalStaff.getUserId(), 1);
		if (activeItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_ACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ACTIVE_ERROR);
		}
		return "redirect:" + Url.URL_STAFF;
	}
	
	@PostMapping("/{uuid}" + Url.URL_DEACTIVE)
	public String postDeactive(@PathVariable("uuid") String uuid, Principal principal, Model model, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Staff originalStaff = service.read(uuid);
		if ((originalStaff == null) || (originalStaff.getUserId() == currentUser.getId())) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_STAFF;
		}
		boolean deactiveItem = userService.status(originalStaff.getUserId(), 0);
		if (deactiveItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DEACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DEACTIVE_ERROR);
		}
		return "redirect:" + Url.URL_STAFF;
	}

}
