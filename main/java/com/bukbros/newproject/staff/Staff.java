package com.bukbros.newproject.staff;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.Valid;

import com.bukbros.newproject.faculty.Faculty;
import com.bukbros.newproject.subject.Subject;
import com.bukbros.newproject.user.User;

@Entity
@Table(name = StaffDatabase.TABLE)
public class Staff {
	@Id
	@Column(name = StaffDatabase.COLUMN_USER_ID)
	private long userId;

	@Column(name = StaffDatabase.COLUMN_CERTIFICATE)
	private String certificate;

	@Valid
	@OneToOne(cascade = { CascadeType.REMOVE })
	@PrimaryKeyJoinColumn
	private User user;

	@ManyToOne
	@JoinColumn(name = StaffDatabase.COLUMN_FACULTY_ID)
	private Faculty faculty;

	@ManyToMany
	@JoinTable(name = StaffDatabase.COLUMN_TABLE_STAFF_SUBJECT, joinColumns = { @JoinColumn(name = StaffDatabase.COLUMN_STAFF_ID) }, inverseJoinColumns = { @JoinColumn(name = StaffDatabase.COLUMN_SUBJECT_ID) })
	private List<Subject> subjects;

	public Staff() {

	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getCertificate() {
		return certificate;
	}

	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Faculty getFaculty() {
		return faculty;
	}

	public void setFaculty(Faculty faculty) {
		this.faculty = faculty;
	}

	public List<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<Subject> subjects) {
		this.subjects = subjects;
	}

}
