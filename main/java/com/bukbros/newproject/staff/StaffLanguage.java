package com.bukbros.newproject.staff;

import java.util.HashMap;

public class StaffLanguage {
	public static final String VALIDATION_NOT_EMPTY = "Không được để trống";
	public static final String VALIDATION_IMAGE_SIZE = "Dung lượng file vượt quá 512 kB (0.5 MB)";	
	public static final String VALIDATION_IMAGE_UPLOAD = "Định dang file không hợp lệ";
	public static final String VALIDATION_STAFF = "Giáo viên không hợp lệ. Vui lòng chọn lại";
	public static final String VALIDATION_SUBJECT = "Môn học không được giảng dạy bởi giáo viên này. Vui lòng chọn lại";
	
	public static final String LABEL_NAME = "Họ và tên";
	public static final String LABEL_USERNAME = "Tên truy cập";
	public static final String LABEL_FACULTY = "Khoa";
	public static final String LABEL_EMAIL = "Email";
	public static final String LABEL_SUBJECT = "Môn học";
	
	public static final String LABEL_CREATE = "Thêm mới nhân viên";
	public static final String LABEL_UPDATE = "Cập nhật nhân viên";
	public static final String LABEL_LIST = "Danh sách nhân viên";
	public static final String LABEL_DETAIL = "Chi tiết nhân viên";
	public static final String LABEL_CHANGE_PASSWORD = "Thay đổi mật khẩu";
	public static final String LABEL_ROLE_USER = "Hủy quyền ADMIN";
	public static final String LABEL_ROLE_ADMIN = "Quyền ADMIN";
	
	public static HashMap<String, String> labels() {
		HashMap<String, String> labels = new HashMap<>();
		labels.put("LABEL_NAME", LABEL_NAME);
		labels.put("LABEL_USERNAME", LABEL_USERNAME);
		labels.put("LABEL_FACULTY", LABEL_FACULTY);
		labels.put("LABEL_EMAIL", LABEL_EMAIL);
		labels.put("LABEL_SUBJECT", LABEL_SUBJECT);
		
		labels.put("LABEL_CREATE", LABEL_CREATE);
		labels.put("LABEL_UPDATE", LABEL_UPDATE);
		labels.put("LABEL_LIST", LABEL_LIST);
		labels.put("LABEL_DETAIL", LABEL_DETAIL);
		labels.put("LABEL_CHANGE_PASSWORD", LABEL_CHANGE_PASSWORD);
		labels.put("LABEL_ROLE_USER", LABEL_ROLE_USER);
		labels.put("LABEL_ROLE_ADMIN", LABEL_ROLE_ADMIN);
		return labels;
	}
}
