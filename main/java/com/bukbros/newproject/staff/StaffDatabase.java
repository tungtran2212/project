package com.bukbros.newproject.staff;

public class StaffDatabase {
	public static final String TABLE = "cr_staff";
	public static final String COLUMN_USER_ID = "user_id";
	public static final String COLUMN_FACULTY_ID = "faculty_id";
	public static final String COLUMN_CERTIFICATE = "certificate";
	

	public static final String COLUMN_TABLE_STAFF_SUBJECT = "cr_staff_subject";
	public static final String COLUMN_STAFF_ID = "staff_id";
	public static final String COLUMN_SUBJECT_ID = "subject_id";
}
