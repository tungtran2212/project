package com.bukbros.newproject.staff.interfaces;

import java.util.List;

import com.bukbros.newproject.interfaces.RepositoryInterface;
import com.bukbros.newproject.staff.Staff;

public interface StaffRepositoryInterface extends RepositoryInterface<Staff>{
	Staff getStaffByUsername(String username);
	
	List<Staff> getStaffsInUpdate(long id);
	
	Staff read(long id, boolean isGetSubjects);
	
	List<Staff> getStaffByFaculty(long facultyId, boolean isActive);
	
	List<Staff> getStaffBySubject(long subjectId, boolean isActive);
	
	long countStaff();
}
