package com.bukbros.newproject.staff.interfaces;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.bukbros.newproject.interfaces.ServiceInterface;
import com.bukbros.newproject.staff.Staff;

public interface StaffServiceInterface extends ServiceInterface<Staff>{
	Staff getStaffByUsername(String username);
	
	boolean create(HttpServletRequest request, Staff staff);
	
	boolean update(HttpServletRequest request, Staff originalStaff, Staff staff);
	
	List<Staff> getStaffsInUpdate(long id);
	
	boolean isStaffValid(long id, int type, long facultyId);
	
	Staff read(long id, boolean isGetSubjects);
	
	List<Staff> getStaffByFaculty(long facultyId, boolean isActive);
	
	List<Staff> getStaffBySubject(long subjectId, boolean isActive);
	
	long countStaff();
}
