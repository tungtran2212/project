package com.bukbros.newproject.staff;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bukbros.newproject.staff.interfaces.StaffRepositoryInterface;
import com.bukbros.newproject.subject.Subject;
@Repository
@Transactional
public class StaffRepositoryImpl implements StaffRepositoryInterface{
	@Autowired
	private SessionFactory sessionFactory;
	
	private Logger logger = LogManager.getLogger(StaffRepositoryImpl.class);
	
	@Override
	public boolean create(Staff staff) {
		try {
			sessionFactory.getCurrentSession().persist(staff);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public List<Staff> read(boolean isActive) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Staff> criteria = builder.createQuery(Staff.class);
			Root<Staff> root = criteria.from(Staff.class);
			criteria.select(root);
			if (isActive) {
				criteria.where(builder.equal(root.get("user").get("status"), 1));
			}
//			criteria.orderBy(builder.desc(root.get("id")));
			List<Staff> staffs = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !staffs.isEmpty() ? staffs : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean update(Staff staff) {
		try {
			sessionFactory.getCurrentSession().update(staff);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public boolean delete(Staff staff) {
		try {
			sessionFactory.getCurrentSession().delete(staff);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public Staff read(long id) {
		try {
			Staff staff = sessionFactory.getCurrentSession().get(Staff.class, id);
			return staff;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}		
	}

	@Override
	public boolean status(long id, int status) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaUpdate<Staff> criteria = builder.createCriteriaUpdate(Staff.class);
		    Root<Staff> root = criteria.from(Staff.class);
		    criteria.set(root.get("status"), status)
		    		.where(builder.equal(root.get("id"), id));
		    sessionFactory.getCurrentSession().createQuery(criteria).executeUpdate();
		    return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public Staff read(String uuid) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<Staff> criteria = builder.createQuery(Staff.class);
		    Root<Staff> root = criteria.from(Staff.class);
		    criteria.select(root)
		     		.where(builder.equal(root.get("user").get("uuid"), UUID.fromString(uuid)));
		    Staff staff = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();//tra ve null khi ko tim dc
		    Hibernate.initialize(staff.getSubjects());
		    return staff; 
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public Staff getStaffByUsername(String username) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<Staff> criteria = builder.createQuery(Staff.class);
		    Root<Staff> root = criteria.from(Staff.class);
		    criteria.select(root).where(builder.equal(root.get("user").get("username"), username));
		    Staff staff = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
		    return staff;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public List<Staff> getStaffsInUpdate(long id) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Staff> criteria = builder.createQuery(Staff.class);
			Root<Staff> root = criteria.from(Staff.class);
			criteria.select(root)
					.where(builder.or(builder.equal(root.get("user").get("status"), 1),
									  builder.equal(root.get("userId"), id)))
					.orderBy(builder.asc(root.get("userId")));
			List<Staff> staffs = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !staffs.isEmpty() ? staffs : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public Staff read(long id, boolean isGetSubjects) {
		try {
			Staff staff = sessionFactory.getCurrentSession().get(Staff.class, id);
			if (isGetSubjects) {
				Hibernate.initialize(staff.getSubjects());
			}
			return staff;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}		
	}

	@Override
	public List<Staff> getStaffByFaculty(long facultyId, boolean isActive) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<Staff> criteria = builder.createQuery(Staff.class);
		    Root<Staff> root = criteria.from(Staff.class);
		    List<Predicate> predicates = new ArrayList<>();
			predicates.add(builder.equal(root.get("faculty"), facultyId));
			if (isActive) {
				predicates.add(builder.equal(root.get("status"), 1));
			}
		    criteria.select(root)
		    		.where(predicates.toArray(new Predicate[predicates.size()]))
		    		.orderBy(builder.desc(root.get("userId")));
		    List<Staff> staffs = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
		    return !staffs.isEmpty() ? staffs : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public List<Staff> getStaffBySubject(long subjectId, boolean isActive) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<Staff> criteria = builder.createQuery(Staff.class);
		    Root<Staff> root = criteria.from(Staff.class);
		    Join<Staff, Subject> subject = root.join("subjects", JoinType.INNER);
		    List<Predicate> predicates = new ArrayList<>();
		    predicates.add(builder.equal(subject.get("id"), subjectId));
			if (isActive) {
				predicates.add(builder.equal(root.get("status"), 1));
			}
		    criteria.select(root)
		    		.where(predicates.toArray(new Predicate[predicates.size()]))
		    		.orderBy(builder.desc(root.get("userId")));
		    List<Staff> staffs = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
		    return !staffs.isEmpty() ? staffs : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public long countStaff() {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
			Root<Staff> root = criteria.from(Staff.class);
			criteria.select(builder.count(root));
			long count = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
			return count;
		} catch (Exception e) {
			logger.error(e);
			return 0;
		}
	}

}
