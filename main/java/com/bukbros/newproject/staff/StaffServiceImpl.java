package com.bukbros.newproject.staff;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bukbros.newproject.configuration.Role;
import com.bukbros.newproject.function.StringFunction;
import com.bukbros.newproject.staff.interfaces.StaffRepositoryInterface;
import com.bukbros.newproject.staff.interfaces.StaffServiceInterface;
import com.bukbros.newproject.user.User;
import com.bukbros.newproject.user.interfaces.UserServiceInterface;
@Service
public class StaffServiceImpl implements StaffServiceInterface{
	@Autowired
	private StaffRepositoryInterface repository;	
	
	@Autowired
	private UserServiceInterface userService;		
	
	@Override
	public boolean create(Staff staff) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Staff> read(boolean isActive) {
		return repository.read(isActive);
	}

	@Override
	public boolean update(long id, Staff object) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(Staff object) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Staff read(long id) {
		return repository.read(id);
	}

	@Override
	public boolean status(long id, int status) {
		return repository.status(id, status);
	}

	@Override
	public Staff read(String uuid) {
		return repository.read(uuid);
	}

	@Override
	public Staff getStaffByUsername(String username) {
		return repository.getStaffByUsername(username);
	}
	
	@Override
	public boolean create(HttpServletRequest request, Staff staff) {		
		staff.setCertificate(StringFunction.trimExLineFeed(staff.getCertificate()));
		staff.getUser().setRole(Role.ROLE_STAFF);
		User newUser = userService.create(request, staff.getUser());
		if (newUser != null) {
			staff.setUserId(newUser.getId());
		} else {
			return false;
		}
		boolean createStaff = repository.create(staff);
		if (!createStaff) {
			userService.delete(request, newUser);
		}
		return createStaff;
	}

	@Override
	@Transactional
	public boolean update(HttpServletRequest request, Staff originalStaff, Staff staff) {
		staff.setCertificate(StringFunction.trimExLineFeed(staff.getCertificate()));
		staff.setUserId(originalStaff.getUserId());
		boolean updateStaff = repository.update(staff);
		if (updateStaff) {
			boolean updateUser = userService.update(request, originalStaff.getUser(), staff.getUser());
			return updateUser;
		}
		return false;
	}

	@Override
	public List<Staff> getStaffsInUpdate(long id) {
		return repository.getStaffsInUpdate(id);
	}

	@Override
	public boolean isStaffValid(long id, int type, long facultyId) {
		if (id == 0) {
			return true;
		}
		Staff staff = repository.read(id);
		if (staff != null) {
			if (facultyId != 0) {
				if (staff.getFaculty().getId() != facultyId) {
					return false;
				}
			}
			if (type != 0) {
				if (staff.getUser().getStatus() != type) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	@Override
	public Staff read(long id, boolean isGetSubjects) {
		return repository.read(id, isGetSubjects);
	}

	@Override
	public List<Staff> getStaffByFaculty(long facultyId, boolean isActive) {
		return repository.getStaffByFaculty(facultyId, isActive);
	}

	@Override
	public List<Staff> getStaffBySubject(long subjectId, boolean isActive) {
		return repository.getStaffBySubject(subjectId, isActive);
	}

	@Override
	public long countStaff() {
		return repository.countStaff();
	}
}
