package com.bukbros.newproject.language;

import java.util.HashMap;

public class MessageLanguage {
	public static final String MESSAGE_CREATE_SUCCESS = "Thêm mới dữ liệu thành công";
	public static final String MESSAGE_CREATE_ERROR = "Thêm mới dữ liệu thất bại";
	
	public static final String MESSAGE_UPDATE_SUCCESS = "Cập nhật dữ liệu thành công";
	public static final String MESSAGE_UPDATE_ERROR = "Cập nhật dữ liệu thất bại";
	
	public static final String MESSAGE_ACTIVE_SUCCESS = "Kích hoạt dữ liệu thành công";
	public static final String MESSAGE_ACTIVE_ERROR = "Kích hoạt dữ liệu thất bại";
	
	public static final String MESSAGE_DEACTIVE_SUCCESS = "Khóa dữ liệu thành công";
	public static final String MESSAGE_DEACTIVE_ERROR = "Khóa dữ liệu thất bại";
	
	public static final String MESSAGE_DELETE_SUCCESS = "Xóa dữ liệu thành công";
	public static final String MESSAGE_DELETE_ERROR = "Xóa dữ liệu thất bại";
	
	public static final String MESSAGE_OPEN_SUCCESS = "Mở lớp thành công";
	public static final String MESSAGE_OPEN_ERROR = "Mở lớp thất bại";
	
	public static final String MESSAGE_ERROR_NOT_FOUND = "Không có dữ liệu được tìm thấy";
	public static final String MESSAGE_ERROR_FORBIDDEN = "Dữ liệu không được phép truy cập";
	public static final String MESSAGE_ERROR_DATA_EXISTS = "Dữ liệu đã tồn tại";
	public static final String MESSAGE_ERROR_CONSTRAIN = "Dữ liệu đang được sử dụng liên kết với dữ liệu khác.";
	public static final String MESSAGE_ERROR_UNAUTHORIZED = "Vui lòng đăng nhập để truy cập dữ liệu.";
	
	public static HashMap<String, String> messageLanguage() {
		HashMap<String, String> messages = new HashMap<>();
		return messages;
	}
}
