package com.bukbros.newproject.language;

import java.util.HashMap;

public class LabelLanguage {
	public static final String LABEL_BUTTON_CREATE = "Tạo mới";
	public static final String LABEL_BUTTON_READ = "Danh sách";
	public static final String LABEL_BUTTON_UPDATE = "Sửa";
	public static final String LABEL_BUTTON_DELETE = "Xóa hoàn toàn";
	public static final String LABEL_BUTTON_DELETE_SHORT = "Xóa";
	public static final String LABEL_BUTTON_ACTIVE = "Khôi phục";
	public static final String LABEL_BUTTON_DEACTIVE = "Tạm dừng";
	public static final String LABEL_STT = "STT";	
	public static final String LABEL_OPTION = "Quản trị";
	public static final String LABEL_UPDATE = "Cập nhật";
	public static final String LABEL_CREATE = "Thêm mới";
	public static final String LABEL_RESET = "Làm lại";
	
	public static final String LABEL_FOOTER = "Tác giả: Trần Thanh Tùng";
	
	public static HashMap<String, String> labels() {
		HashMap<String, String> labels = new HashMap<>();
		labels.put("LABEL_BUTTON_CREATE", LABEL_BUTTON_CREATE);
		labels.put("LABEL_BUTTON_READ", LABEL_BUTTON_READ);
		labels.put("LABEL_BUTTON_UPDATE", LABEL_BUTTON_UPDATE);
		labels.put("LABEL_BUTTON_DELETE", LABEL_BUTTON_DELETE);
		labels.put("LABEL_BUTTON_DELETE_SHORT", LABEL_BUTTON_DELETE_SHORT);
		labels.put("LABEL_BUTTON_ACTIVE", LABEL_BUTTON_ACTIVE);
		labels.put("LABEL_BUTTON_DEACTIVE", LABEL_BUTTON_DEACTIVE);
		labels.put("LABEL_STT", LABEL_STT);
		labels.put("LABEL_OPTION", LABEL_OPTION);
		labels.put("LABEL_UPDATE", LABEL_UPDATE);
		labels.put("LABEL_CREATE", LABEL_CREATE);
		labels.put("LABEL_RESET", LABEL_RESET);
		labels.put("LABEL_FOOTER", LABEL_FOOTER);
		return labels;
	}
}
