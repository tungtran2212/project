package com.bukbros.newproject.function;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateValidation {
	public static final String DATE_PATTERN = "dd/MM/yyyy"; 
	private static String[] seperators = {"-","/","."};
	
	/**
	 * DateValidation.isValid("29/12/1986","dd/MM/yyyy");
	 */
	public static boolean isValid(String time, String format) {
		if(time == null) return false;
		try {
			if(validTime(time)) {
				time = formatTime(time);
				if(validDateFormat(time, format)) {
					Date date = getDate(time, format);
					return validateData(getDay(date),getMonth(date),getYear(date));
				}
			}			
		} catch (Exception e) {
			return false;
		}
		return false;
	}
	
	public static String convertDateToString(Date date) {
		return getDay(date)+"-"+getMonth(date)+"-"+getYear(date);
	}
	
	public static String formatDate(String dateTime) {
		dateTime = formatTime(dateTime);
		Date date = getDate(dateTime, DATE_PATTERN);
		return getYear(date)+"-"+getMonth(date)+"-"+getDay(date);
	}	
	
	/*public static String formatTimestamp (String dateTime) {
		
	}*/
	
	public static String formatDateFromDatabase(String dateTime) {
		Date date = getDate(dateTime, "yyyy-MM-dd");
		return getDay(date)+"-"+getMonth(date)+"-"+getYear(date);
	}
	
	public static Date convertStringToDate(String dateTime) {
		dateTime = formatTime(dateTime);
		return getDate(dateTime, DATE_PATTERN);
	}
	
	private static boolean validateData(int day, int month, int year) {
		if(!validYear(year)) return false;
		int[] bigMonths = {1,3,5,7,8,10,12};		
		if((month < 13) && (month > 0)) {
			if(isItemInArray(bigMonths, month)) {
				if(day <= 31) {
					return true;
				} else {
					return false;
				}
			} else {
				if(month == 2) {
					if(isLeapYear(year)) {
						if(day <= 29) {
							return true;
						} else {
							return false;
						}
					} else {
						if(day <= 28) {
							return true;
						} else {
							return false;
						}
					}
				} else {
					if(day <= 30) {
						return true;
					} else {
						return false;
					}
				}
			}
			
		} 
		return false;
	}
	
	private static boolean isItemInArray(int[] items, int item) {
		for(int arrItem : items) {
			if(arrItem == item) {
				return true;
			}
		}
		return false;
	}
	
	private static boolean validYear(int year) {
		return ((year >=1000) && (year <=9999));
	}
	
	private static boolean isLeapYear(int year) {
		if (year % 4 == 0) {
            if (year % 100 == 0) {
                if (year % 400 == 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } 
        return false;        
	}
	
	//reformat the string
	private static String formatTime(String time) {
		for(String seperator : seperators) {
			if(time.indexOf(seperator) > 0) {
				time = time.replace(seperator, "/");
			}
		}
		return time;
		
	}
	
	//Check if the time is a valid string with defined seperators
	private static boolean validTime(String time) {
		for(String seperator : seperators) {
			if(time.indexOf(seperator) > 0) {
				return true;
			}
		}
		return false;
	}
	
	private static boolean validDateFormat(String datetime, String format) {		
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		sdf.setLenient(false);
		try {
			@SuppressWarnings("unused")
			Date date = sdf.parse(datetime);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}
	
	private static Date getDate(String datetime, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		sdf.setLenient(false);
		try {
			Date date = sdf.parse(datetime);
			return date;
		} catch (ParseException e) {
			return null;
		}
	}
	
	private static int getDay(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
		return Integer.parseInt(dateFormat.format(date));
	}

	
	private static int getMonth(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM");
		return Integer.parseInt(dateFormat.format(date));
	}
	
	private static int getYear(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
		return Integer.parseInt(dateFormat.format(date));
	}
}

