package com.bukbros.newproject.function;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.TimeZone;

import com.github.slugify.Slugify;

public class StringFunction {
	// Chuyển về tiếng việt không dấu	
	public static String convertTextToSlug(String text) {		
		Slugify slg = new Slugify();
		return slg.slugify(text.trim().replaceAll("đ", "d").replaceAll("Đ", "D").replaceAll("_", ""));		
	}
	// xử lý khi đã tồn tại slug
	public static String generateSlug(String text) {
		return convertTextToSlug(text)+"-"+Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTimeInMillis();
	}

	// Loại bỏ khoảng trắng thừa
	public static String trimSpace(String str) {
		if (str == null) {
			return "";
		}
		return str.replaceAll("\\s{2,}", " ").trim();
	}
	
	// Convert uuid
	public static String convertTextToUuid(String text) {
		text = text.toLowerCase().trim();
		return text.substring(0, 8)+"-"
				+text.substring(8,12)+"-"
				+text.substring(12, 16)+"-"
				+text.substring(16,20)+"-"
				+text.substring(20, text.length());
	}

	// Loại bỏ khoảng trắng ngoại trừ xuống dòng
	public static String trimExLineFeed(String str) {
		if (str == null) {
			return "";
		}
		str = str.replaceAll("^\\s+|\\s+$|\\s*(\n)\\s*|(\\s)\\s*", "$1$2");
		str = str.replace("\t", " ");
		return str;
	}

	// Thay \n trong Database bằng <br /> để hiển thị
	public static String convertLineFeed(String str) {
		str = str.replaceAll("<p>|<\\/p>", "");
		str = str.replaceAll("\n", "<br />");
		return str;
	}

	public static String md5(String str) {
		String result = "";
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("MD5");
			digest.update(str.getBytes());
			BigInteger bigInteger = new BigInteger(1, digest.digest());
			result = bigInteger.toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.getMessage();
		}
		return result;
	}

	public static String randomString(int length) {
		String randomChar = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		String result = "";
		for (int i = 0; i < length; i++) {
			int index = (int) Math.round(Math.random() * (randomChar.length() - 1));
			result += randomChar.charAt(index);
		}
		return result;
	}
}

