package com.bukbros.newproject.interfaces;

import java.util.List;


public interface ServiceInterface<Obj extends Object> {
boolean create(Obj object);
	
	List<Obj> read(boolean isActive);
	
	boolean update(long id, Obj object);
	
	boolean delete(Obj object);
	
	Obj read(long id);
	
	boolean status(long id, int status);
	
	Obj read(String uuid);
}
