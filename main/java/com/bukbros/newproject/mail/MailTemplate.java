package com.bukbros.newproject.mail;

import org.springframework.mail.SimpleMailMessage;

public class MailTemplate {
	public static final SimpleMailMessage forgotPass(String firstName, String lastName, String newPass) {
		SimpleMailMessage message = new SimpleMailMessage();
	    message.setSubject(
	      "Yêu cầu lấy lại mật khẩu.");
	    message.setText(
	          	      "Xin chào " + lastName + " " + firstName + ",\n \n" +
	          	      		"Chúng tôi nhận được yêu cầu lấy lại mật khẩu của bạn từ hệ thống của trường đại học Thăng Long.\n " +
	          	      		"Mật khẩu mới của bạn là: " + newPass + "\n \n" +
	          	      		"Nếu cần hỗ trợ thêm, bạn gửi email đến hòm thư info@thanglong.edu.vn nhé." + "\n \n" +
	          	      		"Trân trọng," + "\n" +
	    					"TRƯỜNG ĐẠI HỌC THĂNG LONG.");
	    return message;
	}
}
