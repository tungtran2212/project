package com.bukbros.newproject.majorSubject;

import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;

import com.bukbros.newproject.course.Course;
import com.bukbros.newproject.major.Major;
import com.bukbros.newproject.subject.Subject;
import com.bukbros.newproject.subject.SubjectLanguage;

@Entity
@Table(name = MajorSubjectDatabase.TABLE)
public class MajorSubject {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = MajorSubjectDatabase.COLUMN_ID)
	private long id;
	
	@Column(name = MajorSubjectDatabase.COLUMN_UUID, updatable = false)
	private UUID uuid;
	
	@ManyToOne
	@JoinColumn(name = MajorSubjectDatabase.COLUMN_COURSE_ID)
	private Course course;
	
	@ManyToOne
	@JoinColumn(name = MajorSubjectDatabase.COLUMN_MAJOR_ID)
	private Major major;
	
	@ManyToOne
	@JoinColumn(name = MajorSubjectDatabase.COLUMN_SUBJECT_ID)
	private Subject subject;
	
	@NotEmpty(message = SubjectLanguage.VALIDATION_NOT_EMPTY)
	@Transient	
	private List<Long> subjectIds;
	
	@Column(name = MajorSubjectDatabase.COLUMN_STATUS)
	private byte status;
	
	public MajorSubject() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Major getMajor() {
		return major;
	}

	public void setMajor(Major major) {
		this.major = major;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}
	
	public List<Long> getSubjectIds() {
		return subjectIds;
	}

	public void setSubjectIds(List<Long> subjectIds) {
		this.subjectIds = subjectIds;
	}

	@PrePersist
	public void prePersist() {
		uuid = UUID.randomUUID();
	}	
}
