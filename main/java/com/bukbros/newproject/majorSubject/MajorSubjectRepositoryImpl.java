package com.bukbros.newproject.majorSubject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bukbros.newproject.majorSubject.interfaces.MajorSubjectRepositoryInterface;

@Repository
@Transactional
public class MajorSubjectRepositoryImpl implements MajorSubjectRepositoryInterface{
	@Autowired
	private SessionFactory sessionFactory;
	
	private Logger logger = LogManager.getLogger(MajorSubjectRepositoryImpl.class);

	@Override
	public boolean create(MajorSubject majorSubject) {
		try {
			sessionFactory.getCurrentSession().persist(majorSubject);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public List<MajorSubject> read(boolean isActive) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<MajorSubject> criteria = builder.createQuery(MajorSubject.class);
			Root<MajorSubject> root = criteria.from(MajorSubject.class);
			criteria.select(root);
			if (isActive) {
				criteria.where(builder.equal(root.get("status"), 1),
				               builder.equal(root.get("course").get("status"), 1),
				               builder.equal(root.get("major").get("status"), 1),
				               builder.equal(root.get("subject").get("status"), 1));
			}
			criteria.orderBy(builder.desc(root.get("id")));
			List<MajorSubject> majorSubjects = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !majorSubjects.isEmpty() ? majorSubjects : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean update(MajorSubject majorSubject) {
		try {
			sessionFactory.getCurrentSession().update(majorSubject);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public boolean delete(MajorSubject majorSubject) {
		try {
			sessionFactory.getCurrentSession().delete(majorSubject);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public MajorSubject read(long id) {
		try {
			MajorSubject majorSubject = sessionFactory.getCurrentSession().get(MajorSubject.class, id);
			return majorSubject;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}		
	}

	@Override
	public boolean status(long id, int status) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaUpdate<MajorSubject> criteria = builder.createCriteriaUpdate(MajorSubject.class);
		    Root<MajorSubject> root = criteria.from(MajorSubject.class);
		    criteria.set(root.get("status"), status)
		    		.where(builder.equal(root.get("id"), id));
		    sessionFactory.getCurrentSession().createQuery(criteria).executeUpdate();
		    return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public MajorSubject read(String uuid) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<MajorSubject> criteria = builder.createQuery(MajorSubject.class);
		    Root<MajorSubject> root = criteria.from(MajorSubject.class);
		    criteria.select(root)
		     		.where(builder.equal(root.get("uuid"), UUID.fromString(uuid)));
		    MajorSubject majorSubject = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();//tra ve null khi ko tim dc
		    return majorSubject; 
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean isMajorSubjectExisted(long courseId, long majorId, long subjectId) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<MajorSubject> criteria = builder.createQuery(MajorSubject.class);
		    Root<MajorSubject> root = criteria.from(MajorSubject.class);
		    List<Predicate> predicates = new ArrayList<>();
		    predicates.add(builder.equal(root.get("course"), courseId));
		    predicates.add(builder.equal(root.get("major"), majorId));
		    predicates.add(builder.equal(root.get("subject"), subjectId));
			criteria.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
			MajorSubject majorSubject = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
		    return majorSubject != null ? true : false;
		} catch (Exception e) {
			logger.error(e);
			return true;
		}
	}

	@Override
	public List<MajorSubject> getMajorSubjectByMajorId(long majorId) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<MajorSubject> criteria = builder.createQuery(MajorSubject.class);
			Root<MajorSubject> root = criteria.from(MajorSubject.class);
			criteria.select(root).where(builder.equal(root.get("major"), majorId));
			List<MajorSubject> majorSubjects = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !majorSubjects.isEmpty() ? majorSubjects : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public List<MajorSubject> getMajorSubjectsInUpdate(long id) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<MajorSubject> criteria = builder.createQuery(MajorSubject.class);
			Root<MajorSubject> root = criteria.from(MajorSubject.class);
			criteria.select(root)
					.where(builder.or(builder.equal(root.get("status"), 1),
									  builder.equal(root.get("id"), id)))
					.orderBy(builder.asc(root.get("id")));
			List<MajorSubject> majorSubjects = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !majorSubjects.isEmpty() ? majorSubjects : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean isMajorSubjectUsed(long id, String reference, Class<? extends Object> referenceClass) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
			Root<?> root = criteria.from(referenceClass);
			List<Predicate> predicates = new ArrayList<>();
			predicates.add(builder.equal(root.get(reference), id));
			criteria.select(builder.count(root))
					.where(predicates.toArray(new Predicate[predicates.size()]));
			int count = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult().intValue();
			return count > 0 ? true : false;
		} catch (Exception e) {
			logger.error(e);
			return true;
		}
	}
}
