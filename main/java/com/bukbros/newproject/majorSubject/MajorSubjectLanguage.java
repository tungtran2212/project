package com.bukbros.newproject.majorSubject;

import java.util.HashMap;

public class MajorSubjectLanguage {
	public static final String LABEL_COURSE_NAME = "Khóa";
	public static final String LABEL_MAJOR_NAME = "Tên ngành học";
	public static final String LABEL_SUBJECT_NAME = "Tên môn";
	
	public static final String IS_SUBJECT_EXISTED = "Môn học đã tồn tại. Vui lòng nhập lại. ";
	
	public static final String LABEL_CREATE = "Thêm mới môn cho ngành";
	public static final String LABEL_UPDATE = "Cập nhật danh sách môn theo ngành";
	public static final String LABEL_LIST = "Danh sách môn của ngành";
	
	public static HashMap<String, String> label() {
		HashMap<String, String> labels = new HashMap<>();
		labels.put("LABEL_COURSE_NAME", LABEL_COURSE_NAME);
		labels.put("LABEL_MAJOR_NAME", LABEL_MAJOR_NAME);
		labels.put("LABEL_SUBJECT_NAME", LABEL_SUBJECT_NAME);
		
		labels.put("LABEL_CREATE", LABEL_CREATE);
		labels.put("LABEL_UPDATE", LABEL_UPDATE);
		labels.put("LABEL_LIST", LABEL_LIST);
		return labels;
	}
}
