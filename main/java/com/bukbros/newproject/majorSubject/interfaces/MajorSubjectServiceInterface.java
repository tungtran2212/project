package com.bukbros.newproject.majorSubject.interfaces;

import java.util.List;

import com.bukbros.newproject.interfaces.ServiceInterface;
import com.bukbros.newproject.majorSubject.MajorSubject;

public interface MajorSubjectServiceInterface extends ServiceInterface<MajorSubject>{
	boolean isMajorSubjectExisted (long courseId, long majorId, long subjectId);
	
	List<MajorSubject> getMajorSubjectByMajorId(long id);
	
	List<MajorSubject> getMajorSubjectsInUpdate(long id);
	
	boolean isMajorSubjectUsed(long id, String reference, Class<? extends Object> referenceClass);
}
