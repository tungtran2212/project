package com.bukbros.newproject.majorSubject;

import java.security.Principal;
import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.newproject.configuration.Role;
import com.bukbros.newproject.configuration.Url;
import com.bukbros.newproject.course.CourseLanguage;
import com.bukbros.newproject.course.interfaces.CourseServiceInterface;
import com.bukbros.newproject.detail.Detail;
import com.bukbros.newproject.language.LabelLanguage;
import com.bukbros.newproject.language.MessageLanguage;
import com.bukbros.newproject.major.Major;
import com.bukbros.newproject.major.MajorConfig;
import com.bukbros.newproject.major.interfaces.MajorServiceInterface;
import com.bukbros.newproject.majorSubject.interfaces.MajorSubjectServiceInterface;
import com.bukbros.newproject.subject.SubjectConfig;
import com.bukbros.newproject.subject.SubjectLanguage;
import com.bukbros.newproject.subject.interfaces.SubjectServiceInterface;
import com.bukbros.newproject.user.User;
import com.bukbros.newproject.user.interfaces.UserServiceInterface;

@Controller
@RequestMapping(value = MajorConfig.MODULE_ADMIN_URL + "/{uuidMajor}" + SubjectConfig.MODULE_URL)
public class MajorSubjectController {
	@Autowired
	private MajorSubjectServiceInterface service;
	
	@Autowired
	private CourseServiceInterface courseService;
	
	@Autowired
	private MajorServiceInterface majorService;
	
	@Autowired
	private SubjectServiceInterface subjectService;
	
	@Autowired
	private UserServiceInterface userService;
	
	@ModelAttribute("module")
	public HashMap<String, String> module() {
		return MajorSubjectConfig.component();
	}
	
	@ModelAttribute("moduleLang")
	public HashMap<String, String> moduleLang() {
		return MajorSubjectLanguage.label();
	}
	
	@ModelAttribute("labelLang")
	public HashMap<String, String> labelLanguage() {
		return LabelLanguage.labels();
	}
	
	@ModelAttribute("majorConfig")
	public HashMap<String, String> majorConfig() {
		return MajorConfig.component();
	}
	
	@ModelAttribute("subjectConfig")
	public HashMap<String, String> subjectConfig() {
		return SubjectConfig.component();
	}
	
	@ModelAttribute("majorMenu")
	public boolean majorMenu() {
		return true;
	}
	
	@GetMapping(Url.URL_CREATE)
	public String getCreate (@PathVariable("uuidMajor") String uuidMajor,
	                         Model model,
	                         Principal principal,
							 RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Major originalMajor = majorService.read(uuidMajor);
		if (originalMajor == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_MAJOR;
		}
		model.addAttribute("createMode", true);
		model.addAttribute("courses", courseService.read(true));
		model.addAttribute("major", originalMajor);
		model.addAttribute("subjects", subjectService.read(true));
		model.addAttribute("majorSubjects", service.getMajorSubjectByMajorId(originalMajor.getId()));
		return MajorSubjectConfig.MODULE_VIEW_ADMIN;
	}
	
	@PostMapping(Url.URL_CREATE)
	public String postCreate (@PathVariable("uuidMajor") String uuidMajor,
	                          @ModelAttribute("validMajorSubject") @Valid MajorSubject majorSubject,
							  BindingResult results,
							  Model model,
							  Principal principal,
							  RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Major originalMajor = majorService.read(uuidMajor);
		if (originalMajor == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_MAJOR;
		}
		model.addAttribute("createMode", true);
		model.addAttribute("major", originalMajor);
		model.addAttribute("courses", courseService.read(true));
		model.addAttribute("subjects", subjectService.read(true));
		model.addAttribute("majorSubject", majorSubject);
		model.addAttribute("majorSubjects", service.getMajorSubjectByMajorId(originalMajor.getId()));
		if (results.hasErrors()) {
			return MajorSubjectConfig.MODULE_VIEW_ADMIN;
		} else {
			boolean validCourse = courseService.isCourseValid(majorSubject.getCourse().getId(), 1);
			if (!validCourse) {
				results.addError(new FieldError("validMajorSubject", "course.id", CourseLanguage.VALIDATION_COURSE));
			}			
			for (long subjectId : majorSubject.getSubjectIds()) {
				boolean validSubject = subjectService.isSubjectValid(subjectId, 1);
				if (!validSubject) {
					results.addError(new FieldError("validMajorSubject", "subjectIds", SubjectLanguage.VALIDATION_SUBJECT));
					break;
				} else {
					boolean isMajorSubjectExisted = service.isMajorSubjectExisted(majorSubject.getCourse().getId(), originalMajor.getId(), subjectId);
					if (isMajorSubjectExisted) {
						results.addError(new FieldError("validMajorSubject", "subjectIds", SubjectLanguage.VALIDATION_SUBJECT));
						break;
					}
				}				
			}			
			if (results.hasFieldErrors("course.id")	|| results.hasFieldErrors("subjectIds")) {
					return MajorSubjectConfig.MODULE_VIEW_ADMIN;
				}
		}
		majorSubject.setMajor(originalMajor);
		boolean createItem = service.create(majorSubject);
		if (createItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_CREATE_SUCCESS);
		} else {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_CREATE_ERROR);
			return MajorSubjectConfig.MODULE_VIEW_ADMIN;
		}
		model.asMap().clear();
		return "redirect:" + MajorConfig.MODULE_ADMIN_URL + "/" + originalMajor.getUuid() + SubjectConfig.MODULE_URL + Url.URL_CREATE;
	}
	
	@PostMapping("/{uuid}" + Url.URL_ACTIVE)
	public String postActive(@PathVariable("uuidMajor") String uuidMajor,
	                         @PathVariable("uuid") String uuid,
	                         Model model,
	                         Principal principal,
	                         RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		MajorSubject originalMajorSubject = service.read(uuid);
		Major originalMajor = majorService.read(uuidMajor);
		if ((originalMajorSubject == null) || (originalMajor == null) || (originalMajorSubject.getMajor().getId() != originalMajor.getId())) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_MAJOR;
		}
		boolean active = service.status(originalMajorSubject.getId(), 1);
		if (active) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_ACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ACTIVE_ERROR);
		}
		return "redirect:" + MajorConfig.MODULE_ADMIN_URL + "/" + originalMajor.getUuid() + SubjectConfig.MODULE_URL + Url.URL_CREATE;
	}
	
	@PostMapping("/{uuid}" + Url.URL_DEACTIVE)
	public String postDeactive(@PathVariable("uuidMajor") String uuidMajor,
	                           @PathVariable("uuid") String uuid,
	                           Model model,
	                           Principal principal,
	                           RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		MajorSubject originalMajorSubject = service.read(uuid);
		Major originalMajor = majorService.read(uuidMajor);
		if ((originalMajorSubject == null) || (originalMajor == null) || (originalMajorSubject.getMajor().getId() != originalMajor.getId())) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_MAJOR;
		}
		boolean deactive = service.status(originalMajorSubject.getId(), 0);
		if (deactive) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DEACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DEACTIVE_ERROR);
		}
		return "redirect:" + MajorConfig.MODULE_ADMIN_URL + "/" + originalMajor.getUuid() + SubjectConfig.MODULE_URL + Url.URL_CREATE;
	}
	
	@PostMapping("/{uuid}" + Url.URL_DELETE )
	public String postDelete(@PathVariable("uuidMajor") String uuidMajor,
	                         @PathVariable("uuid") String uuid,
	                          Model model,
	                          Principal principal,
	                          RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		MajorSubject originalMajorSubject = service.read(uuid);
		Major originalMajor = majorService.read(uuidMajor);
		if ((originalMajorSubject == null) || (originalMajor == null) || (originalMajorSubject.getMajor().getId() != originalMajor.getId())) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_MAJOR;
		}
		boolean isMajorSubjectUsedDetail = service.isMajorSubjectUsed(originalMajorSubject.getId(), "majorSubject", Detail.class);
		if (isMajorSubjectUsedDetail) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_CONSTRAIN);
			return "redirect:" + MajorConfig.MODULE_ADMIN_URL + "/" + originalMajor.getUuid() + SubjectConfig.MODULE_URL + Url.URL_CREATE;
		}
		boolean delete = service.delete(originalMajorSubject);
		if (delete) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DELETE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DELETE_ERROR);
		}
		return "redirect:" + MajorConfig.MODULE_ADMIN_URL + "/" + originalMajor.getUuid() + SubjectConfig.MODULE_URL + Url.URL_CREATE;
	}
}
