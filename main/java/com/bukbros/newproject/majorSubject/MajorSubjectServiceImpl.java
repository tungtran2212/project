package com.bukbros.newproject.majorSubject;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bukbros.newproject.majorSubject.interfaces.MajorSubjectRepositoryInterface;
import com.bukbros.newproject.majorSubject.interfaces.MajorSubjectServiceInterface;
import com.bukbros.newproject.subject.Subject;

@Service
public class MajorSubjectServiceImpl implements MajorSubjectServiceInterface{
	@Autowired
	private MajorSubjectRepositoryInterface repository;

	@Override
	@Transactional
	public boolean create(MajorSubject majorSubject) {
		Subject subject = new Subject();
		majorSubject.setSubject(subject);
		for (long subjectId : majorSubject.getSubjectIds()) {
			majorSubject.getSubject().setId(subjectId);
			boolean createItem = repository.create(majorSubject);
			if (!createItem) {
				return false;
			}
		}
		return true;
	}

	@Override
	public List<MajorSubject> read(boolean isActive) {
		return repository.read(isActive);
	}

	@Override
	public boolean update(long id, MajorSubject majorSubject) {
		majorSubject.setId(id);
		return repository.update(majorSubject);
	}

	@Override
	public boolean delete(MajorSubject majorSubject) {
		return repository.delete(majorSubject);
	}

	@Override
	public MajorSubject read(long id) {
		return repository.read(id);
	}

	@Override
	public boolean status(long id, int status) {
		return repository.status(id, status);
	}

	@Override
	public MajorSubject read(String uuid) {
		return repository.read(uuid);
	}

	@Override
	public boolean isMajorSubjectExisted(long courseId, long majorId, long subjectId) {
		return repository.isMajorSubjectExisted(courseId, majorId, subjectId);
	}

	@Override
	public List<MajorSubject> getMajorSubjectByMajorId(long majorId) {
		return repository.getMajorSubjectByMajorId(majorId);
	}

	@Override
	public List<MajorSubject> getMajorSubjectsInUpdate(long id) {
		return repository.getMajorSubjectsInUpdate(id);
	}

	@Override
	public boolean isMajorSubjectUsed(long id, String reference, Class<? extends Object> referenceClass) {
		return repository.isMajorSubjectUsed(id, reference, referenceClass);
	}
}
