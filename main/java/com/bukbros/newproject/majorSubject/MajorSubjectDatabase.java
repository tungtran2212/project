package com.bukbros.newproject.majorSubject;

public class MajorSubjectDatabase {
	public static final String TABLE = "cr_major_subject";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_UUID = "uuid";
	public static final String COLUMN_COURSE_ID = "course_id";
	public static final String COLUMN_MAJOR_ID = "major_id";
	public static final String COLUMN_SUBJECT_ID = "subject_id";
	public static final String COLUMN_STATUS = "status";
}
