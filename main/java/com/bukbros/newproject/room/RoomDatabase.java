package com.bukbros.newproject.room;

public class RoomDatabase {
	public static final String TABLE = "cr_room";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_UUID = "uuid";
	public static final String COLUMN_SLUG = "slug";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_STATUS = "status";
}
