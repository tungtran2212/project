package com.bukbros.newproject.room;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bukbros.newproject.function.StringFunction;
import com.bukbros.newproject.room.interfaces.RoomRepositoryInterface;
import com.bukbros.newproject.room.interfaces.RoomServiceInterface;

@Service
public class RoomServiceImpl implements RoomServiceInterface{
	@Autowired
	private RoomRepositoryInterface repository;
	
	@Override
	public boolean create(Room room) {
		String slug = StringFunction.convertTextToSlug(room.getName());
		boolean isSlugExisted = repository.isSlugExisted(slug);
		if (isSlugExisted) {
			room.setSlug(StringFunction.generateSlug(room.getName()));
		} else {
			room.setSlug(slug);
		}
		return repository.create(room);
	}

	@Override
	public List<Room> read(boolean isActive) {
		return repository.read(isActive);
	}

	@Override
	public boolean update(long id, Room room) {
		room.setId(id);
		return repository.update(room);
	}

	@Override
	public boolean delete(Room room) {
		return repository.delete(room);
	}

	@Override
	public Room read(long id) {
		return repository.read(id);
	}

	@Override
	public boolean status(long id, int status) {
		return repository.status(id, status);
	}

	@Override
	public Room read(String uuid) {
		return repository.read(uuid);
	}

	@Override
	public boolean isSlugExisted(String slug) {
		return repository.isSlugExisted(slug);
	}

	@Override
	public List<Room> getRoomsInUpdate(long id) {
		return repository.getRoomsInUpdate(id);
	}

	@Override
	public boolean isNameExisted(String name, long id) {
		return repository.isNameExisted(name, id);
	}
	
	@Override
	public boolean isRoomValid(long id, int type) {
		if (id == 0) {
			return true;
		}
		Room room = repository.read(id);
		if (room != null) {
			if (type != 0) {
				if (room.getStatus() == type) {
					return true;
				}
				return false;
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean isRoomUsed(long id, String reference, Class<? extends Object> referenceClass) {
		return repository.isRoomUsed(id, reference, referenceClass);
	}

	@Override
	public long countRoom() {
		return repository.countRoom();
	}

}
