package com.bukbros.newproject.room;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import com.bukbros.newproject.function.StringFunction;

@Entity
@Table(name = RoomDatabase.TABLE)
public class Room {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = RoomDatabase.COLUMN_ID)
	private long id;

	@Column(name = RoomDatabase.COLUMN_UUID, updatable = false)
	private UUID uuid;
	
	@Column(name = RoomDatabase.COLUMN_SLUG, updatable = false)
	private String slug;
	
	@Pattern(regexp = "^[^!@~`#$?%^&*():;\\[\\]{}\\\\/|<>\"']{1,100}$", message = RoomLanguage.VALIDATION_NAME)
	@Column(name = RoomDatabase.COLUMN_NAME)
	private String name;
	
	@Column(name = RoomDatabase.COLUMN_STATUS)
	private byte status;
	
	public Room() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = StringFunction.trimSpace(name);
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}
	
	@PrePersist
	public void prePersist() {
		uuid = UUID.randomUUID();
	}
}
