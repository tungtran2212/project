package com.bukbros.newproject.room;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bukbros.newproject.faculty.FacultyRepositoryImpl;
import com.bukbros.newproject.room.interfaces.RoomRepositoryInterface;
@Repository
@Transactional
public class RoomRepositoryImpl implements RoomRepositoryInterface{
	@Autowired
	private SessionFactory sessionFactory;
	
	private Logger logger = LogManager.getLogger(FacultyRepositoryImpl.class);
	
	@Override
	public boolean create(Room room) {
		try {
			sessionFactory.getCurrentSession().persist(room);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public List<Room> read(boolean isActive) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Room> criteria = builder.createQuery(Room.class);
			Root<Room> root = criteria.from(Room.class);
			criteria.select(root);
			if (isActive) {
				criteria.where(builder.equal(root.get("status"), 1));
			}
			criteria.orderBy(builder.desc(root.get("id")));
			List<Room> faculties = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !faculties.isEmpty() ? faculties : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean update(Room room) {
		try {
			sessionFactory.getCurrentSession().update(room);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public boolean delete(Room room) {
		try {
			sessionFactory.getCurrentSession().delete(room);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public Room read(long id) {
		try {
			Room faculties = sessionFactory.getCurrentSession().get(Room.class, id);
			return faculties;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}		
	}

	@Override
	public boolean status(long id, int status) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaUpdate<Room> criteria = builder.createCriteriaUpdate(Room.class);
		    Root<Room> root = criteria.from(Room.class);
		    criteria.set(root.get("status"), status)
		    		.where(builder.equal(root.get("id"), id));
		    sessionFactory.getCurrentSession().createQuery(criteria).executeUpdate();
		    return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public Room read(String uuid) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<Room> criteria = builder.createQuery(Room.class);
		    Root<Room> root = criteria.from(Room.class);
		    criteria.select(root)
		     		.where(builder.equal(root.get("uuid"), UUID.fromString(uuid)));
		    Room faculties = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();//tra ve null khi ko tim dc
		    return faculties; 
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean isSlugExisted(String slug) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<String> criteria = builder.createQuery(String.class);
		    Root<Room> root = criteria.from(Room.class);
		    criteria.select(root.get("slug")).where(builder.equal(root.get("slug"), slug));
		    String dbSlug = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
		    return slug.equalsIgnoreCase(dbSlug) ? true : false;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public List<Room> getRoomsInUpdate(long id) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Room> criteria = builder.createQuery(Room.class);
			Root<Room> root = criteria.from(Room.class);
			criteria.select(root)
					.where(builder.or(builder.equal(root.get("status"), 1),
									  builder.equal(root.get("id"), id)))
					.orderBy(builder.asc(root.get("id")));
			List<Room> rooms = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !rooms.isEmpty() ? rooms : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean isNameExisted(String name, long id) {
		try {
			String dbName;
			String sql = "SELECT " + RoomDatabase.COLUMN_NAME
								   + " FROM " + RoomDatabase.TABLE
								   + " WHERE " + RoomDatabase.COLUMN_NAME + " REGEXP :" + RoomDatabase.COLUMN_NAME;
			String nameRegex = "^" + name + "$";
			if (id == 0) {
				dbName = sessionFactory.getCurrentSession().createSQLQuery(sql)
						   							.setParameter(RoomDatabase.COLUMN_NAME, nameRegex)
						   							.uniqueResult().toString();
			} else {
				sql += " AND " + RoomDatabase.COLUMN_ID + " != :" + RoomDatabase.COLUMN_ID;
        		dbName = sessionFactory.getCurrentSession().createSQLQuery(sql)
        											.setParameter(RoomDatabase.COLUMN_NAME, nameRegex)
        											.setParameter(RoomDatabase.COLUMN_ID, id)
        											.uniqueResult().toString();
			}
			return name.equalsIgnoreCase(dbName) ? true : false;
		} catch(Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public boolean isRoomUsed(long id, String reference, Class<? extends Object> referenceClass) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
			Root<?> root = criteria.from(referenceClass);
			List<Predicate> predicates = new ArrayList<>();
			predicates.add(builder.equal(root.get(reference), id));
			criteria.select(builder.count(root))
					.where(predicates.toArray(new Predicate[predicates.size()]));
			int count = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult().intValue();
			return count > 0 ? true : false;
		} catch (Exception e) {
			logger.error(e);
			return true;
		}
	}

	@Override
	public long countRoom() {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
			Root<Room> root = criteria.from(Room.class);
			criteria.select(builder.count(root));
			long count = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
			return count;
		} catch (Exception e) {
			logger.error(e);
			return 0;
		}
	}

}
