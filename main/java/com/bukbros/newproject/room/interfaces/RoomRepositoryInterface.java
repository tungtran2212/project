package com.bukbros.newproject.room.interfaces;

import java.util.List;

import com.bukbros.newproject.interfaces.RepositoryInterface;
import com.bukbros.newproject.room.Room;

public interface RoomRepositoryInterface extends RepositoryInterface<Room>{
	boolean isSlugExisted(String slug);
	
	boolean isNameExisted(String name, long id);
	
	List<Room> getRoomsInUpdate(long id);
	
	boolean isRoomUsed(long id, String reference, Class<? extends Object> referenceClass);
	
	long countRoom();
}
