package com.bukbros.newproject.room.interfaces;

import java.util.List;

import com.bukbros.newproject.interfaces.ServiceInterface;
import com.bukbros.newproject.room.Room;

public interface RoomServiceInterface extends ServiceInterface<Room>{
	boolean isSlugExisted(String slug);
	
	boolean isNameExisted(String name, long id);
	
	List<Room> getRoomsInUpdate(long id);
	
	boolean isRoomValid(long id, int type);
	
	boolean isRoomUsed(long id, String reference, Class<? extends Object> referenceClass);
	
	long countRoom();
}
