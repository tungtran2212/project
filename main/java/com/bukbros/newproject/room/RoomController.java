package com.bukbros.newproject.room;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.newproject.configuration.Role;
import com.bukbros.newproject.configuration.Url;
import com.bukbros.newproject.detail.Detail;
import com.bukbros.newproject.language.LabelLanguage;
import com.bukbros.newproject.language.MessageLanguage;
import com.bukbros.newproject.room.interfaces.RoomServiceInterface;
import com.bukbros.newproject.user.User;
import com.bukbros.newproject.user.interfaces.UserServiceInterface;

@Controller
@RequestMapping(RoomConfig.MODULE_ADMIN_URL)
public class RoomController {
	@Autowired
	private RoomServiceInterface service;

	@Autowired
	private UserServiceInterface userService;

	@ModelAttribute("module")
	public HashMap<String, String> module() {
		return RoomConfig.component();
	}

	@ModelAttribute("moduleLang")
	public HashMap<String, String> moduleLang() {
		return RoomLanguage.label();
	}

	@ModelAttribute("labelLang")
	public HashMap<String, String> labelLanguage() {
		return LabelLanguage.labels();
	}

	@ModelAttribute("roomMenu")
	public boolean roomMenu() {
		return true;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String getRoom(Principal principal, Model model, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("listMode", true);
		List<Room> rooms = service.read(false);
		if (rooms == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
		} else {
			model.addAttribute("rooms", rooms);
		}
		return RoomConfig.MODULE_VIEW_ADMIN;
	}

	@GetMapping(Url.URL_CREATE)
	public String getCreate(Model model, Principal principal, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("createMode", true);
		return RoomConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping(Url.URL_CREATE)
	public String postCreate(@ModelAttribute("validRoom") @Valid Room room,
	                         BindingResult results,
	                         Model model,
	                         Principal principal,
	                         RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("createMode", true);
		model.addAttribute("room", room);
		if (results.hasErrors()) {
			return RoomConfig.MODULE_VIEW_ADMIN;
		} else {
			boolean isNameExisted = service.isNameExisted(room.getName(), 0);
			if (isNameExisted) {
				results.addError(new FieldError("validRoom", "name", RoomLanguage.IS_NAME_EXISTED));
			}
			if (results.hasFieldErrors("name")) {
				return RoomConfig.MODULE_VIEW_ADMIN;
			}
		}
		boolean createItem = service.create(room);
		if (createItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_CREATE_SUCCESS);
		} else {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_CREATE_ERROR);
			return RoomConfig.MODULE_VIEW_ADMIN;
		}
		model.asMap().clear();
		return "redirect:" + Url.URL_ROOM;
	}

	@GetMapping("/{uuid}" + Url.URL_UPDATE)
	public String getUpdate(@PathVariable("uuid") String uuid,
	                        Model model,
	                        Principal principal,
	                        RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Room originalRoom = service.read(uuid);
		if (originalRoom == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_ROOM;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("room", originalRoom);
		return RoomConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping("/{uuid}" + Url.URL_UPDATE)
	public String postUpdate(@PathVariable("uuid") String uuid,
	                         @ModelAttribute("validRoom") @Valid Room room,
	                         BindingResult results,
	                         Principal principal,
	                         Model model,
	                         RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Room originalRoom = service.read(uuid);
		if (originalRoom == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_ROOM;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("room", room);
		if (results.hasErrors()) {
			return RoomConfig.MODULE_VIEW_ADMIN;
		} else {
			boolean isNameExisted = service.isNameExisted(room.getName(), originalRoom.getId());
			if (isNameExisted) {
				results.addError(new FieldError("validRoom", "name", RoomLanguage.IS_NAME_EXISTED));
			}
			if (results.hasFieldErrors("name")) {
				return RoomConfig.MODULE_VIEW_ADMIN;
			}
		}
		if (service.update(originalRoom.getId(), room)) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_UPDATE_SUCCESS);
		} else {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_UPDATE_ERROR);
			return RoomConfig.MODULE_VIEW_ADMIN;
		}
		model.asMap().clear();
		return "redirect:" + Url.URL_ROOM;
	}

	@PostMapping("/{uuid}" + Url.URL_ACTIVE)
	public String activeRoom(@PathVariable("uuid") String uuid,
	                         Principal principal,
	                         RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Room originalRoom = service.read(uuid);
		if (originalRoom == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_ROOM;
		}
		boolean active = service.status(originalRoom.getId(), 1);
		if (active) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_ACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ACTIVE_ERROR);
		}
		return "redirect:" + Url.URL_ROOM;
	}

	@PostMapping("/{uuid}" + Url.URL_DEACTIVE)
	public String deactiveRoom(@PathVariable("uuid") String uuid,
	                           Principal principal,
	                           RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Room originalRoom = service.read(uuid);
		if (originalRoom == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_ROOM;
		}
		boolean deactive = service.status(originalRoom.getId(), 0);
		if (deactive) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DEACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DEACTIVE_ERROR);
		}
		return "redirect:" + Url.URL_ROOM;
	}

	@PostMapping("/{uuid}" + Url.URL_DELETE)
	public String deleteSubject(@PathVariable("uuid") String uuid,
	                            Model model,
	                            Principal principal,
	                            RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Room originalRoom = service.read(uuid);
		if (originalRoom == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_ROOM;
		}
		boolean isRoomUsedDetail = service.isRoomUsed(originalRoom.getId(), "room", Detail.class);
		if (isRoomUsedDetail) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_CONSTRAIN);
			return "redirect:" + Url.URL_ROOM + "/" + originalRoom.getUuid() + Url.URL_UPDATE;
		}
		boolean delete = service.delete(originalRoom);
		if (delete) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DELETE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DELETE_ERROR);
		}
		return "redirect:" + Url.URL_ROOM;
	}
}
