package com.bukbros.newproject.room;

import java.util.HashMap;

public class RoomLanguage {
	public static final String VALIDATION_NAME = "Tên không hợp lệ. Vui lòng nhập lại";	
	public static final String VALIDATION_ROOM = "Phòng học không hợp lệ. Vui lòng chọn lại";
	
	public static final String IS_NAME_EXISTED = "Tên đã tồn tại. Vui lòng nhập lại.";
	
	public static final String LABEL_SLUG = "Slug";
	public static final String LABEL_NAME = "Tên phòng học";
	
	public static final String LABEL_CREATE = "Thêm mới phòng học";
	public static final String LABEL_UPDATE = "Cập nhật phòng học";
	public static final String LABEL_LIST = "Danh sách phòng học";
	
	public static HashMap<String, String> label() {
		HashMap<String, String> labels = new HashMap<>();
		labels.put("LABEL_SLUG", LABEL_SLUG);
		labels.put("LABEL_NAME", LABEL_NAME);
		labels.put("LABEL_CREATE", LABEL_CREATE);
		labels.put("LABEL_UPDATE", LABEL_UPDATE);
		labels.put("LABEL_LIST", LABEL_LIST);
		return labels;
	}
}
