package com.bukbros.newproject.subject;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bukbros.newproject.subject.interfaces.SubjectRepositoryInterface;
import com.bukbros.newproject.subject.interfaces.SubjectServiceInterface;

@Service
public class SubjectServiceImpl implements SubjectServiceInterface{
	@Autowired
	private SubjectRepositoryInterface repository;
	
	@Override
	public boolean create(Subject subject) {
		return repository.create(subject);
	}

	@Override
	public List<Subject> read(boolean isActive) {
		return repository.read(isActive);
	}

	@Override
	public boolean update(long id, Subject subject) {
		subject.setId(id);
		return repository.update(subject);
	}

	@Override
	public boolean delete(Subject subject) {
		return repository.delete(subject);
	}

	@Override
	public Subject read(long id) {
		return repository.read(id);
	}

	@Override
	public boolean status(long id, int status) {
		return repository.status(id, status);
	}

	@Override
	public Subject read(String uuid) {
		return repository.read(uuid);
	}

	@Override
	public boolean isTitleExisted(String title, long id) {
		return repository.isTitleExisted(title, id);
	}

	@Override
	public boolean isCodeExisted(String code) {
		return repository.isCodeExisted(code);
	}

	@Override
	public List<Subject> getSubjectsInUpdate(long id) {
		return repository.getSubjectsInUpdate(id);
	}

	@Override
	public boolean isSubjectValid(long id, int type) {
		if (id == 0) {
			return true;
		}
		Subject subject = repository.read(id);
		if (subject != null) {
			if (type != 0) {
				if (subject.getStatus() == type) {
					return true;
				}
				return false;
			}
			return true;
		}
		return false;
	}

	@Override
	public List<Subject> getSubjectsInUpdate(List<Long> ids) {
		return repository.getSubjectsInUpdate(ids);
	}

	@Override
	public boolean isSubjectUsed(long id, String reference, Class<? extends Object> referenceClass, boolean toOne) {
		return repository.isSubjectUsed(id, reference, referenceClass, toOne);
	}

	@Override
	public long countSubject() {
		return repository.countSubject();
	}

}
