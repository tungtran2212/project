package com.bukbros.newproject.subject;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.bukbros.newproject.function.StringFunction;

@Entity
@Table( name = SubjectDatabase.TABLE)
public class Subject {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = SubjectDatabase.COLUMN_ID)
	private long id;
	
	@Column(name = SubjectDatabase.COLUMN_UUID, updatable = false)
	private UUID uuid;
	
	@Size(min = 1, max = 100, message = SubjectLanguage.VALIDATION_TITLE)
	@Column(name = SubjectDatabase.COLUMN_TITLE)
	private String title;

	@Pattern(regexp = "^[^!@~`#$?%^&*():;\\[\\]{}\\\\/|<>\"']{1,100}$", message = SubjectLanguage.VALIDATION_CODE)
	@Column(name = SubjectDatabase.COLUMN_CODE)
	private String code;
	
	@Pattern(regexp = "^([1-9][0-9][0-9]){1,3}$",message = SubjectLanguage.VALIDATION_CREADIT)
	@Column(name = SubjectDatabase.COLUMN_CREADIT)
	private String creadit;
	
	@Column(name = SubjectDatabase.COLUMN_STATUS)
	private byte status;
	
	public Subject() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = StringFunction.trimSpace(title);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = StringFunction.trimSpace(code);
	}

	public String getCreadit() {
		return creadit;
	}

	public void setCreadit(String creadit) {
		this.creadit = creadit.trim();
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}
	
	@PrePersist
	public void prePersist() {
		uuid = UUID.randomUUID();
	}
}
