package com.bukbros.newproject.subject;

import java.util.HashMap;

public class SubjectLanguage {
	public static final String VALIDATION_NOT_EMPTY = "Không được để trống";
	public static final String VALIDATION_TITLE = "Tên môn học không hợp lệ.Vui lòng nhập lại";
	public static final String VALIDATION_CODE = "Mã môn học không hợp lệ.Vui lòng nhập lại";
	public static final String VALIDATION_SUBJECT = "Môn học không hợp lệ.Vui lòng nhập lại";
	public static final String VALIDATION_CREADIT = "Số tín chỉ không hợp lệ.Vui lòng nhập lại";
	
	public static final String IS_TITLE_EXISTED = "Tên môn đã tồn tại. Vui lòng nhập lại.";
	public static final String IS_CODE_EXISTED = "Mã môn đã tồn tại. Vui lòng nhập lại.";
	
	public static final String LABEL_TITLE = "Tên môn học";
	public static final String LABEL_CODE = "Mã môn";
	public static final String LABEL_CREADIT = "Số tín chỉ";
	
	public static final String LABEL_CREATE = "Thêm mới môn học";
	public static final String LABEL_UPDATE = "Cập nhật môn học";
	public static final String LABEL_LIST = "Danh sách môn học";
	
	public static HashMap<String, String> label() {
		HashMap<String, String> labels = new HashMap<>();
		labels.put("LABEL_TITLE", LABEL_TITLE);
		labels.put("LABEL_CODE", LABEL_CODE);
		labels.put("LABEL_CREADIT", LABEL_CREADIT);
		
		labels.put("LABEL_CREATE", LABEL_CREATE);
		labels.put("LABEL_UPDATE", LABEL_UPDATE);
		labels.put("LABEL_LIST", LABEL_LIST);
		return labels;
	}
}
