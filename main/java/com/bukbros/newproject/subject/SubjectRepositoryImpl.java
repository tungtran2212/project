package com.bukbros.newproject.subject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bukbros.newproject.subject.interfaces.SubjectRepositoryInterface;
@Repository
@Transactional
public class SubjectRepositoryImpl implements SubjectRepositoryInterface{
	@Autowired
	private SessionFactory sessionFactory;
	
	private Logger logger = LogManager.getLogger(SubjectRepositoryImpl.class);
	@Override
	public boolean create(Subject subject) {
		try {
			sessionFactory.getCurrentSession().persist(subject);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public List<Subject> read(boolean isActive) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Subject> criteria = builder.createQuery(Subject.class);
			Root<Subject> root = criteria.from(Subject.class);
			criteria.select(root);
			if (isActive) {
				criteria.where(builder.equal(root.get("status"), 1));
			}
			criteria.orderBy(builder.desc(root.get("id")));
			List<Subject> subjects = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !subjects.isEmpty() ? subjects : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean update(Subject subject) {
		try {
			sessionFactory.getCurrentSession().update(subject);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public boolean delete(Subject subject) {
		try {
			sessionFactory.getCurrentSession().delete(subject);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public Subject read(long id) {
		try {
			Subject subjects = sessionFactory.getCurrentSession().get(Subject.class, id);
			return subjects;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}		
	}

	@Override
	public boolean status(long id, int status) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaUpdate<Subject> criteria = builder.createCriteriaUpdate(Subject.class);
		    Root<Subject> root = criteria.from(Subject.class);
		    criteria.set(root.get("status"), status)
		    		.where(builder.equal(root.get("id"), id));
		    sessionFactory.getCurrentSession().createQuery(criteria).executeUpdate();
		    return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public Subject read(String uuid) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<Subject> criteria = builder.createQuery(Subject.class);
		    Root<Subject> root = criteria.from(Subject.class);
		    criteria.select(root)
		     		.where(builder.equal(root.get("uuid"), UUID.fromString(uuid)));
		    Subject subjects = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();//tra ve null khi ko tim dc
		    return subjects; 
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean isTitleExisted(String title, long id) {
		try {
			String dbTitle;
			String sql = "SELECT " + SubjectDatabase.COLUMN_TITLE
								   + " FROM " + SubjectDatabase.TABLE
								   + " WHERE " + SubjectDatabase.COLUMN_TITLE + " REGEXP :" + SubjectDatabase.COLUMN_TITLE;
			String titleRegex = "^" + title + "$";
			if (id == 0) {
				dbTitle = sessionFactory.getCurrentSession().createSQLQuery(sql)
						   							.setParameter(SubjectDatabase.COLUMN_TITLE, titleRegex)
						   							.uniqueResult().toString();
			} else {
				sql += " AND " + SubjectDatabase.COLUMN_ID + " != :" + SubjectDatabase.COLUMN_ID;
				dbTitle = sessionFactory.getCurrentSession().createSQLQuery(sql)
        											.setParameter(SubjectDatabase.COLUMN_TITLE, titleRegex)
        											.setParameter(SubjectDatabase.COLUMN_ID, id)
        											.uniqueResult().toString();
			}
			return title.equalsIgnoreCase(dbTitle) ? true : false;
		} catch(Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public boolean isCodeExisted(String code) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<String> criteria = builder.createQuery(String.class);
		    Root<Subject> root = criteria.from(Subject.class);
			criteria.select(root.get("code")).where(builder.equal(root.get("code"), code));
		    String dbCode = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
		    return dbCode.equalsIgnoreCase(dbCode) ? true : false;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public List<Subject> getSubjectsInUpdate(long id) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Subject> criteria = builder.createQuery(Subject.class);
			Root<Subject> root = criteria.from(Subject.class);
			criteria.select(root)
					.where(builder.or(builder.equal(root.get("status"), 1),
									  builder.equal(root.get("id"), id)))
					.orderBy(builder.asc(root.get("id")));
			List<Subject> subjects = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !subjects.isEmpty() ? subjects : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public List<Subject> getSubjectsInUpdate(List<Long> ids) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Subject> criteria = builder.createQuery(Subject.class);
			Root<Subject> root = criteria.from(Subject.class);
			List<Predicate> predicates = new ArrayList<>();
			if (ids.isEmpty()) {
				predicates.add(builder.equal(root.get("status"), 1));
			} else {
				predicates.add(builder.or(builder.equal(root.get("status"), 1),
										  root.get("id").in(ids)));
			}
			criteria.select(root)
					.where(predicates.toArray(new Predicate[predicates.size()]))
					.orderBy(builder.asc(root.get("id")));
			List<Subject> subjects = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !subjects.isEmpty() ? subjects : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean isSubjectUsed(long id, String reference, Class<? extends Object> referenceClass, boolean toOne) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
			Root<?> root = criteria.from(referenceClass);
			List<Predicate> predicates = new ArrayList<>();
			if (!toOne) {
				Join<?, Subject> object = root.join(reference, JoinType.INNER);
				predicates.add(builder.equal(object.get("id"), id));
			} else {
				predicates.add(builder.equal(root.get(reference), id));
			}
			criteria.select(builder.count(root))
					.where(predicates.toArray(new Predicate[predicates.size()]));
			int count = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult().intValue();
			return count > 0 ? true : false;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public long countSubject() {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
			Root<Subject> root = criteria.from(Subject.class);
			criteria.select(builder.count(root));
			long count = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
			return count;
		} catch (Exception e) {
			logger.error(e);
			return 0;
		}
	}

}
