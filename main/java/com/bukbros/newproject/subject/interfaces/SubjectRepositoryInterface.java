package com.bukbros.newproject.subject.interfaces;

import java.util.List;

import com.bukbros.newproject.interfaces.RepositoryInterface;
import com.bukbros.newproject.subject.Subject;

public interface SubjectRepositoryInterface extends RepositoryInterface<Subject>{
	boolean isTitleExisted(String title, long id);
	
	boolean isCodeExisted(String code);
	
	List<Subject> getSubjectsInUpdate(long id);
	
	List<Subject> getSubjectsInUpdate(List<Long> ids);
	
	boolean isSubjectUsed(long id, String reference, Class<? extends Object> referenceClass, boolean toOne);
	
	long countSubject();
}
