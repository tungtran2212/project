package com.bukbros.newproject.subject.interfaces;

import java.util.List;

import com.bukbros.newproject.interfaces.ServiceInterface;
import com.bukbros.newproject.subject.Subject;

public interface SubjectServiceInterface extends ServiceInterface<Subject>{
	boolean isTitleExisted(String title, long id);
	
	boolean isCodeExisted(String code);
	
	List<Subject> getSubjectsInUpdate(long id);
	
	List<Subject> getSubjectsInUpdate(List<Long> ids);
	
	boolean isSubjectValid(long id, int type);
	
	boolean isSubjectUsed(long id, String reference, Class<? extends Object> referenceClass, boolean toOne);
	
	long countSubject();
}
