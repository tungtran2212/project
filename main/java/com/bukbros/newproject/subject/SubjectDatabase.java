package com.bukbros.newproject.subject;

public class SubjectDatabase {
	public static final String TABLE = "cr_subject";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_UUID = "uuid";
	public static final String COLUMN_TITLE = "title";
	public static final String COLUMN_CODE = "code";
	public static final String COLUMN_CREADIT = "creadit";
	public static final String COLUMN_STATUS = "status";
}
