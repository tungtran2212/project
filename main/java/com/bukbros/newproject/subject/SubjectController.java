package com.bukbros.newproject.subject;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.newproject.configuration.Role;
import com.bukbros.newproject.configuration.Url;
import com.bukbros.newproject.language.LabelLanguage;
import com.bukbros.newproject.language.MessageLanguage;
import com.bukbros.newproject.majorSubject.MajorSubject;
import com.bukbros.newproject.staff.Staff;
import com.bukbros.newproject.staff.StaffLanguage;
import com.bukbros.newproject.staff.interfaces.StaffServiceInterface;
import com.bukbros.newproject.subject.interfaces.SubjectServiceInterface;
import com.bukbros.newproject.user.User;
import com.bukbros.newproject.user.interfaces.UserServiceInterface;

@Controller
@RequestMapping(SubjectConfig.MODULE_ADMIN_URL)
public class SubjectController {
	@Autowired
	private SubjectServiceInterface service;

	@Autowired
	private UserServiceInterface userService;
	
	@Autowired
	private StaffServiceInterface staffService;

	@ModelAttribute("module")
	public HashMap<String, String> module() {
		return SubjectConfig.component();
	}

	@ModelAttribute("moduleLang")
	public HashMap<String, String> moduleLang() {
		return SubjectLanguage.label();
	}

	@ModelAttribute("labelLang")
	public HashMap<String, String> labelLanguage() {
		return LabelLanguage.labels();
	}
	
	@ModelAttribute("staffLang")
	public HashMap<String, String> staffLang() {
		return StaffLanguage.labels();
	}

	@ModelAttribute("subjectMenu")
	public boolean subjectMenu() {
		return true;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String getSubject(Principal principal, Model model, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("listMode", true);
		List<Subject> subjects = service.read(false);
		if (subjects == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
		} else {
			model.addAttribute("subjects", subjects);
		}
		return SubjectConfig.MODULE_VIEW_ADMIN;
	}

	@GetMapping(Url.URL_CREATE)
	public String getCreate(Model model, Principal principal, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("createMode", true);
		return SubjectConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping(Url.URL_CREATE)
	public String postCreate(@ModelAttribute("validSubject") @Valid Subject subject,
							 BindingResult results,
							 Principal principal,
							 Model model,
							 RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("createMode", true);
		model.addAttribute("subject", subject);
		if (results.hasErrors()) {
			return SubjectConfig.MODULE_VIEW_ADMIN;
		} else {
			boolean isTitleExisted = service.isTitleExisted(subject.getTitle(), 0);
			if (isTitleExisted) {
				results.addError(new FieldError("validSubject", "title", SubjectLanguage.IS_TITLE_EXISTED));
			}
			boolean isCodeExisted = service.isCodeExisted(subject.getCode());
			if (isCodeExisted) {
				results.addError(new FieldError("validSubject", "code", SubjectLanguage.IS_CODE_EXISTED));
			}
			if (results.hasFieldErrors("title") || results.hasFieldErrors("code")) {
				return SubjectConfig.MODULE_VIEW_ADMIN;
			}
		}
		boolean createItem = service.create(subject);
		if (createItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_CREATE_SUCCESS);
		} else {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_CREATE_ERROR);
			return SubjectConfig.MODULE_VIEW_ADMIN;
		}
		model.asMap().clear();
		return "redirect:" + Url.URL_SUBJECT;
	}

	@GetMapping("/{uuid}" + Url.URL_UPDATE)
	public String getUpdate(@PathVariable("uuid") String uuid,
							Model model,
							Principal principal,
							RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Subject originalSubject = service.read(uuid);
		if (originalSubject == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_SUBJECT;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("subject", originalSubject);
		return SubjectConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping("/{uuid}" + Url.URL_UPDATE)
	public String postUpdate(@PathVariable("uuid") String uuid,
							 @ModelAttribute("validSubject") @Valid Subject subject,
							 BindingResult results,
							 Principal principal,
							 Model model,
							 RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Subject originalSubject = service.read(uuid);
		if (originalSubject == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_SUBJECT;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("subject", subject);
		if (results.hasErrors()) {
			return SubjectConfig.MODULE_VIEW_ADMIN;
		} else {
			if (!subject.getTitle().equalsIgnoreCase(originalSubject.getTitle())) {
				boolean isTitleExisted = service.isTitleExisted(subject.getTitle(), originalSubject.getId());
				if (isTitleExisted) {
					results.addError(new FieldError("validSubject", "title", SubjectLanguage.IS_TITLE_EXISTED));
				}
			}
			if (!subject.getCode().equalsIgnoreCase(originalSubject.getCode())) {
				boolean isCodeExisted = service.isCodeExisted(subject.getCode());
				if (isCodeExisted) {
					results.addError(new FieldError("validSubject", "code", SubjectLanguage.IS_CODE_EXISTED));
				}
			}
			if (results.hasFieldErrors("title") || results.hasFieldErrors("code")) {
				return SubjectConfig.MODULE_VIEW_ADMIN;
			}
		}
		if (service.update(originalSubject.getId(), subject)) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_UPDATE_SUCCESS);
		} else {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_UPDATE_ERROR);
			return SubjectConfig.MODULE_VIEW_ADMIN;
		}
		model.asMap().clear();
		return "redirect:" + Url.URL_SUBJECT;
	}

	@PostMapping("/{uuid}" + Url.URL_ACTIVE)
	public String activeSubject(@PathVariable("uuid") String uuid, Principal principal, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Subject originalSubject = service.read(uuid);
		if (originalSubject == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_SUBJECT;
		}
		boolean active = service.status(originalSubject.getId(), 1);
		if (active) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_ACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ACTIVE_ERROR);
		}
		return "redirect:" + Url.URL_SUBJECT;
	}

	@PostMapping("/{uuid}" + Url.URL_DEACTIVE)
	public String deactiveSubject(@PathVariable("uuid") String uuid, Principal principal, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Subject originalSubject = service.read(uuid);
		if (originalSubject == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_SUBJECT;
		}
		boolean deactive = service.status(originalSubject.getId(), 0);
		if (deactive) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DEACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DEACTIVE_ERROR);
		}
		return "redirect:" + Url.URL_SUBJECT;
	}

	@PostMapping("/{uuid}" + Url.URL_DELETE)
	public String deleteSubject(@PathVariable("uuid") String uuid,
								Model model,
								Principal principal,
								RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Subject originalSubject = service.read(uuid);
		if (originalSubject == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_SUBJECT;
		}
		boolean isSubjectUsedStaff = service.isSubjectUsed(originalSubject.getId(), "subjects", Staff.class, false);
		boolean isSubjectUsedMajor = service.isSubjectUsed(originalSubject.getId(), "subject", MajorSubject.class, true);
		if (isSubjectUsedStaff || isSubjectUsedMajor) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_CONSTRAIN);
			return "redirect:" + Url.URL_SUBJECT + "/" + originalSubject.getUuid() + Url.URL_UPDATE;
		}
		boolean delete = service.delete(originalSubject);
		if (delete) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DELETE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DELETE_ERROR);
		}
		return "redirect:" + Url.URL_SUBJECT;
	}

	@GetMapping("/{uuid}" + Url.URL_DETAIL)
	public String getDetail(@PathVariable("uuid") String uuid,
							Model model,
							Principal principal,
							RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Subject originalSubject = service.read(uuid);
		if (originalSubject == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_SUBJECT;
		}
		model.addAttribute("detailMode", true);
		model.addAttribute("subject", originalSubject);
		model.addAttribute("staffs", staffService.getStaffBySubject(originalSubject.getId(), false));
		return SubjectConfig.MODULE_VIEW_ADMIN;
	}
}
