package com.bukbros.newproject.major;

public class MajorDatabase {
	public static final String TABLE = "cr_major";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_UUID = "uuid";
	public static final String COLUMN_FACULTTY_ID = "faculty_id";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_CODE = "code";
	public static final String COLUMN_STATUS = "status";
}
