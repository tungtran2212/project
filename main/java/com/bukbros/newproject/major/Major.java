package com.bukbros.newproject.major;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import com.bukbros.newproject.faculty.Faculty;
import com.bukbros.newproject.function.StringFunction;

@Entity
@Table( name = MajorDatabase.TABLE)
public class Major {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = MajorDatabase.COLUMN_ID)
	private long id;
	
	@Column(name = MajorDatabase.COLUMN_UUID, updatable = false)
	private UUID uuid;
		
	@ManyToOne
	@JoinColumn(name = MajorDatabase.COLUMN_FACULTTY_ID)
	private Faculty faculty;
	
	@Pattern(regexp = "^[^0-9!@~`#$?%^&*():;\\[\\]{}\\\\/|<>\"']{1,100}$", message = MajorLanguage.VALIDATION_NAME)
	@Column(name = MajorDatabase.COLUMN_NAME)
	private String name;
	
	@Pattern(regexp = "^[^0-9!@~`#$?%^&*():;\\[\\]{}\\\\/|<>\"']{1,100}$", message = MajorLanguage.VALIDATION_CODE)
	@Column(name = MajorDatabase.COLUMN_CODE)
	private String code;
	
	@Column(name = MajorDatabase.COLUMN_STATUS)
	private byte status;
	
	public Major() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public Faculty getFaculty() {
		return faculty;
	}

	public void setFaculty(Faculty faculty) {
		this.faculty = faculty;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = StringFunction.trimSpace(name);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = StringFunction.trimSpace(code);
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}
	
	@PrePersist
	public void prePersist() {
		uuid = UUID.randomUUID();
	}
}
