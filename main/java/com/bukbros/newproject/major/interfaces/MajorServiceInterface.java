package com.bukbros.newproject.major.interfaces;

import java.util.List;

import com.bukbros.newproject.interfaces.ServiceInterface;
import com.bukbros.newproject.major.Major;

public interface MajorServiceInterface extends ServiceInterface<Major>{
	boolean isNameExisted(String name, long id);
	
	boolean isCodeExisted(String code);
	
	List<Major> getMajorsInUpdate(long id);
	
	boolean isMajorValid(long id, int type, long facultyId);
	
	boolean isMajorUsed(long id, String reference, Class<? extends Object> referenceClass);
	
	List<Major> getMajorByFaculty(long facultyId, boolean isActive);
	
	long countMajor();
}
