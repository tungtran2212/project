package com.bukbros.newproject.major;

import java.util.HashMap;

public class MajorLanguage {
	public static final String VALIDATION_NAME = "Tên ngành học không hợp lệ";
	public static final String VALIDATION_CODE = "Mã ngành học không hợp lệ";
	public static final String VALIDATION_MAJOR = "Ngành không hợp lệ. Vui lòng chọn lại";
	
	public static final String IS_CODE_EXISTED = "Mã đã tồn tại. Vui lòng nhập lại. ";
	public static final String IS_NAME_EXISTED = "Tên đã tồn tại. Vui lòng nhập lại. ";
	
	public static final String LABEL_FACULTY_NAME = "Khoa";
	public static final String LABEL_NAME = "Tên ngành học";
	public static final String LABEL_CODE = "Mã ngành";
	
	public static final String LABEL_CREATE = "Thêm mới ngành";
	public static final String LABEL_UPDATE = "Cập nhật ngành";
	public static final String LABEL_LIST = "Danh sách ngành";
	public static final String LABEL_ADD_SUBJECT = "Thêm môn";
	
	public static HashMap<String, String> labels() {
		HashMap<String, String> labels = new HashMap<>();
		labels.put("LABEL_FACULTY_NAME", LABEL_FACULTY_NAME);
		labels.put("LABEL_NAME", LABEL_NAME);
		labels.put("LABEL_CODE", LABEL_CODE);
		
		labels.put("LABEL_CREATE", LABEL_CREATE);
		labels.put("LABEL_UPDATE", LABEL_UPDATE);
		labels.put("LABEL_LIST", LABEL_LIST);
		labels.put("LABEL_ADD_SUBJECT", LABEL_ADD_SUBJECT);
		return labels;
	}
}
