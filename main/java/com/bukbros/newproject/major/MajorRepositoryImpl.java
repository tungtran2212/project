package com.bukbros.newproject.major;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bukbros.newproject.faculty.FacultyRepositoryImpl;
import com.bukbros.newproject.major.interfaces.MajorRepositoryInterface;

@Repository
@Transactional
public class MajorRepositoryImpl implements MajorRepositoryInterface {

	@Autowired
	private SessionFactory sessionFactory;

	private Logger logger = LogManager.getLogger(FacultyRepositoryImpl.class);

	@Override
	public boolean create(Major major) {
		try {
			sessionFactory.getCurrentSession().persist(major);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public List<Major> read(boolean isActive) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Major> criteria = builder.createQuery(Major.class);
			Root<Major> root = criteria.from(Major.class);
			criteria.select(root);
			if (isActive) {
				criteria.where(builder.equal(root.get("status"), 1),
							   builder.equal(root.get("faculty").get("status"), 1));
			}
			criteria.orderBy(builder.desc(root.get("id")));
			List<Major> majors = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !majors.isEmpty() ? majors : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean update(Major major) {
		try {
			sessionFactory.getCurrentSession().update(major);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public boolean delete(Major major) {
		try {
			sessionFactory.getCurrentSession().delete(major);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public Major read(long id) {
		try {
			Major majors = sessionFactory.getCurrentSession().get(Major.class, id);
			return majors;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean status(long id, int status) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaUpdate<Major> criteria = builder.createCriteriaUpdate(Major.class);
			Root<Major> root = criteria.from(Major.class);
			criteria.set(root.get("status"), status)
					.where(builder.equal(root.get("id"), id));
			sessionFactory.getCurrentSession().createQuery(criteria).executeUpdate();
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public Major read(String uuid) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Major> criteria = builder.createQuery(Major.class);
			Root<Major> root = criteria.from(Major.class);
			criteria.select(root)
					.where(builder.equal(root.get("uuid"), UUID.fromString(uuid)));
			Major majors = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();// tra
																								   // ve
																								   // null
																								   // khi
																								   // ko
																								   // tim
																								   // dc
			return majors;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean isNameExisted(String name, long id) {
		try {
			String dbName;
			String sql = "SELECT " + MajorDatabase.COLUMN_NAME + " FROM " + MajorDatabase.TABLE + " WHERE " + MajorDatabase.COLUMN_NAME +
					" REGEXP :" + MajorDatabase.COLUMN_NAME;
			String nameRegex = "^" + name + "$";
			if (id == 0) {
				dbName = sessionFactory.getCurrentSession().createSQLQuery(sql)
						.setParameter(MajorDatabase.COLUMN_NAME, nameRegex)
						.uniqueResult().toString();
			} else {
				sql += " AND " + MajorDatabase.COLUMN_ID + " != :" + MajorDatabase.COLUMN_ID;
				dbName = sessionFactory.getCurrentSession().createSQLQuery(sql)
						.setParameter(MajorDatabase.COLUMN_NAME, nameRegex)
						.setParameter(MajorDatabase.COLUMN_ID, id)
						.uniqueResult().toString();
			}
			return name.equalsIgnoreCase(dbName) ? true : false;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public boolean isCodeExisted(String code) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<String> criteria = builder.createQuery(String.class);
			Root<Major> root = criteria.from(Major.class);
			criteria.select(root.get("code")).where(builder.equal(root.get("code"), code));
			String dbCode = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
			return code.equalsIgnoreCase(dbCode) ? true : false;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public List<Major> getMajorsInUpdate(long id) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Major> criteria = builder.createQuery(Major.class);
			Root<Major> root = criteria.from(Major.class);
			criteria.select(root)
					.where(builder.or(builder.and(builder.equal(root.get("status"), 1),
												  builder.equal(root.get("faculty").get("status"), 1)),
									  builder.equal(root.get("id"), id)))
					.orderBy(builder.asc(root.get("id")));
			List<Major> majors = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !majors.isEmpty() ? majors : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean isMajorUsed(long id, String reference, Class<? extends Object> referenceClass) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
			Root<?> root = criteria.from(referenceClass);
			List<Predicate> predicates = new ArrayList<>();
			predicates.add(builder.equal(root.get(reference), id));
			criteria.select(builder.count(root))
					.where(predicates.toArray(new Predicate[predicates.size()]));
			int count = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult().intValue();
			return count > 0 ? true : false;
		} catch (Exception e) {
			logger.error(e);
			return true;
		}
	}

	@Override
	public List<Major> getMajorByFaculty(long facultyId, boolean isActive) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Major> criteria = builder.createQuery(Major.class);
			Root<Major> root = criteria.from(Major.class);
			List<Predicate> predicates = new ArrayList<>();
			predicates.add(builder.equal(root.get("faculty"), facultyId));
			if (isActive) {
				predicates.add(builder.equal(root.get("status"), 1));
			}
			criteria.select(root)
					.where(predicates.toArray(new Predicate[predicates.size()]))
					.orderBy(builder.asc(root.get("id")));
			List<Major> majors = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !majors.isEmpty() ? majors : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public long countMajor() {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
			Root<Major> root = criteria.from(Major.class);
			criteria.select(builder.count(root));
			long count = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
			return count;
		} catch (Exception e) {
			logger.error(e);
			return 0;
		}
	}

}
