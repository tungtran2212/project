package com.bukbros.newproject.major;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.newproject.classroom.Classroom;
import com.bukbros.newproject.configuration.Role;
import com.bukbros.newproject.configuration.Url;
import com.bukbros.newproject.faculty.FacultyLanguage;
import com.bukbros.newproject.faculty.interfaces.FacultyServiceInterface;
import com.bukbros.newproject.language.LabelLanguage;
import com.bukbros.newproject.language.MessageLanguage;
import com.bukbros.newproject.major.interfaces.MajorServiceInterface;
import com.bukbros.newproject.majorSubject.MajorSubject;
import com.bukbros.newproject.subject.SubjectConfig;
import com.bukbros.newproject.user.User;
import com.bukbros.newproject.user.interfaces.UserServiceInterface;

@Controller
@RequestMapping(value = MajorConfig.MODULE_ADMIN_URL)
public class MajorController {
	@Autowired
	private MajorServiceInterface service;
	
	@Autowired
	private FacultyServiceInterface facultyService;
	
	@Autowired
	private UserServiceInterface userService;
	
	@ModelAttribute("module")
	public HashMap<String, String> module() {
		return MajorConfig.component();
	}
	
	@ModelAttribute("moduleLang")
	public HashMap<String, String> moduleLang() {
		return MajorLanguage.labels();
	}
	
	@ModelAttribute("labelLang")
	public HashMap<String, String> labelLanguage() {
		return LabelLanguage.labels();
	}
	
	@ModelAttribute("subjectConfig")
	public HashMap<String, String> subjectConfig() {
		return SubjectConfig.component();
	}
	
	@ModelAttribute("majorMenu")
	public boolean majorMenu() {
		return true;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String getAllFaculty(Principal principal, Model model, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("listMode", true);
		List<Major> majors = service.read(false);
		if (majors == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
		} else {
			model.addAttribute("majors", majors);					
		}
		return MajorConfig.MODULE_VIEW_ADMIN;
	}
	
	@GetMapping(Url.URL_CREATE)
	public String getCreate (Model model,
	                         Principal principal,
							 RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("faculties", facultyService.read(true));
		model.addAttribute("createMode", true);
		return MajorConfig.MODULE_VIEW_ADMIN;
	}
	
	@PostMapping(Url.URL_CREATE)
	public String postCreate (@ModelAttribute("validMajor") @Valid Major major,
							  BindingResult results,
							  Model model,
							  Principal principal,
							  RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("createMode", true);
		model.addAttribute("faculties", facultyService.read(true));
		model.addAttribute("major", major);
		if(results.hasErrors()) {
			return MajorConfig.MODULE_VIEW_ADMIN;
		}else {
			boolean isCodeExisted = service.isCodeExisted(major.getCode());
			if (isCodeExisted) {
				results.addError(new FieldError("validMajor", "code", MajorLanguage.IS_CODE_EXISTED));
			}
			boolean isNameExisted = service.isNameExisted(major.getName(), 0);
			if (isNameExisted) {
				results.addError(new FieldError("validMajor", "name", MajorLanguage.IS_NAME_EXISTED));
			}
			boolean validFaculty = facultyService.isFacultyValid(major.getFaculty().getId(), 1);
			if (!validFaculty) {
				results.addError(new FieldError("validMajor", "faculty.id", FacultyLanguage.VALIDATION_FACULTY));
			}
			if (results.hasFieldErrors("code")	|| results.hasFieldErrors("name") || results.hasFieldErrors("faculty.id")) {
				return MajorConfig.MODULE_VIEW_ADMIN;
			}
		}
		boolean createItem = service.create(major);
		if (createItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_CREATE_SUCCESS);
		} else {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_CREATE_ERROR);
			return MajorConfig.MODULE_VIEW_ADMIN;
		}
		model.asMap().clear();
		return "redirect:" + Url.URL_MAJOR;
	}
	
	@GetMapping("/{uuid}" + Url.URL_UPDATE)
	public String getUpdate (@PathVariable("uuid") String uuid,
	                         Model model,
	                         Principal principal,
	                         RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Major originalMajor = service.read(uuid);
		if (originalMajor == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_MAJOR;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("faculties", facultyService.getFacultiesInUpdate(originalMajor.getFaculty().getId()));
		model.addAttribute("major", originalMajor);
		return MajorConfig.MODULE_VIEW_ADMIN;
	}
	
	@PostMapping("/{uuid}" + Url.URL_UPDATE)
	public String postUpdate (@PathVariable("uuid") String uuid,
	                          @ModelAttribute("validMajor") @Valid Major major,
	                          BindingResult results,
	                          Principal principal,
							  Model model,
							  RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Major originalMajor = service.read(uuid);
		if (originalMajor == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_MAJOR;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("faculties", facultyService.getFacultiesInUpdate(originalMajor.getFaculty().getId()));
		model.addAttribute("major", major);
		if (results.hasErrors()) {
			return MajorConfig.MODULE_VIEW_ADMIN;
		} else {
			if (!major.getCode().equalsIgnoreCase(originalMajor.getCode())) {
				boolean isCodeExisted = service.isCodeExisted(major.getCode());
				if (isCodeExisted) {
					results.addError(new FieldError("validMajor", "code", MajorLanguage.IS_CODE_EXISTED));
				}
			}
			if (!major.getName().equalsIgnoreCase(originalMajor.getName())) {
				boolean isNameExisted = service.isNameExisted(major.getName(), originalMajor.getId());
				if (isNameExisted) {
					results.addError(new FieldError("validMajor", "name", MajorLanguage.IS_NAME_EXISTED));
				}
			}
			boolean validFaculty = false;
			if (major.getFaculty().getId() != originalMajor.getFaculty().getId()) {
				validFaculty= facultyService.isFacultyValid(major.getFaculty().getId(), 1);
			} else {
				validFaculty = facultyService.isFacultyValid(major.getFaculty().getId(), 0);
			}
			if (!validFaculty) {
				results.addError(new FieldError("validMajor", "faculty.id", FacultyLanguage.VALIDATION_FACULTY));
			}
			if (results.hasFieldErrors("code")	|| results.hasFieldErrors("name") || results.hasFieldErrors("faculty.id")) {
				return MajorConfig.MODULE_VIEW_ADMIN;
			}
		}
		if (service.update(originalMajor.getId(), major)) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_UPDATE_SUCCESS);
		} else {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_UPDATE_ERROR);
			return MajorConfig.MODULE_VIEW_ADMIN;
		}
		return "redirect:" + Url.URL_MAJOR;
	}
	
	@PostMapping("/{uuid}" + Url.URL_ACTIVE)
	public String postActive(@PathVariable("uuid") String uuid, Principal principal, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Major originalMajor = service.read(uuid);
		if (originalMajor == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_MAJOR;
		}
		boolean active = service.status(originalMajor.getId(), 1);
		if (active) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_ACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ACTIVE_ERROR);
		}
		return "redirect:" + Url.URL_MAJOR;
	}
	
	@PostMapping("/{uuid}" + Url.URL_DEACTIVE)
	public String postDeactive(@PathVariable("uuid") String uuid, Principal principal, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Major originalMajor = service.read(uuid);
		if (originalMajor == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_MAJOR;
		}
		boolean deactive = service.status(originalMajor.getId(), 0);
		if (deactive) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DEACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DEACTIVE_ERROR);
		}
		return "redirect:" + Url.URL_MAJOR;
	}
	
	@PostMapping("/{uuid}" + Url.URL_DELETE )
	public String postDelete(@PathVariable("uuid") String uuid,
	                            Model model,
	                            Principal principal,
	                            RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Major originalMajor = service.read(uuid);
		if (originalMajor == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_MAJOR;
		}
		boolean isMajorUsedSubject = service.isMajorUsed(originalMajor.getId(), "major", MajorSubject.class);
		boolean isMajorUsedClassroom = service.isMajorUsed(originalMajor.getId(), "major", Classroom.class);
		if (isMajorUsedClassroom || isMajorUsedSubject) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_CONSTRAIN);
			return "redirect:" + Url.URL_MAJOR + "/" + originalMajor.getUuid() + Url.URL_UPDATE;
		}
		boolean delete = service.delete(originalMajor);
		if (delete) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DELETE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DELETE_ERROR);
		}
		return "redirect:" + Url.URL_MAJOR;
	}

}
