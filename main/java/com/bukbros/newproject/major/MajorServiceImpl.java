package com.bukbros.newproject.major;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bukbros.newproject.major.interfaces.MajorRepositoryInterface;
import com.bukbros.newproject.major.interfaces.MajorServiceInterface;

@Service
public class MajorServiceImpl implements MajorServiceInterface{
	@Autowired
	private MajorRepositoryInterface repository;

	@Override
	public boolean create(Major major) {
		return repository.create(major);
	}

	@Override
	public List<Major> read(boolean isActive) {
		return repository.read(isActive);
	}

	@Override
	public boolean update(long id, Major major) {
		major.setId(id);
		return repository.update(major);
	}

	@Override
	public boolean delete(Major major) {
		return repository.delete(major);
	}

	@Override
	public Major read(long id) {
		return read(id);
	}

	@Override
	public boolean status(long id, int status) {
		return repository.status(id, status);
	}

	@Override
	public Major read(String uuid) {
		return repository.read(uuid);
	}

	@Override
	public boolean isNameExisted(String name, long id) {
		return repository.isNameExisted(name, id);
	}

	@Override
	public boolean isCodeExisted(String code) {
		return repository.isCodeExisted(code);
	}

	@Override
	public List<Major> getMajorsInUpdate(long id) {
		return repository.getMajorsInUpdate(id);
	}

	@Override
	public boolean isMajorValid(long id, int type, long facultyId) {
		if (id == 0) {
			return true;
		}
		Major major = repository.read(id);
		if (major != null) {
			if (facultyId != 0) {
				if (major.getFaculty().getId() != facultyId) {
					return false;
				}
			}
			if (type != 0) {
				if (major.getStatus() != type) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean isMajorUsed(long id, String reference, Class<? extends Object> referenceClass) {
		return repository.isMajorUsed(id, reference, referenceClass);
	}

	@Override
	public List<Major> getMajorByFaculty(long facultyId, boolean isActive) {
		return repository.getMajorByFaculty(facultyId, isActive);
	}

	@Override
	public long countMajor() {
		return repository.countMajor();
	}
}
