package com.bukbros.newproject.detail;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bukbros.newproject.detail.interfaces.DetailRepositoryInterface;
@Repository
@Transactional
public class DetailRepositoryImpl implements DetailRepositoryInterface{
	@Autowired
	private SessionFactory sessionFactory;
	
	private Logger logger = LogManager.getLogger(DetailRepositoryImpl.class);
	@Override
	public boolean create(Detail detail) {
		try {
			sessionFactory.getCurrentSession().persist(detail);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public List<Detail> read(boolean isActive) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Detail> criteria = builder.createQuery(Detail.class);
			Root<Detail> root = criteria.from(Detail.class);
			criteria.select(root);
			if (isActive) {
				criteria.where(builder.equal(root.get("status"), 1));
			}
			criteria.orderBy(builder.desc(root.get("id")));
			List<Detail> details = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !details.isEmpty() ? details : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean update(Detail detail) {
		try {
			sessionFactory.getCurrentSession().update(detail);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public boolean delete(Detail detail) {
		try {
			sessionFactory.getCurrentSession().delete(detail);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public Detail read(long id) {
		try {
			Detail detail = sessionFactory.getCurrentSession().get(Detail.class, id);
			return detail;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}	
	}

	@Override
	public boolean status(long id, int status) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaUpdate<Detail> criteria = builder.createCriteriaUpdate(Detail.class);
		    Root<Detail> root = criteria.from(Detail.class);
		    criteria.set(root.get("status"), status)
		    		.where(builder.equal(root.get("id"), id));
		    sessionFactory.getCurrentSession().createQuery(criteria).executeUpdate();
		    return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public Detail read(String uuid) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<Detail> criteria = builder.createQuery(Detail.class);
		    Root<Detail> root = criteria.from(Detail.class);
		    criteria.select(root)
		     		.where(builder.equal(root.get("uuid"), UUID.fromString(uuid)));
		    Detail detail = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
		    return detail; 
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean isDetailExisted(long majorSubjectId, int term, long roomId, long staffId) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<Detail> criteria = builder.createQuery(Detail.class);
		    Root<Detail> root = criteria.from(Detail.class);
		    List<Predicate> predicates = new ArrayList<>();
		    predicates.add(builder.equal(root.get("majorSubject"), majorSubjectId));
		    predicates.add(builder.equal(root.get("term"),term));
		    predicates.add(builder.equal(root.get("room"), roomId));
		    predicates.add(builder.equal(root.get("staff"), staffId));
			criteria.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
			Detail detail = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
		    return detail != null ? true : false;
		} catch (Exception e) {
			logger.error(e);
			return true;
		}
	}

	@Override
	public boolean isDetailUsed(long id, String reference, Class<? extends Object> referenceClass) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
			Root<?> root = criteria.from(referenceClass);
			List<Predicate> predicates = new ArrayList<>();
			predicates.add(builder.equal(root.get(reference), id));
			criteria.select(builder.count(root))
					.where(predicates.toArray(new Predicate[predicates.size()]));
			int count = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult().intValue();
			return count > 0 ? true : false;
		} catch (Exception e) {
			logger.error(e);
			return true;
		}
	}

	@Override
	public List<Detail> getDetailByStaff(long staffId, boolean isActive) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<Detail> criteria = builder.createQuery(Detail.class);
		    Root<Detail> root = criteria.from(Detail.class);
		    List<Predicate> predicates = new ArrayList<>();
		    predicates.add(builder.equal(root.get("staff"), staffId));
		    if (isActive) {
		    	predicates.add(builder.equal(root.get("status"), 2));
			}
			criteria.select(root).where(predicates.toArray(new Predicate[predicates.size()]))
					.orderBy(builder.desc(root.get("id")));
			List<Detail> details = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
		    return !details.isEmpty() ? details : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

}
