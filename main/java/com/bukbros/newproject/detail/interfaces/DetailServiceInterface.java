package com.bukbros.newproject.detail.interfaces;

import java.util.List;

import com.bukbros.newproject.detail.Detail;
import com.bukbros.newproject.interfaces.ServiceInterface;

public interface DetailServiceInterface extends ServiceInterface<Detail>{
	
	boolean isDetailExisted(long majorSubjectId, int term, long roomId, long staffId);
	
	boolean isDetailValid(long id, int status);
	
	boolean isDetailUsed(long id, String reference, Class<? extends Object> referenceClass);
	
	List<Detail> getDetailByStaff(long staffId, boolean isActive);
}
