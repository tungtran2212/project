package com.bukbros.newproject.detail;

import java.util.HashMap;

import com.bukbros.newproject.configuration.Url;
import com.bukbros.newproject.configuration.View;
import com.bukbros.newproject.detailTime.DetailTimeConfig;

public class DetailConfig {
	public static final String MODULE_CODE = "detail";
	public static final String MODULE_NAME = "Lịch học";
	public static final String MODULE_URL = "/" + MODULE_CODE;
	public static final String MODULE_ADMIN_URL = Url.URL_ADMIN + MODULE_URL;
	public static final String MODULE_DATABASE_TABLE = MODULE_CODE;
	public static final String MODULE_VIEW_ADMIN = View.VIEW_ADMIN_URL + View.VIEW_MODULE_FOLDER + MODULE_CODE + View.VIEW_INDEX;
	
	public static final String URL_TIME = DetailTimeConfig.MODULE_URL;
	
	public static final long MAX_TERM = 15;
	
	public static HashMap<String, String> component() {
		HashMap<String, String> components = new HashMap<>();
		components.put("MODULE_CODE", MODULE_CODE);
		components.put("MODULE_NAME", MODULE_NAME);
		components.put("MODULE_URL", MODULE_URL);
		components.put("MODULE_ADMIN_URL", MODULE_ADMIN_URL);
		components.put("URL_TIME", URL_TIME);
		return components;
	}
}
