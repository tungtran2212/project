package com.bukbros.newproject.detail;

import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.Range;

import com.bukbros.newproject.detailTime.DetailTime;
import com.bukbros.newproject.majorSubject.MajorSubject;
import com.bukbros.newproject.room.Room;
import com.bukbros.newproject.staff.Staff;

@Entity
@Table(name = DetailDatabase.TABLE)
public class Detail {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = DetailDatabase.COLUMN_ID)
	private long id;

	@Column(name = DetailDatabase.COLUMN_UUID, updatable = false)
	private UUID uuid;

	@ManyToOne
	@JoinColumn(name = DetailDatabase.COLUMN_MAJOR_SUBJECT_ID)
	private MajorSubject majorSubject;

	@Range(min = 1, max = DetailConfig.MAX_TERM, message = DetailLanguage.VALIDATION_TERM)
	@Column(name = DetailDatabase.COLUMN_TERM)
	private int term;

	@ManyToOne
	@JoinColumn(name = DetailDatabase.COLUMN_ROOM_ID)
	private Room room;

	@ManyToOne
	@JoinColumn(name = DetailDatabase.COLUMN_USER_ID)
	private Staff staff;

	@Column(name = DetailDatabase.COLUMN_STATUS)
	private byte status;

	@OneToMany(mappedBy = "detail", cascade = CascadeType.REMOVE)
	private List<DetailTime> detailTimes;
	
	@Transient
	private long countStudent;

	public Detail() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public MajorSubject getMajorSubject() {
		return majorSubject;
	}

	public void setMajorSubject(MajorSubject majorSubject) {
		this.majorSubject = majorSubject;
	}

	public int getTerm() {
		return term;
	}

	public void setTerm(int term) {
		this.term = term;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public List<DetailTime> getDetailTimes() {
		return detailTimes;
	}

	public void setDetailTimes(List<DetailTime> detailTimes) {
		this.detailTimes = detailTimes;
	}

	public long getCountStudent() {
		return countStudent;
	}

	public void setCountStudent(long countStudent) {
		this.countStudent = countStudent;
	}

	@PrePersist
	public void prePersist() {
		uuid = UUID.randomUUID();
	}
}
