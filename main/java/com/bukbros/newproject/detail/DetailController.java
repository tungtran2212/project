package com.bukbros.newproject.detail;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.newproject.configuration.Role;
import com.bukbros.newproject.configuration.Url;
import com.bukbros.newproject.detail.interfaces.DetailServiceInterface;
import com.bukbros.newproject.language.LabelLanguage;
import com.bukbros.newproject.language.MessageLanguage;
import com.bukbros.newproject.majorSubject.MajorSubject;
import com.bukbros.newproject.majorSubject.interfaces.MajorSubjectServiceInterface;
import com.bukbros.newproject.registration.Registration;
import com.bukbros.newproject.room.RoomLanguage;
import com.bukbros.newproject.room.interfaces.RoomServiceInterface;
import com.bukbros.newproject.staff.Staff;
import com.bukbros.newproject.staff.StaffLanguage;
import com.bukbros.newproject.staff.interfaces.StaffServiceInterface;
import com.bukbros.newproject.subject.Subject;
import com.bukbros.newproject.user.User;
import com.bukbros.newproject.user.interfaces.UserServiceInterface;

@Controller
@RequestMapping(value = DetailConfig.MODULE_ADMIN_URL)
public class DetailController {
	@Autowired
	private DetailServiceInterface service;

	@Autowired
	private UserServiceInterface userService;

	@Autowired
	private MajorSubjectServiceInterface majorSubjectService;

	@Autowired
	private RoomServiceInterface roomService;

	@Autowired
	private StaffServiceInterface staffService;

	@ModelAttribute("module")
	public HashMap<String, String> module() {
		return DetailConfig.component();
	}

	@ModelAttribute("moduleLang")
	public HashMap<String, String> moduleLang() {
		return DetailLanguage.labels();
	}

	@ModelAttribute("labelLang")
	public HashMap<String, String> labelLanguage() {
		return LabelLanguage.labels();
	}

	@ModelAttribute("detailMenu")
	public boolean detailMenu() {
		return true;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String getAllDetail(Principal principal, Model model, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("listMode", true);
		List<Detail> details = service.read(false);
		if (details == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
		} else {
			model.addAttribute("details", details);
		}
		return DetailConfig.MODULE_VIEW_ADMIN;
	}

	@GetMapping(Url.URL_CREATE)
	public String getCreate(Model model, Principal principal, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("createMode", true);
		model.addAttribute("majorSubjects", majorSubjectService.read(true));
		model.addAttribute("rooms", roomService.read(true));
		model.addAttribute("staffs", staffService.read(true));
		model.addAttribute("maxTerm", DetailConfig.MAX_TERM);
		return DetailConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping(Url.URL_CREATE)
	public String postCreate(@ModelAttribute("validDetail") @Valid Detail detail,
							 BindingResult results,
							 Model model,
							 Principal principal,
							 RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("createMode", true);
		model.addAttribute("majorSubjects", majorSubjectService.read(true));
		model.addAttribute("rooms", roomService.read(true));
		model.addAttribute("staffs", staffService.read(true));
		model.addAttribute("detail", detail);
		model.addAttribute("maxTerm", DetailConfig.MAX_TERM);
		if (results.hasErrors()) {
			return DetailConfig.MODULE_VIEW_ADMIN;
		} else {
			boolean validRoom = roomService.isRoomValid(detail.getRoom().getId(), 1);
			if (!validRoom) {
				results.addError(new FieldError("validDetail", "room.id", RoomLanguage.VALIDATION_ROOM));
			}
			boolean validStaff = true;
			Staff staff = staffService.read(detail.getStaff().getUserId(), true);
			if ((staff == null) || (staff.getUser().getStatus() == 0)) {
				validStaff = false;
			}
			if (!validStaff) {
				results.addError(new FieldError("validDetail", "staff.userId", StaffLanguage.VALIDATION_STAFF));
			}
			boolean validMajorSubject = true;
			MajorSubject majorSubject = majorSubjectService.read(detail.getMajorSubject().getId());
			if ((majorSubject == null) || (majorSubject.getStatus() == 0)) {
				validMajorSubject = false;
			}
			if (!validMajorSubject) {
				results.addError(new FieldError("validDetail", "majorSubject.id", DetailLanguage.VALIDATION_MAJORSUBJECT));
			}
			if (validMajorSubject && validStaff) {
				boolean validSubject = false;
				if (staff.getSubjects() != null) {
					for (Subject subject : staff.getSubjects()) {
						if (subject.getId() == majorSubject.getSubject().getId()) {
							validSubject = true;
						}
					}
				}
				if (!validSubject) {
					results.addError(new FieldError("validDetail", "staff.userId", StaffLanguage.VALIDATION_SUBJECT));
				}
			}
			if (results.hasFieldErrors("room.id") || results.hasFieldErrors("staff.userId") || results.hasFieldErrors("majorSubject.id")) {
				return DetailConfig.MODULE_VIEW_ADMIN;
			}
		}
		boolean createItem = service.create(detail);
		if (createItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_CREATE_SUCCESS);
		} else {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_CREATE_ERROR);
			return DetailConfig.MODULE_VIEW_ADMIN;
		}
		model.asMap().clear();
		return "redirect:" + DetailConfig.MODULE_ADMIN_URL;
	}

	@GetMapping("/{uuid}" + Url.URL_UPDATE)
	public String getUpdate(@PathVariable("uuid") String uuid, Model model, Principal principal, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Detail originalDetail = service.read(uuid);
		if (originalDetail == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + DetailConfig.MODULE_ADMIN_URL;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("majorSubjects", majorSubjectService.getMajorSubjectsInUpdate(originalDetail.getMajorSubject().getId()));
		model.addAttribute("rooms", roomService.getRoomsInUpdate(originalDetail.getRoom().getId()));
		model.addAttribute("staffs", staffService.getStaffsInUpdate(originalDetail.getStaff().getUserId()));
		model.addAttribute("detail", originalDetail);
		model.addAttribute("maxTerm", DetailConfig.MAX_TERM);
		return DetailConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping("/{uuid}" + Url.URL_UPDATE)
	public String postUpdate(@PathVariable("uuid") String uuid,
							 @ModelAttribute("validDetail") @Valid Detail detail,
							 BindingResult results,
							 Principal principal,
							 Model model,
							 RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Detail originalDetail = service.read(uuid);
		if (originalDetail == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + DetailConfig.MODULE_ADMIN_URL;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("majorSubjects", majorSubjectService.getMajorSubjectsInUpdate(originalDetail.getMajorSubject().getId()));
		model.addAttribute("rooms", roomService.getRoomsInUpdate(originalDetail.getRoom().getId()));
		model.addAttribute("staffs", staffService.getStaffsInUpdate(originalDetail.getStaff().getUserId()));
		model.addAttribute("detail", detail);
		model.addAttribute("maxTerm", DetailConfig.MAX_TERM);
		if (results.hasErrors()) {
			return DetailConfig.MODULE_VIEW_ADMIN;
		} else {
			boolean validRoom = false;
			if (originalDetail.getRoom().getId() == detail.getRoom().getId()) {
				validRoom = roomService.isRoomValid(detail.getRoom().getId(), 0);
			} else {
				validRoom = roomService.isRoomValid(detail.getRoom().getId(), 1);
			}
			if (!validRoom) {
				results.addError(new FieldError("validDetail", "room.id", RoomLanguage.VALIDATION_ROOM));
			}
			boolean validStaff = true;
			Staff staff = staffService.read(detail.getStaff().getUserId(), true);
			if ((staff == null) || (staff.getUser().getStatus() == 0)) {
				validStaff = false;
			}
			if (!validStaff) {
				results.addError(new FieldError("validDetail", "staff.userId", StaffLanguage.VALIDATION_STAFF));
			}
			boolean validMajorSubject = true;
			MajorSubject majorSubject = majorSubjectService.read(detail.getMajorSubject().getId());
			if ((majorSubject == null) || (majorSubject.getStatus() == 0)) {
				validMajorSubject = false;
			}
			if (!validMajorSubject) {
				results.addError(new FieldError("validDetail", "majorSubject.id", DetailLanguage.VALIDATION_MAJORSUBJECT));
			}
			if (validMajorSubject && validStaff) {
				boolean validSubject = false;
				if (staff.getSubjects() != null) {
					for (Subject subject : staff.getSubjects()) {
						if (subject.getId() == majorSubject.getSubject().getId()) {
							validSubject = true;
						}
					}
				}
				if (!validSubject) {
					results.addError(new FieldError("validDetail", "staff.userId", StaffLanguage.VALIDATION_SUBJECT));
				}
			}
			if (results.hasFieldErrors("room.id") || results.hasFieldErrors("staff.userId") || results.hasFieldErrors("majorSubject.id")) {
				return DetailConfig.MODULE_VIEW_ADMIN;
			}
		}
		boolean updateItem = service.update(originalDetail.getId(), detail);
		if (updateItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_UPDATE_SUCCESS);
		} else {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_UPDATE_ERROR);
			return DetailConfig.MODULE_VIEW_ADMIN;
		}
		model.asMap().clear();
		return "redirect:" + DetailConfig.MODULE_ADMIN_URL;
	}

	@PostMapping("/{uuid}" + Url.URL_ACTIVE)
	public String postActive(@PathVariable("uuid") String uuid, Principal principal, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Detail originalDetail = service.read(uuid);
		if (originalDetail == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + DetailConfig.MODULE_ADMIN_URL;
		}
		boolean active = service.status(originalDetail.getId(), 1);
		if (active) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_ACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ACTIVE_ERROR);
		}
		return "redirect:" + DetailConfig.MODULE_ADMIN_URL;
	}

	@PostMapping("/{uuid}" + Url.URL_DEACTIVE)
	public String postDeactive(@PathVariable("uuid") String uuid, Principal principal, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Detail originalDetail = service.read(uuid);
		if (originalDetail == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + DetailConfig.MODULE_ADMIN_URL;
		}
		boolean deactive = service.status(originalDetail.getId(), 0);
		if (deactive) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DEACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DEACTIVE_ERROR);
		}
		return "redirect:" + DetailConfig.MODULE_ADMIN_URL;
	}

	@PostMapping("/{uuid}" + Url.URL_DELETE)
	public String postDelete(@PathVariable("uuid") String uuid, Model model, Principal principal, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Detail originalDetail = service.read(uuid);
		if (originalDetail == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + DetailConfig.MODULE_ADMIN_URL;
		}
		boolean isDetailUsedRegistration = service.isDetailUsed(originalDetail.getId(), "detail", Registration.class);
		if (isDetailUsedRegistration) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_CONSTRAIN);
			return "redirect:" + DetailConfig.MODULE_ADMIN_URL + "/" + originalDetail.getUuid() + Url.URL_UPDATE;
		}
		boolean delete = service.delete(originalDetail);
		if (delete) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DELETE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DELETE_ERROR);
		}
		return "redirect:" + DetailConfig.MODULE_ADMIN_URL;
	}
	
	@PostMapping("/{uuid}" + Url.URL_OPEN)
	public String postOpen(@PathVariable("uuid") String uuid, Principal principal, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Detail originalDetail = service.read(uuid);
		if (originalDetail == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + DetailConfig.MODULE_ADMIN_URL;
		}
		boolean active = service.status(originalDetail.getId(), 2);
		if (active) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_OPEN_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_OPEN_ERROR);
		}
		return "redirect:" + DetailConfig.MODULE_ADMIN_URL;
	}

}
