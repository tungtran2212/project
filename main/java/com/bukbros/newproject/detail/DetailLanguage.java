package com.bukbros.newproject.detail;

import java.util.HashMap;

public class DetailLanguage {
	public static final String VALIDATION_TERM = "Kỳ học không đúng, vui lòng chọn lại";
	public static final String VALIDATION_MAJORSUBJECT = "Khóa - Ngành - Môn không hợp lệ, vui lòng chọn lại";
	public static final String VALIDATION_DETAIL = "Buổi học không hợp lệ, vui lòng chọn lại";
	
	public static final String LABEL_COURSE = "Khóa";
	public static final String LABEL_MAJOR = "Ngành";
	public static final String LABEL_SUBJECT = "Môn";
	public static final String LABEL_ROOM = "Phòng";
	public static final String LABEL_STAFF = "Giáo viên";
		
	public static final String LABEL_CREATE = "Thêm mới " + DetailConfig.MODULE_NAME.toLowerCase();
	public static final String LABEL_UPDATE = "Cập nhật " + DetailConfig.MODULE_NAME.toLowerCase();
	public static final String LABEL_LIST = "Danh sách " + DetailConfig.MODULE_NAME.toLowerCase();
	public static final String LABEL_ADD_TIME = "Thời gian";
	
		
	public static HashMap<String, String> labels() {
		HashMap<String, String> labels = new HashMap<>();
		labels.put("LABEL_COURSE", LABEL_COURSE);
		labels.put("LABEL_MAJOR", LABEL_MAJOR);
		labels.put("LABEL_SUBJECT", LABEL_SUBJECT);
		labels.put("LABEL_ROOM", LABEL_ROOM);
		labels.put("LABEL_STAFF", LABEL_STAFF);
		
		labels.put("LABEL_CREATE", LABEL_CREATE);
		labels.put("LABEL_UPDATE", LABEL_UPDATE);
		labels.put("LABEL_LIST", LABEL_LIST);
		labels.put("LABEL_ADD_TIME", LABEL_ADD_TIME);
		
		return labels;
	}
}
