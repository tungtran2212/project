package com.bukbros.newproject.detail;

public class DetailDatabase {
	public static final String TABLE = "cr_detail";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_UUID = "uuid";
	public static final String COLUMN_MAJOR_SUBJECT_ID = "major_subject_id";
	public static final String COLUMN_TERM = "term";
	public static final String COLUMN_ROOM_ID = "room_id";
	public static final String COLUMN_USER_ID = "user_id";
	public static final String COLUMN_STATUS = "status";
}
