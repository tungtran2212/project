package com.bukbros.newproject.detail;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bukbros.newproject.detail.interfaces.DetailRepositoryInterface;
import com.bukbros.newproject.detail.interfaces.DetailServiceInterface;
import com.bukbros.newproject.registration.interfaces.RegistrationServiceInterface;
@Service
public class DetailServiceImpl implements DetailServiceInterface{
	@Autowired
	private DetailRepositoryInterface repository;
	
	@Autowired
	private RegistrationServiceInterface registrationService;
		
	@Override
	public boolean create(Detail detail) {
		return repository.create(detail);
	}

	@Override
	public List<Detail> read(boolean isActive) {
		List<Detail> details = repository.read(isActive);
		if (details != null) {
			for (Detail detail : details) {
				detail.setCountStudent(registrationService.countStudentByDetail(detail.getId()));
			}
		}
		return details;
	}

	@Override
	public boolean update(long id, Detail detail) {
		detail.setId(id);
		return repository.update(detail);
	}

	@Override
	public boolean delete(Detail detail) {
		return repository.delete(detail);
	}

	@Override
	public Detail read(long id) {
		return repository.read(id);
	}

	@Override
	public boolean status(long id, int status) {
		return repository.status(id, status);
	}

	@Override
	public Detail read(String uuid) {
		return repository.read(uuid);
	}

	@Override
	public boolean isDetailExisted(long majorSubjectId, int term, long roomId, long staffId) {
		return repository.isDetailExisted(majorSubjectId, term, roomId, staffId);
	}
	
	@Override
	public boolean isDetailValid(long id, int type) {
		if (id == 0) {
			return true;
		}
		Detail detail = repository.read(id);
		if (detail != null) {
			if (type != 0) {
				if (detail.getStatus() == type) {
					return true;
				}
				return false;
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean isDetailUsed(long id, String reference, Class<? extends Object> referenceClass) {
		return repository.isDetailUsed(id, reference, referenceClass);
	}

	@Override
	public List<Detail> getDetailByStaff(long staffId, boolean isActive) {
		return repository.getDetailByStaff(staffId, isActive);
	}
}
