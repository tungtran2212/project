package com.bukbros.newproject.user;

public class UserDatabase {
	public static final String TABLE = "cr_user";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_UUID = "uuid";
	public static final String COLUMN_USERNAME = "username";
	public static final String COLUMN_PASSWORD = "password";
	public static final String COLUMN_EMAIL = "email";
	public static final String COLUMN_FIRSTNAME = "first_name";
	public static final String COLUMN_LASTNAME = "last_name";
	public static final String COLUMN_ADDRESS = "address";
	public static final String COLUMN_GENDER = "gender";
	public static final String COLUMN_DOB = "dob";
	public static final String COLUMN_PHONE = "phone";
	public static final String COLUMN_IMAGE = "image";
	public static final String COLUMN_ROLE = "role";
	public static final String COLUMN_STATUS = "status";
	public static final String COLUMN_CREATED_DATE = "created_date";
}
