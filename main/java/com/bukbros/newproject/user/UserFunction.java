package com.bukbros.newproject.user;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
@Component
public class UserFunction {
	public static String encoderPassword (String password) {
		 if(password.length() == 0) {
			 return null;
		 }
		 BCryptPasswordEncoder encoderPassword = new BCryptPasswordEncoder();
		 return encoderPassword.encode(password);
	 }
	 
	//ham so sanh
		public static boolean checkPassword(String oldPassword, String encryptedPassword) {
			BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
			return encoder.matches(oldPassword, encryptedPassword);
		}
}
