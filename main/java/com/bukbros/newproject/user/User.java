package com.bukbros.newproject.user;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import com.bukbros.newproject.function.StringFunction;

@Entity
@Table(name = UserDatabase.TABLE)
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = UserDatabase.COLUMN_ID)
	private long id;

	@Column(name = UserDatabase.COLUMN_UUID, updatable = false)
	private UUID uuid;

	@Pattern(regexp = "^[a-zA-Z0-9]{4,30}$", message = UserLanguage.VALIDATION_USERNAME)
	@Column(name = UserDatabase.COLUMN_USERNAME, updatable = false)
	private String username;

	@Size(min = 6, max = 100, message = UserLanguage.VALIDATION_PASSWORD)
	@Column(name = UserDatabase.COLUMN_PASSWORD, updatable = false)
	private String password;

	@Transient
	private String confirmPassword;

	@Transient
	private String oldPassword;

	@NotBlank(message = UserLanguage.VALIDATION_EMAIL)
	@Email(message = UserLanguage.VALIDATION_EMAIL)
	@Column(name = UserDatabase.COLUMN_EMAIL)
	private String email;

	@Pattern(regexp = "^[\\p{L}\\d\\s]{1,50}$", message = UserLanguage.VALIDATION_FIRSTNAME)
	@Column(name = UserDatabase.COLUMN_FIRSTNAME)
	private String firstName;

	@Pattern(regexp = "^[\\p{L}\\d\\s]{1,50}$", message = UserLanguage.VALIDATION_LASTNAME)
	@Column(name = UserDatabase.COLUMN_LASTNAME)
	private String lastName;

	@Size(min = 6, max = 100, message = UserLanguage.VALIDATION_ADDRESS)
	@Column(name = UserDatabase.COLUMN_ADDRESS)
	private String address;

	@Column(name = UserDatabase.COLUMN_GENDER)
	private byte gender;

	@NotNull(message = UserLanguage.VALIDATION_DOB)
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@Temporal(TemporalType.DATE)
	@Column(name = UserDatabase.COLUMN_DOB)
	private Date dob;

	@Pattern(regexp = "^[0-9]{8,20}$", message = UserLanguage.VALIDATION_PHONE)
	@Column(name = UserDatabase.COLUMN_PHONE)
	private String phone;

	@Column(name = UserDatabase.COLUMN_IMAGE)
	private String image;

	@Column(name = UserDatabase.COLUMN_ROLE, updatable = false)
	private String role;

	@Column(name = UserDatabase.COLUMN_STATUS)
	private byte status;

	@Column(name = UserDatabase.COLUMN_CREATED_DATE, insertable = false, updatable = false)
	@Temporal(TemporalType.DATE)
	private Date createdDate;

	@Transient
	private MultipartFile avatarFile;

	public User() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username.trim();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password.trim();
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email.trim();
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = StringFunction.trimSpace(firstName);
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = StringFunction.trimSpace(lastName);
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = StringFunction.trimSpace(address);
	}

	public byte getGender() {
		return gender;
	}

	public void setGender(byte gender) {
		this.gender = gender;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = StringFunction.trimSpace(phone);
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public MultipartFile getAvatarFile() {
		return avatarFile;
	}

	public void setAvatarFile(MultipartFile avatarFile) {
		this.avatarFile = avatarFile;
	}

	@PrePersist // random UUID trc khi tao moi
	public void prePersist() {
		uuid = UUID.randomUUID();
	}
}
