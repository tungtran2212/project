package com.bukbros.newproject.user;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.bukbros.newproject.configuration.GlobalConfiguration;
import com.bukbros.newproject.upload.UploadService;
import com.bukbros.newproject.user.interfaces.UserRepositoryInterface;
import com.bukbros.newproject.user.interfaces.UserServiceInterface;
@Service
public class UserServiceImpl implements UserServiceInterface{
	@Autowired
	private UserRepositoryInterface repository;
	
	@Autowired
	private UploadService uploadService;
	
	@Override
	public User create(HttpServletRequest request, User user) {		
		MultipartFile avatarFile = user.getAvatarFile();
		if (avatarFile.getSize() > 0) {
			String fileName = uploadService.generateFileName(avatarFile, user.getUsername());
			boolean saveFile = uploadService.saveFile(avatarFile, fileName, GlobalConfiguration.USER_AVATAR_FOLDER, request);
			if (saveFile) {
				user.setImage(GlobalConfiguration.USER_AVATAR_LINK + fileName);
			} else {
				return null;
			}
		} else {
			return null;
		}
		user.setPassword(UserFunction.encoderPassword(user.getPassword()));
		User createUser = repository.createUser(user);
		if (createUser == null) {
			uploadService.deleteFile(user.getImage().replace("/resources", "upload"), request);
		}		
		return createUser;
	}
	
	@Override
	public boolean update(HttpServletRequest request, User originalUser, User user) {
		user.setId(originalUser.getId());
		MultipartFile avatarFile = user.getAvatarFile();
		boolean saveFile = false;
		if ((avatarFile != null) && (avatarFile.getSize() > 0)) {
			String fileName = uploadService.generateFileName(avatarFile, originalUser.getUsername());
			saveFile = uploadService.saveFile(avatarFile, fileName, GlobalConfiguration.USER_AVATAR_FOLDER, request);
			if (saveFile) {
				user.setImage(GlobalConfiguration.USER_AVATAR_LINK + fileName);
			} else {
				return false;
			}
		} else {
			user.setImage(originalUser.getImage());
		}
		boolean updateUser = repository.update(user);
		if (updateUser) {
			if (saveFile) {
				uploadService.deleteFile(originalUser.getImage().replace("/resources", "upload"), request);
			}	
		} else {
			if (saveFile) {
				uploadService.deleteFile(user.getImage().replace("/resources", "upload"), request);
			} 
		}
		return updateUser;
	}
	
	@Override
	public boolean update(long id, User object) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public List<User> read(boolean isActive) {
		return repository.read(isActive);
	}
	
	@Override
	public boolean delete(HttpServletRequest request, User user) {
		boolean deleteUser = repository.delete(user);
		if (deleteUser && (user.getImage() != null)) {
			uploadService.deleteFile(user.getImage().replace("/resources", "upload"), request);
		}
		return deleteUser;
	}

	@Override
	public boolean delete(User object) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public User read(long id) {
		return repository.read(id);
	}

	@Override
	public boolean status(long id, int status) {
		return repository.status(id, status);
	}

	@Override
	public User read(String uuid) {
		return repository.read(uuid);
	}

	@Override
	public boolean isUsernameExisted(String username) {
		return repository.isUsernameExisted(username);
	}

	@Override
	public boolean isEmailExisted(String email) {
		return repository.isEmailExisted(email);
	}

	@Override
	public User getUserByUsername(String username) {
		return repository.getUserByUsername(username);
	}

	@Override
	public boolean updatePassword(long id, String password) {
		return repository.updatePassword(id, UserFunction.encoderPassword(password));
	}

	@Override
	public boolean updateRole(long id, String role) {
		return repository.updateRole(id, role);
	}

	@Override
	public boolean create(User object) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean confirmPassword(String password, String confirmPassword) {
		if (password.equals(confirmPassword)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean maxDate(Date dob) {
		Date today = new Date();
		if (dob.after(today)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isUserValid(long id, int type) {
		if (id == 0) {
			return true;
		}
		User user = repository.read(id);
		if (user != null) {
			if (type != 0) {
				if (user.getStatus() == type) {
					return true;
				}
				return false;
			}
			return true;
		}
		return false;
	}
}
