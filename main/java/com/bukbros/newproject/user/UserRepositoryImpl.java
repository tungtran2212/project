package com.bukbros.newproject.user;

import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bukbros.newproject.user.interfaces.UserRepositoryInterface;

@Repository
@Transactional
public class UserRepositoryImpl implements UserRepositoryInterface{
	@Autowired
	private SessionFactory sessionFactory;
	
	private Logger logger = LogManager.getLogger(UserRepositoryImpl.class);
	@Override
	public boolean create(User user) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<User> read(boolean isActive) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<User> criteria = builder.createQuery(User.class);
			Root<User> root = criteria.from(User.class);
			criteria.select(root);
			if (isActive) {
				criteria.where(builder.equal(root.get("status"), 1));
			}
			criteria.orderBy(builder.desc(root.get("id")));
			List<User> users = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !users.isEmpty() ? users : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean update(User user) {
		try {
			sessionFactory.getCurrentSession().update(user);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public boolean delete(User user) {
		try {
			sessionFactory.getCurrentSession().delete(user);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public User read(long id) {
		try {
			User user = sessionFactory.getCurrentSession().get(User.class, id);
			return user;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}		
	}

	@Override
	public boolean status(long id, int status) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaUpdate<User> criteria = builder.createCriteriaUpdate(User.class);
		    Root<User> root = criteria.from(User.class);
		    criteria.set(root.get("status"), status)
		    		.where(builder.equal(root.get("id"), id));
		    sessionFactory.getCurrentSession().createQuery(criteria).executeUpdate();
		    return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public User read(String uuid) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<User> criteria = builder.createQuery(User.class);
		    Root<User> root = criteria.from(User.class);
		    criteria.select(root)
		     		.where(builder.equal(root.get("uuid"), UUID.fromString(uuid)));
		    User user = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();//tra ve null khi ko tim dc
		    return user; 
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean isUsernameExisted(String username) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<String> criteria = builder.createQuery(String.class);
		    Root<User> root = criteria.from(User.class);
		    criteria.select(root.get("username")).where(builder.equal(root.get("username"), username));
		    String dbUsername = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
		    return username.equalsIgnoreCase(dbUsername) ? true : false;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public boolean isEmailExisted(String email) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<String> criteria = builder.createQuery(String.class);
		    Root<User> root = criteria.from(User.class);
		    criteria.select(root.get("email")).where(builder.equal(root.get("email"), email));
		    String dbEmail = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
		    return email.equalsIgnoreCase(dbEmail) ? true : false;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public User getUserByUsername(String username) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<User> criteria = builder.createQuery(User.class);
		    Root<User> root = criteria.from(User.class);
		    criteria.select(root).where(builder.equal(root.get("username"), username));
		    User user = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
		    return user;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean updatePassword(long id, String password) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaUpdate<User> criteria = builder.createCriteriaUpdate(User.class);
		    Root<User> root = criteria.from(User.class);
		    criteria.set(root.get("password"), password)
		    		.where(builder.equal(root.get("id"), id));
		    sessionFactory.getCurrentSession().createQuery(criteria).executeUpdate();
		    return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public boolean updateRole(long id, String role) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaUpdate<User> criteria = builder.createCriteriaUpdate(User.class);
		    Root<User> root = criteria.from(User.class);
		    criteria.set(root.get("role"), role)
		    		.where(builder.equal(root.get("id"), id));
		    sessionFactory.getCurrentSession().createQuery(criteria).executeUpdate();
		    return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public User createUser(User user) {
		try {
			sessionFactory.getCurrentSession().save(user);
			return (user.getId() > 0) ? user : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
		
	}

}
