package com.bukbros.newproject.user.interfaces;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.bukbros.newproject.interfaces.ServiceInterface;
import com.bukbros.newproject.user.User;

public interface UserServiceInterface extends ServiceInterface<User> {
	User create(HttpServletRequest request, User user);
	
	boolean update(HttpServletRequest request, User originalUser, User user);
	
	boolean delete(HttpServletRequest request, User user);
	
	boolean isUsernameExisted(String username);

	boolean isEmailExisted(String email);

	User getUserByUsername(String username);

	boolean updatePassword(long id, String password);

	boolean updateRole(long id, String role);
	
	boolean confirmPassword(String password, String confirmPassword);
	
	boolean maxDate(Date dob);
	
	boolean isUserValid(long id, int type);
}
