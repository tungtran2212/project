package com.bukbros.newproject.user.interfaces;

import com.bukbros.newproject.interfaces.RepositoryInterface;
import com.bukbros.newproject.user.User;

public interface UserRepositoryInterface extends RepositoryInterface<User> {
	boolean isUsernameExisted(String username);

	boolean isEmailExisted(String email);

	User getUserByUsername(String username);

	boolean updatePassword(long id, String password);

	boolean updateRole(long id, String role);
	
	User createUser(User user);
}
