package com.bukbros.newproject.user;

import java.util.HashMap;

public class UserLanguage {
	public static final String VALIDATION_USERNAME = "Tên truy cập phải có tối thiểu 4 và tối đa là 30 ký tự";
	public static final String VALIDATION_PASSWORD = "Mật khẩu phải có tối thiểu {min} và tối đa là {max} ký tự";
	public static final String VALIDATION_FIRSTNAME = "Tên không hợp lệ, vui lòng nhập lại";
	public static final String VALIDATION_LASTNAME = "Họ không hợp lệ, vui lòng nhập lại";
	public static final String VALIDATION_EMAIL = "Email không hợp lệ, vui lòng nhập đúng định dạng email";
	public static final String VALIDATION_DOB = "Ngày sinh không hợp lệ. Ví dụ: 20-11-1995";
	public static final String VALIDATION_ADDRESS = "Địa chỉ phải có tối thiểu {min} và tối đa là {max} ký tự";
	public static final String VALIDATION_PHONE = "Số điện thoại không hợp lệ";
	public static final String VALIDATION_AGREE = "Bạn chưa đồng ý với điều khoản";
	public static final String VALIDATION_CONFIRM_PASS = "Mật khẩu nhập lại không đúng";
	public static final String VALIDATION_SIZE_MIN_4_MAX_30 = "Chỉ được nhập từ 4 đến 30 ký tự";
	public static final String VALIDATION_SIZE_MIN_6_MAX_30 = "Chỉ được nhập từ 6 đến 30 ký tự";
	public static final String VALIDATION_REGEX_a_A_0 = "Chỉ được nhập chữ không dấu hoặc số";
	public static final String VALIDATION_REGEX_NOT_SPACE = "Không được nhập khoảng trắng";
	public static final String VALIDATION_MAX_50 = "Chỉ được nhập tối đa 50 ký tự";
	public static final String VALIDATION_PASS = "Mật khẩu không đúng";	
	public static final String VALIDATION_IMAGE_SIZE = "Dung lượng file vượt quá 512 kB (0.5 MB)";	
	public static final String VALIDATION_IMAGE_UPLOAD = "Định dang file không hợp lệ";
	public static final String VALIDATION_SPECIAL_CHARACTERS = "Không được sử dụng ký tự đặc biệt";	
	
	public static final String MESSAGE_EMAIL_VALID = "Email đã có người đăng ký. Vui lòng chọn email khác.";
	public static final String MESSAGE_USERNAME_VALID = "Tên truy cập đã có người đăng ký. Vui lòng chọn tên khác.";
	public static final String MESSAGE_FORGOT_PASS_SUCCESS = "Lấy lại mật khẩu thành công. Mật khẩu mới của bạn là: ";
	public static final String MESSAGE_FORGOT_PASS_FAILURE = "Lấy lại mật khẩu thất bại. Hãy thử lại !";
	public static final String MESSAGE_CHANGE_PASS_SUCCESS = "Đổi mật khẩu thành công";
	public static final String MESSAGE_CHANGE_PASS_FAILURE = "Đổi mật khẩu thất bại";	
	
	public static final String LABEL_NAME = "Tên";
	public static final String LABEL_USERNAME = "Tên truy cập";
	public static final String LABEL_EMAIL = "Email";
	
	public static final String LABEL_CREATE = "Thêm mới thành viên";
	public static final String LABEL_UPDATE = "Cập nhật thành viên";
	public static final String LABEL_LIST = "Danh sách thành viên";
	public static final String LABEL_DETAIL = "Chi tiết thành viên";
	public static final String LABEL_CHANGE_PASSWORD = "Thay đổi mật khẩu";
	
	public static final String USER_LOGIN_MESSAGE_INVALID = "<b>Có lỗi xảy ra !</b> Tên truy cập hoặc mật khẩu không đúng.";
	public static final String USER_LOGIN_MESSAGE_LOGOUT = "<b>Thành công !</b> Bạn đã đăng xuất khỏi hệ thống.";
	public static final String USER_MESSAGE_UNKNOWN = "<b>Có lỗi xảy ra !</b> Lỗi chưa xác định, vui lòng liên hệ ban quản trị.";
	public static final String USER_FORGOT_PASS_MESSAGE_INVALID = "Tên truy cập hoặc email không đúng.";
	public static final String USER_FORGOT_PASS_MESSAGE_SUCCESS = "Mật khẩu mới đã được gửi tới email của bạn.";
	
	public static HashMap<String, String> label() {
		HashMap<String, String> labels = new HashMap<>();
		labels.put("LABEL_NAME", LABEL_NAME);
		labels.put("LABEL_USERNAME", LABEL_USERNAME);
		labels.put("LABEL_EMAIL", LABEL_EMAIL);
		
		labels.put("LABEL_CREATE", LABEL_CREATE);
		labels.put("LABEL_UPDATE", LABEL_UPDATE);
		labels.put("LABEL_LIST", LABEL_LIST);
		labels.put("LABEL_DETAIL", LABEL_DETAIL);
		labels.put("LABEL_CHANGE_PASSWORD", LABEL_CHANGE_PASSWORD);
		return labels;
	}
}
