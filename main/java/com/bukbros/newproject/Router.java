package com.bukbros.newproject;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.newproject.classroom.interfaces.ClassroomServiceInterface;
import com.bukbros.newproject.configuration.Role;
import com.bukbros.newproject.configuration.Url;
import com.bukbros.newproject.configuration.View;
import com.bukbros.newproject.course.interfaces.CourseServiceInterface;
import com.bukbros.newproject.detail.Detail;
import com.bukbros.newproject.detail.DetailConfig;
import com.bukbros.newproject.detail.DetailLanguage;
import com.bukbros.newproject.detail.interfaces.DetailServiceInterface;
import com.bukbros.newproject.detailTime.DetailTime;
import com.bukbros.newproject.detailTime.interfaces.DetailTimeServiceInterface;
import com.bukbros.newproject.faculty.interfaces.FacultyServiceInterface;
import com.bukbros.newproject.function.StringFunction;
import com.bukbros.newproject.language.MessageLanguage;
import com.bukbros.newproject.mail.MailService;
import com.bukbros.newproject.mail.MailTemplate;
import com.bukbros.newproject.major.interfaces.MajorServiceInterface;
import com.bukbros.newproject.registration.Registration;
import com.bukbros.newproject.registration.RegistrationConfig;
import com.bukbros.newproject.registration.interfaces.RegistrationServiceInterface;
import com.bukbros.newproject.room.interfaces.RoomServiceInterface;
import com.bukbros.newproject.staff.Staff;
import com.bukbros.newproject.staff.interfaces.StaffServiceInterface;
import com.bukbros.newproject.student.Student;
import com.bukbros.newproject.student.StudentConfig;
import com.bukbros.newproject.student.StudentLanguage;
import com.bukbros.newproject.student.interfaces.StudentServiceInterface;
import com.bukbros.newproject.subject.interfaces.SubjectServiceInterface;
import com.bukbros.newproject.user.User;
import com.bukbros.newproject.user.UserLanguage;
import com.bukbros.newproject.user.interfaces.UserServiceInterface;

@Controller
public class Router {
	@Autowired
	private UserServiceInterface userService;

	@Autowired
	private DetailTimeServiceInterface detailTimeService;

	@Autowired
	private MailService mailService;

	@Autowired
	private DetailServiceInterface detailService;

	@Autowired
	private RegistrationServiceInterface registrationService;
	
	@Autowired
	private StaffServiceInterface staffService; 

	@Autowired
	private StudentServiceInterface studentService;
	
	@Autowired
	private FacultyServiceInterface facultyService;
	
	@Autowired
	private MajorServiceInterface majorService;
	
	@Autowired
	private CourseServiceInterface courseService;
	
	@Autowired
	private SubjectServiceInterface subjectService;
	
	@Autowired
	private ClassroomServiceInterface classroomService;
	
	@Autowired
	private RoomServiceInterface roomService;
	
	@ModelAttribute("studentLang")
	public HashMap<String, String> studentLang() {
		return StudentLanguage.labels();
	}
	
	@ModelAttribute("detailLang")
	public HashMap<String, String> detailLang() {
		return DetailLanguage.labels();
	}

	@GetMapping("/")
	public String home(Principal principal, RedirectAttributes redirectAttributes) {
		if (principal != null) {
			User currentUser = userService.getUserByUsername(principal.getName());
			if (currentUser == null) {
				redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
				return "redirect:" + Url.URL_LOGIN;
			}
			if (currentUser.getRole().equalsIgnoreCase(Role.ROLE_STUDENT) || currentUser.getRole().equalsIgnoreCase(Role.ROLE_STAFF)) {
				return "redirect:" + Url.URL_TIME_TABLE;
			}
			if (currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
				return "redirect:" + Url.URL_ADMIN;
			}
		}
		redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
		return "redirect:" + Url.URL_LOGIN;
	}

	@GetMapping(Url.URL_LOGIN)
	public String getLogin(HttpSession session, @RequestParam(value = "message", required = false) String message, Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			model.asMap().clear();
			return "redirect:/";
		}
		model.addAttribute("loginMode", true);
		if ((message != null) && (!message.isEmpty())) {
			if (message.equals("invalid")) {
				model.addAttribute("message", UserLanguage.USER_LOGIN_MESSAGE_INVALID);
			} else if (message.equals("logout")) {
				model.addAttribute("message", UserLanguage.USER_LOGIN_MESSAGE_LOGOUT);
			} else {
				model.addAttribute("message", UserLanguage.USER_MESSAGE_UNKNOWN);
			}
		}
		return "login";
	}

	@GetMapping(Url.URL_ADMIN)
	public String defaultSuccessUrl(Principal principal, Model model, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			return "redirect:/";
		}
		model.addAttribute("countStaff", staffService.countStaff());
		model.addAttribute("countStudent", studentService.countStudent());
		model.addAttribute("countFaculty", facultyService.countFaculty());
		model.addAttribute("countMajor", majorService.countMajor());
		model.addAttribute("countCourse", courseService.countCourse());
		model.addAttribute("countSubject", subjectService.countSubject());
		model.addAttribute("countClassroom", classroomService.countClassroom());
		model.addAttribute("countRoom", roomService.countRoom());
		return "admin/module/dashboard";
	}

	@GetMapping(Url.URL_ERROR_NOT_FOUND)
	public String errorUrl() {
		return "error-not-found";
	}

	@GetMapping(Url.URL_FORGOT_PASSWORD)
	public String getForgotPass(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			model.asMap().clear();
			return "redirect:/";
		}
		return "forgot-password";
	}

	@PostMapping(Url.URL_FORGOT_PASSWORD)
	public String postForgotPass(HttpSession session,
								 @ModelAttribute("forgotPassword") @Valid User user,
								 BindingResult results,
								 Model model,
								 RedirectAttributes redirectAttributes,
								 HttpServletRequest request) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			model.asMap().clear();
			return "redirect:/";
		}
		boolean hasError = false;
		if (results.hasFieldErrors("username") || results.hasFieldErrors("email")) {
			hasError = true;
		}
		User originalUser = userService.getUserByUsername(user.getUsername());
		if (originalUser == null) {
			hasError = true;
		} else if (!user.getEmail().equalsIgnoreCase(originalUser.getEmail())) {
			hasError = true;
		}
		if (hasError) {
			model.addAttribute("errorMessage", UserLanguage.USER_FORGOT_PASS_MESSAGE_INVALID);
			return "forgot-password";
		} else {
			String newPass = StringFunction.randomString(8);
			boolean updatePass = userService.updatePassword(originalUser.getId(), newPass);
			if (updatePass) {
				SimpleMailMessage mailMessage = MailTemplate.forgotPass(originalUser.getFirstName(), originalUser.getLastName(), newPass);
				mailService.sendMail(originalUser.getEmail(), mailMessage.getSubject(), mailMessage.getText());
				redirectAttributes.addFlashAttribute("successMessage", UserLanguage.USER_FORGOT_PASS_MESSAGE_SUCCESS);
			} else {
				model.addAttribute("errorMessage", MessageLanguage.MESSAGE_UPDATE_ERROR);
				return "forgot-password";
			}
		}
		return "redirect:" + Url.URL_LOGIN;
	}

	@GetMapping(Url.URL_TIME_TABLE)
	public String getTimeTable(Principal principal, Model model, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		List<DetailTime> timeTables = new ArrayList<DetailTime>();
		if (currentUser.getRole().equalsIgnoreCase(Role.ROLE_STUDENT)) {
			timeTables = detailTimeService.getTimeTableByStudent(currentUser.getId());
		} else {
			timeTables = detailTimeService.getTimeTableByStaff(currentUser.getId());
		}
		model.addAttribute("timeMenu", true);
		if (timeTables == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
		} else {
			model.addAttribute("timeTables", timeTables);
		}
		return View.VIEW_ADMIN_URL + View.VIEW_MODULE_FOLDER + RegistrationConfig.MODULE_TIMETABLE + View.VIEW_INDEX;
	}

	@GetMapping(Url.URL_CLASS + "/{uuidDetail}" + Url.URL_DETAIL)
	public String getDetail(@PathVariable("uuidDetail") String uuidDetail,
							Model model,
							Principal principal,
							RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		Detail originalDetail = detailService.read(uuidDetail);
		if ((originalDetail == null) || ((originalDetail.getStatus() != 2) && (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)))) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + RegistrationConfig.MODULE_URL;
		}
		if (currentUser.getRole().equalsIgnoreCase(Role.ROLE_STAFF) || (currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN))) {
			model.addAttribute("classMenu", true);
		}
		model.addAttribute("detailMode", true);
		model.addAttribute("detail", originalDetail);
		model.addAttribute("detailTimes", detailTimeService.getDetailTimeByDetailId(originalDetail.getId(), true));
		model.addAttribute("registrations", registrationService.getRegistrationByDetailId(originalDetail.getId()));
		return DetailConfig.MODULE_VIEW_ADMIN;
	}

	@GetMapping(Url.URL_CLASS)
	public String getClass(Model model,
						   Principal principal,
						   RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		Staff currentUser = staffService.getStaffByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		List<Detail> details = detailService.getDetailByStaff(currentUser.getUserId(), true);
		model.addAttribute("classMenu", true);
		model.addAttribute("managerMode", true);
		model.addAttribute("details", details);
		return DetailConfig.MODULE_VIEW_ADMIN;
	}
	
	@GetMapping(StudentConfig.MODULE_URL + "/{uuidStudent}" + Url.URL_POINT)
	public String getClass(@PathVariable("uuidStudent") String uuidStudent,
	                       Model model,
						   Principal principal,
						   RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		Student currentUser = studentService.read(uuidStudent);
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		List<Registration> registrations = registrationService.getRegistrationByStudent(currentUser.getUserId());
		model.addAttribute("pointMenu", true);
		model.addAttribute("pointMode", true);
		model.addAttribute("registrations", registrations);
		return StudentConfig.MODULE_VIEW_ADMIN;
	}
}
