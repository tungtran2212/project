package com.bukbros.newproject.student.interfaces;

import java.util.List;

import com.bukbros.newproject.interfaces.RepositoryInterface;
import com.bukbros.newproject.student.Student;

public interface StudentRepositoryInterface extends RepositoryInterface<Student>{
	Student getStudentByUsername(String username);
	
	List<Student> getStudentByClassroom(long classroomId, boolean isActive);
	
	long countStudent();
}
