package com.bukbros.newproject.student.interfaces;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.bukbros.newproject.interfaces.ServiceInterface;
import com.bukbros.newproject.student.Student;

public interface StudentServiceInterface extends ServiceInterface<Student>{
	Student getStudentByUsername(String username);
	
	boolean create(HttpServletRequest request, Student student);
	
	boolean update(HttpServletRequest request, Student originalStudent, Student student);
	
	List<Student> getStudentByClassroom(long classroomId, boolean isActive);
	
	long countStudent();
}
