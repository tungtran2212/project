package com.bukbros.newproject.student;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import com.bukbros.newproject.classroom.Classroom;
import com.bukbros.newproject.function.StringFunction;
import com.bukbros.newproject.user.User;

@Entity
@Table(name = StudentDatabase.TABLE)
public class Student {
	@Id
	@Column(name = StudentDatabase.COLUMN_USER_ID)
	private long userId;

	@Pattern(regexp = "^[\\p{L}\\d\\s]{0,100}$", message = StudentLanguage.VALIDATION_GUARDIAN)
	@Column(name = StudentDatabase.COLUMN_GUARDIAN)
	private String guardian;

	@Pattern(regexp = "^[0-9]{0,20}$", message = StudentLanguage.VALIDATION_PHONE)
	@Column(name = StudentDatabase.COLUMN_PHONE_GUARDIAN)
	private String phoneGuardian;

	@Valid
	@OneToOne(cascade = { CascadeType.REMOVE })
	@PrimaryKeyJoinColumn
	private User user;

	@ManyToOne
	@JoinColumn(name = StudentDatabase.COLUMN_CLASSROOM_ID)
	private Classroom classroom;

	public Student() {
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getGuardian() {
		return guardian;
	}

	public void setGuardian(String guardian) {
		this.guardian = StringFunction.trimSpace(guardian);
	}

	public String getPhoneGuardian() {
		return phoneGuardian;
	}

	public void setPhoneGuardian(String phoneGuardian) {
		this.phoneGuardian = StringFunction.trimSpace(phoneGuardian);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public Classroom getClassroom() {
		return classroom;
	}

	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
	}

}
