package com.bukbros.newproject.student;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bukbros.newproject.student.interfaces.StudentRepositoryInterface;

@Repository
@Transactional
public class StudentRepositoryImpl implements StudentRepositoryInterface{
	@Autowired
	private SessionFactory sessionFactory;
	
	private Logger logger = LogManager.getLogger(StudentRepositoryImpl.class);
	
	@Override
	public boolean create(Student student) {
		try {
			sessionFactory.getCurrentSession().persist(student);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public List<Student> read(boolean isActive) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Student> criteria = builder.createQuery(Student.class);
			Root<Student> root = criteria.from(Student.class);
			criteria.select(root);
			if (isActive) {
				criteria.where(builder.equal(root.get("user").get("status"), 1));
			}
			criteria.orderBy(builder.asc(root.get("user").get("username")));
			List<Student> students = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !students.isEmpty() ? students : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean update(Student student) {
		try {
			sessionFactory.getCurrentSession().update(student);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public boolean delete(Student student) {
		try {
			sessionFactory.getCurrentSession().delete(student);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public Student read(long id) {
		try {
			Student student = sessionFactory.getCurrentSession().get(Student.class, id);
			return student;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}		
	}

	@Override
	public boolean status(long id, int status) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaUpdate<Student> criteria = builder.createCriteriaUpdate(Student.class);
		    Root<Student> root = criteria.from(Student.class);
		    criteria.set(root.get("status"), status)
		    		.where(builder.equal(root.get("id"), id));
		    sessionFactory.getCurrentSession().createQuery(criteria).executeUpdate();
		    return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public Student read(String uuid) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<Student> criteria = builder.createQuery(Student.class);
		    Root<Student> root = criteria.from(Student.class);
		    criteria.select(root)
		     		.where(builder.equal(root.get("user").get("uuid"), UUID.fromString(uuid)));
		    Student student = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();//tra ve null khi ko tim dc
		    return student; 
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public Student getStudentByUsername(String username) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<Student> criteria = builder.createQuery(Student.class);
		    Root<Student> root = criteria.from(Student.class);
		    criteria.select(root).where(builder.equal(root.get("user").get("username"), username));
		    Student student = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
		    return student;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public List<Student> getStudentByClassroom(long classroomId, boolean isActive) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<Student> criteria = builder.createQuery(Student.class);
		    Root<Student> root = criteria.from(Student.class);
		    List<Predicate> predicates = new ArrayList<>();
			predicates.add(builder.equal(root.get("classroom"), classroomId));
			if (isActive) {
				predicates.add(builder.equal(root.get("status"), 1));
			}
		    criteria.select(root)
		    		.where(predicates.toArray(new Predicate[predicates.size()]))
		    		.orderBy(builder.asc(root.get("user").get("username")));
		    List<Student> students = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
		    return !students.isEmpty() ? students : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public long countStudent() {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
			Root<Student> root = criteria.from(Student.class);
			criteria.select(builder.count(root));
			long count = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
			return count;
		} catch (Exception e) {
			logger.error(e);
			return 0;
		}
	}

}
