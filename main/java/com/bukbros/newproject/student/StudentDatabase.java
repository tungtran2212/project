package com.bukbros.newproject.student;

public class StudentDatabase {
	public static final String TABLE = "cr_student";
	public static final String COLUMN_USER_ID = "user_id";
	public static final String COLUMN_MAJOR_ID = "major_id";
	public static final String COLUMN_GUARDIAN = "guardian";
	public static final String COLUMN_PHONE_GUARDIAN = "phone_guardian";
	public static final String COLUMN_CLASSROOM_ID = "classroom_id";
}
