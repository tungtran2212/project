package com.bukbros.newproject.student;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bukbros.newproject.configuration.Role;
import com.bukbros.newproject.student.interfaces.StudentRepositoryInterface;
import com.bukbros.newproject.student.interfaces.StudentServiceInterface;
import com.bukbros.newproject.user.User;
import com.bukbros.newproject.user.interfaces.UserServiceInterface;

@Service
public class StudentServiceImpl implements StudentServiceInterface{
	@Autowired
	private StudentRepositoryInterface repository;	
	
	@Autowired
	private UserServiceInterface userService;	
	
	@Override
	public boolean create(Student student) {
		return repository.create(student);
	}

	@Override
	public List<Student> read(boolean isActive) {
		return repository.read(isActive);
	}

	@Override
	public boolean update(long id, Student object) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(Student object) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Student read(long id) {
		return repository.read(id);
	}

	@Override
	public boolean status(long id, int status) {
		return repository.status(id, status);
	}

	@Override
	public Student read(String uuid) {
		return repository.read(uuid);
	}

	@Override
	public Student getStudentByUsername(String username) {
		return repository.getStudentByUsername(username);
	}
	
	@Override
	public boolean create(HttpServletRequest request, Student student) {		
		student.getUser().setRole(Role.ROLE_STUDENT);
		User newUser = userService.create(request, student.getUser());
		if (newUser != null) {
			student.setUserId(newUser.getId());
		} else {
			return false;
		}
		boolean createStudent = repository.create(student);
		if (!createStudent) {
			userService.delete(request, newUser);
		}
		return createStudent;
	}

	@Override
	@Transactional
	public boolean update(HttpServletRequest request, Student originalStudent, Student student) {
		student.setUserId(originalStudent.getUserId());
		boolean updateStudent = repository.update(student);
		if (updateStudent) {
			boolean updateUser = userService.update(request, originalStudent.getUser(), student.getUser());
			return updateUser;
		}
		return false;
	}

	@Override
	public List<Student> getStudentByClassroom(long classroomId, boolean isActive) {
		return repository.getStudentByClassroom(classroomId, isActive);
	}

	@Override
	public long countStudent() {
		return repository.countStudent();
	}
}
