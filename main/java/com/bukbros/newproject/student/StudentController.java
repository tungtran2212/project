package com.bukbros.newproject.student;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.newproject.classroom.ClassroomLanguage;
import com.bukbros.newproject.classroom.interfaces.ClassroomServiceInterface;
import com.bukbros.newproject.configuration.Role;
import com.bukbros.newproject.configuration.Url;
import com.bukbros.newproject.function.StringFunction;
import com.bukbros.newproject.language.LabelLanguage;
import com.bukbros.newproject.language.MessageLanguage;
import com.bukbros.newproject.student.interfaces.StudentServiceInterface;
import com.bukbros.newproject.upload.UploadService;
import com.bukbros.newproject.user.User;
import com.bukbros.newproject.user.UserConfig;
import com.bukbros.newproject.user.UserFunction;
import com.bukbros.newproject.user.UserLanguage;
import com.bukbros.newproject.user.interfaces.UserServiceInterface;

@Controller
@RequestMapping(StudentConfig.MODULE_ADMIN_URL)
public class StudentController {
	@Autowired
	private StudentServiceInterface service;

	@Autowired
	private ClassroomServiceInterface classroomService;

	@Autowired
	private UserServiceInterface userService;

	@Autowired
	private UploadService uploadService;

	@ModelAttribute("module")
	public HashMap<String, String> module() {
		return StudentConfig.component();
	}

	@ModelAttribute("moduleLang")
	public HashMap<String, String> moduleLang() {
		return StudentLanguage.labels();
	}

	@ModelAttribute("labelLang")
	public HashMap<String, String> labelLanguage() {
		return LabelLanguage.labels();
	}

	@ModelAttribute("studentMenu")
	public boolean studentMenu() {
		return true;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String getAllStudent(Principal principal, Model model, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("listMode", true);
		List<Student> students = service.read(false);
		if (students == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
		} else {
			model.addAttribute("students", students);
		}
		return StudentConfig.MODULE_VIEW_ADMIN;
	}

	@GetMapping(Url.URL_CREATE)
	public String getCreate(Model model, Principal principal, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("createMode", true);
		model.addAttribute("classrooms", classroomService.read(true));
		return StudentConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping(Url.URL_CREATE)
	public String postCreate(HttpServletRequest request,
							 Principal principal,
							 @ModelAttribute("validStudent") @Valid Student student,
							 BindingResult results,
							 Model model,
							 RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("createMode", true);
		model.addAttribute("student", student);
		model.addAttribute("classrooms", classroomService.read(true));
		MultipartFile avatarFile = student.getUser().getAvatarFile();
		if (avatarFile.getSize() > 0) {
			if (!uploadService.validFileSize(avatarFile, UserConfig.MAX_IMAGE_FILE_SIZE)) {
				results.addError(new FieldError("validStudent", "user.avatarFile", StudentLanguage.VALIDATION_IMAGE_SIZE));
			}
			if (!(uploadService.validFileExt(avatarFile, UserConfig.EXT_IMAGE_FILE_UPLOAD))) {
				results.addError(new FieldError("validStudent", "user.avatarFile", StudentLanguage.VALIDATION_IMAGE_UPLOAD));
			}
		} else {
			results.addError(new FieldError("validStudent", "user.avatarFile", StudentLanguage.VALIDATION_NOT_EMPTY));
		}
		if (results.hasErrors()) {
			return StudentConfig.MODULE_VIEW_ADMIN;
		} else {
			boolean isUsernameExisted = userService.isUsernameExisted(student.getUser().getUsername());
			if (isUsernameExisted) {
				results.addError(new FieldError("validStudent", "user.username", UserLanguage.MESSAGE_USERNAME_VALID));
			}
			boolean isEmailExisted = userService.isEmailExisted(student.getUser().getEmail());
			if (isEmailExisted) {
				results.addError(new FieldError("validStudent", "user.email", UserLanguage.MESSAGE_EMAIL_VALID));
			}
			boolean confirmPassword = userService.confirmPassword(student.getUser().getPassword(), student.getUser().getConfirmPassword());
			if (!confirmPassword) {
				results.addError(new FieldError("validStudent", "user.confirmPassword", UserLanguage.VALIDATION_CONFIRM_PASS));
			}
			boolean maxDate = userService.maxDate(student.getUser().getDob());
			if (maxDate) {
				results.addError(new FieldError("validStudent", "user.dob", UserLanguage.VALIDATION_DOB));
			}
			boolean validClassroom = classroomService.isClassroomValid(student.getClassroom().getId(), 1);
			if (!validClassroom) {
				results.addError(new FieldError("validStudent", "classroom.id", ClassroomLanguage.VALIDATON_CLASSROOM));
			}
			if (results.hasFieldErrors("user.username") || results.hasFieldErrors("user.email") || results.hasFieldErrors("user.confirmPassword") ||
					results.hasFieldErrors("user.dob") || results.hasFieldErrors("classroom.id")) {
				return StudentConfig.MODULE_VIEW_ADMIN;
			}
		}
		boolean createItem = service.create(request, student);
		if (createItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_CREATE_SUCCESS);
		} else {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_CREATE_ERROR);
			return StudentConfig.MODULE_VIEW_ADMIN;
		}
		model.asMap().clear();
		return "redirect:" + Url.URL_STUDENT;
	}

	@GetMapping("/{uuid}" + Url.URL_UPDATE)
	public String getupdate(@PathVariable("uuid") String uuid,
							Principal principal,
							Model model,
							RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Student originalStudent = service.read(uuid);
		if (originalStudent == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_STUDENT;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("classrooms", classroomService.getClassroomsInUpdate(originalStudent.getClassroom().getId()));
		model.addAttribute("student", originalStudent);
		model.addAttribute("originalStudent", originalStudent);
		return StudentConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping("/{uuid}" + Url.URL_UPDATE)
	public String postUpdate(@PathVariable("uuid") String uuid,
							 HttpServletRequest request,
							 Principal principal,
							 @ModelAttribute("validStudent") @Valid Student student,
							 BindingResult results,
							 Model model,
							 RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Student originalStudent = service.read(uuid);
		if (originalStudent == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_STUDENT;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("classrooms", classroomService.getClassroomsInUpdate(originalStudent.getClassroom().getId()));
		model.addAttribute("student", student);
		model.addAttribute("originalStudent", originalStudent);
		MultipartFile avatarFile = student.getUser().getAvatarFile();
		if (avatarFile.getSize() > 0) {
			if (!uploadService.validFileSize(avatarFile, UserConfig.MAX_IMAGE_FILE_SIZE)) {
				results.addError(new FieldError("validStudent", "user.avatarFile", UserLanguage.VALIDATION_IMAGE_SIZE));
			}
			if (!(uploadService.validFileExt(avatarFile, UserConfig.EXT_IMAGE_FILE_UPLOAD))) {
				results.addError(new FieldError("validStudent", "user.avatarFile", UserLanguage.VALIDATION_IMAGE_UPLOAD));
			}
		}
		if (results.hasErrors()) {
			return StudentConfig.MODULE_VIEW_ADMIN;
		} else {
			if (!StringFunction.trimSpace(student.getUser().getEmail()).equalsIgnoreCase(originalStudent.getUser().getEmail())) {
				boolean isEmailExisted = userService.isEmailExisted(StringFunction.trimSpace(student.getUser().getEmail()));
				if (isEmailExisted) {
					results.addError(new FieldError("validStudent", "user.email", UserLanguage.MESSAGE_EMAIL_VALID));
				}
			}
			boolean maxDate = userService.maxDate(student.getUser().getDob());
			if (maxDate) {
				results.addError(new FieldError("validStudent", "user.dob", UserLanguage.VALIDATION_DOB));
			}
			boolean validClassroom = false;
			if (originalStudent.getClassroom().getId() == student.getClassroom().getId()) {
				validClassroom = classroomService.isClassroomValid(student.getClassroom().getId(), 0);
			} else {
				validClassroom = classroomService.isClassroomValid(student.getClassroom().getId(), 1);
			}
			if (!validClassroom) {
				results.addError(new FieldError("validStudent", "classroom.id", ClassroomLanguage.VALIDATON_CLASSROOM));
			}
			if (results.hasFieldErrors("user.email") || results.hasFieldErrors("user.dob") || results.hasFieldErrors("classroom.id")) {
				return StudentConfig.MODULE_VIEW_ADMIN;
			}
		}
		if (service.update(request, originalStudent, student)) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_UPDATE_SUCCESS);
		} else {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_UPDATE_ERROR);
			return StudentConfig.MODULE_VIEW_ADMIN;
		}
		model.asMap().clear();
		return "redirect:" + Url.URL_STUDENT;
	}

	@GetMapping("/{uuid}" + Url.URL_DETAIL)
	public String getDetail(@PathVariable("uuid") String uuid, Principal principal, Model model, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		Student student = service.read(uuid);
		if (student == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("detailMode", true);
		model.addAttribute("student", student);
		return StudentConfig.MODULE_VIEW_ADMIN;
	}

	@GetMapping(Url.URL_CHANGE_PASSWORD)
	public String getChangePassword(Principal principal, Model model, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		Student originalStudent = service.getStudentByUsername(principal.getName());
		if (originalStudent == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("changeCurrentPassMode", true);
		return StudentConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping(Url.URL_CHANGE_PASSWORD)
	public String postChangePassword(Principal principal,
									 @ModelAttribute("changePassword") @Valid Student student,
									 BindingResult results,
									 Model model,
									 RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		Student originalStudent = service.getStudentByUsername(principal.getName());
		if (originalStudent == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("changeCurrentPassMode", true);
		boolean checkPassword = UserFunction.checkPassword(student.getUser().getOldPassword(), originalStudent.getUser().getPassword());
		if (!checkPassword) {
			results.addError(new FieldError("changePassword", "user.oldPassword", UserLanguage.VALIDATION_PASS));
		}
		boolean confirmPassword = userService.confirmPassword(student.getUser().getPassword(), student.getUser().getConfirmPassword());
		if (!confirmPassword) {
			results.addError(new FieldError("changePassword", "user.confirmPassword", UserLanguage.VALIDATION_CONFIRM_PASS));
		}
		if (results.hasFieldErrors("user.password") || results.hasFieldErrors("user.oldPassword") || results.hasFieldErrors("confirmPassword")) {
			return StudentConfig.MODULE_VIEW_ADMIN;
		}
		if (userService.updatePassword(originalStudent.getUser().getId(), student.getUser().getPassword())) {
			redirectAttributes.addFlashAttribute("successMessage", UserLanguage.MESSAGE_CHANGE_PASS_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", UserLanguage.MESSAGE_CHANGE_PASS_FAILURE);
		}
		return "redirect:" + Url.URL_ADMIN;
	}

	@GetMapping("/{uuid}" + Url.URL_CHANGE_PASSWORD)
	public String getChangePassword(@PathVariable("uuid") String uuid,
									Principal principal,
									Model model,
									RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Student originalStudent = service.read(uuid);
		if (originalStudent == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_STUDENT;
		}
		model.addAttribute("changePassMode", true);
		model.addAttribute("student", originalStudent);
		return StudentConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping("/{uuid}" + Url.URL_CHANGE_PASSWORD)
	public String postChangePassword(@PathVariable("uuid") String uuid,
									 Principal principal,
									 @ModelAttribute("changePassword") @Valid Student student,
									 BindingResult results,
									 Model model,
									 RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Student originalStudent = service.read(uuid);
		if (originalStudent == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_STUDENT;
		}
		model.addAttribute("changePassMode", true);
		model.addAttribute("student", originalStudent);
		boolean confirmPassword = userService.confirmPassword(student.getUser().getPassword(), student.getUser().getConfirmPassword());
		if (!confirmPassword) {
			results.addError(new FieldError("changePassword", "user.confirmPassword", UserLanguage.VALIDATION_CONFIRM_PASS));
		}
		if (results.hasFieldErrors("user.password") || results.hasFieldErrors("user.confirmPassword")) {
			return StudentConfig.MODULE_VIEW_ADMIN;
		}
		if (userService.updatePassword(originalStudent.getUser().getId(), student.getUser().getPassword())) {
			redirectAttributes.addFlashAttribute("successMessage", UserLanguage.MESSAGE_CHANGE_PASS_SUCCESS);
		} else {
			model.addAttribute("errorMessage", UserLanguage.MESSAGE_CHANGE_PASS_FAILURE);
			return StudentConfig.MODULE_VIEW_ADMIN;
		}
		model.asMap().clear();
		return "redirect:" + Url.URL_STUDENT;
	}

	@PostMapping("/{uuid}" + Url.URL_ACTIVE)
	public String postActive(@PathVariable("uuid") String uuid, Principal principal, Model model, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Student originalStudent = service.read(uuid);
		if (originalStudent == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + StudentConfig.MODULE_ADMIN_URL;
		}
		boolean activeItem = userService.status(originalStudent.getUserId(), 1);
		if (activeItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_ACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ACTIVE_ERROR);
		}
		return "redirect:" + StudentConfig.MODULE_ADMIN_URL;
	}

	@PostMapping("/{uuid}" + Url.URL_DEACTIVE)
	public String postDeactive(@PathVariable("uuid") String uuid, Principal principal, Model model, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + StudentConfig.MODULE_ADMIN_URL;
		}
		Student originalStudent = service.read(uuid);
		if (originalStudent == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_STAFF;
		}
		boolean deactiveItem = userService.status(originalStudent.getUserId(), 0);
		if (deactiveItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DEACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DEACTIVE_ERROR);
		}
		return "redirect:" + StudentConfig.MODULE_ADMIN_URL;
	}
}
