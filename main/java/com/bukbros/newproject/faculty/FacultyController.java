package com.bukbros.newproject.faculty;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.newproject.configuration.Role;
import com.bukbros.newproject.configuration.Url;
import com.bukbros.newproject.faculty.interfaces.FacultyServiceInterface;
import com.bukbros.newproject.language.LabelLanguage;
import com.bukbros.newproject.language.MessageLanguage;
import com.bukbros.newproject.major.Major;
import com.bukbros.newproject.major.MajorLanguage;
import com.bukbros.newproject.major.interfaces.MajorServiceInterface;
import com.bukbros.newproject.staff.Staff;
import com.bukbros.newproject.staff.StaffLanguage;
import com.bukbros.newproject.staff.interfaces.StaffServiceInterface;
import com.bukbros.newproject.user.User;
import com.bukbros.newproject.user.interfaces.UserServiceInterface;

@Controller
@RequestMapping(FacultyConfig.MODULE_ADMIN_URL)
public class FacultyController {
	@Autowired
	private FacultyServiceInterface service;

	@Autowired
	private UserServiceInterface userService;
	
	@Autowired
	private StaffServiceInterface staffService;
	
	@Autowired
	private MajorServiceInterface majorService;

	@ModelAttribute("module")
	public HashMap<String, String> module() {
		return FacultyConfig.component();
	}

	@ModelAttribute("moduleLang")
	public HashMap<String, String> moduleLang() {
		return FacultyLanguage.label();
	}

	@ModelAttribute("majorLang")
	public HashMap<String, String> majorLang() {
		return MajorLanguage.labels();
	}
	
	@ModelAttribute("staffLang")
	public HashMap<String, String> staffLang() {
		return StaffLanguage.labels();
	}
	
	@ModelAttribute("labelLang")
	public HashMap<String, String> labelLanguage() {
		return LabelLanguage.labels();
	}

	@ModelAttribute("facultyMenu")
	public boolean facultyMenu() {
		return true;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String getAllFaculty(Principal principal, Model model, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("listMode", true);
		List<Faculty> faculties = service.read(false);
		if (faculties == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
		} else {
			model.addAttribute("faculties", faculties);
		}
		return FacultyConfig.MODULE_VIEW_ADMIN;
	}

	@GetMapping(Url.URL_CREATE)
	public String getCreate(Model model,
							Principal principal,
							RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("createMode", true);
		return FacultyConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping(Url.URL_CREATE)
	public String postCreate(@ModelAttribute("validFaculty") @Valid Faculty faculty,
							 BindingResult results,
							 Principal principal,
							 Model model,
							 RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		model.addAttribute("createMode", true);
		model.addAttribute("faculty", faculty);
		if (results.hasErrors()) {
			return FacultyConfig.MODULE_VIEW_ADMIN;
		} else {
			boolean isNameExisted = service.isNameExisted(faculty.getName(), 0);
			if (isNameExisted) {
				results.addError(new FieldError("validFaculty", "name", FacultyLanguage.IS_NAME_EXISTED));
			}
			if (results.hasFieldErrors("name")) {
				return FacultyConfig.MODULE_VIEW_ADMIN;
			}
		}
		boolean createItem = service.create(faculty);
		if (createItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_CREATE_SUCCESS);
		} else {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_CREATE_ERROR);
			return FacultyConfig.MODULE_VIEW_ADMIN;
		}
		model.asMap().clear();
		return "redirect:" + Url.URL_FACULTY;
	}

	@GetMapping("/{uuid}" + Url.URL_UPDATE)
	public String getUpdate(@PathVariable("uuid") String uuid,
							Principal principal,
							Model model,
							RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Faculty originalFaculty = service.read(uuid);
		if (originalFaculty == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_FACULTY;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("faculty", originalFaculty);
		return FacultyConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping("/{uuid}" + Url.URL_UPDATE)
	public String postUpdate(@PathVariable("uuid") String uuid,
							 @ModelAttribute("validFaculty") @Valid Faculty faculty,
							 BindingResult results,
							 Principal principal,
							 Model model,
							 RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Faculty originalFaculty = service.read(uuid);
		if (originalFaculty == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_FACULTY;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("faculty", faculty);
		if (results.hasErrors()) {
			return FacultyConfig.MODULE_VIEW_ADMIN;
		} else {
			if (!faculty.getName().equalsIgnoreCase(originalFaculty.getName())) {
				boolean isNameExisted = service.isNameExisted(faculty.getName(), originalFaculty.getId());
				if (isNameExisted) {
					results.addError(new FieldError("validFaculty", "name", FacultyLanguage.IS_NAME_EXISTED));
				}
			}
			if (results.hasFieldErrors("name")) {
				return FacultyConfig.MODULE_VIEW_ADMIN;
			}
		}
		if (service.update(originalFaculty.getId(), faculty)) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_UPDATE_SUCCESS);
		} else {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_UPDATE_ERROR);
			return FacultyConfig.MODULE_VIEW_ADMIN;
		}
		model.asMap().clear();
		return "redirect:" + Url.URL_FACULTY;
	}

	@PostMapping("/{uuid}" + Url.URL_ACTIVE)
	public String postActive(@PathVariable("uuid") String uuid, Principal principal, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Faculty originalFaculty = service.read(uuid);
		if (originalFaculty == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_FACULTY;
		}
		boolean active = service.status(originalFaculty.getId(), 1);
		if (active) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_ACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ACTIVE_ERROR);
		}
		return "redirect:" + Url.URL_FACULTY;
	}

	@PostMapping("/{uuid}" + Url.URL_DEACTIVE)
	public String postDeactive(@PathVariable("uuid") String uuid, Principal principal, RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Faculty originalFaculty = service.read(uuid);
		if (originalFaculty == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_FACULTY;
		}
		boolean deactive = service.status(originalFaculty.getId(), 0);
		if (deactive) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DEACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DEACTIVE_ERROR);
		}
		return "redirect:" + Url.URL_FACULTY;
	}

	@PostMapping("/{uuid}" + Url.URL_DELETE)
	public String postDelete(@PathVariable("uuid") String uuid,
							 Model model,
							 Principal principal,
							 RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Faculty originalFaculty = service.read(uuid);
		if (originalFaculty == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_FACULTY;
		}
		boolean isFacultyUsedStaff = service.isFacultyUsed(originalFaculty.getId(), "faculty", Staff.class);
		boolean isFacultyUsedMajor = service.isFacultyUsed(originalFaculty.getId(), "faculty", Major.class);
		if (isFacultyUsedMajor || isFacultyUsedStaff) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_CONSTRAIN);
			return "redirect:" + Url.URL_FACULTY + "/" + originalFaculty.getUuid() + Url.URL_UPDATE;
		}
		boolean delete = service.delete(originalFaculty);
		if (delete) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DELETE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DELETE_ERROR);
			return "redirect:" + Url.URL_FACULTY + "/" + originalFaculty.getUuid() + Url.URL_UPDATE;
		}
		return "redirect:" + Url.URL_FACULTY;
	}

	@GetMapping("/{uuid}" + Url.URL_DETAIL)
	public String getDetail(@PathVariable("uuid") String uuid,
							Principal principal,
							Model model,
							RedirectAttributes redirectAttributes) {
		if (principal == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_UNAUTHORIZED);
			return "redirect:" + Url.URL_LOGIN;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (currentUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_LOGIN;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_FORBIDDEN);
			return "redirect:" + Url.URL_ADMIN;
		}
		Faculty originalFaculty = service.read(uuid);
		if (originalFaculty == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ERROR_NOT_FOUND);
			return "redirect:" + Url.URL_FACULTY;
		}
		model.addAttribute("detailMode", true);
		model.addAttribute("faculty", originalFaculty);
		model.addAttribute("staffs", staffService.getStaffByFaculty(originalFaculty.getId(), false));
		model.addAttribute("majors", majorService.getMajorByFaculty(originalFaculty.getId(), false));
		return FacultyConfig.MODULE_VIEW_ADMIN;
	}
}
