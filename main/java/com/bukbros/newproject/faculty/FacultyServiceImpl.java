package com.bukbros.newproject.faculty;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bukbros.newproject.faculty.interfaces.FacultyRepositoryInterface;
import com.bukbros.newproject.faculty.interfaces.FacultyServiceInterface;
@Service
public class FacultyServiceImpl implements FacultyServiceInterface{
	@Autowired
	private FacultyRepositoryInterface repository;

	@Override
	public boolean create(Faculty faculty) {
		return repository.create(faculty);
	}

	@Override
	public List<Faculty> read(boolean isActive) {
		return repository.read(isActive);
	}

	@Override
	public boolean update(long id, Faculty faculty) {
		faculty.setId(id);
		return repository.update(faculty);
	}

	@Override
	public boolean delete(Faculty faculty) {
		return repository.delete(faculty);
	}

	@Override
	public Faculty read(long id) {
		return repository.read(id);
	}

	@Override
	public boolean status(long id, int status) {
		return repository.status(id, status);
	}

	@Override
	public Faculty read(String uuid) {
		return repository.read(uuid);
	}

	@Override
	public boolean isNameExisted(String name, long id) {
		return repository.isNameExisted(name, id);
	}

	@Override
	public List<Faculty> getFacultiesInUpdate(long id) {
		return repository.getFacultiesInUpdate(id);
	}

	@Override
	public boolean isFacultyValid(long id, int type) {
		if (id == 0) {
			return true;
		}
		Faculty faculty = repository.read(id);
		if (faculty != null) {
			if (type != 0) {
				if (faculty.getStatus() == type) {
					return true;
				}
				return false;
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean isFacultyUsed(long id, String reference, Class<? extends Object> referenceClass) {
		return repository.isFacultyUsed(id, reference, referenceClass);
	}

	@Override
	public long countFaculty() {
		return repository.countFaculty();
	}

}
