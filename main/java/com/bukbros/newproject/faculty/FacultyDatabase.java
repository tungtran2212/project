package com.bukbros.newproject.faculty;

public class FacultyDatabase {
	public static final String TABLE = "cr_faculty";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_UUID = "uuid";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_STATUS = "status";
}
