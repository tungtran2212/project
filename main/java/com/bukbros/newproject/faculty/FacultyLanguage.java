package com.bukbros.newproject.faculty;

import java.util.HashMap;

public class FacultyLanguage {
	public static final String VALIDATION_NAME = "Tên khoa không hợp lệ";
	public static final String VALIDATION_FACULTY = "Khoa không hợp lệ. Vui lòng chọn lại";
	
	public static final String IS_NAME_EXISTED = "Tên khoa đã tồn tại. Vui lòng nhập lại. ";
	
	public static final String LABEL_NAME = "Tên khoa";
	
	public static final String LABEL_CREATE = "Thêm mới khoa";
	public static final String LABEL_UPDATE = "Cập nhật khoa";
	public static final String LABEL_LIST = "Danh sách khoa";
	
	public static HashMap<String, String> label() {
		HashMap<String, String> labels = new HashMap<>();
		labels.put("LABEL_NAME", LABEL_NAME);
		
		labels.put("LABEL_CREATE", LABEL_CREATE);
		labels.put("LABEL_UPDATE", LABEL_UPDATE);
		labels.put("LABEL_LIST", LABEL_LIST);
		return labels;
	}
}
