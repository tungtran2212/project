package com.bukbros.newproject.faculty;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bukbros.newproject.faculty.interfaces.FacultyRepositoryInterface;

@Repository
@Transactional
public class FacultyRepositoryImpl implements FacultyRepositoryInterface{
	@Autowired
	private SessionFactory sessionFactory;
	
	private Logger logger = LogManager.getLogger(FacultyRepositoryImpl.class);
	
	@Override
	public boolean create(Faculty faculty) {
		try {
			sessionFactory.getCurrentSession().persist(faculty);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public List<Faculty> read(boolean isActive) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Faculty> criteria = builder.createQuery(Faculty.class);
			Root<Faculty> root = criteria.from(Faculty.class);
			criteria.select(root);
			if (isActive) {
				criteria.where(builder.equal(root.get("status"), 1));
			}
			criteria.orderBy(builder.desc(root.get("id")));
			List<Faculty> faculties = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !faculties.isEmpty() ? faculties : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean update(Faculty faculty) {
		try {
			sessionFactory.getCurrentSession().update(faculty);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public boolean delete(Faculty faculty) {
		try {
			sessionFactory.getCurrentSession().delete(faculty);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public Faculty read(long id) {
		try {
			Faculty faculties = sessionFactory.getCurrentSession().get(Faculty.class, id);
			return faculties;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}		
	}

	@Override
	public boolean status(long id, int status) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaUpdate<Faculty> criteria = builder.createCriteriaUpdate(Faculty.class);
		    Root<Faculty> root = criteria.from(Faculty.class);
		    criteria.set(root.get("status"), status)
		    		.where(builder.equal(root.get("id"), id));
		    sessionFactory.getCurrentSession().createQuery(criteria).executeUpdate();
		    return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public Faculty read(String uuid) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
	        CriteriaQuery<Faculty> criteria = builder.createQuery(Faculty.class);
		    Root<Faculty> root = criteria.from(Faculty.class);
		    criteria.select(root)
		     		.where(builder.equal(root.get("uuid"), UUID.fromString(uuid)));
		    Faculty faculties = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();//tra ve null khi ko tim dc
		    return faculties; 
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean isNameExisted(String name, long id) {
		try {
			String dbName;
			String sql = "SELECT " + FacultyDatabase.COLUMN_NAME
								   + " FROM " + FacultyDatabase.TABLE
								   + " WHERE " + FacultyDatabase.COLUMN_NAME + " REGEXP :" + FacultyDatabase.COLUMN_NAME;
			String nameRegex = "^" + name + "$";
			if (id == 0) {
				dbName = sessionFactory.getCurrentSession().createSQLQuery(sql)
						   							.setParameter(FacultyDatabase.COLUMN_NAME, nameRegex)
						   							.uniqueResult().toString();
			} else {
				sql += " AND " + FacultyDatabase.COLUMN_ID + " != :" + FacultyDatabase.COLUMN_ID;
        		dbName = sessionFactory.getCurrentSession().createSQLQuery(sql)
        											.setParameter(FacultyDatabase.COLUMN_NAME, nameRegex)
        											.setParameter(FacultyDatabase.COLUMN_ID, id)
        											.uniqueResult().toString();
			}
			return name.equalsIgnoreCase(dbName) ? true : false;
		} catch(Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Override
	public List<Faculty> getFacultiesInUpdate(long id) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Faculty> criteria = builder.createQuery(Faculty.class);
			Root<Faculty> root = criteria.from(Faculty.class);
			criteria.select(root)
					.where(builder.or(builder.equal(root.get("status"), 1),
									  builder.equal(root.get("id"), id)))
					.orderBy(builder.asc(root.get("id")));
			List<Faculty> faculties = sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
			return !faculties.isEmpty() ? faculties : null;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public boolean isFacultyUsed(long id, String reference, Class<? extends Object> referenceClass) {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
			Root<?> root = criteria.from(referenceClass);
			List<Predicate> predicates = new ArrayList<>();
			predicates.add(builder.equal(root.get(reference), id));
			criteria.select(builder.count(root))
					.where(predicates.toArray(new Predicate[predicates.size()]));
			int count = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult().intValue();
			return count > 0 ? true : false;
		} catch (Exception e) {
			logger.error(e);
			return true;
		}
	}

	@Override
	public long countFaculty() {
		try {
			CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
			Root<Faculty> root = criteria.from(Faculty.class);
			criteria.select(builder.count(root));
			long count = sessionFactory.getCurrentSession().createQuery(criteria).uniqueResult();
			return count;
		} catch (Exception e) {
			logger.error(e);
			return 0;
		}
	}

}
