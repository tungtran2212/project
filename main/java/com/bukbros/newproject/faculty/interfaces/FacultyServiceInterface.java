package com.bukbros.newproject.faculty.interfaces;

import java.util.List;

import com.bukbros.newproject.faculty.Faculty;
import com.bukbros.newproject.interfaces.ServiceInterface;

public interface FacultyServiceInterface extends ServiceInterface<Faculty>{
	boolean isNameExisted(String name, long id);
	
	List<Faculty> getFacultiesInUpdate(long id);
	
	boolean isFacultyValid(long id, int type);
	
	boolean isFacultyUsed(long id, String reference, Class<? extends Object> referenceClass);
	
	long countFaculty();
}
