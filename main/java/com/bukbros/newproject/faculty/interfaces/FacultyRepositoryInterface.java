package com.bukbros.newproject.faculty.interfaces;

import java.util.List;

import com.bukbros.newproject.faculty.Faculty;
import com.bukbros.newproject.interfaces.RepositoryInterface;

public interface FacultyRepositoryInterface extends RepositoryInterface<Faculty>{
	boolean isNameExisted(String name, long id);
	
	List<Faculty> getFacultiesInUpdate(long id);
	
	boolean isFacultyUsed(long id, String reference, Class<? extends Object> referenceClass);
	
	long countFaculty();
}
