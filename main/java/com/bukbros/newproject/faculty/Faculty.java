package com.bukbros.newproject.faculty;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import com.bukbros.newproject.function.StringFunction;

@Entity
@Table(name = FacultyDatabase.TABLE)
public class Faculty {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = FacultyDatabase.COLUMN_ID)
	private long id;
	
	@Column(name = FacultyDatabase.COLUMN_UUID, updatable = false)
	private UUID uuid;

	@Pattern(regexp = "^[^0-9!@~`#$?%^&*():;\\[\\]{}\\\\/|<>\"']{1,100}$", message = FacultyLanguage.VALIDATION_NAME)
	@Column(name = FacultyDatabase.COLUMN_NAME)
	private String name;
	
	@Column(name = FacultyDatabase.COLUMN_STATUS)
	private byte status;
	
	public Faculty() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = StringFunction.trimSpace(name);
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}
	
	@PrePersist // random UUID trc khi tao moi
	public void prePersist() {
		uuid = UUID.randomUUID();
	}
}
