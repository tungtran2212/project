$(document).ready(function() { 	
    $('.delete-item').click(function() {    	
    	var id = $(this).attr("data-value");
    	$.confirm({
    	    title: '<i class="fas fa-exclamation-triangle text-warning"></i> Thông báo',
    	    content: 'Bạn có chắc chắn muốn tạm dừng dữ liệu này ?',
    	    buttons: {
    	        confirm: {
    	        	 text: '<i class="fas fa-lock"></i>&nbsp; Tạm dừng ',
    	             btnClass: 'btn-red',
    	             keys: ['enter'],
    	             action: function(){
    	            	 $('#item-'+id).submit();
    	             }    	        	
    	        },
    	        cancel: {
    	        	 text: '<i class="fas fa-ban"></i>&nbsp; Hủy',   	             
	   	             action: function(){
	   	            	 close();
	   	             }   
    	        }
    	    }
    	});
    	return false;
    });
    
    $('.restore-item').click(function() {
    	var id = $(this).attr("data-value");
    	$.confirm({
    	    title: '<i class="fas fa-exclamation-triangle text-warning"></i> Thông báo',
    	    content: 'Bạn có chắc chắn muốn khôi phục lại dữ liệu này ?',
    	    buttons: {
    	        confirm: {
    	        	 text: '<i class="fas fa-lock-open"></i>&nbsp; Khôi phục ',
    	             btnClass: 'btn-blue',
    	             keys: ['enter'],
    	             action: function(){
    	            	 $('#item-'+id).submit();
    	             }    	        	
    	        },
    	        cancel: {
    	        	 text: '<i class="fas fa-ban"></i>&nbsp; Hủy',   	             
	   	             action: function(){
	   	            	 close();
	   	             }   
    	        }
    	    }
    	});
    	return false;
    });
    
    $('.complete-remove').click(function() {
    	var id = $(this).attr("data-value");
    	$.confirm({
    	    title: '<i class="fas fa-exclamation-triangle text-danger"></i> Thông báo',
    	    content: 'Bạn có chắc chắn muốn xóa hoàn toàn dữ liệu này ?',
    	    buttons: {
    	        confirm: {
    	        	 text: '<i class="fas fa-trash-alt"></i>&nbsp; Xóa hoàn toàn ',
    	             btnClass: 'btn-red',
    	             keys: ['enter'],
    	             action: function(){
    	            	 $('#completeRemove-'+id).submit();
    	             }    	        	
    	        },
    	        cancel: {
    	        	 text: '<i class="fas fa-ban"></i>&nbsp; Hủy',   	             
	   	             action: function(){
	   	            	 close();
	   	             }   
    	        }
    	    }
    	});
    	return false;
    });
     
    $('.role-admin-update').click(function() {
    	var id = $(this).attr("data-value");
    	$.confirm({
    	    title: '<i class="fas fa-exclamation-triangle text-warning"></i> Thông báo',
    	    content: 'Bạn có chắc chắn thiết lập quyền ADMIN cho người dùng này ?',
    	    buttons: {
    	        confirm: {
    	        	 text: '<i class="fas fa-check"></i>&nbsp; Đồng ý ',
    	             btnClass: 'btn-red',
    	             keys: ['enter'],
    	             action: function(){
    	            	 $('#role-admin-update-'+id).submit();
    	             }    	        	
    	        },
    	        cancel: {
    	        	 text: '<i class="fas fa-ban"></i>&nbsp; Hủy',   	             
	   	             action: function(){
	   	            	 close();
	   	             }   
    	        }
    	    }
    	});
    	return false;
    });
    
    $('.role-user-update').click(function() {
    	var id = $(this).attr("data-value");
    	$.confirm({
    	    title: '<i class="fas fa-exclamation-triangle text-warning"></i> Thông báo',
    	    content: 'Bạn có chắc chắn muốn hủy quyền ADMIN của người dùng ?',
    	    buttons: {
    	        confirm: {
    	        	 text: '<i class="fas fa-check"></i>&nbsp; Đồng ý ',
    	             btnClass: 'btn-red',
    	             keys: ['enter'],
    	             action: function(){
    	            	 $('#role-user-update-'+id).submit();
    	             }    	        	
    	        },
    	        cancel: {
    	        	 text: '<i class="fas fa-ban"></i>&nbsp; Hủy',   	             
	   	             action: function(){
	   	            	 close();
	   	             }   
    	        }
    	    }
    	});
    	return false;
    });
    
    $('.register-item').click(function() {
    	var id = $(this).attr("data-value");
    	$.confirm({
    	    title: '<i class="fas fa-exclamation-triangle text-warning"></i> Thông báo',
    	    content: 'Bạn có chắc chắn muốn đăng ký học môn này ?',
    	    buttons: {
    	        confirm: {
    	        	 text: '<i class="fas fa-check"></i>&nbsp; Đồng ý ',
    	             btnClass: 'btn-blue',
    	             keys: ['enter'],
    	             action: function(){
    	            	 $('#item-'+id).submit();
    	             }    	        	
    	        },
    	        cancel: {
    	        	 text: '<i class="fas fa-ban"></i>&nbsp; Hủy',   	             
	   	             action: function(){
	   	            	 close();
	   	             }   
    	        }
    	    }
    	});
    	return false;
    });
    
    $('.cancel-register-item').click(function() {
    	var id = $(this).attr("data-value");
    	$.confirm({
    	    title: '<i class="fas fa-exclamation-triangle text-warning"></i> Thông báo',
    	    content: 'Bạn có chắc chắn muốn hủy đăng ký học môn này ?',
    	    buttons: {
    	        confirm: {
    	        	 text: '<i class="fas fa-check"></i>&nbsp; Đồng ý ',
    	             btnClass: 'btn-blue',
    	             keys: ['enter'],
    	             action: function(){
    	            	 $('#item-'+id).submit();
    	             }    	        	
    	        },
    	        cancel: {
    	        	 text: '<i class="fas fa-ban"></i>&nbsp; Hủy',   	             
	   	             action: function(){
	   	            	 close();
	   	             }   
    	        }
    	    }
    	});
    	return false;
    });
    
    $('.open-register').click(function() {
    	var id = $(this).attr("data-value");
    	$.confirm({
    	    title: '<i class="fas fa-exclamation-triangle text-warning"></i> Thông báo',
    	    content: 'Bạn có chắc chắn muốn mở đăng ký học lớp này ?',
    	    buttons: {
    	        confirm: {
    	        	 text: '<i class="fas fa-check"></i>&nbsp; Đồng ý ',
    	             btnClass: 'btn-blue',
    	             keys: ['enter'],
    	             action: function(){
    	            	 $('#open-register-'+id).submit();
    	             }    	        	
    	        },
    	        cancel: {
    	        	 text: '<i class="fas fa-ban"></i>&nbsp; Hủy',   	             
	   	             action: function(){
	   	            	 close();
	   	             }   
    	        }
    	    }
    	});
    	return false;
    });
    
    $('.open').click(function() {
    	var id = $(this).attr("data-value");
    	$.confirm({
    	    title: '<i class="fas fa-exclamation-triangle text-warning"></i> Thông báo',
    	    content: 'Bạn có chắc chắn muốn lớp này đi vào hoạt động ?',
    	    buttons: {
    	        confirm: {
    	        	 text: '<i class="fas fa-check"></i>&nbsp; Đồng ý ',
    	             btnClass: 'btn-blue',
    	             keys: ['enter'],
    	             action: function(){
    	            	 $('#open-'+id).submit();
    	             }    	        	
    	        },
    	        cancel: {
    	        	 text: '<i class="fas fa-ban"></i>&nbsp; Hủy',   	             
	   	             action: function(){
	   	            	 close();
	   	             }   
    	        }
    	    }
    	});
    	return false;
    });
});