$(document).ready(function() {
	$('.with-search').select2({
		"language" : {
			"noResults" : function() {
				return "Không có kết quả được tìm thấy.";
			}
		},
		placeholder : "Click để chọn",
		templateSelection : function(data) {
			var $result = $("<span></span>");
			$result.text(data.text.trim());
			return $result;
		},
		escapeMarkup : function(markup) {
			return markup;
		}
	});
});