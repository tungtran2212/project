$(document)
		.ready(
				function() {
					// Setup datepicker language viet nam
					$.fn.datepicker.dates['vi'] = {
						days : [ "chủ nhật", "thứ 2", "thứ 3", "thứ 4", "thứ 5", "thứ 6", "thứ 7" ],
						daysShort : [ "CN", "t2", "t3", "t4", "t5", "t6", "t7" ],
						daysMin : [ "CN", "t2", "t3", "t4", "t5", "t6", "t7" ],
						months : [ "Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8",
								"Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12" ],
						monthsShort : [ "Th 1", "Th 2", "Th 3", "Th 4", "Th 5", "Th 6", "Th 7", "Th 8", "Th", "Th 10", "Th 11",
								"Th 12" ],
						today : "Hôm nay",
						clear : "Xóa",
						format : "dd-mm-yyyy",
						titleFormat : "MM yyyy", /* Leverages same syntax as 'format' */
						weekStart : 0
					};
					// Hiển thị ngày tháng năm
					$(function() {
						$(".datepicker-top").datepicker({
							format : "dd-mm-yyyy",
							autoclose : true,
							todayHighlight : true,
							language : 'vi',
							orientation : 'bottom'
						})
						$(".datepicker-bottom").datepicker({
							format : "dd-mm-yyyy",
							autoclose : true,
							todayHighlight : true,
							language : 'vi',
							orientation : 'top'
						})
					});
				});