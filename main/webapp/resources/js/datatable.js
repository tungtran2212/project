$(document).ready(function() {
	// DataTables
	var table = $('table[id|="datatable"]').DataTable({
		"language" : {
			"lengthMenu" : "Xem _MENU_ danh mục",
			"search" : "Tìm kiếm:",
			"zeroRecords" : "Không thể tìm thấy dữ liệu đang tìm kiếm.",
			"info" : "Xem _START_ tới _END_ của _TOTAL_ danh mục",
			"infoEmpty" : "Không có danh mục nào",
			"infoFiltered" : "(Tìm kiếm trong _MAX_ danh mục)",
			"oPaginate" : {
				"sFirst" : "<<",
				"sPrevious" : "<",
				"sNext" : ">",
				"sLast" : ">>"
			},
			"pagingType" : "full_numbers",
			"aLengthMenu" : [ 10, 20, 50 ],
			"columnDefs" : [ {
				"searchable" : false,
				"orderable" : false,
				"targets" : 0
			} ],
			"order" : [ [ 0, 'asc' ] ]
		}
	});

	table.on('order.dt search.dt', function() {
		table.table(this).column(0, {
			search : 'applied',
			order : 'applied'
		}).nodes().each(function(cell, i) {
			cell.innerHTML = i + 1;
		});
	}).draw();
});