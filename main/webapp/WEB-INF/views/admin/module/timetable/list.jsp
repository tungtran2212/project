<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="row">
	<div class="col-sm-6">
		<h5 class="text-uppercase mt-1 mb-0">
			<i class="fas fa-list-ul"></i> Thời khóa biểu
		</h5>
	</div>
</div>
<hr class="my-3">
<table class="table table-bordered">
	<thead>
		<tr>
		  <c:forEach begin="2" end="7" var="weekDay">
		    <th class="text-center">Thứ ${weekDay}</th>
		  </c:forEach>
		</tr>
	</thead>
	<tbody>
		<tr>
			<c:forEach begin="2" end="7" var="weekDay">
			 <td class="p-0">
				 <div class="list-group list-group-flush">
	        <c:forEach items="${timeTables}" var="timeTable">
	          <c:if test="${timeTable.weekDay == weekDay}">
	            <a href="<c:url value="${appUrl.URL_CLASS.concat('/').concat(timeTable.detail.uuid).concat(appUrl.URL_DETAIL)}" />" class="list-group-item list-group-item-action">
	            - ${timeTable.detail.majorSubject.subject.title}<br>
	            - ${timeTable.detail.room.name}<br>
	            - Ca: <c:forEach items="${timeTable.blocks}" var="block" varStatus="loop">${block}${!loop.last ? ', ' : ''}</c:forEach>
	            </a>
	          </c:if>
	        </c:forEach>
         </div>
       </td>
			</c:forEach>
		</tr>
	</tbody>
</table>