<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:import url="../../core/header.jsp"></c:import>
<c:import url="../../core/nav.jsp"></c:import>
<c:import url="../../core/menu.jsp"></c:import>
<c:import url="breadcrumb.jsp"></c:import>
<!-- Notify -->
<c:if test="${not empty successMessage || not empty errorMessage}">
	<c:if test="${not empty successMessage}">
		<p class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert">&times;</button><strong>Thành công!</strong> ${successMessage}</p>
	</c:if>
	<c:if test="${not empty errorMessage}">
		<p class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert">&times;</button><strong>Thất bại!</strong> ${errorMessage}</p>
	</c:if>	
</c:if>
<!-- End notify -->
<c:import url="form.jsp"></c:import>
<hr>
<c:import url="list.jsp"></c:import>
<c:import url="../../core/footer.jsp"></c:import>