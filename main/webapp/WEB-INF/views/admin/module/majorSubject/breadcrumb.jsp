<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href='<c:url value="${appUrl.URL_ADMIN}" />'>Trang chủ</a></li>
    <li class="breadcrumb-item"><a href='<c:url value="${majorConfig.MODULE_ADMIN_URL }" />'>${majorConfig.MODULE_NAME}</a></li>
    <li class="breadcrumb-item active" aria-current="page">${major.name}</li>
  </ol>
</nav>