<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="row">
	<div class="col-sm-10">
		<h5 class="text-uppercase mt-1 mb-0">
			<i class="fas fa-plus"></i> ${moduleLang.LABEL_CREATE.concat(' ').concat(major.name)}
		</h5>
	</div>
	<div class="col-sm-2 text-right">
		</a> <a href='<c:url value='${appUrl.URL_MAJOR}' />' class="btn btn-info"><i class="fas fa-bars"></i> ${labelLang.LABEL_BUTTON_READ}</a>
	</div>
</div>
<hr class="my-3">
<c:url var="urlForm" value="${appUrl.URL_MAJOR.concat('/').concat(major.uuid).concat(subjectConfig.MODULE_URL).concat(appUrl.URL_CREATE)}"></c:url>
<form:form action="${urlForm}" method="post" modelAttribute="validMajorSubject">
	<div class="form-group row">
		<label for="major.id" class="col-sm-3 col-form-label text-right">Ngành học</label>
		<div class="col-sm-7 col-form-label">${major.name}</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Tên khóa</label>
		<div class="col-sm-7">
			<select class="form-control" name="course.id" path="course.id">
				<c:forEach var="course" items="${courses}">
					<option value="${course.id}" ${course.id == majorSubject.course.id ? 'selected' : ''}>${course.code}</option>
				</c:forEach>
			</select>
			<form:errors path="course.id" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Môn học</label>
		<div class="col-sm-7">
			<select class="form-control with-search" name="subjectIds" path="subjectIds" multiple="multiple">
				<c:forEach var="subject" items="${subjects}">
					<option value="${subject.id}" 
					 <c:forEach var="subjectSelected" items="${majorSubject.subjectIds}">${subject.id == subjectSelected ? 'selected' : ''}</c:forEach>>
					 ${subject.title}</option>
				</c:forEach>
			</select>
			<form:errors path="subjectIds" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Trạng thái</label>
		<div class="col-sm-7 col-form-label">
			<div class="custom-control custom-radio custom-control-inline">
				<input class="custom-control-input" type="radio" name="status" id="active" value="1" checked="checked"> <label
					class="custom-control-label" for="active"> Kích hoạt</label>
			</div>
			<div class="custom-control custom-radio custom-control-inline">
				<input class="custom-control-input" type="radio" name="status" id="deactive" value="0" ${majorSubject.status == 0 ? 'checked="checked"' : ''}>
				<label class="custom-control-label" for="deactive"> Tạm dừng</label>
			</div>
		</div>
	</div>
	<div class="form-group row">
		<div class="col-sm-7 offset-sm-3">
			<hr>
			<button type="submit" class="btn btn-primary">
				<i class="fas fa-plus"></i> ${labelLang.LABEL_CREATE}
			</button>
			<button type="reset" class="btn">
				<i class="fas fa-redo-alt"></i> ${labelLang.LABEL_RESET}
			</button>
		</div>
	</div>
</form:form>