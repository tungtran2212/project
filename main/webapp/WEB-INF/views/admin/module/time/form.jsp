<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="row">
	<div class="col-sm-6">
		<h5 class="text-uppercase mt-1 mb-0">
			<i class="fas fa-plus"></i> ${moduleLang.LABEL_CREATE}
		</h5>
	</div>
	<div class="col-sm-6 text-right">
		<a href='<c:url value='${module.MODULE_ADMIN_URL}' />' class="btn btn-info"><i class="fas fa-bars"></i> ${labelLang.LABEL_BUTTON_READ}</a>
	</div>
</div>
<hr class="my-3">
<c:if test="${createMode}">
	<c:url var="urlForm" value="${module.MODULE_ADMIN_URL.concat('/').concat(detail.uuid).concat(module.URL_TIME).concat(appUrl.URL_CREATE)}"></c:url>
</c:if>
<form:form action="${urlForm}" method="post" modelAttribute="validDetailTime">
	<div class="form-group row">
		<label for="detail.majorSubject.course.id" class="col-sm-3 col-form-label text-right">Khóa học</label>
		<div class="col-sm-7 col-form-label">${detail.majorSubject.course.code.concat(' - ')}
			<c:choose>
				<c:when test="${detail.term % 3 == 0}">
					Năm	<fmt:parseNumber pattern="#" value="${detail.term / 3}" /> - Kỳ 3
				</c:when>
				<c:otherwise>
				Năm	<fmt:parseNumber pattern="#" integerOnly="true" value="${(detail.term / 3) + 1}" /> - Kỳ ${detail.term % 3}
				</c:otherwise>
			</c:choose>
		</div>
	</div>
	<div class="form-group row">
		<label for="detail.majorSubject.major.id" class="col-sm-3 col-form-label text-right">Ngành học - Môn học</label>
		<div class="col-sm-7 col-form-label">${detail.majorSubject.major.name.concat(' - ').concat(detail.majorSubject.subject.title)}</div>
	</div>
	<div class="form-group row">
		<label for="detail.room.id" class="col-sm-3 col-form-label text-right">Phòng học - Giáo viên giảng dạy</label>
		<div class="col-sm-7 col-form-label">${detail.room.name.concat(' - ').concat(detail.staff.user.firstName.concat(" ").concat(detail.staff.user.lastName))}</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Thứ</label>
		<div class="col-sm-7">
			<select class="form-control" name="weekDay" path="weekDay">
				<c:forEach var="valueWeekDay" begin="2" end="7">
					<option value="${valueWeekDay}" ${detailTime.weekDay == valueWeekDay ? 'selected="selected"' : ''}>Thứ ${valueWeekDay}</option>
				</c:forEach>
			</select>
			<form:errors path="weekDay" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Ca</label>
		<div class="col-sm-7">
			<select class="form-control with-search" name="blocks" path="blocks" multiple="multiple">
				<c:forEach var="valueBlock" begin="1" end="${maxBlock}">
					<option value="${valueBlock}"
						<c:forEach items="${detailTime.blocks}" var="block">${block == valueBlock ? 'selected="selected"' : '' }</c:forEach>>
						Ca ${valueBlock}
					</option>
				</c:forEach>
			</select>
			<form:errors path="blocks" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Trạng thái</label>
		<div class="col-sm-7 col-form-label">
			<div class="custom-control custom-radio custom-control-inline">
				<input class="custom-control-input" type="radio" name="status" id="active" value="1" checked="checked"> <label
					class="custom-control-label" for="active"> Kích hoạt</label>
			</div>
			<div class="custom-control custom-radio custom-control-inline">
				<input class="custom-control-input" type="radio" name="status" id="deactive" value="0" ${time.status == 0 ? 'checked="checked"' : ''}> <label
					class="custom-control-label" for="deactive"> Tạm dừng</label>
			</div>
		</div>
	</div>
	<div class="form-group row">
		<div class="col-sm-7 offset-sm-3">
	    <hr>
			<button type="submit" class="btn btn-primary">
				<i class="fas fa-plus"></i> ${labelLang.LABEL_CREATE}
			</button>
			<button type="reset" class="btn">
				<i class="fas fa-redo-alt"></i> ${labelLang.LABEL_RESET}
			</button>
		</div>
	</div>
</form:form>
