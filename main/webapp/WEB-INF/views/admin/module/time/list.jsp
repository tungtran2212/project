<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="row">
	<div class="col-sm-6">
		<h5 class="text-uppercase mt-1 mb-0">
			<i class="fas fa-list-ul"></i> ${moduleLang.LABEL_LIST.concat(' ').concat(major.name)}
		</h5>
	</div>
</div>
<hr class="my-3">
<table id="datatable" class="table table-hover">
	<thead>
		<tr>
			<th class="text-center">${labelLang.LABEL_STT}</th>
			<th class="text-center">${moduleLang.LABEL_WEEKDAY}</th>
			<th class="text-center">${moduleLang.LABEL_BLOCK}</th>
			<th class="text-center">${labelLang.LABEL_OPTION}</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${detailTimes}" var="detailTime">
			<tr ${detailTime.status != 1 ? 'class="bg-light"' : ''}>
				<td class="text-center"></td>
				<td class="text-center">${detailTime.weekDay}</td>
				<td class="text-center">${detailTime.block}</td>
				<td class="text-center">
					<div class="row">
						<div class="col-sm-7 text-right">
							<c:choose>
								<c:when test="${detailTime.status == 1}">
									<c:set var="activeLabel" value="${labelLang.LABEL_BUTTON_DEACTIVE}" />
									<c:set value="delete-item" var="statusClass" />
									<c:set var="btnClass" value="btn-secondary" />
									<c:set var="icon">
										<i class="fas fa-lock"></i>
									</c:set>
									<c:url
										value="${module.MODULE_ADMIN_URL.concat('/').concat(detail.uuid).concat(module.URL_TIME).concat('/').concat(detailTime.uuid).concat(appUrl.URL_DEACTIVE)}"
										var="actionLink" />
								</c:when>
								<c:otherwise>
									<c:set var="activeLabel" value="${labelLang.LABEL_BUTTON_ACTIVE}" />
									<c:set value="restore-item" var="statusClass" />
									<c:set var="btnClass" value="btn-warning" />
									<c:set var="icon">
										<i class="fas fa-lock-open"></i>
									</c:set>
									<c:url
										value="${module.MODULE_ADMIN_URL.concat('/').concat(detail.uuid).concat(module.URL_TIME).concat('/').concat(detailTime.uuid).concat(appUrl.URL_ACTIVE)}"
										var="actionLink" />
								</c:otherwise>
							</c:choose>
							<form:form action="${actionLink}" method="post" id="item-${detailTime.uuid}">
								<button type="submit" class="btn ${btnClass} btn-sm ${statusClass}" data-value="${detailTime.uuid}">${icon}&nbsp;${activeLabel}</button>
							</form:form>
						</div>
						<div class="col-sm-5 text-left">
							<c:url var="urlForm"
								value="${module.MODULE_ADMIN_URL.concat('/').concat(detail.uuid).concat(module.URL_TIME).concat('/').concat(detailTime.uuid).concat(appUrl.URL_DELETE)}"></c:url>
							<form:form action="${urlForm}" method="post" id="completeRemove-${detailTime.uuid}">
								<button type="submit" class="btn btn-sm btn-danger complete-remove" name="delete" data-value="${detailTime.uuid}">
									<i class="fas fa-trash-alt"></i> ${labelLang.LABEL_BUTTON_DELETE_SHORT}
								</button>
							</form:form>
						</div>
					</div>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>