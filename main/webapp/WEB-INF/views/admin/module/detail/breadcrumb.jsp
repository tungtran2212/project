<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href='<c:url value="${appUrl.URL_ADMIN}" />'>Trang chủ</a></li>
    <c:if test="${(currentUser.role == role.ROLE_ADMIN) && !managerMode && !detailMode}">
      <li class="breadcrumb-item"><a href='<c:url value="${appUrl.URL_ADMIN.concat(appUrl.URL_DETAIL)}" />'>Lịch học</a></li>
    </c:if>
    <li class="breadcrumb-item active" aria-current="page">
   	<c:choose>
   		<c:when test="${listMode}">${moduleLang.LABEL_LIST}</c:when>
   		<c:when test="${createMode}">${moduleLang.LABEL_CREATE}</c:when>
   		<c:when test="${updateMode}">${moduleLang.LABEL_UPDATE}</c:when>
   		<c:when test="${detailMode}">${detail.majorSubject.subject.title}</c:when>
   		<c:when test="${managerMode}">Quản lý lớp</c:when>
   	</c:choose>
    </li>
  </ol>
</nav>