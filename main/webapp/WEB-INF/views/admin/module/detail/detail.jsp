<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="row">
	<div class="col-sm-7">
		<h5 class="text-uppercase mt-1 mb-0">
			<i class="fas fa-list-ul"></i> ${detail.majorSubject.subject.title}
		</h5>
	</div>
	<c:if test="${(currentUser.role == role.ROLE_ADMIN) || (currentUser.role == role.ROLE_STAFF)}">
		<div class="col-sm-5 text-right">
			<c:url value="${appUrl.URL_REGISTRATION.concat('/').concat(detail.uuid).concat(appUrl.URL_EXCEL).concat(appUrl.URL_UPLOAD)}" var="uploadUrl" />
			<form:form action="${uploadUrl}" method="post" enctype="multipart/form-data">
				<div class="row">
					<div class="col-sm-6 text-right">
						<a class="btn btn-success"
							href='<c:url value="${appUrl.URL_REGISTRATION.concat('/').concat(detail.uuid).concat(appUrl.URL_EXCEL).concat(appUrl.URL_DOWNLOAD)}"/>'>
							<i class="fas fa-download"></i> Xuất
						</a>
						<button type="submit" class="btn btn-warning"">
							<i class="fas fa-upload"></i> Nhập
						</button>
					</div>
					<div class="col-sm-6 pl-0">
						<input type="file" name="uploadFile" path="uploadFile" class="form-control">
					</div>
				</div>
			</form:form>
		</div>
	</c:if>
</div>
<hr>
<div class="row text-uppercase">
	<div class="col-sm-4 border-right">
		Khóa:&nbsp;<b>${detail.majorSubject.course.code}</b>
	</div>
	<div class="col-sm-4 border-right">
		Khoa:&nbsp;<b>${detail.majorSubject.major.faculty.name}</b>
	</div>
	<div class="col-sm-4">
		Ngành:&nbsp;<b>${detail.majorSubject.major.name}</b>
	</div>
</div>
<hr>
<div class="row text-uppercase">
	<div class="col-sm-4 border-right">
		Chủ nhiệm:&nbsp;<b>${detail.staff.user.firstName.concat(" ").concat(detail.staff.user.lastName)}</b>
	</div>
	<div class="col-sm-4 border-right">
		Phòng:&nbsp;<b>${detail.room.name}</b>
	</div>
	<div class="col-sm-4">
		Thời gian:<br> <b><c:forEach var="detailTime" items="${detailTimes}">
        Thứ ${detailTime.weekDay} - Ca: <c:forEach items="${detailTime.blocks}" var="block" varStatus="loop">${block}${!loop.last ? ', ' : ''}</c:forEach>
				<br>
			</c:forEach></b>
	</div>
</div>
<hr>
<h5>Danh sách học sinh</h5>
<hr>
<table id="datatable" class="table table-hover">
	<thead>
		<tr>
			<th class="text-center align-middle">${appLabel.LABEL_STT}</th>
			<th class="text-center align-middle">${studentLang.LABEL_USERNAME}</th>
			<th class="text-center align-middle">${studentLang.LABEL_NAME}</th>
			<th class="text-center align-middle">Điểm 1</th>
			<th class="text-center align-middle">Điểm 2</th>
			<th class="text-center align-middle">Điểm tổng kết</th>
			<c:if test="${(currentUser.role == role.ROLE_ADMIN) || (currentUser.role == role.ROLE_STAFF)}">
				<th class="text-center" ${(registration != null) || (currentUser.role == role.ROLE_STAFF) ? '' : ' width="30%"'}>${appLabel.LABEL_OPTION}</th>
			</c:if>
		</tr>
	</thead>
	<tbody>
		<c:if test="${(currentUser.role == role.ROLE_ADMIN) || (currentUser.role == role.ROLE_STAFF)}">
			<c:if test="${registration != null}">
				<c:forEach items="${registrations}" var="registrationItem">
					<c:if test="${registration.uuid == registrationItem.uuid}">
						<tr ${(registrationItem.student.user.status != 1) || (registrationItem.status != 1) ? 'class="bg-light"' : ''}>
							<td class="text-center"></td>
							<td class="text-center">
								<a href='<c:url value="${appUrl.URL_STUDENT.concat('/').concat(registrationItem.student.user.uuid).concat(appUrl.URL_DETAIL)}"/>'>${registrationItem.student.user.username}</a>
							</td>
							<td class="text-center">${registrationItem.student.user.firstName.concat(" ").concat(registrationItem.student.user.lastName)}</td>
							<td class="text-center">
								<c:url var="urlForm" value="${appUrl.URL_REGISTRATION.concat('/').concat(registrationItem.uuid).concat(appUrl.URL_UPDATE)}"></c:url>
								<form:form action="${urlForm}" method="post" modelAttribute="validRegistration">
									<input type="text" name="firstPoint" class="form-control" value="${registration.firstPoint}" path="firstPoint">
							</td>
							<td class="text-center">
								<input type="text" name="secondPoint" class="form-control" value="${registration.secondPoint}" path="secondPoint">
							</td>
							<td class="text-center">${registrationItem.finalPoint}</td>
							<td class="text-center">
								<button type="submit" class="btn btn-primary btn-sm">
									<i class="fas fa-edit"></i> ${appLabel.LABEL_UPDATE}
								</button>
								</form:form>
							</td>
						</tr>
					</c:if>
				</c:forEach>
			</c:if>
		</c:if>
		<c:forEach items="${registrations}" var="registrationItem">
			<c:if test="${registrationItem.uuid != registration.uuid}">
				<tr ${(registrationItem.student.user.status != 1) || (registrationItem.status != 1) ? 'class="bg-light"' : ''}>
					<td class="text-center"></td>
					<td class="text-center">
						<a href='<c:url value="${appUrl.URL_STUDENT.concat('/').concat(registrationItem.student.user.uuid).concat(appUrl.URL_DETAIL)}"/>'>${registrationItem.student.user.username}</a>
					</td>
					<td class="text-center">${registrationItem.student.user.firstName.concat(" ").concat(registrationItem.student.user.lastName)}</td>
					<td class="text-center">${registrationItem.firstPoint}</td>
					<td class="text-center">${registrationItem.secondPoint}</td>
					<td class="text-center">${registrationItem.finalPoint}</td>
					<c:if test="${(currentUser.role == role.ROLE_ADMIN) || (currentUser.role == role.ROLE_STAFF)}">
						<td class="text-center">
							<c:if test="${currentUser.role == role.ROLE_ADMIN}">
								<div class="row p-0 m-0">
									<div class="col-sm-3 p-0 m-0 pr-2">
										<a href='<c:url value="${appUrl.URL_REGISTRATION.concat('/').concat(registrationItem.uuid).concat(appUrl.URL_UPDATE)}" />'
											class="btn btn-primary btn-sm btn-block">
											<i class="fas fa-edit"></i>&nbsp;${appLabel.LABEL_BUTTON_UPDATE}
										</a>
									</div>
									<div class="col-sm-6 p-0 m-0 pr-2">
										<c:choose>
											<c:when test="${registrationItem.status == 1}">
												<c:set var="activeLabel" value="${appLabel.LABEL_BUTTON_DEACTIVE}" />
												<c:set value="delete-item" var="statusClass" />
												<c:set var="btnClass" value="btn-secondary" />
												<c:set var="icon">
													<i class="fas fa-lock"></i>
												</c:set>
												<c:url value="${appUrl.URL_ADMIN.concat(appUrl.URL_REGISTRATION).concat('/').concat(registrationItem.uuid).concat(appUrl.URL_DEACTIVE)}"
													var="actionLink" />
											</c:when>
											<c:otherwise>
												<c:set var="activeLabel" value="${appLabel.LABEL_BUTTON_ACTIVE}" />
												<c:set value="restore-item" var="statusClass" />
												<c:set var="btnClass" value="btn-warning" />
												<c:set var="icon">
													<i class="fas fa-lock-open"></i>
												</c:set>
												<c:url value="${appUrl.URL_ADMIN.concat(appUrl.URL_REGISTRATION).concat('/').concat(registrationItem.uuid).concat(appUrl.URL_ACTIVE)}"
													var="actionLink" />
											</c:otherwise>
										</c:choose>
										<form:form action="${actionLink}" method="post" id="item-${registrationItem.uuid}">
											<button type="submit" class="btn ${btnClass} btn-sm ${statusClass} btn-block" data-value="${registrationItem.uuid}">${icon}&nbsp;${activeLabel}</button>
										</form:form>
									</div>
									<div class="col-sm-3 p-0 m-0">
										<c:url value="${appUrl.URL_ADMIN.concat(appUrl.URL_REGISTRATION).concat('/').concat(registrationItem.uuid).concat(appUrl.URL_DELETE)}"
											var="deleteAction" />
										<form:form action="${deleteAction}" method="post" id="completeRemove-${registrationItem.uuid}">
											<button type="submit" class="btn btn-sm btn-danger complete-remove btn-block" name="delete" data-value="${registrationItem.uuid}">
												<i class="fas fa-trash-alt"></i> ${appLabel.LABEL_BUTTON_DELETE_SHORT}
											</button>
										</form:form>
									</div>
								</div>
							</c:if>
							<c:if test="${currentUser.role == role.ROLE_STAFF}">
								<a href='<c:url value="${appUrl.URL_REGISTRATION.concat('/').concat(registrationItem.uuid).concat(appUrl.URL_UPDATE)}" />'
									class="btn btn-primary btn-sm">
									<i class="fas fa-edit"></i>&nbsp;${appLabel.LABEL_BUTTON_UPDATE}
								</a>
							</c:if>
						</td>
					</c:if>
				</tr>
			</c:if>
		</c:forEach>
	</tbody>
</table>