<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="row">
	<c:if test="${createMode}">
		<div class="col-sm-6">
			<h5 class="text-uppercase mt-1 mb-0">
				<i class="fas fa-plus"></i> ${moduleLang.LABEL_CREATE}
			</h5>
		</div>
		<div class="col-sm-6 text-right">
			<a href='<c:url value='${module.MODULE_ADMIN_URL}' />' class="btn btn-info"><i class="fas fa-bars"></i> ${labelLang.LABEL_BUTTON_READ}</a>
		</div>
	</c:if>
	<c:if test="${updateMode}">
		<div class="col-sm-6">
			<h5 class="text-uppercase mt-1 mb-0">
				<i class="fas fa-plus"></i> ${moduleLang.LABEL_UPDATE}
			</h5>
		</div>
		<div class="col-sm-6 text-right">
			<a href='<c:url value='${module.MODULE_ADMIN_URL.concat(appUrl.URL_CREATE)}' />' class="btn btn-success"> <i class="fas fa-plus"></i>
				${labelLang.LABEL_BUTTON_CREATE}
			</a> <a href='<c:url value='${module.MODULE_ADMIN_URL}' />' class="btn btn-info"><i class="fas fa-bars"></i> ${labelLang.LABEL_BUTTON_READ}</a>
		</div>
	</c:if>
</div>
<hr class="my-3">
<c:if test="${createMode}">
	<c:url var="urlForm" value="${module.MODULE_ADMIN_URL.concat(appUrl.URL_CREATE)}"></c:url>
</c:if>
<c:if test="${updateMode}">
	<c:url var="urlForm" value="${module.MODULE_ADMIN_URL.concat('/').concat(detail.uuid).concat(appUrl.URL_UPDATE)}"></c:url>
</c:if>
<form:form action="${urlForm}" method="post" modelAttribute="validDetail">
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Khóa - Ngành - Môn</label>
		<div class="col-sm-7">
			<select class="form-control with-search" name="majorSubject.id" path="majorSubject.id">
				<c:forEach var="majorSubject" items="${majorSubjects}">
					<option value="${majorSubject.id}" ${majorSubject.id == detail.majorSubject.id ? 'selected' : ''}>${majorSubject.course.code.concat(' - ').concat(majorSubject.major.code).concat(' - ').concat(majorSubject.subject.title)}</option>
				</c:forEach>
			</select>
			<form:errors path="majorSubject.id" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Kỳ học</label>
		<div class="col-sm-7">
			<select class="form-control" name="term" path="term">
				<c:forEach begin="1" end="${maxTerm}"  var="numberTerm">
					<c:choose>
						<c:when test="${numberTerm % 3 == 0}">
						<option value="${numberTerm}" ${detail.term == numberTerm ? 'selected' : ''}>Năm <fmt:parseNumber pattern="#" value="${numberTerm / 3}" /> - Kỳ 3 </option>
						</c:when>
						<c:otherwise>
						<option value="${numberTerm}" ${detail.term == numberTerm ? 'selected' : ''}>Năm <fmt:parseNumber pattern="#" integerOnly="true" value="${(numberTerm / 3) + 1}" /> - Kỳ ${numberTerm % 3} </option>
						</c:otherwise>
					</c:choose>					
				</c:forEach>
			</select>
			<form:errors path="term" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Phòng học</label>
		<div class="col-sm-7">
			<select class="form-control" name="room.id" path="room.id">
				<c:forEach var="room" items="${rooms}">
					<option value="${room.id}" ${room.id == detail.room.id ? 'selected' : ''}>${room.name}</option>
				</c:forEach>
			</select>
			<form:errors path="room.id" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Giáo viên giảng dạy</label>
		<div class="col-sm-7">
			<select class="form-control" name="staff.userId" path="staff.userId">
				<c:forEach var="staff" items="${staffs}">
					<option value="${staff.userId}" ${staff.userId == detail.staff.userId ? 'selected' : ''}>${staff.user.firstName.concat(" ").concat(staff.user.lastName)}</option>
				</c:forEach>
			</select>
			<form:errors path="staff.userId" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Trạng thái</label>
		<div class="col-sm-7 col-form-label">
			<div class="custom-control custom-radio custom-control-inline">
				<input class="custom-control-input" type="radio" name="status" id="active" value="1" checked="checked"> <label
					class="custom-control-label" for="active"> Kích hoạt</label>
			</div>
			<div class="custom-control custom-radio custom-control-inline">
				<input class="custom-control-input" type="radio" name="status" id="deactive" value="0" ${detail.status == 0 ? 'checked="checked"' : ''}> <label
					class="custom-control-label" for="deactive"> Mở đăng ký</label>
			</div>
		</div>
	</div>
	<div class="form-group row">
		<c:if test="${createMode}">
			<div class="col-sm-7 offset-sm-3">
        <hr>
				<button type="submit" class="btn btn-primary">
					<i class="fas fa-plus"></i> ${labelLang.LABEL_CREATE}
				</button>
				<button type="reset" class="btn">
					<i class="fas fa-redo-alt"></i> ${labelLang.LABEL_RESET}
				</button>
			</div>
		</c:if>
		<c:if test="${updateMode}">
			<div class="offset-sm-3 col-sm-7">
				<hr>
				<button type="submit" class="btn btn-primary">
					<i class="fas fa-edit"></i> ${labelLang.LABEL_UPDATE}
				</button>
			</div>
		</c:if>
	</div>
</form:form>
<c:if test="${updateMode}">
	<c:url var="urlForm" value="${module.MODULE_ADMIN_URL.concat('/').concat(detail.uuid).concat(appUrl.URL_DELETE)}"></c:url>
	<form:form action="${urlForm}" method="post" id="completeRemove-${detail.uuid}">
    <hr class="my-3">
    <div class="form-group row">
      <div class="col-sm-9 offset-sm-3 text-left">
        <button type="submit" class="btn btn-sm btn-danger complete-remove" name="delete" data-value="${detail.uuid}"><i class="fas fa-trash-alt"></i> ${labelLang.LABEL_BUTTON_DELETE}
        </button>
      </div>
    </div>
  </form:form>
</c:if>
