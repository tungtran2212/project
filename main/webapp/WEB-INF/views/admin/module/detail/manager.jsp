<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="row">
  <div class="col-sm-12">
    <h5 class="text-uppercase mt-1 mb-0">
      <i class="fas fa-list-ul"></i> Danh sách lớp giảng dạy
    </h5>
  </div>
</div>
<hr class="my-3">
<table id="datatable" class="table table-hover">
  <thead>
    <tr>
      <th class="text-center">${appLabel.LABEL_STT}</th>
      <th class="text-center">${detailLang.LABEL_COURSE}</th>
      <th class="text-center">${detailLang.LABEL_MAJOR}</th>
      <th class="text-center">${detailLang.LABEL_SUBJECT}</th>
      <th class="text-center">${detailLang.LABEL_ROOM}</th>
      <th class="text-center">${appLabel.LABEL_OPTION}</th>
    </tr>
  </thead>
  <tbody>
    <c:forEach items="${details}" var="detail">
      <tr ${detail.status != 1 ? 'class="bg-light"' : ''}>
        <td class="text-center"></td>
        <td class="text-center">${detail.majorSubject.course.code}</td>
        <td class="text-center">${detail.majorSubject.major.name}&nbsp;(${detail.majorSubject.major.code})</td>
        <td class="text-center">${detail.majorSubject.subject.title}</td>
        <td class="text-center">${detail.room.name}</td>
        <td class="text-center">
           <a href='<c:url value="${appUrl.URL_CLASS.concat('/').concat(detail.uuid).concat(appUrl.URL_DETAIL)}" />' class="btn btn-primary btn-sm"><i
              class="fas fa-edit"></i>&nbsp;Quản lý</a>
        </td>
      </tr>
    </c:forEach>
  </tbody>
</table>