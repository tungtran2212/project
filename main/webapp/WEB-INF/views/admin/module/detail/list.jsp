<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="row">
	<div class="col-sm-6">
		<h5 class="text-uppercase mt-1 mb-0">
			<i class="fas fa-list-ul"></i> ${moduleLang.LABEL_LIST}
		</h5>
	</div>
	<div class="col-sm-6 text-right">
		<a href="<c:url value='${module.MODULE_ADMIN_URL.concat(appUrl.URL_CREATE)}' />" class="btn btn-success"> <i class="fas fa-plus"></i>
			${labelLang.LABEL_BUTTON_CREATE}
		</a>
	</div>
</div>
<hr class="my-3">
<table id="datatable" class="table table-hover">
	<thead>
		<tr>
			<th class="text-center align-middle">${labelLang.LABEL_STT}</th>
			<th class="text-center align-middle">${moduleLang.LABEL_COURSE}</th>
			<th class="text-center align-middle">${moduleLang.LABEL_MAJOR}</th>
			<th class="text-center align-middle">${moduleLang.LABEL_SUBJECT}</th>
			<th class="text-center align-middle">${moduleLang.LABEL_ROOM}</th>
			<th class="text-center align-middle">Số lượng</th>
			<th class="text-center align-middle" width="25%">${labelLang.LABEL_OPTION}</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${details}" var="detail">
			<tr ${detail.status == 0 ? 'class="bg-light"' : ''}>
				<td class="text-center"></td>
				<td class="text-center">${detail.majorSubject.course.code}</td>
				<td class="text-center">${detail.majorSubject.major.name}&nbsp;(${detail.majorSubject.major.code})</td>
				<td class="text-center">
				  <a href="<c:url value="${appUrl.URL_CLASS.concat('/').concat(detail.uuid).concat(appUrl.URL_DETAIL)}" />">
				    ${detail.majorSubject.subject.title}
				  </a>
				</td>
				<td class="text-center">${detail.room.name}</td>
				<td class="text-center">${detail.countStudent}/30</td>
				<td class="text-center">
				  <div class="row p-0 mx-0 mt-0 mb-2">
				    <div class="col-sm-5 p-0 m-0 pr-2">
              <a href='<c:url value="${module.MODULE_ADMIN_URL.concat('/').concat(detail.uuid).concat(appUrl.URL_UPDATE)}" />' class="btn btn-primary btn-sm btn-block"><i
              class="fas fa-edit"></i>&nbsp;${labelLang.LABEL_BUTTON_UPDATE}</a>
            </div>
            <div class="col-sm-7 p-0 m-0">
				      <c:choose>
		            <c:when test="${detail.status == 0}">
		              <c:set var="activeLabel" value="Mở đăng ký" />
                  <c:set value="open-register" var="statusClass" />
                  <c:set value="open-register-" var="idClass" />
                  <c:set var="btnClass" value="btn-warning" />
                  <c:set var="icon">
                    <i class="fas fa-lock-open"></i>
                  </c:set>
                  <c:url value="${module.MODULE_ADMIN_URL.concat('/').concat(detail.uuid).concat(appUrl.URL_ACTIVE)}" var="actionLink" />
		            </c:when>
		            <c:otherwise>
		              <c:set var="activeLabel" value="${labelLang.LABEL_BUTTON_DEACTIVE}" />
                  <c:set value="delete-item" var="statusClass" />
                  <c:set value="item-" var="idClass" />
                  <c:set var="btnClass" value="btn-secondary" />
                  <c:set var="icon">
                    <i class="fas fa-lock"></i>
                  </c:set>
                  <c:url value="${module.MODULE_ADMIN_URL.concat('/').concat(detail.uuid).concat(appUrl.URL_DEACTIVE)}" var="actionLink" />
		            </c:otherwise>
		          </c:choose>
		          <form:form action="${actionLink}" method="post" id="${idClass}${detail.uuid}">
		            <button type="submit" class="btn ${btnClass} btn-sm btn-block ${statusClass}" data-value="${detail.uuid}">${icon}&nbsp;${activeLabel}</button>
		          </form:form>
				    </div>
				  </div>
				  <div class="row p-0 m-0">
            <div class="col-sm-5 p-0 m-0 pr-2">
              <a href='<c:url value="${module.MODULE_ADMIN_URL.concat('/').concat(detail.uuid).concat(module.URL_TIME).concat(appUrl.URL_CREATE)}" />'
              class="btn btn-info btn-sm btn-block"> <i class="fas fa-clock"></i> ${moduleLang.LABEL_ADD_TIME}</a>
            </div>
            <div class="col-sm-7 p-0 m-0">
              <c:choose>
                <c:when test="${detail.status == 2}">
                  <c:set var="activeLabel" value="Mở đăng ký" />
                  <c:set value="open-register" var="statusClass" />
                  <c:set value="open-register-" var="idClass" />
                  <c:set var="btnClass" value="btn-warning" />
                  <c:set var="icon">
                    <i class="fas fa-lock-open"></i>
                  </c:set>
                  <c:url value="${module.MODULE_ADMIN_URL.concat('/').concat(detail.uuid).concat(appUrl.URL_ACTIVE)}" var="actionLink" />
                </c:when>
                <c:otherwise>
                  <c:set var="activeLabel" value="Hoạt động" />
                  <c:set value="open" var="statusClass" />
                  <c:set value="open-" var="idClass" />
                  <c:set var="btnClass" value="btn-success" />
                  <c:set var="icon">
                    <i class="fas fa-check-circle"></i>
                  </c:set>
                  <c:url value="${module.MODULE_ADMIN_URL.concat('/').concat(detail.uuid).concat(appUrl.URL_OPEN)}" var="actionLink" />
                </c:otherwise>
              </c:choose>
              <form:form action="${actionLink}" method="post" id="${idClass}${detail.uuid}">
                <button type="submit" class="btn ${btnClass} btn-sm btn-block ${statusClass}" data-value="${detail.uuid}">${icon}&nbsp;${activeLabel}</button>
              </form:form>
            </div>
           </div>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>