<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="row">
	<div class="col-sm-6">
		<h5 class="text-uppercase mt-1 mb-0">
			<i class="fas fa-list-ul"></i> ${moduleLang.LABEL_LIST}
		</h5>
	</div>
	<c:if test="${currentUser.role == role.ROLE_ADMIN }">
		<div class="col-sm-6 text-right">
			<a href='<c:url value='${appUrl.URL_FACULTY.concat(appUrl.URL_CREATE)}' />' class="btn btn-success"> <i class="fas fa-plus"></i>
				${labelLang.LABEL_BUTTON_CREATE}
			</a>
		</div>
	</c:if>
</div>
<hr class="my-3">
<table id="datatable" class="table table-hover">
	<thead>
		<tr>
			<th class="text-center">${labelLang.LABEL_STT}</th>
			<th class="text-center">${moduleLang.LABEL_NAME}</th>
			<th class="text-center">${labelLang.LABEL_OPTION}</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${faculties}" var="faculty" varStatus="loop">
			<tr ${faculty.status != 1 ? 'class="bg-light"' : ''}>
				<td class="text-center"></td>
				<td class="text-center"><a href="<c:url value='${appUrl.URL_FACULTY.concat("/").concat(faculty.uuid).concat(appUrl.URL_DETAIL)}' />">${faculty.name}</a></td>
				<td class="text-center">
					<c:choose>
						<c:when test="${faculty.status == 1}">
							<c:set var="activeLabel" value="${labelLang.LABEL_BUTTON_DEACTIVE}" />
							<c:set value="delete-item" var="statusClass" />
							<c:set var="btnClass" value="btn-secondary" />
							<c:set var="icon"><i class="fas fa-lock"></i></c:set>
							<c:url value="${appUrl.URL_FACULTY.concat('/').concat(faculty.uuid).concat(appUrl.URL_DEACTIVE)}" var="actionLink" />
						</c:when>
						<c:otherwise>
							<c:set var="activeLabel" value="${labelLang.LABEL_BUTTON_ACTIVE}" />
							<c:set value="restore-item" var="statusClass" />
							<c:set var="btnClass" value="btn-warning" />
							<c:set var="icon"><i class="fas fa-lock-open"></i></c:set>
							<c:url value="${appUrl.URL_FACULTY.concat('/').concat(faculty.uuid).concat(appUrl.URL_ACTIVE)}" var="actionLink" />
						</c:otherwise>
					</c:choose>
					<form:form action="${actionLink}" method="post" id="item-${faculty.uuid}">
						<a href='<c:url value="${appUrl.URL_FACULTY.concat('/').concat(faculty.uuid).concat(appUrl.URL_UPDATE)}" />' class="btn btn-primary btn-sm">
							<i class="fas fa-edit"></i>&nbsp;${labelLang.LABEL_BUTTON_UPDATE} </a>
						<button type="submit" class="btn ${btnClass} btn-sm ${statusClass}" data-value="${faculty.uuid}">${icon}&nbsp;${activeLabel}</button>
					</form:form>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>