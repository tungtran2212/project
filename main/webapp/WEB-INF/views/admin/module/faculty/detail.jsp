<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
	<div class="col-sm-6">
		<h5 class="text-uppercase mt-1 mb-0">
			<i class="fas fa-list-ul"></i> ${faculty.name}
		</h5>
	</div>
	<div class="col-sm-6 text-right">
		<a href='<c:url value="${appUrl.URL_FACULTY.concat('/').concat(faculty.uuid).concat(appUrl.URL_UPDATE)}" />' class="btn btn-primary">
			<i class="fas fa-edit"></i>&nbsp;${labelLang.LABEL_BUTTON_UPDATE}
		</a>
		<a href='<c:url value='${appUrl.URL_FACULTY.concat(appUrl.URL_CREATE)}' />' class="btn btn-success">
			<i class="fas fa-plus"></i> ${labelLang.LABEL_BUTTON_CREATE}
		</a>
		<a href='<c:url value='${appUrl.URL_FACULTY}' />' class="btn btn-info">
			<i class="fas fa-bars"></i> ${labelLang.LABEL_BUTTON_READ}
		</a>
	</div>
</div>
<hr>
<h5>Danh sách giáo viên</h5>
<hr>
<table id="datatable-staff" class="table table-hover">
	<thead>
		<tr>
			<th class="text-center">${labelLang.LABEL_STT}</th>
			<th class="text-center">${staffLang.LABEL_USERNAME}</th>
			<th class="text-center">${staffLang.LABEL_NAME}</th>
			<th class="text-center">${staffLang.LABEL_EMAIL}</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${staffs}" var="staff">
			<tr ${staff.user.status != 1 ? 'class="bg-light"' : ''}>
				<td class="text-center"></td>
				<td class="text-center">
					<a href='<c:url value="${appUrl.URL_STAFF.concat('/').concat(staff.user.uuid).concat(appUrl.URL_DETAIL)}"/>'>${staff.user.username}</a>
				</td>
				<td class="text-center">${staff.user.firstName.concat(" ").concat(staff.user.lastName)}</td>
				<td class="text-center">${staff.user.email}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<hr>
<h5>Danh sách ngành</h5>
<hr>
<table id="datatable-major" class="table table-hover">
  <thead>
    <tr>
      <th class="text-center">${labelLang.LABEL_STT}</th>
      <th class="text-center">${majorLang.LABEL_CODE}</th>
      <th class="text-center">${majorLang.LABEL_NAME}</th>
    </tr>
  </thead>
  <tbody>
    <c:forEach items="${majors}" var="major">
      <tr ${major.status != 1 ? 'class="bg-light"' : ''}>
        <td class="text-center"></td>
        <td class="text-center">${major.code}</td>
        <td class="text-center">${major.name}</td>
      </tr>
    </c:forEach>
  </tbody>
</table>