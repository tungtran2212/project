<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:import url="../core/header.jsp"></c:import>
<c:import url="../core/nav.jsp"></c:import>
<c:import url="../core/menu.jsp"></c:import>
<!-- breadcrumb -->
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href='<c:url value="${appUrl.URL_ADMIN}" />'>Trang chủ</a>
		</li>
		<li class="breadcrumb-item active" aria-current="page">Dashboard</li>
	</ol>
</nav>
<!-- end breadcrumb -->
<!-- Notify -->
<c:if test="${not empty successMessage || not empty errorMessage}">
	<c:if test="${not empty successMessage}">
		<p class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Thành công!</strong> ${successMessage}
		</p>
	</c:if>
	<c:if test="${not empty errorMessage}">
		<p class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Thất bại!</strong> ${errorMessage}
		</p>
	</c:if>
</c:if>
<!-- End notify -->
<hr>
<div class="row" style="font-size:larger;">
  <div class="col-sm-6 border-right"><b>Số lượng nhân viên: </b> ${countStaff}</div>
  <div class="col-sm-6"><b>Số lượng học sinh: </b> ${countStudent}</div>
</div>
<hr>
<div class="row" style="font-size:larger;">
  <div class="col-sm-6 border-right"><b>Số lượng khoa: </b> ${countFaculty}</div>
  <div class="col-sm-6"><b>Số lượng ngành: </b> ${countMajor}</div>
</div>
<hr>
<div class="row" style="font-size:larger;">
  <div class="col-sm-6 border-right"><b>Số lượng khóa học: </b> ${countCourse}</div>
  <div class="col-sm-6"><b>Số lượng môn học: </b> ${countSubject}</div>
</div>
<hr>
<div class="row" style="font-size:larger;">
  <div class="col-sm-6 border-right"><b>Số lượng lớp: </b> ${countClassroom}</div>
  <div class="col-sm-6"><b>Số lượng phòng học: </b> ${countRoom}</div>
</div>
<hr>
<c:import url="../core/footer.jsp"></c:import>