<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="row">
	<div class="col-sm-12">
		<h5 class="text-uppercase mt-1 mb-0">
			<i class="fas fa-list-ul"></i> ${moduleLang.LABEL_LIST}&nbsp;${student.classroom.major.name}
		</h5>
	</div>
</div>
<hr class="my-3">
<table id="datatable" class="table table-hover table-bordered">
	<thead>
		<tr>
			<th class="text-center align-middle">${appLabel.LABEL_STT}</th>
			<th class="text-center align-middle">${moduleLang.LABEL_COURSE}</th>
			<th class="text-center align-middle">${moduleLang.LABEL_SUBJECT}</th>
			<th class="text-center align-middle" width="25%">${moduleLang.LABEL_TIME}</th>
			<th class="text-center align-middle">${moduleLang.LABEL_ROOM}</th>
			<th class="text-center align-middle">${moduleLang.LABEL_STAFF}</th>
			<th class="text-center align-middle">${moduleLang.LABEL_QUANTITY}</th>
			<th class="text-center align-middle">${moduleLang.LABEL_LIST}</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${detailTimes}" var="detailTime">
			<tr>
				<td class="text-center"></td>
				<td class="text-center">${detailTime.detail.majorSubject.course.code}</td>
				<td>
					<a href="<c:url value="${appUrl.URL_CLASS.concat('/').concat(detailTime.detail.uuid).concat(appUrl.URL_DETAIL)}" />">
					 ${detailTime.detail.majorSubject.subject.title}&nbsp;(${detailTime.detail.majorSubject.subject.creadit}&nbsp;tín chỉ)
					</a>
				</td>
				<td>${detailTime.time}</td>
				<td class="text-center">${detailTime.detail.room.name}</td>
				<td>${detailTime.detail.staff.user.firstName.concat(" ").concat(detailTime.detail.staff.user.lastName)}</td>
				<td class="text-center">${detailTime.countStudent}/30</td>
				<td class="text-center">
					<c:choose>
						<c:when test="${detailTime.isRegister}">
							<c:set var="activeLabel" value="Hủy" />
							<c:set value="cancel-register-item" var="statusClass" />
							<c:set var="btnClass" value="btn-danger" />
							<c:set var="icon">
								<i class="fas fa-times"></i>
							</c:set>
							<c:url value="${module.MODULE_URL.concat('/').concat(detailTime.detail.uuid).concat(appUrl.URL_DELETE)}" var="actionLink" />
						</c:when>
						<c:otherwise>
							<c:set var="activeLabel" value="Đăng ký" />
							<c:set value="register-item" var="statusClass" />
							<c:set var="btnClass" value="btn-primary" />
							<c:set var="icon">
								<i class="fas fa-plus"></i>
							</c:set>
							<c:url value="${module.MODULE_URL.concat('/').concat(detailTime.detail.uuid).concat(appUrl.URL_CREATE)}" var="actionLink" />
						</c:otherwise>
					</c:choose>
					<form:form action="${actionLink}" method="post" id="item-${detailTime.detail.uuid}">
						<button type="submit" class="btn ${btnClass} btn-sm ${statusClass}" data-value="${detailTime.detail.uuid}">${icon}&nbsp;${activeLabel}</button>
					</form:form>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>