<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href='<c:url value="/" />'>Trang chủ</a></li>
    <li class="breadcrumb-item active" aria-current="page">
   	<c:choose>
   		<c:when test="${listMode}">${module.MODULE_NAME}</c:when>
   		<c:when test="${detailMode}">${detail.majorSubject.subject.title}</c:when>
   	</c:choose>
    </li>
  </ol>
</nav>