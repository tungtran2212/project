<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
	<div class="col-sm-6">
		<h5 class="text-uppercase mt-1 mb-0">
			<i class="fas fa-list-ul"></i> ${classroom.name}
		</h5>
	</div>
	<div class="col-sm-6 text-right">
		<a href='<c:url value="${appUrl.URL_CLASSROOM.concat('/').concat(classroom.uuid).concat(appUrl.URL_UPDATE)}" />' class="btn btn-primary">
			<i class="fas fa-edit"></i>&nbsp;${labelLang.LABEL_BUTTON_UPDATE}
		</a>
		<a href='<c:url value='${appUrl.URL_CLASSROOM.concat(appUrl.URL_CREATE)}' />' class="btn btn-success">
			<i class="fas fa-plus"></i> ${labelLang.LABEL_BUTTON_CREATE}
		</a>
		<a href='<c:url value='${appUrl.URL_CLASSROOM}' />' class="btn btn-info">
			<i class="fas fa-bars"></i> ${labelLang.LABEL_BUTTON_READ}
		</a>
	</div>
</div>
<hr>
<div class="row text-uppercase">
	<div class="col-sm-2 border-right">
		Khóa:&nbsp;<b>${classroom.course.code}</b>
	</div>
	<div class="col-sm-2 border-right">
		Khoa:&nbsp;<b>${classroom.major.faculty.name}</b>
	</div>
	<div class="col-sm-4 border-right">
		Ngành:&nbsp;<b>${classroom.major.name}</b>
	</div>
	<div class="col-sm-4">
		Chủ nhiệm:&nbsp;<b>${classroom.staff.user.firstName.concat(" ").concat(classroom.staff.user.lastName)}</b>
	</div>
</div>
<hr>
<h5>Danh sách học sinh</h5>
<hr>
<table id="datatable" class="table table-hover">
	<thead>
		<tr>
			<th class="text-center">${labelLang.LABEL_STT}</th>
			<th class="text-center">${studentLang.LABEL_USERNAME}</th>
			<th class="text-center">${studentLang.LABEL_NAME}</th>
			<th class="text-center">${studentLang.LABEL_EMAIL}</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${students}" var="student">
			<tr ${student.user.status != 1 ? 'class="bg-light"' : ''}>
				<td class="text-center"></td>
				<td class="text-center">
					<a href='<c:url value="${appUrl.URL_STUDENT.concat('/').concat(student.user.uuid).concat(appUrl.URL_DETAIL)}"/>'>${student.user.username}</a>
				</td>
				<td class="text-center">${student.user.firstName.concat(" ").concat(student.user.lastName)}</td>
				<td class="text-center">${student.user.email}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>