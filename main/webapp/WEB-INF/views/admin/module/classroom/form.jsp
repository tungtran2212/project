<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="row">
	<c:if test="${createMode}">
		<div class="col-sm-6">
			<h5 class="text-uppercase mt-1 mb-0">
				<i class="fas fa-plus"></i> ${moduleLang.LABEL_CREATE}
			</h5>
		</div>
		<div class="col-sm-6 text-right">
			<a href='<c:url value='${appUrl.URL_CLASSROOM}' />' class="btn btn-info"><i class="fas fa-bars"></i> ${labelLang.LABEL_BUTTON_READ}</a>
		</div>
	</c:if>
	<c:if test="${updateMode}">
		<div class="col-sm-6">
			<h5 class="text-uppercase mt-1 mb-0">
				<i class="fas fa-plus"></i> ${moduleLang.LABEL_UPDATE}
			</h5>
		</div>
		<div class="col-sm-6 text-right">
			<a href='<c:url value='${appUrl.URL_CLASSROOM.concat(appUrl.URL_CREATE)}' />' class="btn btn-success"> <i class="fas fa-plus"></i>
				${labelLang.LABEL_BUTTON_CREATE}
			</a> <a href='<c:url value='${appUrl.URL_CLASSROOM}' />' class="btn btn-info"><i class="fas fa-bars"></i> ${labelLang.LABEL_BUTTON_READ}</a>
		</div>
	</c:if>
</div>
<hr class="my-3">
<c:if test="${createMode}">
	<c:url var="urlForm" value="${appUrl.URL_CLASSROOM.concat(appUrl.URL_CREATE)}"></c:url>
</c:if>
<c:if test="${updateMode}">
	<c:url var="urlForm" value="${appUrl.URL_CLASSROOM.concat('/').concat(classroom.uuid).concat(appUrl.URL_UPDATE)}"></c:url>
</c:if>
<form:form action="${urlForm}" method="post" modelAttribute="validClassroom">
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Khóa học</label>
		<div class="col-sm-7">
			<select class="form-control" name="course.id" path="course.id">
				<c:forEach var="course" items="${courses}">
					<option value="${course.id}" ${course.id == classroom.course.id ? 'selected' : ''}>${course.code}</option>
				</c:forEach>
			</select>
			<form:errors path="course.id" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Tên lớp</label>
		<div class="col-sm-7">
			<input type="text" name="name" class="form-control" value="${classroom.name}" path="name">
			<form:errors path="name" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Tên khoa</label>
		<div class="col-sm-7">
			<select class="form-control" name="major.faculty.id" path="major.faculty.id">
				<c:forEach var="faculty" items="${faculties}">
					<option value="${faculty.id}" ${faculty.id == classroom.major.faculty.id ? 'selected' : ''}>${faculty.name}</option>
				</c:forEach>
			</select>
			<form:errors path="major.faculty.id" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Ngành học</label>
		<div class="col-sm-7">
			<select class="form-control" name="major.id" path="major.id">
				<c:forEach var="major" items="${majors}">
					<option value="${major.id}" ${major.id == classroom.major.id ? 'selected' : ''}>${major.name}</option>
				</c:forEach>
			</select>
			<form:errors path="major.id" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Giáo viên chủ nhiệm</label>
		<div class="col-sm-7">
			<select class="form-control" name="staff.userId" path="staff.userId">
				<c:forEach var="staff" items="${staffs}">
					<option value="${staff.userId}" ${staff.userId == classroom.staff.userId ? 'selected' : ''}>${staff.user.firstName.concat(" ").concat(staff.user.lastName)}&nbsp;(${staff.user.username})</option>
				</c:forEach>
			</select>
			<form:errors path="staff.userId" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Trạng thái</label>
		<div class="col-sm-7 col-form-label">
			<div class="custom-control custom-radio custom-control-inline">
				<input class="custom-control-input" type="radio" name="status" id="active" value="1" checked="checked"> <label
					class="custom-control-label" for="active"> Kích hoạt</label>
			</div>
			<div class="custom-control custom-radio custom-control-inline">
				<input class="custom-control-input" type="radio" name="status" id="deactive" value="0" ${classroom.status == 0 ? 'checked="checked"' : ''}>
				<label class="custom-control-label" for="deactive"> Tạm dừng</label>
			</div>
		</div>
	</div>
	<div class="form-group row">
		<c:if test="${createMode}">
			<div class="col-sm-7 offset-sm-3">
        <hr />
				<button type="submit" class="btn btn-primary">
					<i class="fas fa-plus"></i> ${labelLang.LABEL_CREATE}
				</button>
				<button type="reset" class="btn">
					<i class="fas fa-redo-alt"></i> ${labelLang.LABEL_RESET}
				</button>
			</div>
		</c:if>
		<c:if test="${updateMode}">
			<div class="offset-sm-3 col-sm-7">
			  <hr />
				<button type="submit" class="btn btn-primary">
					<i class="fas fa-edit"></i> ${labelLang.LABEL_UPDATE}
				</button>
			</div>
		</c:if>
	</div>
</form:form>
<c:if test="${updateMode}">
	<c:url var="urlForm" value="${appUrl.URL_CLASSROOM.concat('/').concat(classroom.uuid).concat(appUrl.URL_DELETE)}"></c:url>
	<form:form action="${urlForm}" method="post" id="completeRemove-${classroom.uuid}">
    <hr class="my-3">
    <div class="form-group row">
      <div class="col-sm-9 offset-sm-3 text-left">
        <button type="submit" class="btn btn-sm btn-danger complete-remove" name="delete" data-value="${classroom.uuid}"><i class="fas fa-trash-alt"></i> ${labelLang.LABEL_BUTTON_DELETE}       
        </button>
      </div>
    </div>
  </form:form>
</c:if>
