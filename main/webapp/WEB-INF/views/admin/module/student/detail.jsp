<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="row">
	<div class="col-sm-6">
		<h5 class="text-uppercase mt-1 mb-0">
			<i class="fas fa-user"></i>&nbsp;${student.user.username}
		</h5>
	</div>
	<div class="col-sm-6 text-right">
		<c:if test="${currentUser.role == role.ROLE_ADMIN}">
			<a href='<c:url value='${appUrl.URL_STUDENT.concat(appUrl.URL_CREATE)}' />' class="btn btn-success">
				<i class="fas fa-plus"></i> ${labelLang.LABEL_BUTTON_CREATE}
			</a>
			<a href='<c:url value='${appUrl.URL_STUDENT}' />' class="btn btn-info">
				<i class="fas fa-bars"></i> ${labelLang.LABEL_BUTTON_READ}
			</a>
		</c:if>
		<a href="<c:url value="${appUrl.URL_STUDENTS.concat('/').concat(student.user.uuid).concat(appUrl.URL_POINT)}" />" class="btn btn-warning">
			<i class="fas fa-star"></i> Bảng điểm
		</a>
	</div>
</div>
<hr class="my-3">
<div class="row mb-3">
	<div class="col-sm-3">
		<img src="<c:url value="${student.user.image}" />" border="0" width="100%">
	</div>
	<div class="col-sm-4 border-right">
		<ul class="list-group list-group-flush">
			<li class="list-group-item">Mã sinh viên : ${student.user.username}</li>
			<li class="list-group-item">Họ và tên : ${student.user.firstName.concat(" ").concat(student.user.lastName)}</li>
			<li class="list-group-item">Khóa học : ${student.classroom.course.code}</li>
			<li class="list-group-item">Khoa : ${student.classroom.major.faculty.name}</li>
			<li class="list-group-item">Ngành học : ${student.classroom.major.name}</li>
			<li class="list-group-item">Lớp : ${student.classroom.name}</li>
			<li class="list-group-item">Giáo viên chủ nhiệm : ${student.classroom.staff.user.firstName.concat(" ").concat(student.classroom.staff.user.lastName)}</li>
		</ul>
	</div>
	<div class="col-sm-4">
		<ul class="list-group list-group-flush">
			<li class="list-group-item">
				Ngày sinh :
				<fmt:formatDate pattern="dd-MM-yyyy" value="${student.user.dob}" />
			</li>
			<li class="list-group-item">Email SV : ${student.user.email}</li>
			<li class="list-group-item">Số điện thoại cá nhân : ${student.user.phone}</li>
			<li class="list-group-item">Địa chỉ : ${student.user.address}</li>
			<li class="list-group-item">Tên người bảo hộ : ${student.guardian}</li>
			<li class="list-group-item">Số điện thoại người bảo hộ : ${student.phoneGuardian}</li>
		</ul>
	</div>
</div>
<c:if test="${currentUser.role == role.ROLE_ADMIN}">
	<hr />
	<div class="text-center">
		<a href='<c:url value='${appUrl.URL_STUDENT.concat("/").concat(student.user.uuid).concat(appUrl.URL_UPDATE)}' />' class="btn btn-primary">
			<i class="fas fa-edit"></i> ${labelLang.LABEL_UPDATE}
		</a>
	</div>
</c:if>
