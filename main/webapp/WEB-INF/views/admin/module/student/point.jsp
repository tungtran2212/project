<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="row">
	<div class="col-sm-12">
		<h5 class="text-uppercase mt-1 mb-0">
			<i class="fas fa-list-ul"></i> Bảng điểm
		</h5>
	</div>
</div>
<hr class="my-3">
<table id="datatable" class="table table-hover">
	<thead>
		<tr>
			<th class="text-center">${appLabel.LABEL_STT}</th>
			<th class="text-center">Môn</th>
			<th class="text-center">Điểm 1</th>
			<th class="text-center">Điểm 2</th>
			<th class="text-center">Điểm tổng kết</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${registrations}" var="registration">
			<tr>
				<td class="text-center"></td>
				<td class="text-center">
					<a href="<c:url value="${appUrl.URL_CLASS.concat('/').concat(registration.detail.uuid).concat(appUrl.URL_DETAIL)}" />">${registration.detail.majorSubject.subject.title}</a>
				</td>
				<td class="text-center">${registration.firstPoint}</td>
				<td class="text-center">${registration.secondPoint}</td>
				<td class="text-center">${registration.finalPoint}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
