<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="row">
	<c:if test="${createMode}">
		<div class="col-sm-6">
			<h5 class="text-uppercase mt-1 mb-0">
				<i class="fas fa-plus"></i> ${moduleLang.LABEL_CREATE}
			</h5>
		</div>
		<div class="col-sm-6 text-right">
			<a href='<c:url value='${appUrl.URL_STUDENT}' />' class="btn btn-info"><i class="fas fa-bars"></i> Danh sách</a>
		</div>
	</c:if>
	<c:if test="${updateMode}">
		<div class="col-sm-6">
			<h5 class="text-uppercase mt-1 mb-0">
				<i class="fas fa-plus"></i> ${moduleLang.LABEL_UPDATE}
			</h5>
		</div>
		<div class="col-sm-6 text-right">
			<c:if test="${currentUser.role == role.ROLE_ADMIN}">
				<a href='<c:url value='${appUrl.URL_STUDENT.concat(appUrl.URL_CREATE)}' />' class="btn btn-success"> <i class="fas fa-plus"></i>
					${labelLang.LABEL_BUTTON_CREATE}
				</a>
			</c:if>
			<a href='<c:url value='${appUrl.URL_STUDENT}' />' class="btn btn-info"><i class="fas fa-bars"></i> ${labelLang.LABEL_BUTTON_READ}</a>
		</div>
	</c:if>
</div>
<hr class="my-3">
<c:if test="${createMode}">
	<c:url var="urlForm" value="${appUrl.URL_STUDENT.concat(appUrl.URL_CREATE)}"></c:url>
</c:if>
<c:if test="${updateMode}">
	<c:url var="urlForm" value="${appUrl.URL_STUDENT.concat('/').concat(originalStudent.user.uuid).concat(appUrl.URL_UPDATE)}"></c:url>
</c:if>
<form:form action="${urlForm}" method="post" modelAttribute="validStudent" enctype="multipart/form-data">
	<c:if test="${createMode}">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label text-right">Tên đăng nhập</label>
			<div class="col-sm-7">
				<input type="text" name="user.username" class="form-control" value="${student.user.username}" placeholder="Ví dụ: baoanh" path="user.username">
				<form:errors path="user.username" cssClass="form-text text-danger" />
			</div>
		</div>
	</c:if>
	<c:if test="${updateMode}">
		<div class="form-group row">
			<label for="username" class="col-sm-3 col-form-label text-right">Tên truy cập</label>
			<div class="col-sm-7 col-form-label">${originalStudent.user.username}</div>
		</div>
	</c:if>
	<c:if test="${createMode}">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label text-right">Mật khẩu</label>
			<div class="col-sm-7">
				<input type="password" name="user.password" class="form-control" placeholder="Mật khẩu" path="user.password">
				<form:errors path="user.password" cssClass="form-text text-danger" />
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3 col-form-label text-right">Nhập lại mật khẩu</label>
			<div class="col-sm-7">
				<input type="password" name="user.confirmPassword" class="form-control" placeholder="Nhập lại mật khẩu" path="user.confirmPassword">
				<form:errors path="user.confirmPassword" cssClass="form-text text-danger" />
			</div>
		</div>
	</c:if>
	<c:if test="${updateMode}">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label text-right">Mật khẩu</label>
			<div class="col-sm-7">
				<a href='<c:url value="${appUrl.URL_STUDENT.concat('/').concat(originalStudent.user.uuid).concat(appUrl.URL_CHANGE_PASSWORD)}"/>'
					class="btn btn-primary"> Đổi mật khẩu</a>
			</div>
		</div>
	</c:if>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Email</label>
		<div class="col-sm-7">
			<input type="text" name="user.email" class="form-control" value="${student.user.email}" placeholder="Ví dụ: contact@email.com" path="user.email">
			<form:errors path="user.email" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Họ</label>
		<div class="col-sm-7">
			<input type="text" name="user.firstName" class="form-control" value="${student.user.firstName}" placeholder="Ví dụ: Trần" path="user.firstName">
			<form:errors path="user.firstName" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Tên</label>
		<div class="col-sm-7">
			<input type="text" name="user.lastName" class="form-control" value="${student.user.lastName}" placeholder="Ví dụ: Bảo Anh" path="user.lastName">
			<form:errors path="user.lastName" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Lớp học</label>
		<div class="col-sm-7">
			<select class="form-control" name="classroom.id" path="classroom.id">
				<c:forEach var="classroom" items="${classrooms}">
					<option value="${classroom.id}" ${classroom.id == student.classroom.id ? 'selected' : ''}>${classroom.name}</option>
				</c:forEach>
			</select>
			<form:errors path="classroom.id" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Địa chỉ</label>
		<div class="col-sm-7">
			<input type="text" name="user.address" class="form-control" value="${student.user.address}"
				placeholder="Ví dụ: 38 Nguyễn Khánh Toàn, Quan Hoa, Cầu Giấy, Hà Nội" path="user.address">
			<form:errors path="user.address" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Giới tính</label>
		<div class="form-group col-sm-3">
			<input type="radio" name="user.gender" value="1" checked> <i class="fas fa-male"></i> Nam
		</div>
		<div class="col-sm-3">
			<input type="radio" name="user.gender" value="0" ${student.user.gender == 0 ? 'checked="checked"' : ''}> <i class="fas fa-female"></i> Nữ
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Ngày sinh</label>
		<div class="col-sm-7">
			<input type="text" class="form-control" name="user.dob" placeholder="Định dạng : ngày-tháng-năm" path="user.dob"  value="<fmt:formatDate pattern="dd-MM-yyyy" value="${student.user.dob}" />">
			<form:errors path="user.dob" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Số điện thoại cá nhân</label>
		<div class="col-sm-7">
			<input type="text" class="form-control datepicker-top" name="user.phone" value="${student.user.phone}" placeholder="Ví dụ: 0912345678" path="user.phone">
			<form:errors path="user.phone" cssClass="form-text text-danger" />
		</div>
	</div>
	<!-- Avatar -->
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Avatar</label>
		<c:choose>
			<c:when test="${createMode}">
				<div class="col-sm-7">
					<input type="file" class="form-control" name="user.avatarFile" path="user.avatarFile"> <small id="avatarFileHelp" class="form-text text-muted">Chỉ chấp
						nhận file <b class="text-uppercase">jpg</b> hoặc <b class="text-uppercase">png</b>. Dung lượng file không vượt quá <b>512 kB</b> (0.5 MB)
					</small>
					<form:errors path="user.avatarFile" cssClass="form-text text-danger" />
				</div>
			</c:when>
			<c:when test="${updateMode}">
				<div class="col-sm-5">
					<input type="file" class="form-control" name="user.avatarFile" path="user.avatarFile"> <small id="avatarFileHelp" class="form-text text-muted">Chỉ chấp
						nhận file <b class="text-uppercase">jpg</b> hoặc <b class="text-uppercase">png</b>. Dung lượng file không vượt quá <b>512 kB</b> (0.5 MB)
					</small>
					<form:errors path="user.avatarFile" cssClass="form-text text-danger" />
				</div>
				<div class="col-sm-2">
					<img src="<c:url value="${originalStudent.user.image}" />" border="0" width="100%">
				</div>
			</c:when>
		</c:choose>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Tên người bảo hộ</label>
		<div class="col-sm-7">
			<input type="text" name="guardian" class="form-control" value="${student.guardian}" placeholder="Ví dụ: Nguyễn Văn A" path="guardian">
			<form:errors path="guardian" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Số điện thoại người bảo hộ</label>
		<div class="col-sm-7">
			<input type="text" class="form-control" name="phoneGuardian" value="${student.phoneGuardian}" placeholder="Ví dụ: 0912345678" path="phoneGuardian">
			<form:errors path="phoneGuardian" cssClass="form-text text-danger" />
		</div>
	</div>
	<c:if test="${currentUser.role == role.ROLE_ADMIN}">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label text-right">Trạng thái</label>
			<div class="col-sm-7 col-form-label">
				<div class="custom-control custom-radio custom-control-inline">
					<input class="custom-control-input" type="radio" name="user.status" id="active" value="1" checked="checked"> <label
						class="custom-control-label" for="active"> Kích hoạt</label>
				</div>
				<div class="custom-control custom-radio custom-control-inline">
					<input class="custom-control-input" type="radio" name="user.status" id="deactive" value="0" ${student.user.status == 0 ? 'checked="checked"' : ''}>
					<label class="custom-control-label" for="deactive"> Tạm dừng</label>
				</div>
			</div>
		</div>
	</c:if>
	<div class="form-group row">
		<c:if test="${createMode}">
			<div class="col-sm-7 offset-sm-3">
				<hr>
				<button type="submit" class="btn btn-primary">
					<i class="fas fa-plus"></i> ${labelLang.LABEL_CREATE}
				</button>
				<button type="reset" class="btn">
					<i class="fas fa-redo-alt"></i> ${labelLang.LABEL_RESET}
				</button>
			</div>
		</c:if>
		<c:if test="${updateMode}">
			<div class="offset-sm-3 col-sm-7">
				<hr />
				<button type="submit" class="btn btn-primary">
					<i class="fas fa-edit"></i> ${labelLang.LABEL_UPDATE}
				</button>
			</div>
		</c:if>
	</div>
</form:form>