<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="row">
	<c:if test="${createMode}">
		<div class="col-sm-6">
			<h5 class="text-uppercase mt-1 mb-0">
				<i class="fas fa-plus"></i> ${moduleLang.LABEL_CREATE}
			</h5>
		</div>
		<div class="col-sm-6 text-right">
			<a href='<c:url value='${appUrl.URL_SUBJECT}' />' class="btn btn-info"><i class="fas fa-bars"></i> ${labelLang.LABEL_BUTTON_READ}</a>
		</div>
	</c:if>
	<c:if test="${updateMode}">
		<div class="col-sm-6">
			<h5 class="text-uppercase mt-1 mb-0">
				<i class="fas fa-plus"></i> ${moduleLang.LABEL_UPDATE}
			</h5>
		</div>
		<div class="col-sm-6 text-right">
			<a href='<c:url value='${appUrl.URL_SUBJECT.concat(appUrl.URL_CREATE)}' />' class="btn btn-success"> <i class="fas fa-plus"></i> ${labelLang.LABEL_BUTTON_CREATE}
			</a> <a href='<c:url value='${appUrl.URL_SUBJECT}' />' class="btn btn-info"><i class="fas fa-bars"></i> ${labelLang.LABEL_BUTTON_READ}</a>
		</div>
	</c:if>
</div>
<hr class="my-3">
<c:if test="${createMode}">
	<c:url var="urlForm" value="${appUrl.URL_SUBJECT.concat(appUrl.URL_CREATE)}"></c:url>
</c:if>
<c:if test="${updateMode}">
	<c:url var="urlForm" value="${appUrl.URL_SUBJECT.concat('/').concat(subject.uuid).concat(appUrl.URL_UPDATE)}"></c:url>
</c:if>
<form:form action="${urlForm}" method="post" modelAttribute="validSubject">
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Tên môn học</label>
		<div class="col-sm-7">
			<input type="text" name="title" class="form-control" value="${subject.title}" path="title">
			<form:errors path="title" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Mã môn học</label>
		<div class="col-sm-7">
			<input type="text" name="code" class="form-control" value="${subject.code}" path="code">
			<form:errors path="code" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Số tín chỉ</label>
		<div class="col-sm-7">
			<input type="text" name="creadit" class="form-control" value="${subject.creadit}" path="creadit">
			<form:errors path="creadit" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Trạng thái</label>
		<div class="col-sm-7 col-form-label">
			<div class="custom-control custom-radio custom-control-inline">
				<input class="custom-control-input" type="radio" name="status" id="active" value="1" checked="checked"> <label
					class="custom-control-label" for="active"> Kích hoạt</label>
			</div>
			<div class="custom-control custom-radio custom-control-inline">
				<input class="custom-control-input" type="radio" name="status" id="deactive" value="0" ${subject.status == 0 ? 'checked="checked"' : ''}>
				<label class="custom-control-label" for="deactive"> Tạm dừng</label>
			</div>
		</div>
	</div>
	<div class="form-group row">
		<c:if test="${createMode}">
			<div class="col-sm-7 offset-sm-3">
				<hr />
				<button type="submit" class="btn btn-primary">
					<i class="fas fa-plus"></i> ${labelLang.LABEL_CREATE}
				</button>
				<button type="reset" class="btn">
					<i class="fas fa-redo-alt"></i> ${labelLang.LABEL_RESET}
				</button>
			</div>
		</c:if>
		<c:if test="${updateMode}">
			<div class="offset-sm-3 col-sm-7">
				<hr />
				<button type="submit" class="btn btn-primary">
					<i class="fas fa-edit"></i> ${labelLang.LABEL_UPDATE}
				</button>
			</div>
		</c:if>
	</div>
</form:form>
<c:if test="${updateMode}">
	<c:url var="urlForm" value="${appUrl.URL_SUBJECT.concat('/').concat(subject.uuid).concat(appUrl.URL_DELETE)}"></c:url>
	<form:form action="${urlForm}" method="post" id="completeRemove-${subject.uuid}">
		<hr class="my-3">
		<div class="form-group row">
			<div class="col-sm-9 offset-sm-3 text-left">
				<button type="submit" class="btn btn-sm btn-danger complete-remove" name="delete" data-value="${subject.uuid}"><i class="fas fa-trash-alt"></i> ${labelLang.LABEL_BUTTON_DELETE}
				</button>
			</div>
		</div>
	</form:form>
</c:if>