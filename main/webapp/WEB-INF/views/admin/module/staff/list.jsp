<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="row">
	<div class="col-sm-6">
		<h5 class="text-uppercase mt-1 mb-0">
			<i class="fas fa-list-ul"></i> ${moduleLang.LABEL_LIST}
		</h5>
	</div>
	<c:if test="${currentUser.role == role.ROLE_ADMIN}">
		<div class="col-sm-6 text-right">
			<a href='<c:url value='${appUrl.URL_STAFF.concat(appUrl.URL_CREATE)}' />' class="btn btn-success"> <i class="fas fa-plus"></i>
				${labelLang.LABEL_BUTTON_CREATE}
			</a>
		</div>
	</c:if>
</div>
<hr class="my-3">
<table id="datatable" class="table table-hover">
	<thead>
		<tr>
			<th class="text-center">${labelLang.LABEL_STT}</th>
			<th class="text-center">${moduleLang.LABEL_USERNAME}</th>
			<th class="text-center">${moduleLang.LABEL_NAME}</th>
			<th class="text-center">${moduleLang.LABEL_FACULTY}</th>
			<th class="text-center">${moduleLang.LABEL_EMAIL}</th>
			<th class="text-center" width="20%">${labelLang.LABEL_OPTION}</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${staffs}" var="staff" varStatus="loop">
			<tr ${staff.user.status != 1 ? 'class="bg-light"' : ''}>
				<td class="text-center"></td>
				<td class="text-center">
					<c:if test="${staff.user.role == role.ROLE_ADMIN}">
						<span class="badge badge-danger">ADMIN</span>
					</c:if>
					<a href='<c:url value="${appUrl.URL_STAFF.concat('/').concat(staff.user.uuid).concat(appUrl.URL_DETAIL)}"/>'>${staff.user.username}</a>
				</td>
				<td class="text-center">${staff.user.firstName.concat(" ").concat(staff.user.lastName)}</td>
				<td class="text-center">${staff.faculty.name}</td>
				<td class="text-center">${staff.user.email}</td>
				<c:if test="${currentUser.role == role.ROLE_ADMIN}">
					<td class="text-center">
						<c:choose>
							<c:when test="${staff.user.status == 1}">
								<c:set var="activeLabel" value="${labelLang.LABEL_BUTTON_DEACTIVE}" />
								<c:set value="delete-item" var="statusClass" />
								<c:set var="btnClass" value="btn-secondary" />
								<c:set var="icon"><i class="fas fa-lock"></i></c:set>
								<c:url value="${appUrl.URL_STAFF.concat('/').concat(staff.user.uuid).concat(appUrl.URL_DEACTIVE)}" var="actionLink" />
							</c:when>
							<c:otherwise>
								<c:set var="activeLabel" value="${labelLang.LABEL_BUTTON_ACTIVE}" />
								<c:set value="restore-item" var="statusClass" />
								<c:set var="btnClass" value="btn-warning" />
								<c:set var="icon"><i class="fas fa-lock-open"></i></c:set>
								<c:url value="${appUrl.URL_STAFF.concat('/').concat(staff.user.uuid).concat(appUrl.URL_ACTIVE)}" var="actionLink" />
							</c:otherwise>
						</c:choose>
						<form:form action="${actionLink}" method="post" id="item-${staff.user.uuid}">
							<a href='<c:url value="${appUrl.URL_STAFF.concat('/').concat(staff.user.uuid).concat(appUrl.URL_UPDATE)}" />' class="btn btn-primary btn-sm">
								<i class="fas fa-edit"></i>${labelLang.LABEL_BUTTON_UPDATE} </a>
							<c:if test="${currentUser.id != staff.user.id}">
								<button type="submit" class="btn ${btnClass} btn-sm ${statusClass}" data-value="${staff.user.uuid}">${icon}&nbsp;${activeLabel}</button>
							</c:if>
						</form:form>
					</td>
				</c:if>
			</tr>
		</c:forEach>
	</tbody>
</table>