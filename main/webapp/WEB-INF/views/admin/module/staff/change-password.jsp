<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<h5 class="text-uppercase mt-1 mb-0">
	<i class="fas fa-edit"></i> Đổi mật khẩu
</h5>
<hr class="my-3">
<c:choose>
	<c:when test="${changeCurrentPassMode}">
		<c:url var="urlForm" value="${appUrl.URL_STAFF.concat(appUrl.URL_CHANGE_PASSWORD)}" />
	</c:when>
	<c:when test="${changePassMode}">
		<c:url var="urlForm" value="${appUrl.URL_STAFF.concat('/').concat(staff.user.uuid).concat(appUrl.URL_CHANGE_PASSWORD)}" />
	</c:when>
</c:choose>
<form:form action="${urlForm}" method="post" modelAttribute="changePassword">
	<c:if test="${changeCurrentPassMode}">
		<!-- Old password -->
		<div class="form-group row">
			<label class="col-sm-3 col-form-label text-right">Mật khẩu cũ <span class="text-danger"> *</span>
			</label>
			<div class="col-sm-7">
				<input type="password" class="form-control" name="user.oldPassword" path="user.oldPassword">
				<form:errors path="user.oldPassword" cssClass="form-text text-danger" />
			</div>
		</div>
	</c:if>
	<!-- New password -->
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Mật khẩu mới <span class="text-danger"> *</span>
		</label>
		<div class="col-sm-7">
			<input type="password" class="form-control" name="user.password" path="user.password">
			<form:errors path="user.password" cssClass="form-text text-danger" />
		</div>
	</div>
	<!-- Confirm password -->
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Nhập lại mật khẩu <span class="text-danger"> *</span>
		</label>
		<div class="col-sm-7">
			<input type="password" class="form-control" name="user.confirmPassword" path="user.confirmPassword">
			<form:errors path="user.confirmPassword" cssClass="form-text text-danger" />
		</div>
	</div>
	<!--Button submit -->
	<div class="form-group row">
		<div class="offset-sm-3 col-sm-7">
			<hr>
			<button type="submit" class="btn btn-primary">
				<i class="fas fa-edit"></i> ${labelLang.LABEL_UPDATE}
			</button>
		</div>
	</div>
</form:form>