<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="row">
	<div class="col-sm-6">
		<h5 class="text-uppercase mt-1 mb-0">
			<i class="fas fa-user"></i>&nbsp;${staff.user.username}<c:if test="${staff.user.role == role.ROLE_ADMIN}">&nbsp;<small><span class="badge badge-danger">ADMIN</span></small></c:if>
		</h5>
	</div>
	<c:if test="${currentUser.role == role.ROLE_ADMIN}">
		<div class="col-sm-6 text-right">
			<a href='<c:url value='${appUrl.URL_STAFF.concat(appUrl.URL_CREATE)}' />' class="btn btn-success"> <i class="fas fa-plus"></i>
				${labelLang.LABEL_BUTTON_CREATE}
			</a>
			<a href='<c:url value='${appUrl.URL_STAFF}' />' class="btn btn-info"><i class="fas fa-bars"></i> ${labelLang.LABEL_BUTTON_READ}</a>
		</div>
	</c:if>
</div>
<hr class="my-3">
<div class="row mb-3">
	<div class="col-sm-3">
		<img src="<c:url value="${staff.user.image}" />" border="0" width="100%">
	</div>
	<div class="col-sm-4 border-right">
		<ul class="list-group list-group-flush">
			<li class="list-group-item">Username : ${staff.user.username}<c:if test="${staff.user.role == role.ROLE_ADMIN}">&nbsp;<span
						class="badge badge-danger">ADMIN</span>
				</c:if>
			<li class="list-group-item">Họ tên : ${staff.user.firstName.concat(" ").concat(staff.user.lastName)}</li>
			<li class="list-group-item">Khoa : ${staff.faculty.name}</li>
			<li class="list-group-item">Email : ${staff.user.email}</li>
			<li class="list-group-item">Ngày sinh : <fmt:formatDate pattern="dd-MM-yyyy" value="${staff.user.dob}" /></li>
			<li class="list-group-item">Giới tính : ${staff.user.gender == 1 ? 'Nam' : 'Nữ'}</li>
			<li class="list-group-item">Số điện thoại : ${staff.user.phone}</li>
			<li class="list-group-item">Địa chỉ : ${staff.user.address}</li>
		</ul>
	</div>
	<div class="col-sm-4">
		<ul class="list-group list-group-flush">
			<li class="list-group-item">Chứng chỉ đạt được : <p class="ml-3">${staff.certificate}</p></li>
		    <li class="list-group-item">Môn dạy : 
		      <c:if test="${not empty staff.subjects}">
				<c:forEach items="${staff.subjects}" var="subject" varStatus="loop">
					<p class="mb-0 ml-3">${loop.index + 1}.&nbsp;${subject.title}</p>         
				</c:forEach>
			  </c:if>
			</li>
			<li class="list-group-item">Lớp chủ nhiệm : 
				<c:forEach items="${classrooms}" var="classroom" varStatus="loop">
					<p class="mb-0 ml-3">${loop.index + 1}.&nbsp;${classroom.name}</p>         
				</c:forEach>
			</li>
		</ul>
	</div>
</div>
<c:if test="${currentUser.role == role.ROLE_ADMIN}">
	<hr />
	<div class="text-center">
	<a href='<c:url value='${appUrl.URL_STAFF.concat("/").concat(staff.user.uuid).concat(appUrl.URL_UPDATE)}' />' class="btn btn-primary"><i class="fas fa-edit"></i> ${labelLang.LABEL_UPDATE}</a>
	</div>
</c:if>
