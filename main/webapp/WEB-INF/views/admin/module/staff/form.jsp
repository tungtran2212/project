<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="row">
	<c:if test="${createMode}">
		<div class="col-sm-6">
			<h5 class="text-uppercase mt-1 mb-0">
				<i class="fas fa-plus"></i> ${moduleLang.LABEL_CREATE}
			</h5>
		</div>
		<div class="col-sm-6 text-right">
			<a href='<c:url value='${appUrl.URL_STAFF}' />' class="btn btn-info"><i class="fas fa-bars"></i> Danh sách</a>
		</div>
	</c:if>
	<c:if test="${updateMode}">
		<div class="col-sm-6">
			<h5 class="text-uppercase mt-1 mb-0">
				<i class="fas fa-plus"></i> ${moduleLang.LABEL_UPDATE}
			</h5>
		</div>
		<div class="col-sm-6 text-right">
			<c:if test="${currentUser.role == role.ROLE_ADMIN}">
				<a href='<c:url value='${appUrl.URL_STAFF.concat(appUrl.URL_CREATE)}' />' class="btn btn-success"> <i class="fas fa-plus"></i>
					${labelLang.LABEL_BUTTON_CREATE}
				</a>
			</c:if>
			<a href='<c:url value='${appUrl.URL_STAFF}' />' class="btn btn-info"><i class="fas fa-bars"></i> ${labelLang.LABEL_BUTTON_READ}</a>
		</div>
	</c:if>
</div>
<hr class="my-3">
<c:if test="${createMode}">
	<c:url var="urlForm" value="${appUrl.URL_STAFF.concat(appUrl.URL_CREATE)}"></c:url>
</c:if>
<c:if test="${updateMode}">
	<c:url var="urlForm" value="${appUrl.URL_STAFF.concat('/').concat(originalStaff.user.uuid).concat(appUrl.URL_UPDATE)}"></c:url>
</c:if>
<form:form action="${urlForm}" method="post" modelAttribute="validStaff" enctype="multipart/form-data">
	<c:if test="${createMode}">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label text-right">Tên đăng nhập</label>
			<div class="col-sm-7">
				<input type="text" name="user.username" class="form-control" value="${staff.user.username}" placeholder="Ví dụ: baoanh" path="user.username">
				<form:errors path="user.username" cssClass="form-text text-danger" />
			</div>
		</div>
	</c:if>
	<c:if test="${updateMode}">
		<div class="form-group row">
			<label for="username" class="col-sm-3 col-form-label text-right">Tên truy cập</label>
			<div class="col-sm-7 col-form-label">${originalStaff.user.username}</div>
		</div>
	</c:if>
	<c:if test="${createMode}">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label text-right">Mật khẩu</label>
			<div class="col-sm-7">
				<input type="password" name="user.password" class="form-control" placeholder="Mật khẩu" path="user.password">
				<form:errors path="user.password" cssClass="form-text text-danger" />
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3 col-form-label text-right">Nhập lại mật khẩu</label>
			<div class="col-sm-7">
				<input type="password" name="user.confirmPassword" class="form-control" placeholder="Nhập lại mật khẩu" path="user.confirmPassword">
				<form:errors path="user.confirmPassword" cssClass="form-text text-danger" />
			</div>
		</div>
	</c:if>
	<c:if test="${updateMode}">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label text-right">Mật khẩu</label>
			<div class="col-sm-7">
				<a href='<c:url value="${appUrl.URL_STAFF.concat('/').concat(originalStaff.user.uuid).concat(appUrl.URL_CHANGE_PASSWORD)}"/>' class="btn btn-primary">
					Đổi mật khẩu</a>
			</div>
		</div>
	</c:if>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Email</label>
		<div class="col-sm-7">
			<input type="text" name="user.email" class="form-control" value="${staff.user.email}" placeholder="Ví dụ: contact@email.com" path="user.email">
			<form:errors path="user.email" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Họ</label>
		<div class="col-sm-7">
			<input type="text" name="user.firstName" class="form-control" value="${staff.user.firstName}" placeholder="Ví dụ: Trần" path="user.firstName">
			<form:errors path="user.firstName" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Tên</label>
		<div class="col-sm-7">
			<input type="text" name="user.lastName" class="form-control" value="${staff.user.lastName}" placeholder="Ví dụ: Bảo Anh" path="user.lastName">
			<form:errors path="user.lastName" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Khoa</label>
		<div class="col-sm-7">
			<select class="form-control with-search" name="faculty.id" path="faculty.id">
				<c:forEach var="faculty" items="${faculties}">
					<option value="${faculty.id}" ${faculty.id == staff.faculty.id ? 'selected' : ''}>${faculty.name}</option>
				</c:forEach>
			</select>
			<form:errors path="faculty.id" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Môn dạy</label>
		<div class="col-sm-7">
			<select id="subject" class="form-control with-search" name="subjects" path="subjects" multiple="multiple">
				<c:forEach items="${subjects}" var="subject">
					<c:if test="${empty staff.subjects}">
						<option value="${subject.id}">${subject.title}</option>
					</c:if>
					<c:if test="${not empty staff.subjects}">
						<option value="${subject.id}"
							<c:forEach items="${staff.subjects}" var="subjectSelected">${subject.id == subjectSelected.id ? ' selected="selected"' : '' }</c:forEach>>
							${subject.title}</option>
					</c:if>
				</c:forEach>
			</select>
			<form:errors path="subjects" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Địa chỉ</label>
		<div class="col-sm-7">
			<input type="text" name="user.address" class="form-control" value="${staff.user.address}"
				placeholder="Ví dụ: 38 Nguyễn Khánh Toàn, Quan Hoa, Cầu Giấy, Hà Nội" path="user.address">
			<form:errors path="user.address" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Giới tính</label>
		<div class="form-group col-sm-3">
			<input type="radio" name="user.gender" value="1" checked> <i class="fas fa-male"></i> Nam
		</div>
		<div class="col-sm-3">
			<input type="radio" name="user.gender" value="0" ${staff.user.gender == 0 ? 'checked="checked"' : ''}> <i class="fas fa-female"></i> Nữ
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Ngày sinh</label>
		<div class="col-sm-7">
			<input type="text" class="form-control" name="user.dob" placeholder="Định dạng : ngày-tháng-năm" path="user.dob"
				value="<fmt:formatDate pattern="dd-MM-yyyy" value="${staff.user.dob}" />">
			<form:errors path="user.dob" cssClass="form-text text-danger" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Số điện thoại</label>
		<div class="col-sm-7">
			<input type="text" class="form-control" name="user.phone" value="${staff.user.phone}" placeholder="Ví dụ: 0912345678"
				path="user.phone">
			<form:errors path="user.phone" cssClass="form-text text-danger" />
		</div>
	</div>
	<!-- Avatar -->
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Avatar</label>
		<c:choose>
			<c:when test="${createMode}">
				<div class="col-sm-7">
					<input type="file" class="form-control" name="user.avatarFile" path="user.avatarFile"> <small id="avatarFileHelp"
						class="form-text text-muted">Chỉ chấp nhận file <b class="text-uppercase">jpg</b> hoặc <b class="text-uppercase">png</b>. Dung lượng
						file không vượt quá <b>512 kB</b> (0.5 MB)
					</small>
					<form:errors path="user.avatarFile" cssClass="form-text text-danger" />
				</div>
			</c:when>
			<c:when test="${updateMode}">
				<div class="col-sm-5">
					<input type="file" class="form-control" name="user.avatarFile" path="user.avatarFile"> <small id="avatarFileHelp"
						class="form-text text-muted">Chỉ chấp nhận file <b class="text-uppercase">jpg</b> hoặc <b class="text-uppercase">png</b>. Dung lượng
						file không vượt quá <b>512 kB</b> (0.5 MB)
					</small>
					<form:errors path="user.avatarFile" cssClass="form-text text-danger" />
				</div>
				<div class="col-sm-2">
					<img src="<c:url value="${originalStaff.user.image}" />" border="0" width="100%">
				</div>
			</c:when>
		</c:choose>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Chứng chỉ đạt được</label>
		<div class="col-sm-7">
			<textarea class="form-control" name="certificate" rows="3" cols="80">${staff.certificate}</textarea>
		</div>
	</div>
	<c:if test="${currentUser.role == role.ROLE_ADMIN}">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label text-right">Trạng thái</label>
			<div class="col-sm-7 col-form-label">
				<div class="custom-control custom-radio custom-control-inline">
					<input class="custom-control-input" type="radio" name="user.status" path="user.status" id="active" value="1" checked="checked"> <label
						class="custom-control-label" for="active"> Kích hoạt</label>
				</div>
				<div class="custom-control custom-radio custom-control-inline">
					<input class="custom-control-input" type="radio" name="user.status" path="user.status" id="deactive" value="0" ${staff.user.status == 0 ? 'checked="checked"' : ''}>
					<label class="custom-control-label" for="deactive"> Tạm dừng</label>
				</div>
			</div>
		</div>
	</c:if>
	<div class="form-group row">
		<c:if test="${createMode}">
			<div class="col-sm-7 offset-sm-3">
				<hr>
				<button type="submit" class="btn btn-primary">
					<i class="fas fa-plus"></i> ${labelLang.LABEL_CREATE}
				</button>
				<button type="reset" class="btn">
					<i class="fas fa-redo-alt"></i> ${labelLang.LABEL_RESET}
				</button>
			</div>
		</c:if>
		<c:if test="${updateMode}">
			<div class="offset-sm-3 col-sm-7">
				<hr />
				<button type="submit" class="btn btn-primary">
					<i class="fas fa-edit"></i> ${labelLang.LABEL_UPDATE}
				</button>
			</div>
		</c:if>
	</div>
</form:form>
<c:if test="${updateMode && (currentUser.id != originalStaff.userId)}">
	<hr />
	<div class="form-group row">
		<div class="offset-sm-3 col-sm-7">
			<c:choose>
				<c:when test="${originalStaff.user.role == role.ROLE_ADMIN}">
					<c:set var="updateRole" value="${moduleLang.LABEL_ROLE_USER}" />
					<c:set var="icon" value="fas fa-ban" />
					<c:set var="btnClass" value="btn-danger" />
					<c:set var="statusClass" value="role-user-update" />
					<c:url var="urlForm" value="${appUrl.URL_STAFF.concat('/').concat(originalStaff.user.uuid).concat(appUrl.URL_ROLE_USER)}"></c:url>
				</c:when>
				<c:when test="${originalStaff.user.role == role.ROLE_STAFF}">
					<c:set var="updateRole" value="${moduleLang.LABEL_ROLE_ADMIN}" />
					<c:set var="icon" value="far fa-user" />
					<c:set var="btnClass" value="btn-danger" />
					<c:set var="statusClass" value="role-admin-update" />
					<c:url var="urlForm" value="${appUrl.URL_STAFF.concat('/').concat(originalStaff.user.uuid).concat(appUrl.URL_ROLE_ADMIN)}"></c:url>
				</c:when>
			</c:choose>
			<form:form action="${urlForm}" method="post" id="${statusClass}-${originalStaff.user.uuid}">
				<button type="submit" class="btn ${btnClass} ${statusClass}" data-value="${originalStaff.user.uuid}">
					<i class="${icon}"></i> ${updateRole}
				</button>
			</form:form>
		</div>
	</div>
</c:if>
