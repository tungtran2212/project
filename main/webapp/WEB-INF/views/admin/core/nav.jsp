<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<nav class="navbar navbar-expand-sm navbar-dark bg-dark mb-3">
	<ul class="navbar-nav mr-auto">
		<li class="nav-item">
			<h4 class="text-uppercase py-0 text-secondary">Đồ án tốt nghiệp</h4>
		</li>
	</ul>
	<ul class="navbar-nav">
		<li class="nav-item dropdown">
			<c:choose>
				<c:when test="${not empty pageContext.request.userPrincipal}">
					<a class="btn btn-outline-light dropdown-toggle home-nav" id="navbarDropdown" role="button" data-toggle="dropdown" href="#"><i class="fas fa-user"></i>&nbsp;${pageContext.request.userPrincipal.name}</a>
					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
				<c:if test="${(currentUser.role == role.ROLE_STAFF) || (currentUser.role == role.ROLE_ADMIN)}">
					<a class="dropdown-item" href='<c:url value="${appUrl.URL_STAFF.concat('/').concat(currentUser.uuid).concat(appUrl.URL_DETAIL)}"/>'><i class="fas fa-user"></i>&nbsp;Tài khoản</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href='<c:url value="${appUrl.URL_STAFF.concat(appUrl.URL_CHANGE_PASSWORD)}"/>'><i class="fas fa-unlock-alt"></i> Đổi
						mật khẩu</a>
					<div class="dropdown-divider"></div>
				</c:if>
				<c:if test="${currentUser.role == role.ROLE_STUDENT}">
					<a class="dropdown-item" href='<c:url value="${appUrl.URL_STUDENT.concat('/').concat(currentUser.uuid).concat(appUrl.URL_DETAIL)}"/>'><i class="fas fa-user"></i>&nbsp;Tài khoản</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href='<c:url value="${appUrl.URL_STUDENT.concat(appUrl.URL_CHANGE_PASSWORD)}"/>'><i class="fas fa-unlock-alt"></i>
						Đổi mật khẩu</a>
					<div class="dropdown-divider"></div>
				</c:if>
				<c:url value="${appUrl.URL_LOGOUT}" var="logout" />
				<form:form action="${logout}" method="post">
					<div class="dropdown-item">
						<button type="submit" class="btn btn-link p-0 m-0 text-dark">
							<i class="fas fa-sign-out-alt"></i> Thoát
						</button>
					</div>
				</form:form>
			</div>
				</c:when>
				<c:otherwise>
					<a class="btn btn-outline-light" href="<c:url value='${appUrl.URL_LOGIN }' />">
						<i class="fas fa-sign-in-alt"></i>&nbsp;Đăng nhập
					</a>
				</c:otherwise>
			</c:choose>
		</li>
	</ul>
</nav>