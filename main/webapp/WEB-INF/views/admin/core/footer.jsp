<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
</div>
</div>
<div class="row mr-0">
	<div class="col-sm-12 text-center">
		<hr>
		${appLabel.LABEL_FOOTER}
	</div>
</div>
</div>
<!--Bootstrap core js -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<!-- Fontawesome js -->
<!-- <script src="https://use.fontawesome.com/0eab6c29bd.js"></script> -->
<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
<!--Datatables js -->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<!-- JS config -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src='<c:url value="/public/js/datatable.js"></c:url>'></script>
<script type="text/javascript" src="<c:url value='/public/js/bootstrap-tagsinput.js' />"></script>
<script type="text/javascript" src="<c:url value='/public/js/select2.js' />"></script>
<script type="text/javascript" src="<c:url value='/public/js/delete-confirm.js' />"></script>
<script type="text/javascript" src="<c:url value='/public/js/datepicker.js' />"></script>
</body>
</html>