<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-2">
			<div class="list-group mb-3">
			<c:choose>
				<c:when test="${currentUser.role == role.ROLE_ADMIN}">
				  <a href='<c:url value="${appUrl.URL_ADMIN}" />' class="list-group-item list-group-item-action bg-info text-light">Dashboard</a>
					<a href='<c:url value="${appUrl.URL_STAFF}" />' class="list-group-item list-group-item-action mt-3${not empty staffMenu ? ' menu-item-active' : ' menu-item' }">Nhân viên</a>
					<a href='<c:url value="${appUrl.URL_STUDENT}" />' class="list-group-item list-group-item-action mt-3${not empty studentMenu ? ' menu-item-active' : ' menu-item' }">Sinh viên</a>
					<a href='<c:url value="${appUrl.URL_FACULTY}" />' class="list-group-item list-group-item-action mt-3${not empty facultyMenu ? ' menu-item-active' : ' menu-item' }">Khoa</a>
					<a href='<c:url value="${appUrl.URL_MAJOR}" />' class="list-group-item list-group-item-action mt-3${not empty majorMenu ? ' menu-item-active' : ' menu-item' }">Ngành học</a>
					<a href='<c:url value="${appUrl.URL_SUBJECT}" />' class="list-group-item list-group-item-action mt-3${not empty subjectMenu ? ' menu-item-active' : ' menu-item' }">Môn học</a>
					<a href='<c:url value="${appUrl.URL_ROOM}" />' class="list-group-item list-group-item-action mt-3${not empty roomMenu ? ' menu-item-active' : ' menu-item' }">Phòng học</a>
					<a href='<c:url value="${appUrl.URL_COURSE}" />' class="list-group-item list-group-item-action mt-3${not empty courseMenu ? ' menu-item-active' : ' menu-item' }">Khóa học</a>
					<a href='<c:url value="${appUrl.URL_CLASSROOM}" />' class="list-group-item list-group-item-action mt-3${not empty classroomMenu ? ' menu-item-active' : ' menu-item' }">Lớp</a>
					<a href='<c:url value="${appUrl.URL_ADMIN.concat(appUrl.URL_DETAIL)}" />' class="list-group-item list-group-item-action mt-3${not empty detailMenu ? ' menu-item-active' : ' menu-item' }">Lịch học</a>
          <a href='<c:url value="${appUrl.URL_TIME_TABLE}" />' class="list-group-item list-group-item-action mt-3${not empty timeMenu ? ' menu-item-active' : ' menu-item'}">Thời khóa biểu</a>
          <a href='<c:url value="${appUrl.URL_CLASS}" />' class="list-group-item list-group-item-action mt-3${not empty classMenu ? ' menu-item-active' : ' menu-item' }">Quản lý lớp</a>				
				</c:when>
				<c:when test="${(currentUser.role == role.ROLE_ADMIN) || (currentUser.role == role.ROLE_STAFF)}">
				  <a href='<c:url value="${appUrl.URL_TIME_TABLE}" />' class="list-group-item list-group-item-action mt-3${not empty timeMenu ? ' menu-item-active' : ' menu-item'}">Thời khóa biểu</a>
					<a href='<c:url value="${appUrl.URL_CLASS}" />' class="list-group-item list-group-item-action mt-3${not empty classMenu ? ' menu-item-active' : ' menu-item' }">Quản lý lớp</a>
				</c:when>
				<c:when test="${currentUser.role == role.ROLE_STUDENT}">
				  <a href='<c:url value="${appUrl.URL_TIME_TABLE}" />' class="list-group-item list-group-item-action mt-3${not empty timeMenu ? ' menu-item-active' : ' menu-item'}">Thời khóa biểu</a>
					<a href='<c:url value="${appUrl.URL_REGISTRATION}" />' class="list-group-item list-group-item-action mt-3${not empty registrationMenu ? ' menu-item-active' : ' menu-item'}">Đăng ký học</a>
					<a href='<c:url value="${appUrl.URL_STUDENTS.concat('/').concat(currentUser.uuid).concat(appUrl.URL_POINT)}" />' class="list-group-item list-group-item-action mt-3${not empty pointMenu ? ' menu-item-active' : ' menu-item'}">Bảng điểm</a>
				</c:when>
			</c:choose>
			</div>
		</div>
		<div class="col-sm-10">