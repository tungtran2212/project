<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>Quên mật khẩu</title>
<link rel="icon" href="<c:url value='/public/images/favicon.ico' />" type="image/x-icon" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="description" content="" />
<meta name="author" content="" />
<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<c:url value='/public/css/style.css' />">
</head>
<body>
<c:import url="admin/core/nav.jsp"></c:import>
	<div class="container-fluid p-0">
		<div class="container">
			<!-- <nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item active">Trang chủ</li>
					<li class="breadcrumb-item active">Quên mật khẩu</li>
				</ol>
			</nav> -->
			<!-- Notify -->
			<c:if test="${not empty successMessage || not empty errorMessage}">
				<c:if test="${not empty successMessage}">
					<p class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>Thành công!</strong> ${successMessage}
					</p>
				</c:if>
				<c:if test="${not empty errorMessage}">
					<p class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>Thất bại!</strong> ${errorMessage}
					</p>
				</c:if>
			</c:if>
			<!-- End notify -->
			<c:url var="forgotPassword" value="${appUrl.URL_FORGOT_PASSWORD}" />
			<form:form action="${forgotPassword}" method="post" modelAttribute="forgotPassword" autocomplete="off">
				<div class="row mt-3">
					<div class="offset-sm-3 col-sm-6">
						<h5>
							<i class="fas fa-key"></i> Quên mật khẩu
						</h5>
						<hr class="my-2">
						<div class=" form-group">
							<label>Tên truy cập</label>
							<div class="input-group">
								<input type="text" class="form-control" name="username" placeholder="Username" path="username">
								<div class="input-group-prepend">
									<span class="input-group-text"> <i class="fas fa-user"></i>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label>Email</label>
							<div class="input-group">
								<input type="text" class="form-control" name="email" placeholder="Email" path="email">
								<div class="input-group-prepend">
									<span class="input-group-text"> <i class="fas fa-envelope"></i>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group text-right">
							<button type="submit" class="btn btn-primary">
								<i class="fas fa-key"></i> Gửi mật khẩu mới
							</button>
							<button type="reset" class="btn">
								<i class="fas fa-redo-alt"></i> Làm lại
							</button>
						</div>
					</div>
				</div>
			</form:form>
			<div class="col-sm-12 text-center">
				<hr>
				${appLabel.LABEL_FOOTER}
			</div>
		</div>
	</div>
	<!--Bootstrap core js -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<!-- Fontawesome js -->
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
</body>
</html>