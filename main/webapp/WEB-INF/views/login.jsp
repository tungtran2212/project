<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>Đăng nhập</title>
<link rel="icon" href="<c:url value='/public/images/favicon.ico' />" type="image/x-icon" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="description" content="" />
<meta name="author" content="" />
<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<c:url value='/public/css/style.css' />">
</head>
<body>
	<c:import url="admin/core/nav.jsp"></c:import>
	<div class="container-fluid p-0">
		<div class="container">
			<!-- <nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item active">Trang chủ</li>
					<li class="breadcrumb-item active">Đăng nhập</li>
				</ol>
			</nav> -->
			<!-- Notify -->
			<c:if test="${not empty successMessage || not empty errorMessage}">
				<c:if test="${not empty successMessage}">
					<p class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>Thành công!</strong> ${successMessage}
					</p>
				</c:if>
				<c:if test="${not empty errorMessage}">
					<p class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>Thất bại!</strong> ${errorMessage}
					</p>
				</c:if>
			</c:if>
			<c:if test="${not empty param.message}">
				<div class="row" style="font-size: 1.1em;">
					<div class="col-sm-12">
						<c:choose>
							<c:when test="${param.message == 'invalid'}">
								<div class="alert alert-danger alert-dismissible fade show" role="alert">
									${message}
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							</c:when>
							<c:when test="${param.message == 'logout'}">
								<div class="alert alert-success alert-dismissible fade show" role="alert">
									${message}
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							</c:when>
							<c:otherwise>
								<div class="alert alert-danger alert-dismissible fade show" role="alert">
									${message}
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</c:if>
			<div class="row mt-3">
				<div class="offset-sm-3 col-sm-6">
					<c:url value="/login" var="login_check"></c:url>
					<form:form modelAttribute="userlogin" class="form-signin" action="${login_check}" method="POST" autocomplete="off">
						<h5>
							<i class="fas fa-sign-in-alt"></i> Đăng nhập
						</h5>
						<hr class="my-2">
						<div class=" form-group">
							<label>Tên truy cập</label>
							<div class="input-group">
								<input type="text" class="form-control" placeholder="Username" name="username" path="username">
								<div class="input-group-prepend">
									<span class="input-group-text"> <i class="fas fa-user"></i>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label>Mật khẩu</label>
							<div class="input-group">
								<input type="password" class="form-control" name="password" placeholder="Password" path="password">
								<div class="input-group-prepend">
									<span class="input-group-text"> <i class="fas fa-key"></i>
									</span>
								</div>
							</div>
						</div>
						<div class="form-check form-group">
							<div class="row">
								<div class="col-sm-6">
									<input class="form-check-input" type="checkbox" id="remember-me" name="remember-me"> <label class="form-check-label"
										for="remember-me">Ghi nhớ đăng nhập</label>
								</div>
								<div class="col-sm-6 text-right">
									<a href="<c:url value="${appUrl.URL_FORGOT_PASSWORD}" />">Quên mật khẩu ? </a>
								</div>
							</div>
						</div>
						<div class="form-group text-right">
							<button type="submit" class="btn btn-primary">
								<i class="fas fa-sign-in-alt"></i> Đăng nhập
							</button>
							<button type="reset" class="btn">
								<i class="fas fa-redo-alt"></i> Làm lại
							</button>
						</div>
					</form:form>
				</div>
			</div>
			<div class="col-sm-12 text-center">
				<hr>
				${appLabel.LABEL_FOOTER}
			</div>
		</div>
	</div>
	<!--Bootstrap core js -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<!-- Fontawesome js -->
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
</body>
</html>