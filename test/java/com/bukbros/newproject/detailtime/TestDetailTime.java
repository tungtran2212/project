package com.bukbros.newproject.detailtime;

import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;

import com.bukbros.newproject.configuration.ApplicationConfiguration;
import com.bukbros.newproject.detailTime.interfaces.DetailTimeRepositoryInterface;

@SpringJUnitWebConfig(ApplicationConfiguration.class)
class TestDetailTime {
	@Autowired
	private DetailTimeRepositoryInterface repository;

	@Test
	void testGetDetailTimeByStudent() {
		assertNull(repository.getDetailTimeByStudent(32));
	}

}
